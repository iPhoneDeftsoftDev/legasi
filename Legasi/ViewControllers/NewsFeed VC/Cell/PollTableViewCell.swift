
//
//  PollTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var option1Textfield: UITextField!
     var vc: BaseVC!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: Load data for Cell
    func loadData(viewcontroller: BaseVC, at indexPath: IndexPath) {
        
        vc = viewcontroller
        
        //Set TextField Placeholder
        
        option1Textfield.setPlaceholder(placholder: "Option \(indexPath.row + 1)", color: .darkGray, size: 16.0)
        option1Textfield.tag = indexPath.row
        
    }
}
