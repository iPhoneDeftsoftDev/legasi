//
//  ImagesCollectionViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
}
