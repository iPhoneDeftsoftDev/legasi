//
//  ImageCollectionCell.swift
//  DemoLegisi
//
//  Created by Apple on 06/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var videoBlurView: UIView!
    @IBOutlet weak var videoButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
