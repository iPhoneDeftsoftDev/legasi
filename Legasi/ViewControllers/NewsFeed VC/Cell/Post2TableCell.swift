//
//  Post2TableCell.swift
//  Legasi
//
//  Created by 123 on 07/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

protocol VideoPlayersDelegates {
    func getVideosDetail(videoUrl:String, name:String)
}

class Post2TableCell: UITableViewCell {

    @IBOutlet weak var profileDetailButton: UIButton!
    @IBOutlet var locationButtonHeight: NSLayoutConstraint!
    @IBOutlet var linkButtonHeight: NSLayoutConstraint!
    @IBOutlet var linkButton: UIButton!
    @IBOutlet weak var submitRSVPButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var endDateLabel: UILabel!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var rsvpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    //MARK: IBOutlet
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var imageCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet var userTypeImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var tagCollectionViewConstraints: NSLayoutConstraint!
     var dummyArray = [TagListData]()
     var imageArray = [mediaPlayer]()
    var delegate: VideoPlayersDelegates?
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //MARK: Load Data
    func loadData() {
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        //Set Shadow to View
//        shadowView.layer.cornerRadius = 7
//        shadowView.layer.shadowColor = UIColor.black.cgColor
//        shadowView.layer.shadowOffset = CGSize.zero
//        shadowView.layer.shadowRadius = 2
//        shadowView.layer.shadowOpacity = 0.3
        
        //Set Radius to image view
        self.profileImageView.layer.cornerRadius = 22.0
        //Set HashTagCollectionView Height
        self.likeButton.layer.cornerRadius = self.likeButton.frame.width/2
         locationLabel.sizeToFit()
        //Set Collectionview
        self.loadHashTagCollectionView()
        self.loadImageCollectionView()
        self.tagCollectionViewConstraints.constant = self.tagCollectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
    private func loadHashTagCollectionView() {
        //Set Collectionview Flow Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.itemSize = CGSize(width:self.tagCollectionView.frame.width, height: 15.0)
        layout.scrollDirection = .vertical
        
        //Set Collectionview Datasource and Delegates
        tagCollectionView.backgroundColor = UIColor.clear
        tagCollectionView.dataSource = self
        tagCollectionView.delegate = self
        tagCollectionView.bounces = false
        tagCollectionView.isPagingEnabled = true
        tagCollectionView.showsVerticalScrollIndicator = false
        tagCollectionView.showsHorizontalScrollIndicator = false
        tagCollectionView.collectionViewLayout = layout
        tagCollectionView.register(UINib(nibName: "HashtagCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HashtagCollectionCell")
    }

    private func loadImageCollectionView() {
        //Set Collectionview Flow Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.itemSize = CGSize(width:self.imageCollectionView.frame.width, height: 200.0)
        layout.scrollDirection = .horizontal
        
        //Set Collectionview Datasource and Delegates
        imageCollectionView.backgroundColor = UIColor.clear
        imageCollectionView.dataSource = self
        imageCollectionView.delegate = self
        imageCollectionView.bounces = false
        imageCollectionView.isPagingEnabled = true
        imageCollectionView.showsVerticalScrollIndicator = false
        imageCollectionView.showsHorizontalScrollIndicator = false
        imageCollectionView.collectionViewLayout = layout
        imageCollectionView.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
        
        //Set Page Control for Collection View
        pageControl.numberOfPages = imageArray.count
    }
}

//MARK: Collection View Datasource
extension Post2TableCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if collectionView == imageCollectionView {
                return imageArray.count
            }else {
                return dummyArray.count
            }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UICollectionViewCell
        if collectionView == imageCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as!ImageCollectionCell
            if (imageArray.count == 0){
                cell.mainImageView.image = #imageLiteral(resourceName: "dummy_placeholder")
                cell.videoBlurView.isHidden = true
            } else{
                if (imageArray[indexPath.row].videoUrl == ""){
                    cell.videoBlurView.isHidden = true
                    let image = imageArray[indexPath.row].imageUrl
                    cell.mainImageView.sd_setImage(with: URL(string:"\(String(describing: image!))"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
                    
                }else{
                    cell.videoBlurView.isHidden = false
                    let image = imageArray[indexPath.row].imageUrl
                    cell.mainImageView.sd_setImage(with: URL(string:"\(String(describing: image))"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
                    cell.videoButton.addTarget(self, action: #selector(self.tapVideoButton(sender:)), for: UIControlEvents.touchUpInside)
                    cell.videoButton.tag = indexPath.row
                }
                
            }
            return cell
            
        }else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HashtagCollectionCell", for: indexPath)
            (cell.viewWithTag(1) as! UILabel).text = "#\(self.dummyArray[indexPath.row].name!)"
            
        }
         return cell
        
    }
    func tapVideoButton(sender:UIButton){
        let image = imageArray[sender.tag].videoUrl
        self.delegate?.getVideosDetail(videoUrl: image!, name: nameLabel.text!)
    }
}

//MARK: Collection View Delegates
extension Post2TableCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize = CGSize()
        if collectionView == imageCollectionView {
            cellSize = CGSize(width:self.imageCollectionView.frame.width, height: 260.0)
        }else {
            let size = self.dummyArray[indexPath.row].name!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
            cellSize =  CGSize(width:size.width + 10.0, height: 15.0)

        }

       return cellSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}
