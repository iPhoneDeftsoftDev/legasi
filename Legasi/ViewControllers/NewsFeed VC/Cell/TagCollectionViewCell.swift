//
//  TagCollectionViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var crossButton: UIButton!
}
