//
//  TableViewCell3.swift
//  Legasi
//
//  Created by 123 on 03/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class Comment2TableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet var profileView: UIView!
    @IBOutlet var commentLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var userTypeImageView: UIImageView!
    @IBOutlet var nameButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Set Corner Radius and Shadow
        self.profileView.setRadius(radius: self.profileView.layer.frame.height/2, borderColor: UIColor.lightGray, borderWidth: 2)
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
