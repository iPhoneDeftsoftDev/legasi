//
//  BarGraphPollTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 21/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BarGraphPollTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pollButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
