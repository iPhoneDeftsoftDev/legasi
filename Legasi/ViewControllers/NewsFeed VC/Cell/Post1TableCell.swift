//
//  Post1TableCell.swift
//  DemoLegisi
//
//  Created by Apple on 06/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftCharts

protocol VideoPlayerDelegates {
    func getVideoDetail(videoUrl:String, name:String)
}

protocol PollSubmitDelegates {
    func pollSubmitMethod(pollId:Int)
}


class Post1TableCell: UITableViewCell {
    
    var dummyArray = [TagListData]()
    var selectedValue = false
    var selectedIndexPath = -1
    //MARK: IBOutlets
    @IBOutlet weak var profileDetailButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet var userTypeImageView: UIImageView!
    @IBOutlet var locationTopConstarint: NSLayoutConstraint!
    @IBOutlet var tagsTopConstraint: NSLayoutConstraint!
    @IBOutlet var linkTopConstraint: NSLayoutConstraint!
    @IBOutlet var locationButtonHeight: NSLayoutConstraint!
    @IBOutlet var linkButton: UIButton!
    @IBOutlet var linkButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var pollTableView: UITableView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var pollViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var hashTagCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var hashTagCollectionViewHeight: NSLayoutConstraint!
    var imageArray = [mediaPlayer]()
    @IBOutlet weak var titleNameLabel: UILabel!
    
    @IBOutlet weak var pollSubmitButton: UIButton!
    @IBOutlet weak var barGraphView: UIView!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var barGraphHeight: NSLayoutConstraint!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var barGraphTypeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var submitButtonHeight: NSLayoutConstraint!
    var delegate : VideoPlayerDelegates?
    var pollDelegate : PollSubmitDelegates?
    var pollArray = [PollList]()
    var chart: Chart?
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //MARK: Load Data
    func loadData() {
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        //Set Shadow to View
//        shadowView.layer.cornerRadius = 7
//        shadowView.layer.shadowColor = UIColor.black.cgColor
//        shadowView.layer.shadowOffset = CGSize.zero
//        shadowView.layer.shadowRadius = 2
//        shadowView.layer.shadowOpacity = 0.3
        locationLabel.sizeToFit()
        //Set Radius to image view
        self.profileImageView.layer.cornerRadius = 22.0
        self.likeButton.layer.cornerRadius = self.likeButton.frame.width/2
        
        //Set Collectionview
        self.loadImageCollectionView()
        self.loadHashTagCollectionView()
        self.loadPollTableView()
        if dummyArray.count == 0{
            tagsTopConstraint.constant = 0
        }
        //Set HashTagCollectionView Height
        self.hashTagCollectionViewHeight.constant = self.hashTagCollectionView.collectionViewLayout.collectionViewContentSize.height
        
    }
    
    private func loadImageCollectionView() {
        //Set Collectionview Flow Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.itemSize = CGSize(width:self.imageCollectionView.frame.width, height: 200.0)
        layout.scrollDirection = .horizontal
        
        //Set Collectionview Datasource and Delegates
        imageCollectionView.backgroundColor = UIColor.clear
        imageCollectionView.dataSource = self
        imageCollectionView.delegate = self
        imageCollectionView.bounces = false
        imageCollectionView.isPagingEnabled = true
        imageCollectionView.showsVerticalScrollIndicator = false
        imageCollectionView.showsHorizontalScrollIndicator = false
        imageCollectionView.collectionViewLayout = layout
        imageCollectionView.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
        
        //Set Page Control for Collection View
        pageControl.numberOfPages = imageArray.count
    }
    
    private func loadHashTagCollectionView() {
        //Set Collectionview Flow Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.itemSize = CGSize(width:self.hashTagCollectionView.frame.width, height: 15.0)
        layout.scrollDirection = .vertical
        
        //Set Collectionview Datasource and Delegates
        hashTagCollectionView.backgroundColor = UIColor.clear
        hashTagCollectionView.dataSource = self
        hashTagCollectionView.delegate = self
        hashTagCollectionView.bounces = false
        hashTagCollectionView.isPagingEnabled = true
        hashTagCollectionView.showsVerticalScrollIndicator = false
        hashTagCollectionView.showsHorizontalScrollIndicator = false
        hashTagCollectionView.collectionViewLayout = layout
        hashTagCollectionView.register(UINib(nibName: "HashtagCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HashtagCollectionCell")
    }
    
    private func loadPollTableView() {
        
        if pollArray.count > 0{
            
            pollTableView.dataSource = self
            var barArray = [(String, Double)]()
            var colorArray = [UIColor]()
            var pollCountArray = [Int]()
            for poll in pollArray {
                barArray.append((poll.title, Double(poll.pollCount)))
                pollCountArray.append(poll.pollCount)
                //setColors
                let red = Double(arc4random_uniform(256))
                let green = Double(arc4random_uniform(256))
                let blue = Double(arc4random_uniform(256))
                
                let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
                colorArray.append(color)
                
            }
            let maxValue = (pollCountArray.max()! < 5 ? 5: pollCountArray.max()) ?? 5
            let byValue = maxValue/5
            
            let chartConfig = BarsChartConfig(
                valsAxisConfig: ChartAxisConfig(from: 0, to: Double(maxValue), by: Double(byValue))
                
            )
            
            let frame = CGRect(x: -20, y: 20, width: self.frame.size.width+20, height: 200)
            let chart = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "",
                yTitle: "",
                bars: barArray,
                color: colorArray,
                barWidth: 20,
                horizontal: true
            )
            self.barGraphView.addSubview(chart.view)
            self.barGraphView.clipsToBounds = false
            
            self.chart = chart
        }
    }
    
    
}

//MARK: Collection View Datasource
extension Post1TableCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imageCollectionView {
            if(imageArray.count == 0){
                return 1
            } else{
                return imageArray.count
            }
        }else {
            return dummyArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == imageCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as!ImageCollectionCell
            if (imageArray.count == 0){
                cell.mainImageView.image = #imageLiteral(resourceName: "dummy_placeholder")
                cell.videoBlurView.isHidden = true
            } else{
                if (imageArray[indexPath.row].videoUrl == ""){
                    cell.videoBlurView.isHidden = true
                    let image = imageArray[indexPath.row].imageUrl
                    cell.mainImageView.sd_setImage(with: URL(string:"\(String(describing: image!))"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
                    
                }else{
                    cell.videoBlurView.isHidden = false
                    let image = imageArray[indexPath.row].imageUrl
                    cell.mainImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
                    cell.videoButton.addTarget(self, action: #selector(self.tapVideoButton(sender:)), for: UIControlEvents.touchUpInside)
                    cell.videoButton.tag = indexPath.row
                }
                
            }
            return cell
            
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HashtagCollectionCell", for: indexPath)
            (cell.viewWithTag(1) as! UILabel).text = "#\(self.dummyArray[indexPath.row].name!)"
            return cell
        }
        
    }
    func tapVideoButton(sender:UIButton){
        let image = imageArray[sender.tag].videoUrl
        self.delegate?.getVideoDetail(videoUrl: image!, name: nameLabel.text!)
    }
}

//MARK: Collection View Delegates
extension Post1TableCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize = CGSize()
        if collectionView == imageCollectionView {
            cellSize = CGSize(width:self.imageCollectionView.frame.width, height: 260.0)
        }else {
            let size = self.dummyArray[indexPath.row].name!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
            cellSize =  CGSize(width:size.width + 10.0, height: 15.0)

        }
        return cellSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}

extension Post1TableCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pollArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BarGraphPollTableViewCell = UITableViewCell.fromNib()
        
        cell.titleLabel.text = pollArray[indexPath.row].title
        if indexPath.row == selectedIndexPath{
            cell.pollButton.setImage(#imageLiteral(resourceName: "bar_radio_selected"), for: .normal)
        } else{
            cell.pollButton.setImage(#imageLiteral(resourceName: "bar_radio_unselected"), for: .normal)
        }
        
        cell.pollButton.addTarget(self, action: #selector(self.tapPollButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.pollButton.tag = indexPath.row
        
        return cell
    }
    
    func tapPollButtonAction(sender:UIButton){
        let indexPath = IndexPath(row: sender.tag, section: 0)
        selectedIndexPath = indexPath.row
        let pollId = pollArray[sender.tag].id
        self.pollDelegate?.pollSubmitMethod(pollId: pollId!)
        pollTableView.reloadData()
        
    }
    
    
}


class ResizableButton: UIButton {
    override var intrinsicContentSize: CGSize {
        let labelSize = titleLabel?.sizeThatFits(CGSize(width: frame.width, height: .greatestFiniteMagnitude)) ?? .zero
        let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right, height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom)
        
        return desiredButtonSize
    }
}
