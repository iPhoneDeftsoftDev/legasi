//
//  TableViewCell2.swift
//  Legasi
//
//  Created by 123 on 03/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class Comment1TableCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet var profileView: UIView!
    @IBOutlet var commentTextLabel: UILabel!
    
    @IBOutlet var userTypeImageView: UIImageView!
    @IBOutlet var nameButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Set Corner Radius and Shadow
        self.likeButton.layer.cornerRadius = self.likeButton.frame.width/2
        self.profileView.setRadius(radius: self.profileView.layer.frame.width/2, borderColor: UIColor.lightGray, borderWidth: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
