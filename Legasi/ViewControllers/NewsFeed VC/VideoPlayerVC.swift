//
//  VideoPlayerVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 05/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerVC: BaseVC {
//VideoPlayerVC
    
    @IBOutlet weak var videoView: UIView!
    var videoUrl:String!
    var videoPlayer: AVPlayer!
    var userName: String!
    var avpController = AVPlayerViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setBackButton()
        setTitle(title: userName)
        hideNavigationBar()
        self.showVideo(videoUrl: videoUrl)
        
    }

    func showVideo(videoUrl: String){
        
        let videoURL = NSURL(string: videoUrl)
        self.videoPlayer = AVPlayer(url: videoURL! as URL)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: self.videoPlayer.currentItem)
        self.avpController.player = self.videoPlayer
        avpController.view.frame = videoView.frame
        self.avpController.showsPlaybackControls = true
        self.avpController.view.isUserInteractionEnabled = true
        //self.avpController.videoGravity = AVLayerVideoGravityResize
        //to fit the screen uncomment uppercode
        self.addChildViewController(avpController)
        videoView.addSubview(avpController.view)
        self.videoPlayer.play()
        
    }
    
    //Notification
    func playerItemDidReachEnd(notification: NSNotification) {
        self.videoPlayer.seek(to: kCMTimeZero)
        self.videoPlayer.pause()
//        self.videoPlayer.play()
    }
    
    //MARK: - BACK BUTTON ACTION
    override func backButtonAction() {
        if self.videoPlayer != nil{
            self.videoPlayer.pause()
            self.videoPlayer = nil
        }
        self.navigationController?.popViewController(animated: false)
        NewsFeedVC.isPostDetail = false
    }
    
    @IBAction func closeVideoPlayer(_ sender: Any) {
        if self.videoPlayer != nil{
            self.videoPlayer.pause()
            self.videoPlayer = nil
        }
        self.navigationController?.popViewController(animated: false)
        NewsFeedVC.isPostDetail = false
    }
    
   

   
}
