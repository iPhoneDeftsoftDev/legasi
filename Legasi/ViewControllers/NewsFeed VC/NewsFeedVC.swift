 //
//  NewsFeedVC.swift
//  Legasi
//
//  Created by 123 on 02/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import SwiftCharts
import EventKit
import MapKit

class NewsFeedVC: BaseVC {
    
    //MARK: IBOutlet
    @IBOutlet weak var bottomSpaceOfButton: NSLayoutConstraint!
    @IBOutlet weak var milesTextField: UITextField!
    @IBOutlet weak var filterTagHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var typePostView: UIView!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet var filterView: UIView!
    @IBOutlet weak var zipcodeTextField: UITextField!
    
    @IBOutlet var postStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet var jobPostHeightConstraint: NSLayoutConstraint!
    @IBOutlet var roundView: RoundView!
    @IBOutlet var jobPostView: UIView!
    @IBOutlet var roundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noRecordLabel: UILabel!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet var typePostButton: [UIButton]!
    @IBOutlet weak var filterTagCollectionView: UICollectionView!
    @IBOutlet var newsfeedTableView: UITableView!
    @IBOutlet var tagsCollectionView: UICollectionView!
    @IBOutlet var popUpView: UIView!
    var pageNumber = 1
    var apiRunning = false
    var pollsId = 0
    //MARK: Variables
    var filterTagListArray = [TagListData]()
     var filterArray = [FilterData]()
    var postType = String()
    var selectedTextField: UITextField!
    var isMenuTap = false
    var coordinates = CLLocationCoordinate2D()
    var filterType = Int()
    var isTop = false
    var isFilterViewShow = false
    let store = EKEventStore()
    var startEventDate = Date()
    var endEventDate = Date()
    static var isPostDetail = true
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.enableMapLocation()
        popUpView.frame = view.frame

        for button in typePostButton {
            button.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
        }
        
        
  //set shadow on views
        locationView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: locationView.frame.origin.y, width: self.view.frame.width-30, height: locationView.frame.height))
        typePostView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: typePostView.frame.origin.y, width: self.view.frame.width-30, height: typePostView.frame.height))
        //tagsView shadow is given below, when tags collection view loaded.
//        tagsView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: tagsView.frame.origin.y, width: self.view.frame.width-30, height: tagsView.frame.height))
    
       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shouldShowLogo = true
        customiseUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        shouldShowLogo = false
    }
    
    //MARK: Private Function
    private func customiseUI() {
        if UIScreen.main.bounds.height >= 712 {
            bottomSpaceOfButton.constant = 55
        }
        if NewsFeedVC.isPostDetail == true{
            pageNumber = 1
            filterType = 2
            isTop = true
            NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
            newsfeedTableView.reloadData()
            noRecordLabel.isHidden = true
            Indicator.sharedInstance.showIndicator()
            self.callAPItoGetNewsFeed(filterType: filterType, tagIds: "")
        } else{
            NewsFeedVC.isPostDetail = true
        }
        setTitle(title: "")
        custumLeftTwoButtons()
        setRightBarButtons()
        
        //Set Table View Height Automatic
        newsfeedTableView.estimatedRowHeight = 340.0
        newsfeedTableView.rowHeight = UITableViewAutomaticDimension
        
        zipcodeTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        locationTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
         isMenuTap = false
         filterView.removeFromSuperview()
    }
    
    //MARK: RemovePopUpView
    private func removePopUpView() {
        self.popUpView.removeFromSuperview()
    }
    
    //MARK: Right Bar Buttons
    func setRightBarButtons() {
        
        let doneView = UIView()
        doneView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        let doneImageView = UIImageView()
        doneImageView.image = #imageLiteral(resourceName: "addWhite")
        doneImageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        let doneButton = UIButton() //Custom back Button
        doneButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
//        let addPostButton = UIButton()          //addPostButton
//        addPostButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//        addPostButton .setImage(#imageLiteral(resourceName: "addWhite"), for: UIControlState.normal)
        doneButton .addTarget(self, action: #selector(self.btnAddPostAction(sender:)), for: UIControlEvents.touchUpInside)
        
        let locationButton = UIButton()         //locationBasedSearchButton
        locationButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        locationButton .setImage(#imageLiteral(resourceName: "locatonIcon"), for: UIControlState.normal)
        locationButton .addTarget(self, action: #selector(self.btnLocationAction(sender:)), for: UIControlEvents.touchUpInside)
        
        doneView.addSubview(doneImageView)
        doneView.addSubview(doneButton)
        let barButtonItem1 = UIBarButtonItem()
        barButtonItem1.customView = doneView
        
        let barButtonItem2 = UIBarButtonItem()
        barButtonItem2.customView = locationButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -20
        
        self.navigationItem.setRightBarButtonItems([barButtonItem1,barButtonItem2,negativeSpacer], animated: true)
        
    }
    
    //MARK: Right Bar Button Actions
    func btnAddPostAction(sender:UIButton) {
        
        UIApplication.shared.keyWindow?.addSubview(popUpView)
       
        if DataManager.role == 1{
            if DataManager.userType == "Student"{
                jobPostHeightConstraint.constant = 0
                roundViewHeightConstraint.constant = 55
                postStackHeightConstraint.constant = 55
                jobPostView.isHidden = true
            }else{
                jobPostView.isHidden = false
            }
        }else{
            jobPostView.isHidden = false
        }
       
    }
    
    func btnLocationAction(sender:UIButton) {
        let storyboard =  UIStoryboard(name:"Search", bundle:nil)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kLocationBasedSearchVC)
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
   
    //MARK: IBActions
    @IBAction func typePostButtonAction(_ sender: UIButton) {
        locationTextField.resignFirstResponder()
        milesTextField.resignFirstResponder()
        zipcodeTextField.resignFirstResponder()
        for type in typePostButton {
            type.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
        }
        sender.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
        var data = FilterData(title: kStudent, id: 0, type: .post, coordinates: nil, ids: nil)
        if sender.tag == 1 {    //Student
            postType = kStudent
            data.title = kStudent
        }
        else if sender.tag == 2 {   //Graduate
            postType = kGraduate
            data.title = kGraduate
        }
        else if sender.tag == 3 {   //School
            postType = kSchool
            data.title = kSchool
        }
        if let index  = filterArray.index(where: {$0.type == FilterType.post}) {
            filterArray[index] = data
        }
        else {
            filterArray.append(data)
        }
        
        self.tagsCollectionView.reloadData()
    }
    
    @IBAction func tagButtonAction(_ sender: Any) {
        if isMenuTap == false {
           isFilterViewShow = true
            filterView.frame = CGRect(x: 0, y: 65, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
            self.view.addSubview(filterView)
            getTagListData()
            isMenuTap = true
        }else{
            filterView.removeFromSuperview()
            isFilterViewShow = false
            var tagIds = String()
            for tag in filterArray{
                if tag.type == .tag {
                    let id = tag.id ?? 0
                    if tagIds.isEmpty {
                        tagIds = "\(id)"
                    }
                    else {
                        tagIds = "\(tagIds),\(id)"
                    }
                }
             }
            pageNumber = 1
            filterType = 1
            NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
            self.callAPItoGetNewsFeed(filterType: filterType, tagIds: tagIds)
            isMenuTap = false
        }
 
    }

    @IBAction func filterSearchButtonAction(_ sender: Any) {
        filterView.removeFromSuperview()
        isFilterViewShow = false
        var tagIds = String()
        for tag in filterArray{
            if tag.type == .post {
                let id = tag.id ?? 0
                if tagIds.isEmpty {
                    tagIds = "\(id)"
                }
                else {
                    tagIds = "\(tagIds),\(id)"
                }
            }
            
            if tag.type == .tag {
                let id = tag.id ?? 0
                if tagIds.isEmpty {
                    tagIds = "\(id)"
                }
                else {
                    tagIds = "\(tagIds),\(id)"
                }
            }
        }
        pageNumber = 1
        filterType = 1
        NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
        self.callAPItoGetNewsFeed(filterType: filterType, tagIds: tagIds)
        isMenuTap = false

    }
    @IBAction func postButtonAction(_ sender: Any) {
        
        //MARK: RemovePopUpView
        removePopUpView()
        
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kAddPostVc) as! AddPostVC
        nextObj.groupId = 0
        self.navigationController?.pushViewController(nextObj, animated: true)
        
    }
    
    @IBAction func jobPostButtonAction(_ sender: Any) {
        
        //MARK: RemovePopUpView
        removePopUpView()
        
        let storyboard =  UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kAddJobVc) as! AddJobVC
        nextObj.groupId = 0
        self.navigationController?.pushViewController(nextObj, animated: true)
    
    }
    
    @IBAction func cancelPopUpButtonAction(_ sender: Any) {
        //MARK: RemovePopUpView
        removePopUpView()
    }
}

//MARK: UITableView DataSource
extension NewsFeedVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsFeedVM.sharedInstance.newsFeedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].type
        
        if type == NewsFeedType.normal {
            let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
            post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Comment
            
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title)?.fromBase64()
            if(decodeMsg == nil){
                post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
                 post1TableCell.titleNameLabel.text = decodeMsg
            }
            
           
             post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            post1TableCell.descriptionLabel.numberOfLines = 2
            post1TableCell.linkButtonHeight.constant = 0
            post1TableCell.linkTopConstraint.constant = 0
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userRole == 1{
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userType == "Student"{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                }
            }else{
                post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
            }
            
            post1TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.profileDetailButton.tag = indexPath.row
            
            
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post1TableCell.delegate = self
            post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            post1TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            post1TableCell.pollDelegate = self
            
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
               // DataManager.likeIcon_url
              
              post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                
            } else{
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
             
            }
            post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.likeButton.tag = indexPath.row
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post1TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post1TableCell.imageArray = imageArray
            }
            
            post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            let isAllowLocation = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowLocation
            if isAllowLocation != 1{
                post1TableCell.locationButtonHeight.constant = 0
                post1TableCell.locationLabel.text = ""
            } else{
                post1TableCell.locationLabel.sizeToFit()
                post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowComment == 1 {
                post1TableCell.commentButton.isHidden = false
                post1TableCell.commentLabel.isHidden = false
            }else{
                post1TableCell.commentButton.isHidden = true
                post1TableCell.commentLabel.isHidden = true
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList.count == 0 {
                 post1TableCell.pollViewHeight.constant = 0
            }
            
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            post1TableCell.locationLabel.isUserInteractionEnabled = true
            post1TableCell.locationLabel.tag = indexPath.row
            post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
            post1TableCell.commentButton.isUserInteractionEnabled = false
            
            post1TableCell.loadData()
            
             return post1TableCell
        }else if type == NewsFeedType.job {
            
            let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userRole == 1{
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userType == "Student"{
                    jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                } else{
                    jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                }
            }else{
                jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
            }
            jobCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            let concatStr:NSMutableAttributedString = NSMutableAttributedString()
            let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
            let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
            let myAttributedTitle = NSAttributedString(string: "Job Title:" , attributes: myAttributeBlackColor)
            
            //Decode Comment
            
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobName!).fromBase64()
            if(decodeMsg == nil){
                let attributedStringTitle = NSAttributedString(string: " \(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobName!)" , attributes: myAttributeGrayColor)
                concatStr.append(myAttributedTitle)
                concatStr.append(attributedStringTitle)
            }else{
                let attributedStringTitle = NSAttributedString(string: " \(decodeMsg ?? "")" , attributes: myAttributeGrayColor)
                concatStr.append(myAttributedTitle)
                concatStr.append(attributedStringTitle)
            }
            jobCell.titleLabel.attributedText = concatStr
            
            
            let concat1Str:NSMutableAttributedString = NSMutableAttributedString()
            let myAttribute1BlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
            let myAttribute1GrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
            let myAttributed1Title = NSAttributedString(string: "Company:" , attributes: myAttribute1BlackColor)
            let attributed1StringTitle = NSAttributedString(string: " \(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].companyName!)" , attributes: myAttribute1GrayColor)
            concat1Str.append(myAttributed1Title)
            concat1Str.append(attributed1StringTitle)
            jobCell.companyName.attributedText = concat1Str
            
            let isAllowLocation = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowLocation
            if isAllowLocation != 1{
                jobCell.locationButtonHeight.constant = 0
                jobCell.locationLabel.text = ""
            } else{
                jobCell.locationLabel.sizeToFit()
                jobCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowComment == 1 {
                jobCell.commentButton.isHidden = false
                jobCell.commentLabel.isHidden = false
            }else{
                jobCell.commentButton.isHidden = true
                jobCell.commentLabel.isHidden = true
            }
            
            jobCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: .touchUpInside)
//            jobCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            jobCell.profileDetailButton.tag = indexPath.row
            
            
            jobCell.skillsLabel.text = ""
            jobCell.skillsTopConstraint.constant = 0

            var type = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobType
            if type == "1"{
                type = "Full Time Job"
            }else if type == "2"{
                type = "Part Time Job"
            }else if type == "3"{
                type = "Paid Internship Job"
            }else {
                type = "Unpaid Internship Job"
            }
            jobCell.jobTypeLabel.text = "\(type!)"
            jobCell.likeLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            jobCell.isJobVC = false
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].status == 1{
                jobCell.availableView.backgroundColor = UIColor.black
                jobCell.availableLabel.text = "Available"
            }else{
                jobCell.availableView.backgroundColor = UIColor.darkGray
                jobCell.availableLabel.text = "Unavailable"
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }
            
        
            
            jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            jobCell.likeButton.tag = indexPath.row
            jobCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"

            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            jobCell.locationLabel.isUserInteractionEnabled = true
            jobCell.locationLabel.tag = indexPath.row
            jobCell.locationLabel.addGestureRecognizer(tapGesture)
            
            jobCell.decriptionLabel.text = ""
            jobCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            jobCell.titleLabel.sizeToFit()
            jobCell.companyName.sizeToFit()
            jobCell.skillsLabel.sizeToFit()
            jobCell.commentButton.isUserInteractionEnabled = false
            jobCell.loadData()
         return jobCell
        }else if type == NewsFeedType.pollSubmit {
            
            let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
            post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            
            //Decode Comment
            
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title)?.fromBase64()
            if(decodeMsg == nil){
                post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
                post1TableCell.titleNameLabel.text = decodeMsg
            }
            
            
            post1TableCell.linkButtonHeight.constant = 0
            post1TableCell.linkTopConstraint.constant = 0
            post1TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.profileDetailButton.tag = indexPath.row
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userRole == 1{
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userType == "Student"{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                } else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                }
            }else{
                post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
            }
            post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            post1TableCell.descriptionLabel.numberOfLines = 2
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            post1TableCell.pollDelegate = self
            post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.likeButton.tag = indexPath.row

            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                // DataManager.likeIcon_url
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowComment == 1 {
                post1TableCell.commentButton.isHidden = false
                post1TableCell.commentLabel.isHidden = false
            }else{
                post1TableCell.commentButton.isHidden = true
                post1TableCell.commentLabel.isHidden = true
            }
            post1TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post1TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post1TableCell.imageArray = imageArray
            }
            
            post1TableCell.delegate = self
            let isAllowLocation = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowLocation
            if isAllowLocation != 1{
                post1TableCell.locationButtonHeight.constant = 0
                post1TableCell.locationLabel.text = ""
            } else{
                post1TableCell.locationLabel.sizeToFit()
                post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            }
            
            post1TableCell.pollSubmitButton.addTarget(self, action: #selector(self.tapPollSubmitButton(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.pollSubmitButton.tag = indexPath.row
            post1TableCell.locationLabel.sizeToFit()
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList.count == 0 {
                post1TableCell.pollViewHeight.constant = 0

            }else{
                post1TableCell.pollArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList
                //polled by me
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isPolled == "Yes"{
                    post1TableCell.barGraphTypeHeight.constant = 0
                    post1TableCell.submitButtonHeight.constant = 0
                    post1TableCell.pollViewHeight.constant = 200
                }else {//polled by other
                    post1TableCell.barGraphTypeHeight.constant = 197
                    post1TableCell.submitButtonHeight.constant = 90
                    if post1TableCell.pollArray.count == 5 {
                        post1TableCell.pollViewHeight.constant = 484 + 30
                    }else{
                        post1TableCell.pollViewHeight.constant = 484
                    }
                }
            }
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            post1TableCell.locationLabel.isUserInteractionEnabled = true
            post1TableCell.locationLabel.tag = indexPath.row
            post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
            post1TableCell.commentButton.isUserInteractionEnabled = false
            post1TableCell.loadData()
            
            return post1TableCell
        
        } else {
            let post2TableCell : Post2TableCell = UITableViewCell.fromNib()
            
            post2TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Comment
            
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title)?.fromBase64()
            if(decodeMsg == nil){
               post2TableCell.titleLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
               post2TableCell.titleLabel.text = decodeMsg
            }
            
             post2TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            post2TableCell.descriptionLabel.numberOfLines = 2
            post2TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post2TableCell.profileDetailButton.tag = indexPath.row
             post2TableCell.linkButtonHeight.constant = 0
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowComment == 1 {
                post2TableCell.commentButton.isHidden = false
                post2TableCell.commentLabel.isHidden = false
            }else{
                post2TableCell.commentButton.isHidden = true
                post2TableCell.commentLabel.isHidden = true
            }

            post2TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post2TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
           let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post2TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post2TableCell.dateLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            
            let startDate = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].date
            let startTime = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].startTime
            post2TableCell.startDateLabel.text = "\(startDate!) at \(startTime!)"
            
            let endDate = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].endDate
            let endTime = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].endTime
            post2TableCell.endDateLabel.text = "\(endDate!) at \(endTime!)"
            
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                // DataManager.likeIcon_url
                post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }

            post2TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post2TableCell.likeButton.tag = indexPath.row
            post2TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post2TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post2TableCell.imageArray = imageArray
            }
            
            post2TableCell.rsvpViewHeightConstraint.constant = 0
           
            
            let isAllowLocation = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isAllowLocation
            if isAllowLocation != 1{
                post2TableCell.locationButtonHeight.constant = 0
                post2TableCell.locationLabel.text = ""
            } else{
                post2TableCell.locationLabel.sizeToFit()
                post2TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userRole == 1{
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].userType == "Student"{
                    post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                } else{
                    post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                }
            }else{
                post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
            }
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            post2TableCell.locationLabel.isUserInteractionEnabled = true
            post2TableCell.locationLabel.tag = indexPath.row
            post2TableCell.locationLabel.addGestureRecognizer(tapGesture)
            post2TableCell.delegate = self
            post2TableCell.commentButton.isUserInteractionEnabled = false
            post2TableCell.loadData()
            return post2TableCell
        }
      
    }
    
    func lblClick(tapGesture:UITapGestureRecognizer){
        
        if ConnectionCheck.isConnectedToNetwork(){
            LocationManager.sharedManager.getCoordinates(address: NewsFeedVM.sharedInstance.newsFeedArray[tapGesture.view!.tag].location!) { (coordinates) in
                
                let latitude: CLLocationDegrees = coordinates!.latitude
                let longitude: CLLocationDegrees = coordinates!.longitude
                
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = NewsFeedVM.sharedInstance.newsFeedArray[tapGesture.view!.tag].location
                mapItem.openInMaps(launchOptions: options)
                
                
            }
        }else {
            
            self.showAlert(message: kInternetConnectionMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }
    }
    
    
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func tapRSVPButtonAction(sender:UIButton){
       let dateStr = NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].date
       let endDateStr = NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].endDate
       startEventDate = (dateStr?.dateFromString(format: .dateTime))!
       endEventDate = (endDateStr?.dateFromString(format: .dateTime))!
        submitRSVPMethod(event_id: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, value:1,eventName: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].title)
    }
    func tapLikeButtonAction(sender:UIButton){
        if NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].isLike == 1{
            
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, type: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].type.rawValue)
        }else{
            self.likeOnNewsFeedMethod(likeValue: 1,eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, type: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].type.rawValue)
        }
    }

    func tapPollSubmitButton(sender:UIButton){
        pollSubmitData(pollId: pollsId, eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId)
    }
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                Indicator.isEnabledIndicator = false
                
                if !apiRunning {
                    Indicator.sharedInstance.showIndicator()
                    self.callAPItoGetNewsFeed(filterType: filterType, tagIds: "")
                }
            }
        }
    
}

extension NewsFeedVC : VideoPlayerDelegates, VideoPlayersDelegates, PollSubmitDelegates{
    
    func getVideoDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
      }
    
    func getVideosDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    func pollSubmitMethod(pollId: Int) {
        pollsId = pollId
    }
}
//MARK: CollectionView DataSource
extension NewsFeedVC : UICollectionViewDelegate   ,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  collectionView == filterTagCollectionView {
            return filterTagListArray.count
        }
        return filterArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if  collectionView == filterTagCollectionView {
            let cell = filterTagCollectionView.dequeueReusableCell(withReuseIdentifier: kSearchGroupCell, for: indexPath)
            (cell.viewWithTag(1) as! UILabel).text = "#\(filterTagListArray[indexPath.row].name!)"
            if filterArray.contains(where: {$0.id == filterTagListArray[indexPath.row].tagId}) {
                (cell.viewWithTag(1) as! UILabel).textColor = UIColor.LegasiColor.blue.colorWith(alpha: 1.0)
                tagsCollectionView.reloadData()
            }else{
                (cell.viewWithTag(1) as! UILabel).textColor = UIColor.darkGray
            }
            return cell

        }else{
            let cell = tagsCollectionView.dequeueReusableCell(withReuseIdentifier: kTagCell, for: indexPath)as!TagCollectionViewCell
            cell.tagLabel.text = filterArray[indexPath.row].title
            cell.crossButton.tag = indexPath.row
            cell.crossButton.addTarget(self, action: #selector(self.removeTag(sender:)), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if  collectionView == filterTagCollectionView {
            let size: CGSize = self.filterTagListArray[indexPath.row].name!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
            return  CGSize(width:size.width + 40.0, height: 15.0)
        }else{
            let size: CGSize = self.filterArray[indexPath.row].title.size(attributes: nil)
            return  CGSize(width:size.width + 30.0, height: 25.0)
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.filterTagHeightConstraint.constant = self.filterTagCollectionView.contentSize.height + 80
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  collectionView == filterTagCollectionView {
            if let index = filterArray.index(where: {$0.id == filterTagListArray[indexPath.row].tagId}) {
                filterArray.remove(at: index)
                tagsCollectionView.reloadData()
                filterTagCollectionView.reloadData()
            }
            else {
                let data = FilterData(title: filterTagListArray[indexPath.row].name!, id: filterTagListArray[indexPath.row].tagId!, type: .tag, coordinates: nil, ids: nil)
                filterArray.append(data)
                tagsCollectionView.reloadData()
                filterTagCollectionView.reloadData()
            }
        }
    }
    
    func removeTag(sender: UIButton) {
        if filterArray[sender.tag].type == .address {
            milesTextField.text = ""
            zipcodeTextField.text = ""
            locationTextField.text = ""
            coordinates = CLLocationCoordinate2D()
        }
        else if filterArray[sender.tag].type == .miles {
            milesTextField.text = ""
        }
        else if filterArray[sender.tag].type == .post {
            postType = ""
            for button in typePostButton {
                button.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            }
        }
        filterArray.remove(at: sender.tag)

        tagsCollectionView.reloadData()
        var tagIds = String()
        for tag in filterArray{
            if tag.type == .tag {
                let id = tag.id ?? 0
                if tagIds.isEmpty {
                    tagIds = "\(id)"
                }
                else {
                    tagIds = "\(tagIds),\(id)"
                }
            }
        }
        filterTagCollectionView.reloadData()

        pageNumber = 1
        filterType = 1
        NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
        self.callAPItoGetNewsFeed(filterType: filterType, tagIds: tagIds)
        
        
    }
    
}

//MARK: UITableView Delegates
extension NewsFeedVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
        nextObj.isThroughMyPost = true
        nextObj.feedIndex = indexPath.row
        nextObj.newsFeedId = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].eventId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
}

//MARK: TextField Delegates
extension NewsFeedVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.tagsCollectionView.reloadData()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        if textField == zipcodeTextField{
            if (milesTextField.text?.isEmpty)!{
                self.showAlert(message: "Please enter miles first", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }
        } else if textField == locationTextField {
            textField.resignFirstResponder()
            if (milesTextField.text?.isEmpty)!{
                self.showAlert(message: "Please enter miles first", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else{
                let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
                locationVC.delegate = self
                self.navigationController?.pushViewController(locationVC, animated: true)
            }
            
        }
        
        selectedTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == zipcodeTextField || textField == locationTextField) {
            if zipcodeTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty && locationTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
                if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray.remove(at: index)
                    self.tagsCollectionView.reloadData()
                }
            }
            else {
            LocationManager.sharedManager.getCoordinates(address: textField.text!, completed: { (coordinates) in
                if coordinates != nil {
                    self.coordinates = coordinates!
                    
                    var title = textField.text!
                    if !self.milesTextField.text!.isEmpty {
                        title = "\(self.milesTextField.text!) miles from \(textField.text!)"
                    }
                    let data = FilterData(title: title, id: 0, type: .address, coordinates: coordinates,ids: nil)
                    if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                        self.filterArray[index] = data
                    }
                    else {
                        self.filterArray.append(data)
                    }
                    self.tagsCollectionView.reloadData()
                }
                else {
                    self.coordinates = CLLocationCoordinate2D()
                    if textField == self.zipcodeTextField {
                        self.showAlert(message: "Please enter valid zip code")
                    }
                    else {
                        self.showAlert(message: "Please enter valid address")
                    }
                }
            })
            }
        }
        if textField == milesTextField {
            if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
                if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray.remove(at: index)
                    self.tagsCollectionView.reloadData()
                }
            }
            else {
                var title = "\(textField.text!) miles"
                if !self.locationTextField.text!.isEmpty {
                    title = "\(title) from \(self.locationTextField.text!)"
                }
                let data = FilterData(title: title, id: 0, type: .address, coordinates: nil, ids: nil)
                if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray[index] = data
                }
                else {
                    self.filterArray.append(data)
                }
            }
        }
        
        self.tagsCollectionView.reloadData()
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        if(textField == zipcodeTextField){
            locationTextField.text = ""
        }
        else if(textField == locationTextField) {
            zipcodeTextField.text = ""
        }
        self.coordinates = CLLocationCoordinate2D()
    }
    
}
extension NewsFeedVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        if !address.isEmpty {
            locationTextField.text = address
            var title = selectedTextField.text!
            if !self.milesTextField.text!.isEmpty {
                title = "\(self.milesTextField.text!) miles from \(selectedTextField.text!)"
            }
            self.coordinates.latitude = latitude
            self.coordinates.longitude = longitude
            let data = FilterData(title: title, id: 0, type: .address, coordinates: coordinates,ids: nil)
            if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                self.filterArray[index] = data
            }
            else {
                self.filterArray.append(data)
            }
            self.tagsCollectionView.reloadData()
            
        }
    }
}

//MARK: API Methods
extension NewsFeedVC {
    
    func getTagListData(){
        NewsFeedVM.sharedInstance.getTagList(userId: DataManager.user_id!) { (success, message, error) in
            if(success == 1) {
                self.filterTagListArray = NewsFeedVM.sharedInstance.tagListArray
                self.filterTagCollectionView.reloadData()
                self.filterTagCollectionView.performBatchUpdates({}, completion: { (finish) in
                    
                     self.tagsView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: self.tagsView.frame.origin.y, width: self.view.frame.width-30, height: self.tagsView.frame.height))
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int,type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.callAPItoGetNewsFeed(filterType: self.filterType, tagIds: "")
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }

    func pollSubmitData(pollId: Int, eventId:Int){
        NewsFeedVM.sharedInstance.voteOnPoll(userId: DataManager.user_id!, eventId: eventId, pollId: pollId) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.callAPItoGetNewsFeed(filterType: self.filterType, tagIds: "")
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func callAPItoGetNewsFeed(filterType: Int, tagIds: String) {
        
        apiRunning = true
        NewsFeedVM.sharedInstance.newsFeedDetail(userId: DataManager.user_id!, filterApply: filterType, typeOfPost: postType, miles: milesTextField.text!, lat: self.coordinates.latitude, long: self.coordinates.longitude, ids: tagIds, page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.pageNumber += 1
                self.noRecordLabel.isHidden = true
                self.newsfeedTableView.isHidden = false
                self.newsfeedTableView.reloadData()
                if self.isTop == true{
                    self.newsfeedTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    self.isTop = false
                    self.geoFacingMethod()
                }
                
            }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                        if self.isFilterViewShow == false{
                            self.noRecordLabel.isHidden = false
                            self.newsfeedTableView.isHidden = true
                        }
                    }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            self.newsfeedTableView.reloadData()
             self.apiRunning = false
        }
    }
    
    func submitRSVPMethod(event_id:Int, value:Int, eventName:String){
        var value = value
        if value == 0{
            value = 2
        }
        NewsFeedVM.sharedInstance.SubmitRSVPRequest(userId: DataManager.user_id!, eventId: event_id, value: value) { (success, message , error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                
                self.callAPItoGetNewsFeed(filterType: self.filterType, tagIds: "")
                self.createEventinTheCalendar(with: eventName, forDate: self.startEventDate, toDate: self.endEventDate)
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func createEventinTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    self.showAlert(message: "failed to save event with error : \(error)", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                   
                }
                
            } else {
                //we have error in getting access to device calnedar
                self.showAlert(message: error?.localizedDescription, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                
            }
        }
    }
    func geoFacingMethod(){
        let appDelegate = UIApplication.shared.delegate as!AppDelegate
       
       NewsFeedVM.sharedInstance.GeoFancingMethod(userId: DataManager.user_id!, lat: appDelegate.latitude, long: appDelegate.longitude) { (success, message , error) in
            if(success == 1) {
                for data in NewsFeedVM.sharedInstance.geoFacingArray {
                    let geofenceRegion = CLCircularRegion(center: data.coordinates, radius: 200, identifier: "\(data.eventId)");
                    geofenceRegion.notifyOnExit = true;
                    geofenceRegion.notifyOnEntry = true;
                    LocationManager.sharedManager.startMonitoring(region: geofenceRegion)
                }
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}






