//
//  PostDetailVC.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import EventKit
import MapKit


class PostDetailVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet var postDetailTableView: UITableView!
    
    @IBOutlet weak var bottomConstraintForTextView: NSLayoutConstraint!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var postDetailHeight: NSLayoutConstraint!
    //MARK: Variables
    var newsFeedId:Int!
    var feedDict = NewsFeedData()
    var index = 0
    var feedIndex = 0
    var pollsId = 0
    var isCommentPost: Bool!
    var selectedIndexPath = -1
    var replyOnCommentId: Int!
    var isThroughMyPost = false
    let store = EKEventStore()
    var startEventDate = Date()
    var endEventDate = Date()
    var eventLocation = String()
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.hideKeyboardWhenTappedAround()        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name(rawValue: "PostNotify"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchPostData(notification:)), name: NSNotification.Name(rawValue: "PostNotify"), object: nil)
    }
    //MARK: NSNOTIFICATION METHOD
    func fetchPostData(notification: NSNotification){
        print(notification)
        let postDict = notification.userInfo as NSDictionary!
        newsFeedId = postDict?["newsFeedId"]as!Int
        getPostDetailMethod()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
    }
    //MARK: - Notification methods for keyboard present and dismiss
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        //set constraints of bottom of text view
        self.bottomConstraintForTextView.constant = keyboardFrame.size.height
        
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        if(NewsFeedVM.sharedInstance.feedDetail.commentsArray.count > 1){
            self.scrollAtLast()
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        //set constraints of bottom of text view
        self.bottomConstraintForTextView.constant = 0;
        
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
        //dispatch time to scroll at last
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            if(NewsFeedVM.sharedInstance.feedDetail.commentsArray.count  > 1){
                self.scrollAtLast()
            }
        })
    }
    func scrollAtLast(){//scroll at last index
        if NewsFeedVM.sharedInstance.feedDetail.commentsArray.count > 1 {
            self.postDetailTableView.scrollToBottom()
            
            //            let path = IndexPath(row: NewsFeedVM.sharedInstance.feedDetail.commentsArray.count - 1, section: 0)
            //            UIView.animate(withDuration: 0.4, animations: { () -> Void in
            //                self.postDetailTableView.scrollToRow(at: path,
            //                                                     at: .bottom, animated: true)
            //            })
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CustomizeUI()
    }
    
    //MARK: Private Functions
    private func CustomizeUI() {
        
        setTitle(title: kPostDetailTitle)
        commentTextField.setPlaceholder()
        setBackButton()
        
        //textfield hidden
        sendView.isHidden = true
        postDetailHeight.constant = 0
        
        
        self.view.backgroundColor = UIColor.white
        NotificationCenter.default.removeObserver(self, name: Notification.Name("commentButtonClicked"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.commentButtonClicked(notification:)), name: Notification.Name("commentButtonClicked"), object: nil)
        //Hide keyBoard when appliation goes to background
        NotificationCenter.default.addObserver(self, selector: #selector(hideSendView), name: NSNotification.Name(rawValue: "hideSendView"), object: nil)
        getPostDetailMethod()
        //Set Table View Height Automatic
        postDetailTableView.estimatedRowHeight = 340.0
        postDetailTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    //MARK: Set More Button
    override func setMoreButton(){
        
        let deletePostView = UIView()
        deletePostView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let deletePostImageView = UIImageView()
        deletePostImageView.image = #imageLiteral(resourceName: "delete_icon")
        deletePostImageView.frame = CGRect(x: 24, y: 11, width: 20, height: 20)
        let deletePostButton = UIButton() //Custom back Button
        deletePostButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        //        let moreButton = UIButton() //Custom back Button
        //        moreButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        //        moreButton.setImage(#imageLiteral(resourceName: "delete_icon"), for: UIControlState.normal)
        deletePostButton.addTarget(self, action: #selector(self.moreButtonAction), for: UIControlEvents.touchUpInside)
        let rightBarButton = UIBarButtonItem()
        deletePostView.addSubview(deletePostImageView)
        deletePostView.addSubview(deletePostButton)
        rightBarButton.customView = deletePostView
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0;
        
        self.navigationItem.setRightBarButtonItems([rightBarButton, negativeSpacer], animated: false)
    }
    override func moreButtonAction() {
        self.showAlert(message: "Are you sure you want to delete this post?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
                
                self.deleteJobMethod()
        })
        
        
        
        
    }
    
    override func backButtonAction() {
        if isThroughMyPost == false{
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            // let vc = navVc.visibleViewController
            topViewController.selectedIndex = 0
            navVc.popViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        self.view.endEditing(true)
        NewsFeedVC.isPostDetail = false
        
    }
    //    func hideKeyboardWhenTappedAround() {
    //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
    //        tap.cancelsTouchesInView = false
    //        view.addGestureRecognizer(tap)
    //    }
    
    @IBAction func commentSendButtonAction(_ sender: Any) {
        sendView.isHidden = true
        postDetailHeight.constant = 0
        if commentTextField.text! == ""{
            commentTextField.resignFirstResponder()
        }else{
            commentTextField.resignFirstResponder()
            if commentTextField.isEmpty{
                //            self.showAlert(message: "Please enter the text first.")
            }
            sendView.isHidden = true
            postDetailHeight.constant = 0
            commentTextField.resignFirstResponder()
            if isCommentPost == true{
                self.addCommentOnNewFeed()
            }else{
                self.replyOnCommentMethod(commentId:replyOnCommentId)
                
            }
        }
    }
    func dismissKeyboard() {
        self.sendView.isHidden = true
        self.postDetailHeight.constant = 0
        view.endEditing(true)
    }
    func hideSendView() {
        self.sendView.isHidden = true
        self.postDetailHeight.constant = 0
    }
    
}

//MARK: UITableView DataSources
extension PostDetailVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if index != 0{
            if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1{
                return NewsFeedVM.sharedInstance.feedDetail.commentsArray.count + 1
            }
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //MARK: Load cells from xib.
        switch indexPath.row {
        case 0:
            
            let type = NewsFeedVM.sharedInstance.feedDetail.type
            
            if type == NewsFeedType.normal {
                
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.feedDetail.name
                let link = NewsFeedVM.sharedInstance.feedDetail.link
                if link == ""{
                    post1TableCell.linkButtonHeight.constant = 0
                    post1TableCell.linkTopConstraint.constant = 0
                } else{
                    post1TableCell.linkButton.setTitle("\(link!)", for: .normal)
                    
                    post1TableCell.linkButton.addTarget(self, action: #selector(self.tapLinkButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                    post1TableCell.linkButton.tag = indexPath.row
                }
                //Decode Message
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.title)?.fromBase64()
                if(decodeMsg == nil){
                    post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.feedDetail.title
                }else{
                    post1TableCell.titleNameLabel.text = decodeMsg
                }
                
                post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.feedDetail.Description
                post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.likes!)"
                post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.comments!)"
                post1TableCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.commentButton.tag = indexPath.row
                
                post1TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.nameButton.tag = indexPath.row
                
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row
                if NewsFeedVM.sharedInstance.feedDetail.isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                
                let image = NewsFeedVM.sharedInstance.feedDetail.profilePic
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post1TableCell.pollDelegate = self
                
                post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.feedDetail.time
                post1TableCell.dummyArray = NewsFeedVM.sharedInstance.feedDetail.newsFeedTagList
                let imageArray = NewsFeedVM.sharedInstance.feedDetail.mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                
                let isAllowLocation = NewsFeedVM.sharedInstance.feedDetail.isAllowLocation
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.feedDetail.location!
                }
                
                post1TableCell.delegate = self
                if NewsFeedVM.sharedInstance.feedDetail.pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                }
                if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                if NewsFeedVM.sharedInstance.feedDetail.userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    } else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                
                post1TableCell.loadData()
                
                return post1TableCell
            }else if type == NewsFeedType.job {
                
                let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
                let image = NewsFeedVM.sharedInstance.feedDetail.profilePic
                jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "dummyImage"))
                jobCell.nameLabel.text = NewsFeedVM.sharedInstance.feedDetail.name
                let concatStr:NSMutableAttributedString = NSMutableAttributedString()
                let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributedTitle = NSAttributedString(string: "Job Title:" , attributes: myAttributeBlackColor)
                
                //Decode Message
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.jobName!).fromBase64()
                if(decodeMsg == nil){
                    let attributedStringTitle = NSAttributedString(string: " \(NewsFeedVM.sharedInstance.feedDetail.jobName!)" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }else{
                    let attributedStringTitle = NSAttributedString(string: " \(decodeMsg ?? "")" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }
                jobCell.titleLabel.attributedText = concatStr
                
                
                let concat1Str:NSMutableAttributedString = NSMutableAttributedString()
                let myAttribute1BlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttribute1GrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributed1Title = NSAttributedString(string: "Company:" , attributes: myAttribute1BlackColor)
                let attributed1StringTitle = NSAttributedString(string: " \(NewsFeedVM.sharedInstance.feedDetail.companyName!)" , attributes: myAttribute1GrayColor)
                concat1Str.append(myAttributed1Title)
                concat1Str.append(attributed1StringTitle)
                jobCell.companyName.attributedText = concat1Str
                
                
                let concat2Str:NSMutableAttributedString = NSMutableAttributedString()
                let myAttribute2BlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttribute2GrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributed2Title = NSAttributedString(string: "Skills:" , attributes: myAttribute2BlackColor)
                let attributed2StringTitle = NSAttributedString(string: " \(NewsFeedVM.sharedInstance.feedDetail.skills!)" , attributes: myAttribute2GrayColor)
                concat1Str.append(myAttributed2Title)
                concat1Str.append(attributed2StringTitle)
                jobCell.skillsLabel.attributedText = concat2Str
                
                
                let isAllowLocation = NewsFeedVM.sharedInstance.feedDetail.isAllowLocation
                if isAllowLocation != 1{
                    jobCell.locationButtonHeight.constant = 0
                    jobCell.locationLabel.text = ""
                } else{
                    jobCell.locationLabel.sizeToFit()
                    jobCell.locationLabel.text = NewsFeedVM.sharedInstance.feedDetail.location
                }
                
                jobCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.nameButton.tag = indexPath.row
                
                
                var type = NewsFeedVM.sharedInstance.feedDetail.jobType
                if type == "1"{
                    type = "Full Time Job"
                }else if type == "2"{
                    type = "Part Time Job"
                }else if type == "3"{
                    type = "Paid Internship Job"
                }else {
                    type = "Unpaid Internship Job"
                }
                jobCell.jobTypeLabel.text = "\(type!)"
                jobCell.decriptionLabel.text = NewsFeedVM.sharedInstance.feedDetail.Description
                jobCell.timeLabel.text = NewsFeedVM.sharedInstance.feedDetail.time
                jobCell.likeLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.likes!)"
                jobCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.comments!)"
                jobCell.isJobVC = false
                if NewsFeedVM.sharedInstance.feedDetail.status == 1{
                    jobCell.availableView.backgroundColor = UIColor.black
                    jobCell.availableLabel.text = "Available"
                }else{
                    jobCell.availableView.backgroundColor = UIColor.darkGray
                    jobCell.availableLabel.text = "Unavailable"
                }
                if NewsFeedVM.sharedInstance.feedDetail.isLike == 1{
                    // DataManager.likeIcon_url
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                jobCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.commentButton.tag = indexPath.row
                
                jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.likeButton.tag = indexPath.row
                jobCell.locationLabel.sizeToFit()
                jobCell.titleLabel.sizeToFit()
                jobCell.companyName.sizeToFit()
                jobCell.skillsLabel.sizeToFit()
                if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1 {
                    jobCell.commentButton.isHidden = false
                    jobCell.commentLabel.isHidden = false
                }else{
                    jobCell.commentButton.isHidden = true
                    jobCell.commentLabel.isHidden = true
                }
                if NewsFeedVM.sharedInstance.feedDetail.userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.userType == "Student"{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    } else{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                jobCell.locationLabel.isUserInteractionEnabled = true
                jobCell.locationLabel.tag = indexPath.row
                jobCell.locationLabel.addGestureRecognizer(tapGesture)
                jobCell.loadData()
                return jobCell
            }else if type == NewsFeedType.pollSubmit {
                
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.feedDetail.name
                //Decode Message
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.title)?.fromBase64()
                if(decodeMsg == nil){
                    post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.feedDetail.title
                }else{
                    post1TableCell.titleNameLabel.text = decodeMsg
                }
                post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.feedDetail.Description
                let link = NewsFeedVM.sharedInstance.feedDetail.link
                if link == ""{
                    post1TableCell.linkButtonHeight.constant = 0
                    post1TableCell.linkTopConstraint.constant = 0
                } else{
                    post1TableCell.linkButton.setTitle("\(link!)", for: .normal)
                    
                    post1TableCell.linkButton.addTarget(self, action: #selector(self.tapLinkButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                    post1TableCell.linkButton.tag = indexPath.row
                }
                
                let image = NewsFeedVM.sharedInstance.feedDetail.profilePic
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.likes!)"
                post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.comments!)"
                post1TableCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.commentButton.tag = indexPath.row
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row
                
                post1TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.nameButton.tag = indexPath.row
                
                if NewsFeedVM.sharedInstance.feedDetail.isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                
                post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.feedDetail.time
                post1TableCell.dummyArray = NewsFeedVM.sharedInstance.feedDetail.newsFeedTagList
                let imageArray = NewsFeedVM.sharedInstance.feedDetail.mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                let isAllowLocation = NewsFeedVM.sharedInstance.feedDetail.isAllowLocation
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.feedDetail.location!
                }
                if NewsFeedVM.sharedInstance.feedDetail.userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    } else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                post1TableCell.pollDelegate = self
                post1TableCell.pollSubmitButton.addTarget(self, action: #selector(self.tapPollSubmitButton(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.pollSubmitButton.tag = indexPath.row
                post1TableCell.locationLabel.sizeToFit()
                post1TableCell.delegate = self
                if NewsFeedVM.sharedInstance.feedDetail.pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                    
                }else{
                    post1TableCell.pollArray = NewsFeedVM.sharedInstance.feedDetail.pollList
                    //polled by me
                    if NewsFeedVM.sharedInstance.feedDetail.isPolled == "Yes"{
                        post1TableCell.barGraphTypeHeight.constant = 0
                        post1TableCell.submitButtonHeight.constant = 0
                        post1TableCell.pollViewHeight.constant = 200
                    }else {//polled by other
                        post1TableCell.barGraphTypeHeight.constant = 197
                        post1TableCell.submitButtonHeight.constant = 90
                        if post1TableCell.pollArray.count == 5 {
                            post1TableCell.pollViewHeight.constant = 484 + 30
                        }else{
                            post1TableCell.pollViewHeight.constant = 484
                        }
                        
                    }
                }
                if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                
                post1TableCell.loadData()
                return post1TableCell
                
            } else {
                let post2TableCell : Post2NewTableViewCell = UITableViewCell.fromNib()
                
                post2TableCell.nameLabel.text = NewsFeedVM.sharedInstance.feedDetail.name
                //Decode Message
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.title)?.fromBase64()
                if(decodeMsg == nil){
                    post2TableCell.titleLabel.text = NewsFeedVM.sharedInstance.feedDetail.title
                }else{
                    post2TableCell.titleLabel.text = decodeMsg
                }
                
                post2TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.feedDetail.Description
                let link = NewsFeedVM.sharedInstance.feedDetail.link
                if link == ""{
                    post2TableCell.linkButtonHeight.constant = 0
                } else{
                    post2TableCell.linkButton.setTitle("\(link!)", for: .normal)
                    
                    post2TableCell.linkButton.addTarget(self, action: #selector(self.tapLinkButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                    post2TableCell.linkButton.tag = indexPath.row
                }
                post2TableCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post2TableCell.commentButton.tag = indexPath.row
                
                let startDate = NewsFeedVM.sharedInstance.feedDetail.date
                let startTime = NewsFeedVM.sharedInstance.feedDetail.startTime
                post2TableCell.startDateLabel.text = "\(startDate!) at \(startTime!)"
                
                let endDate = NewsFeedVM.sharedInstance.feedDetail.endDate
                let endTime = NewsFeedVM.sharedInstance.feedDetail.endTime
                post2TableCell.endDateLabel.text = "\(endDate!) at \(endTime!)"
                
                
                post2TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post2TableCell.nameButton.tag = indexPath.row
                
                post2TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post2TableCell.likeButton.tag = indexPath.row
                
                if NewsFeedVM.sharedInstance.feedDetail.isLike == 1{
                    // DataManager.likeIcon_url
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                
                
                post2TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.likes!)"
                
                post2TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.comments!)"
                
                let image = NewsFeedVM.sharedInstance.feedDetail.profilePic
                post2TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                
                post2TableCell.dateLabel.text = NewsFeedVM.sharedInstance.feedDetail.time
                post2TableCell.dummyArray = NewsFeedVM.sharedInstance.feedDetail.newsFeedTagList
                let imageArray = NewsFeedVM.sharedInstance.feedDetail.mediaListData!
                if imageArray.count == 0{
                    post2TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post2TableCell.imageArray = imageArray
                }
                let isAllowLocation = NewsFeedVM.sharedInstance.feedDetail.isAllowLocation
                if isAllowLocation != 1{
                    post2TableCell.locationButtonHeight.constant = 0
                    post2TableCell.locationLabel.text = ""
                } else{
                    post2TableCell.locationLabel.sizeToFit()
                    post2TableCell.locationLabel.text = NewsFeedVM.sharedInstance.feedDetail.location!
                }
                let rsvpDone = NewsFeedVM.sharedInstance.newsFeedArray[feedIndex].rsvpStatus
                if rsvpDone == 1{
                    post2TableCell.submitRSVPButton.setTitle("RSVP Done", for: .normal)
                    post2TableCell.submitRSVPButton.isUserInteractionEnabled = false
                }else{
                    post2TableCell.submitRSVPButton.isUserInteractionEnabled = true
                    post2TableCell.submitRSVPButton.setTitle("RSVP", for: .normal)
                    post2TableCell.submitRSVPButton.addTarget(self, action: #selector(self.tapRSVPButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                    post2TableCell.submitRSVPButton.tag = indexPath.row
                }
                
                if NewsFeedVM.sharedInstance.feedDetail.userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.userType == "Student"{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    } else{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                post2TableCell.delegate = self
                if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1 {
                    post2TableCell.commentButton.isHidden = false
                    post2TableCell.commentLabel.isHidden = false
                }else{
                    post2TableCell.commentButton.isHidden = true
                    post2TableCell.commentLabel.isHidden = true
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post2TableCell.locationLabel.isUserInteractionEnabled = true
                post2TableCell.locationLabel.tag = indexPath.row
                post2TableCell.locationLabel.addGestureRecognizer(tapGesture)
                if NewsFeedVM.sharedInstance.feedDetail.numberofTicket == 1{
                    post2TableCell.ticketsLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.numberofTicket!) ticket"
                }else{
                    post2TableCell.ticketsLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.numberofTicket!) tickets"
                }
                
                post2TableCell.loadData()
                
                return post2TableCell
            }
            
            break
            
        default:
            break
        }
        if NewsFeedVM.sharedInstance.feedDetail.isAllowComment == 1{
            let commentType = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].type
            if commentType == .comment{
                let comment1TableCell : Comment1TableCell = UITableViewCell.fromNib()
                let name = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].name
                comment1TableCell.nameButton.setTitle(name, for: .normal)
                
                comment1TableCell.nameButton.addTarget(self, action: #selector(self.tapCommentNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                comment1TableCell.nameButton.tag = indexPath.row
                if NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].userType == "Student"{
                        comment1TableCell.userTypeImageView.image =  #imageLiteral(resourceName: "studentIcon")
                    }else{
                        comment1TableCell.userTypeImageView.image =  #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    comment1TableCell.userTypeImageView.image =  #imageLiteral(resourceName: "adminIcon")
                }
                //Decode Comment
                
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].commentText)?.fromBase64()
                if(decodeMsg == nil){
                    comment1TableCell.commentTextLabel.text = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].commentText
                }else{
                    comment1TableCell.commentTextLabel.text = decodeMsg
                }
                
                comment1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].comments!)"
                comment1TableCell.likeLabel.text = "\(NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].likes!)"
                comment1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].time
                
                comment1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeOnCommentAction(sender:)), for: UIControlEvents.touchUpInside)
                comment1TableCell.likeButton.tag = indexPath.row
                
                if NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].isLikes == 1{
                    // DataManager.likeIcon_url
                    comment1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage:  #imageLiteral(resourceName: "likeBlue"))
                } else{
                    comment1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage:  #imageLiteral(resourceName: "likeWhite"))
                }
                
                comment1TableCell.commentButton.addTarget(self, action: #selector(self.tapReplyOnCommentAction(sender:)), for: UIControlEvents.touchUpInside)
                comment1TableCell.commentButton.tag = indexPath.row
                let image = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].imageUrl
                comment1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage:  #imageLiteral(resourceName: "userImage"))
                
                return comment1TableCell
            }else{
                let replyCell : Comment2TableCell = UITableViewCell.fromNib()
                let name = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].name
                if NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].userRole == 1{
                    if NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].userType == "Student"{
                        replyCell.userTypeImageView.image =  #imageLiteral(resourceName: "studentIcon")
                    }else{
                        replyCell.userTypeImageView.image =  #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    replyCell.userTypeImageView.image =  #imageLiteral(resourceName: "adminIcon")
                }
                
                replyCell.nameButton.setTitle(name, for: .normal)
                replyCell.nameButton.addTarget(self, action: #selector(self.tapCommentNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                replyCell.nameButton.tag = indexPath.row
                
                //Decode Reply On Comment
                
                let decodeMsg = (NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].commentText)?.fromBase64()
                if(decodeMsg == nil){
                    replyCell.commentLabel.text = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].commentText
                }else{
                    replyCell.commentLabel.text = decodeMsg
                }
                replyCell.timeLabel.text = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].time
                let image = NewsFeedVM.sharedInstance.feedDetail.commentsArray[indexPath.row - 1].imageUrl
                replyCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage:  #imageLiteral(resourceName: "userImage"))
                return replyCell
            }
            
        }
        let cell = UITableViewCell()
        return cell
        
    }
    func lblClick(tapGesture:UITapGestureRecognizer){
        if ConnectionCheck.isConnectedToNetwork(){
            LocationManager.sharedManager.getCoordinates(address: NewsFeedVM.sharedInstance.feedDetail.location!) { (coordinates) in
                let latitude: CLLocationDegrees = coordinates!.latitude
                let longitude: CLLocationDegrees = coordinates!.longitude
                
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = NewsFeedVM.sharedInstance.feedDetail.location!
                mapItem.openInMaps(launchOptions: options)
                
            }
        }else {
            
            self.showAlert(message: kInternetConnectionMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }
        
    }
    
    func tapCommentNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = NewsFeedVM.sharedInstance.feedDetail.commentsArray[sender.tag - 1].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func tapRSVPButtonAction(sender:UIButton){
        let dateStr = NewsFeedVM.sharedInstance.feedDetail.startDateTime
        startEventDate = Date(milliseconds: dateStr!)
        let endDateStr = NewsFeedVM.sharedInstance.feedDetail.endDateTime
        endEventDate = Date(milliseconds: endDateStr!)
        eventLocation = NewsFeedVM.sharedInstance.feedDetail.location
        submitRSVPMethod(event_id: NewsFeedVM.sharedInstance.feedDetail.eventId, value:1,eventName: NewsFeedVM.sharedInstance.feedDetail.title)
    }
    
    func tapLinkButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kLinkWebViewVC)as!LinkWebViewVC
        nextObj.linkUrl = NewsFeedVM.sharedInstance.feedDetail.link
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    func tapPollSubmitButton(sender:UIButton){
        pollSubmitData(pollId: pollsId, eventId: NewsFeedVM.sharedInstance.feedDetail.eventId)
    }
    func tapCommentButtonAction(sender:UIButton){
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 0.0
        sendView.isHidden = false
        postDetailHeight.constant = 45
        NotificationCenter.default.post(name: Notification.Name("commentButtonClicked"), object: nil)
        commentTextField.becomeFirstResponder()
        commentTextField.text = ""
        isCommentPost = true
    }
    func tapReplyOnCommentAction(sender:UIButton){
        replyOnCommentId = NewsFeedVM.sharedInstance.feedDetail.commentsArray[sender.tag - 1].commentId
        sendView.isHidden = false
        postDetailHeight.constant = 45
        
        NotificationCenter.default.post(name: Notification.Name("commentButtonClicked"), object: nil)
        commentTextField.becomeFirstResponder()
        commentTextField.text = ""
        isCommentPost = false
        
    }
    
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = NewsFeedVM.sharedInstance.feedDetail.userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    func tapLikeButtonAction(sender:UIButton){
        if NewsFeedVM.sharedInstance.feedDetail.isLike == 1{
            self.likeOnNewsFeedMethod(likeValue: 2)
        }else{
            self.likeOnNewsFeedMethod(likeValue: 1)
        }
    }
    func tapLikeOnCommentAction(sender:UIButton){
        if NewsFeedVM.sharedInstance.feedDetail.commentsArray[sender.tag - 1].isLikes == 1{
            self.likeOnCommentMethod(likeValue: 2, commentId: NewsFeedVM.sharedInstance.feedDetail.commentsArray[sender.tag - 1].commentId)
        }else{
            self.likeOnCommentMethod(likeValue: 1, commentId: NewsFeedVM.sharedInstance.feedDetail.commentsArray[sender.tag - 1].commentId)
        }
    }
    
}

extension PostDetailVC : VideoPlayerDelegates, VideoNewPlayersDelegates, PollSubmitDelegates{
    
    func getVideoDetail(videoUrl: String, name:String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    
    func getVideosDetail(videoUrl: String, name:String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    func pollSubmitMethod(pollId: Int) {
        pollsId = pollId
    }
}

//MARK: UITextField Delegate
extension PostDetailVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendView.isHidden = true
        postDetailHeight.constant = 0
        if commentTextField.text! == ""{
            commentTextField.resignFirstResponder()
        }else{
            commentTextField.resignFirstResponder()
            if commentTextField.isEmpty{
                //   self.showAlert(message: "Please enter the text first.")
            }
            sendView.isHidden = true
            postDetailHeight.constant = 0
            commentTextField.resignFirstResponder()
            if isCommentPost == true{
                self.addCommentOnNewFeed()
            }else{
                self.replyOnCommentMethod(commentId:replyOnCommentId)
                
            }
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.returnKeyType = .default
        textField.reloadInputViews()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.commentTextField.textColor = UIColor.darkGray
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("ok")
        return true
    }
    
}
//MARK: APIMethods
extension PostDetailVC {
    
    func getPostDetailMethod(){
        
        NewsFeedVM.sharedInstance.getEachNewfeed(userId: DataManager.user_id!, newfeedId: newsFeedId) { (success, message , error) in
            if(success == 1) {
                if NewsFeedVM.sharedInstance.feedDetail.userId == DataManager.user_id {
                    self.setMoreButton()
                }
                self.index = 1
                self.postDetailTableView.reloadData()
                let lastScrollOffset = self.postDetailTableView.contentOffset
                self.postDetailTableView.setContentOffset(lastScrollOffset, animated: false)
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func pollSubmitData(pollId: Int, eventId:Int){
        NewsFeedVM.sharedInstance.voteOnPoll(userId: DataManager.user_id!, eventId: eventId, pollId: pollId) { (success, message, error) in
            if(success == 1) {
                self.getPostDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func addCommentOnNewFeed(){
        //Encode Comment
        let encodedMsg :String = commentTextField.text!.trimmingCharacters(in: .whitespaces).toBase64()
        NewsFeedVM.sharedInstance.CommentNewFeed(userId: DataManager.user_id!, newfeedId: newsFeedId, comment: encodedMsg) { (success, message, error) in
            if(success == 1) {
                self.commentTextField.text = ""
                self.getPostDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func commentButtonClicked(notification : NSNotification){
        commentTextField.leftViewMode = UITextFieldViewMode.always
        commentTextField.rightViewMode = UITextFieldViewMode.always
        commentTextField.leftView = UIView(frame:CGRect(x:0,y:0,width: 8,height: commentTextField.frame.size.height))
        commentTextField.rightView = UIView(frame:CGRect(x:0,y:0,width: 36,height: commentTextField.frame.size.height))
        commentTextField.layer.cornerRadius = 4
        
        sendView.isHidden = false
        postDetailHeight.constant = 45
        commentTextField.becomeFirstResponder()
        commentTextField.text = ""
        
    }
    
    
    func likeOnNewsFeedMethod(likeValue:Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: newsFeedId, newfeedType: NewsFeedVM.sharedInstance.feedDetail.type.rawValue, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.getPostDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    
    func likeOnCommentMethod(likeValue:Int,commentId:Int){
        NewsFeedVM.sharedInstance.LikeOnComment(userId:  DataManager.user_id!, commentId: commentId, eventId: newsFeedId, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.getPostDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    
    func replyOnCommentMethod(commentId:Int){
        //Encode Reply
        let encodedMsg :String = commentTextField.text!.trimmingCharacters(in: .whitespaces).toBase64()
        NewsFeedVM.sharedInstance.AddReplyService(userId: DataManager.user_id!, commentId: commentId, eventId: newsFeedId, text: encodedMsg) { (success, message, error) in
            if(success == 1) {
                self.getPostDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func submitRSVPMethod(event_id:Int, value:Int, eventName:String){
        var value = value
        if value == 0{
            value = 2
        }
        NewsFeedVM.sharedInstance.SubmitRSVPRequest(userId: DataManager.user_id!, eventId: event_id, value: value) { (success, message , error) in
            if(success == 1) {
                NewsFeedVM.sharedInstance.newsFeedArray[self.feedIndex].rsvpStatus = 1
                self.getPostDetailMethod()
                self.createEventinTheCalendar(with: eventName, forDate: self.startEventDate, toDate: self.endEventDate)
                self.showAlert(message: "RSVP Done Successfully", title: kAlert, otherButtons: nil, cancelTitle: kOk)
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func createEventinTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: self.store)
                
                if eventStartDate.compare(eventEndDate) == .orderedSame{
                    event.isAllDay = true
                }
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                event.location = self.eventLocation
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    self.showAlert(message: "failed to save event with error : \(error)", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                    
                }
                
            } else {
                //we have error in getting access to device calnedar
                self.showAlert(message: error?.localizedDescription, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                
            }
        }
    }
    func deleteJobMethod(){
        
        AddJobPostVM.sharedInstance.DeleteNewfeed(userId: DataManager.user_id!, newfeedId: newsFeedId) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (success) in
                    NewsFeedVC.isPostDetail = true
                    self.navigationController?.popViewController(animated: false)
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}

