//
//  AddPostVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class AddPostVC: BaseVC{
    //MARK: IBOutlets
    @IBOutlet var tagSearchView: UIView!
    @IBOutlet weak var jobTitleTextfield: UITextField!
    @IBOutlet weak var pollUploadButton: UIButton!
    @IBOutlet weak var shareLinkButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var videoUploadButton: UIButton!
    @IBOutlet weak var imageUploadButton: UIButton!
    @IBOutlet weak var allowCommentSwitch: UISwitch!
    @IBOutlet weak var shareLinkTextField: UITextField!
    @IBOutlet weak var shareLinkHeight: NSLayoutConstraint!
    @IBOutlet weak var tagCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tagSearchTableview: UITableView!
    @IBOutlet weak var tagSearchTextfield: UITextField!
    @IBOutlet weak var imagesCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var locationTextfield: UITextField!
    @IBOutlet weak var shareLocationSwitch: UISwitch!
    @IBOutlet weak var publicTextField: UITextField!
    @IBOutlet weak var addMorePollHeight: NSLayoutConstraint!
    @IBOutlet weak var fieldViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pollTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addBodyTextfield: UITextView!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var pollTableView: UITableView!
    @IBOutlet weak var addMorePoll: UIButton!
    //MARK: Variables
    var filterTagListArray = [TagListData]()
    var imageDict = [String:Data]()
    var tagSearchArray = [TagListData]()
    var allTagSearchArray = [TagListData]()
    var publicArray = ["Public","\(DataManager.nameForGraduate!)", "Students","Friends","friends of friends"]
    var selectedTextField: UITextField!
    var shareLocation:String!
    var allowComments:String!
    var isTap = false
    var isGalleryTap = false
    var isVideoTap = false
    var isShareLink = false
    var isTapPoll = false
    var getPollArray = [String]()
    var tagsArray = [Int]()
    let imageArray = NSMutableArray()
    var selectedAddressLatitude = 0.0
    var selectedAddressLongitude = 0.0
    var selectedIndexPath = Int()
    var mediaType = String()
    var index: Int!
    var isEdit = false
    var newsFeedType:Int!
    var pollStringData = String()
    var groupId: Int!
    //MARK: CLass Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        custumUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setTitle(title: "ADD POST")
        setBackButton()
        self.setCollectionView()
    }
    
    
    override func viewWillLayoutSubviews() {
        self.tagCollectionViewHeight.constant = self.tagCollectionView.contentSize.height + 30
        
    }
    //MARK: Private Method
    private func custumUI(){
        jobTitleTextfield.setPlaceholder()
        locationTextfield.setPlaceholder()
        shareLinkTextField.setPlaceholder()
        publicTextField.setPlaceholder()
        tagSearchTextfield.setPlaceholder()

        addBodyTextfield.textColor = UIColor.darkGray
        
        
        tagSearchView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
       
        imagesCollectionViewHeight.constant = 0
        pollTableViewHeight.constant = 0
        shareLinkTextField.isHidden = true
        shareLinkHeight.constant = 0
        self.pickerDelegate = self
        mediaType = "3"
        newsFeedType = 1
        shareLocation = "1"
        allowComments = "1"
        tagSearchTableview.tableFooterView = UIView()
         tagSearchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    //MARK: Collection View Layout set
    func setCollectionView() {
        //Set Collectionview Flow Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 5.0
        layout.itemSize = CGSize(width:self.tagCollectionView.frame.width, height: 30.0)
        layout.scrollDirection = .vertical
        
        //Set Collectionview Datasource and Delegates
        tagCollectionView.backgroundColor = UIColor.clear
        tagCollectionView.bounces = false
        tagCollectionView.isPagingEnabled = true
        tagCollectionView.showsVerticalScrollIndicator = false
        tagCollectionView.showsHorizontalScrollIndicator = false
        tagCollectionView.collectionViewLayout = layout
        
        
    }
    //MARK: IBActions
    @IBAction func addTagButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        tagSearchTextfield.text = ""
        
        UIApplication.shared.keyWindow?.addSubview(tagSearchView)
        
    }
    
    @IBAction func galleryButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        //        isTap = true
        if isVideoTap == true{
            if isGalleryTap == false{
                videoUploadButton.setImage(#imageLiteral(resourceName: "Camera"), for: .normal)
                imageUploadButton.setImage(#imageLiteral(resourceName: "gallery_selected"), for: .normal)
                isVideoTap = false
                isGalleryTap = true
                imageArray.removeAllObjects()
                imageDict = [String:Data]()
                mediaType = "1"
                newsFeedType = 1
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 50
            }
            
        }else{
            if isGalleryTap == false{
                imageUploadButton.setImage(#imageLiteral(resourceName: "gallery_selected"), for: .normal)
                isGalleryTap = true
                mediaType = "1"
                newsFeedType = 1
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 50
                
            }else{
                imageUploadButton.setImage(#imageLiteral(resourceName: "gallery"), for: .normal)
                isGalleryTap = false
                mediaType = "3"
                newsFeedType = 1
                imageArray.removeAllObjects()
                imageDict = [String:Data]()
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 0
            }
            
        }
        
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        //        if isTapPoll == true{
        //            pollTableViewHeight.constant = pollTableView.contentSize.height
        //        }
        //        if isShareLink == true {
        //            shareLinkTextField.isHidden = false
        //            shareLinkHeight.constant = 62
        //        }
    }
    
    @IBAction func locationButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
        locationVC.delegate = self
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    @IBAction func shareLocationSwitchAction(_ sender: UISwitch) {
        if (sender.isOn == true){
            shareLocation = "1"
        }
        else{
            shareLocation = "2"
        }
    }
    @IBAction func allowCommentSwitchAction(_ sender: UISwitch) {
        if (sender.isOn == true){
            allowComments = "1"
        }
        else{
            allowComments = "2"
        }
        
    }
    @IBAction func cameraButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        // isTap = true
        if isGalleryTap == true{
            if isVideoTap == false{
                imageUploadButton.setImage(#imageLiteral(resourceName: "gallery"), for: .normal)
                videoUploadButton.setImage(#imageLiteral(resourceName: "camera_selected"), for: .normal)
                isVideoTap = true
                isGalleryTap = false
                mediaType = "2"
                newsFeedType = 1
                imageArray.removeAllObjects()
                imageDict = [String:Data]()
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 50
            }
            
        } else{
            if isVideoTap == false{
                videoUploadButton.setImage(#imageLiteral(resourceName: "camera_selected"), for: .normal)
                isVideoTap = true
                mediaType = "2"
                newsFeedType = 1
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 50
                
            }else{
                videoUploadButton.setImage(#imageLiteral(resourceName: "Camera"), for: .normal)
                isVideoTap = false
                mediaType = "3"
                newsFeedType = 1
                imageArray.removeAllObjects()
                imageDict = [String:Data]()
                imagesCollectionView.reloadData()
                imagesCollectionViewHeight.constant = 0
            }
            
        }
        
        
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        //        if isTapPoll == true{
        //            pollTableViewHeight.constant = pollTableView.contentSize.height
        //        }
        //        if isShareLink == true {
        //            shareLinkTextField.isHidden = false
        //            shareLinkHeight.constant = 62
        //        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if isShareLink == false{
            isShareLink = true
            shareLinkButton.setImage(#imageLiteral(resourceName: "share_icon_selected"), for: .normal)
            shareLinkTextField.isHidden = false
            shareLinkHeight.constant = 82
            shareLinkTextField.text = ""
            
        } else{
            isShareLink = false
            shareLinkButton.setImage(#imageLiteral(resourceName: "share_icon"), for: .normal)
            shareLinkTextField.isHidden = true
            shareLinkHeight.constant = 0
        }
        
        
        
        //        if isTap == true {
        //            imagesCollectionViewHeight.constant = 50
        //        }
        //        if isTapPoll == true{
        //            pollTableViewHeight.constant = pollTableView.contentSize.height
        //        }
        
    }
    
    @IBAction func addPollButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if isTapPoll == false{
            isTapPoll = true
            pollUploadButton.setImage(#imageLiteral(resourceName: "thumb_icon"), for: .normal)
            newsFeedType = 3
            getPollArray.append("")
            getPollArray.append("")
            pollTableView.reloadData()
            pollTableViewHeight.constant = pollTableView.contentSize.height
        } else{
            isTapPoll = false
            pollUploadButton.setImage(#imageLiteral(resourceName: "graph_unselectd"), for: .normal)
            newsFeedType = 1
            getPollArray.removeAll()
            pollTableView.reloadData()
            pollTableViewHeight.constant = 0
        }
        
        
        
        //        if isTap == true {
        //          imagesCollectionViewHeight.constant = 50
        //        }
        // newsFeedType = 3
        //        if isShareLink == true {
        //            shareLinkTextField.isHidden = false
        //            shareLinkHeight.constant = 62
        //        }
        //        if isTapPoll == false{
        //            getPollArray.append("")
        //            getPollArray.append("")
        //            pollTableView.reloadData()
        //            pollTableViewHeight.constant = pollTableView.contentSize.height
        //            isTapPoll = true
        //        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func addMorePollButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        if isEdit != true{
            
            if getPollArray.count > 0{
                
                if (getPollArray.contains("")) {
                    
                    self.showAlert(message: "Please Enter Details", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }else{
                    if getPollArray.count < 5{
                        getPollArray.append("")
                        pollTableView.reloadData()
                        pollTableViewHeight.constant = pollTableView.contentSize.height
                        if getPollArray.count == 5{
                            pollTableViewHeight.constant = pollTableView.contentSize.height - addMorePollHeight.constant
                            addMorePollHeight.constant = 0
                        }
                        self.view.layoutIfNeeded()
                    }
                }
            } else {
                self.showAlert(message: "Please Enter Details", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }
            
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func postButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        postButton.isUserInteractionEnabled = false

        Indicator.sharedInstance.showIndicator()
        if jobTitleTextfield.text == kEmptyString || ((jobTitleTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            Indicator.sharedInstance.hideIndicator()
            postButton.isUserInteractionEnabled = true
            self.showAlert(message: kPostTitleMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if locationTextfield.text == kEmptyString || ((locationTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            Indicator.sharedInstance.hideIndicator()
            postButton.isUserInteractionEnabled = true
            self.showAlert(message: kLocationMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }else if(shareLinkTextField.text != "" && !shareLinkTextField.isValidUrl){
            Indicator.sharedInstance.hideIndicator()
            postButton.isUserInteractionEnabled = true
            self.showAlert(message: "Enter Valid Url", title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        }else if getPollArray.count > 0{
            if getPollArray.contains(""){
                Indicator.sharedInstance.hideIndicator()
                postButton.isUserInteractionEnabled = true
                self.showAlert(message: "Please Enter Polls", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else{
                addPostMethod()
            }
        }
        else{
            addPostMethod()
            
        }
        
    }
    
    @IBAction func addSearchTagButtonAcion(_ sender: Any) {
        
        if tagSearchTextfield.text == kEmptyString || ((tagSearchTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kTagMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }else{
            addNewTagMethod()
            
        }
    }
    
    @IBAction func crossSearchTagButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        tagSearchView.removeFromSuperview()
    }
}
//MARK: UITableView Datasources and Delegates
extension AddPostVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == pollTableView){
            return getPollArray.count
        }else{
            return tagSearchArray.count
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == pollTableView){
            
            return 30.0
        }else{
            // If no section header title, no section header needed
            return 0.0
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == pollTableView){
            let label = UILabel()
            label.text = "Add poll"
            label.textColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
            label.frame = CGRect(x: 10, y: 4, width: 150, height: 20)
            label.font = UIFont.LegasiFont.regular.fontWithSize(size: 16.0)
            
            // Create header view and add label as a subview
            let view = UIView()
            view.frame = CGRect(x: 10, y: 4, width: 150, height: 25)
            view.backgroundColor = UIColor.clear
            view.addSubview(label)
            return view
        }else{
            return UIView()
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == pollTableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: kPollCell, for: indexPath)as! PollTableViewCell
            cell.selectionStyle = .none
            cell.loadData(viewcontroller: self, at: indexPath)
            cell.option1Textfield.delegate = self
            if getPollArray.count > indexPath.row {
                
                cell.option1Textfield.text = getPollArray[indexPath.row]
            }else{
                
                cell.option1Textfield.text = ""
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: kTagSearchCell, for: indexPath)as! tagSearchTableViewCell
            cell.selectionStyle = .none
            cell.tagSearchLabel.text = tagSearchArray[indexPath.row].name
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if((tagCollectionView) != nil){
            
            let id = tagSearchArray[indexPath.row].tagId
            
            if !filterTagListArray.contains(where: {$0.tagId == id}) {
                filterTagListArray.append(tagSearchArray[indexPath.row])
            }
            
            tagCollectionView.reloadData()
            tagCollectionViewHeight.constant = tagCollectionView.contentSize.height + 30
            tagSearchView.removeFromSuperview()
            
        }
        
    }
    
}
//MARK: UIColeectionView Datasources and Delegates
extension AddPostVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tagCollectionView {
            return filterTagListArray.count + 2
        }else {
            return imageArray.count + 1
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == tagCollectionView){
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kTagCell, for: indexPath)as!TagCollectionViewCell
            
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kTitleCell, for: indexPath)
                return cell
                
            }
            
            if indexPath.row == filterTagListArray.count+1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kAddCell, for: indexPath)
                return cell
            } else{
                cell.tagLabel.text = filterTagListArray[indexPath.row-1].name
                cell.setRadius(radius: 5.0)
            }
            
            return cell
            
        }else{
            
            if imageArray.count == indexPath.row && imageArray.count < 9{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kAddCell, for: indexPath)
                if mediaType == "1"{
                    (cell.viewWithTag(200) as! UILabel).text = "Add Images +"
                }else{
                    (cell.viewWithTag(200) as! UILabel).text = "Add Videos +"
                }
                return cell
            } else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kImagesCell, for: indexPath)as!ImagesCollectionViewCell
                if indexPath.row < imageArray.count {
                    cell.postImageView.image = imageArray[indexPath.row] as? UIImage
                    cell.crossButton.addTarget(self, action: #selector(self.tapButtonCrossAction(sender:)), for: UIControlEvents.touchUpInside)
                    cell.crossButton.tag = indexPath.row
                }
                
                
                return cell
                
            }
            
        }
    }
    func tapButtonCrossAction(sender:UIButton){
        
        self.showAlert(message: "Are you sure you want to delete this image?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
               
                self.imageArray.removeObject(at: sender.tag)
                self.imageDict["\(sender.tag)"] = nil
                self.imagesCollectionView.reloadData()
        })
        
      }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == tagCollectionView){
            if indexPath.row == 0 {
                return CGSize(width:40, height: 30)
            }
            if indexPath.row == filterTagListArray.count+1 {
                return CGSize(width: 80, height: 30)
            }
            let size: CGSize = self.filterTagListArray[indexPath.row - 1].name!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
            return  CGSize(width:size.width + 30.0, height: 25.0)
        }else {
            if imageArray.count == indexPath.row && imageArray.count < 9{
                let string = "Add Gallery +"
                let size: CGSize = string.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
                return  CGSize(width:size.width + 30.0, height: 30.0)
            }else{
                return  CGSize(width:50, height: 50.0)
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if(collectionView == tagCollectionView){
            if indexPath.row == filterTagListArray.count+1  {
                
                getTagListData()
                tagSearchTextfield.text = ""
                UIApplication.shared.keyWindow?.addSubview(tagSearchView)
            }
            
        }else{
            // if indexPath.row == imageArray.count  {
            
            selectedIndexPath = indexPath.row
            self.imagePickerDelegate = self
            if mediaType == "1"{
                self.showImagePicker()
            }else{
                self.showImagePicker(mediaType:.video)
            }
            
            //}
        }
    }
}
//MARK: Custom Image Picker Delegates
extension AddPostVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {//Get Image from Picker and use the image
        imageDict["\(selectedIndexPath)"] = image.jpegData(.medium)
        if imageArray.count > selectedIndexPath {
            imageArray[selectedIndexPath] = image
        }
        else {
            imageArray.add(image)
        }
        imagesCollectionView.reloadData()
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
        if imageArray.count > selectedIndexPath {
            imageArray[selectedIndexPath] = thumbnail
        }
        else {
            imageArray.add(thumbnail)
        }
        imagesCollectionView.reloadData()
        do {
            let data =  try Data(contentsOf: videoUrl, options: .mappedIfSafe)
            imageDict["\(selectedIndexPath)"] = data
        }catch {
            print(error)
            return
        }
    }
}
//MARK: APIMethods
extension AddPostVC {
    func getTagListData(){
        NewsFeedVM.sharedInstance.getTagList(userId: DataManager.user_id!) { (success, message, error) in
            if(success == 1) {
                self.tagSearchArray = NewsFeedVM.sharedInstance.tagListArray
                self.allTagSearchArray = NewsFeedVM.sharedInstance.tagListArray
                self.tagSearchTableview.reloadData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func addNewTagMethod(){
        
        AddJobPostVM.sharedInstance.AddNewTag(userId: DataManager.user_id!, tagTitle: tagSearchTextfield.text!) { (success, message, error) in
            if(success == 1) {
                let dict = AddJobPostVM.sharedInstance.infoDict
                let data = TagListData(name: dict?[APIKeys.kTagTitle]as?String, tagId: dict?[APIKeys.kTagId]as?Int)
                self.filterTagListArray.append(data)
                self.tagCollectionView.reloadData()
                self.tagCollectionViewHeight.constant = self.tagCollectionView.contentSize.height + 30
                self.tagSearchView.removeFromSuperview()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    func addPostMethod(){
        postButton.isUserInteractionEnabled = false
        Indicator.sharedInstance.showIndicator()
        var privacyStr: String!
        if publicTextField.text == "Public"{
            privacyStr = "1"
        }else if publicTextField.text == "\(DataManager.nameForGraduate!)"{
            privacyStr = "2"
        }else if publicTextField.text == "Students"{
            privacyStr = "3"
        }else if publicTextField.text == "Friends"{
            privacyStr = "4"
        }else{
            privacyStr = "5"
        }
        tagsArray.removeAll()
        for tags in filterTagListArray {
            tagsArray.append(tags.tagId!)
        }
        var tagString = ""
        
        if tagsArray.count > 0 {
            for id in tagsArray{
                if tagString == "" {
                    tagString = "\(id)"
                } else {
                    tagString = "\(tagString),\(id)"
                }
            }
        }
        
        var array = [Data]()
        for (_,value) in imageDict {
            array.append(value)
        }
        
        var thumbArray = [Data]()
        
        
        var mimeType: MimeType = .image
        if mediaType == "2" {
            mimeType = .video
            for image in imageArray {
                thumbArray.append((image as! UIImage).jpegData(.medium)!)
                //thumbArray.append((image as AnyObject).jpegData(.medium))
            }
            if imageArray.count == 0{
                mediaType = "3"
            }
        }
        if getPollArray.count > 0{
            pollStringData = getPollArray.joined(separator: ",")
            newsFeedType = 3
        }
        var postBody = ""
        if addBodyTextfield.text! != "Add Body" {
            postBody = addBodyTextfield.text!
        }
        let encodedMsg :String = jobTitleTextfield.text!.trimmingCharacters(in: .whitespaces).toBase64()
        
        
        AddJobPostVM.sharedInstance.addNewJobPost(userId: DataManager.user_id!, newsFeedType: newsFeedType, jobName: "", companyName: "", location: locationTextfield.text!, description: "", skills: "", jobType: 0, lat: selectedAddressLatitude, long: selectedAddressLongitude, title: encodedMsg, body: postBody, privacy: privacyStr , allowComment: allowComments, shareLocation: shareLocation, tags: tagString, mediaType: mediaType, pollOption: pollStringData, shareLink: shareLinkTextField.text!, mediaName: "media", vedioArray: thumbArray, thumbNailName: "mediaThumbnail", mediaArray: array, mimeType: mimeType, groupId: groupId) { (success, message, error) in
            if(success == 1) {
                Indicator.sharedInstance.hideIndicator()
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.postButton.isUserInteractionEnabled = true
                    NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else {
                Indicator.sharedInstance.hideIndicator()
                self.postButton.isUserInteractionEnabled = true
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    
}
//MARK: UITextField & UITextView Delegates
extension AddPostVC : UITextFieldDelegate, UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == publicTextField) {
            publicTextField.text = publicArray[0]
            setPicker(textField: textField, array: publicArray)
        }
        if textField == shareLinkTextField{
            if shareLinkTextField.text == kEmptyString{
                shareLinkTextField.text  = "http://"
            }
        }
        selectedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == jobTitleTextfield || textField == locationTextfield || textField == tagSearchTextfield || textField == shareLinkTextField{
            
        }else{
            let indexPath = IndexPath(row: textField.tag, section: 0)
            let cell = pollTableView.cellForRow(at: indexPath) as? PollTableViewCell
            if textField == cell?.option1Textfield {
                getPollArray[textField.tag] = textField.text!
            }
            
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == jobTitleTextfield {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 50 // Bool
        }
        if textField == shareLinkTextField{
            return true
        } else{
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (tagSearchTextfield.text?.isEmpty)!{
            tagSearchArray = allTagSearchArray
            
        } else{
            tagSearchArray = tagSearchArray.filter({($0.name?.lowercased().contains(tagSearchTextfield.text!.lowercased()))!})
            
        }
        tagSearchTableview.reloadData()
        
    }

    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Add Body"
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Add Body"){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        var frame = textView.frame
        frame.size.height = textView.contentSize.height
        textView.frame = frame
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            return false
        }
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.characters.count // for Swift use count(newText)
//        return numberOfChars < 150;
           return true
    }
    
    
}



//MARK: Custom Picker Delegates
extension AddPostVC: LegasiPickerDelegate {
    
    func didSelectPickerViewAtIndex(index: Int) {
        if(selectedTextField == publicTextField) {
            publicTextField.text = publicArray[index]
        }
    }
}



extension AddPostVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        if !address.isEmpty {
            locationTextfield.text = address
            selectedAddressLatitude = latitude
            selectedAddressLongitude = longitude
            
        }
    }
}


