//
//  LinkWebViewVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 11/01/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LinkWebViewVC: BaseVC{

    
    
    @IBOutlet var linkWebView: UIWebView!
    var linkUrl:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIWebView.loadRequest(NSURLRequest(URL: NSURL(string: "google.ca")))
       
        setBackButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         linkWebView.loadRequest(URLRequest(url: URL(string: linkUrl)!))
    }
    
    

}
