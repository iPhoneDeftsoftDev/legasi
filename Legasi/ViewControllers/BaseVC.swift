//
//  BaseVC.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import Photos
import AVFoundation

enum MediaPickerType: String {
    
    case image = "public.image"
    case video = "public.movie"
    case both = "public.image,public.movie"
}
@objc protocol  LegasiPickerDelegate {
    @objc optional func didSelectPickerViewAtIndex(index: Int)
    @objc optional func didSelectDatePicker(date: Date)
    @objc optional func didClickOnDoneButton()
}
 protocol CustomImagePickerDelegate {
   func didPickImage(_ image: UIImage)
   func didPickVideo(_ thumbnail: UIImage, videoUrl: URL)
}

var shouldShowLogo = true

class BaseVC: UIViewController {
    
    //MARK: Variables
    var pickerView = UIPickerView()
    var pickerArray = [String]()
    var pickerDelegate: LegasiPickerDelegate?
    var datePickerView = UIDatePicker()
    var drawerContainer: MMDrawerController?
    var toolbar:UIToolbar!
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var imagePickerDelegate: CustomImagePickerDelegate?
    static var both = [MediaPickerType]()
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.custumLeftTwoButtons), name: NSNotification.Name(rawValue: "schoolLogoUpdated"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        debugPrint(" *** MEMORY WARNING *** ")
    }
    
    //MARK: Load Left Side Menu
    func loadLeftSideMenu() {
         let appsDelegate = UIApplication.shared.delegate as! AppDelegate
        appsDelegate.loadLeftSideMenu()
    }

    //MARK: ***** NAVIGATION BAR METHODS *****
    
    //MARK: Set Navigation Title
    func setTitle(title:String) {
        self.navigationController?.isNavigationBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.LegasiColor.blue.colorWith(alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let titleButton = UIButton(frame: CGRect(x: 50, y:0, width:110, height:30))
        titleButton.titleLabel?.textAlignment = .center
        titleButton.setTitle(title, for: .normal)
        titleButton.setTitleColor(.white, for: .normal)
        titleButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.navigationItem.titleView = titleButton
    }
    
    //MARK: Hide Navigation Bar
    func hideNavigationBar(){
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: Set Back Button
    func setBackButton(){
        let backView = UIView()
        backView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let backImageView = UIImageView()
        backImageView.image =  #imageLiteral(resourceName: "backWhite")
        if #available(iOS 11, *) {
            backImageView.frame = CGRect(x: 0, y: 12, width: 16, height: 16)
        }else{
            backImageView.frame = CGRect(x: 8, y: 12, width: 16, height: 16)
        }
        backImageView.contentMode = .scaleAspectFit
        let backButton = UIButton() //Custom back Button
        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//        backButton.backgroundColor = UIColor.gray
//        let backButton = UIButton() //Custom back Button
//        backButton.frame = CGRect(x: 0, y: 0, width: 42, height: 36)
//        backButton.backgroundColor = UIColor.red
//        backButton.setImage(#imageLiteral(resourceName: "backWhite"), for: UIControlState.normal)
        
        backButton.addTarget(self, action: #selector(self.backButtonAction), for: UIControlEvents.touchUpInside)
//        if #available(iOS 11, *) {
//        doneView.translatesAutoresizingMaskIntoConstraints = false
//        }
        backView.addSubview(backImageView)
        backView.addSubview(backButton)
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = backView
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -15;
        
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, leftBarButton], animated: false)
    }
    
    
    func setDoneButton(){
        let doneView = UIView()
        doneView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let doneImageView = UIImageView()
        doneImageView.image =  #imageLiteral(resourceName: "tick_white")
        doneImageView.frame = CGRect(x: 24, y: 12, width: 16, height: 16)
        let doneButton = UIButton() //Custom back Button
        doneButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)

        doneButton.addTarget(self, action: #selector(self.doneButtonAction), for: UIControlEvents.touchUpInside)

        doneView.addSubview(doneImageView)
        doneView.addSubview(doneButton)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = doneView
        self.navigationItem.rightBarButtonItem = rightBarButton
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0;
        self.navigationItem.setRightBarButtonItems([negativeSpacer, rightBarButton], animated: false)
    }
    
    func backButtonAction() {
        self.view.endEditing(true)
        let backDone = self.navigationController?.popViewController(animated: true)
        if backDone == nil {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func btnAddChatClicked(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kChatUserVC)
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    
    //MARK: Set More Button
    func setMoreButton(){
        let moreView = UIView()
        moreView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let moreImageView = UIImageView()
        moreImageView.image =  #imageLiteral(resourceName: "moreIcon")
        moreImageView.frame = CGRect(x: 16, y: 8, width: 24, height: 24)
        moreImageView.contentMode = .scaleAspectFit
        let moreButton = UIButton() //Custom back Button
        moreButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//        moreButton.setImage(#imageLiteral(resourceName: "moreIcon"), for: UIControlState.normal)
        moreButton.addTarget(self, action: #selector(self.moreButtonAction), for: UIControlEvents.touchUpInside)
        moreView.addSubview(moreImageView)
        moreView.addSubview(moreButton)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = moreView
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0;
        
        self.navigationItem.setRightBarButtonItems([rightBarButton, negativeSpacer], animated: false)
    }
    
    func moreButtonAction() {
        
    }
    
    func btnChatRightMenuClicked(sender:UIButton){
        
    }
   
    
    func doneButtonAction() {
        
    }
    
    
    func custumRightTwoButtons(){
        
        let addButtonView = UIView()
//        addButtonView.backgroundColor = UIColor.brown
        addButtonView.frame = CGRect(x: 0, y: 0, width: 19, height: 30)
        let addButtonImageView = UIImageView()
        addButtonImageView.image = #imageLiteral(resourceName: "moreIcon")
        addButtonImageView.contentMode = .scaleAspectFit
        addButtonImageView.frame = CGRect(x: 0, y: 3, width: 24, height: 24)
        let addButton = UIButton()
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
//        let addButton = UIButton() //Custom Button
//        addButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
//        addButton.setImage(#imageLiteral(resourceName: "moreIcon"), for: UIControlState.normal)
        addButton .addTarget(self, action: #selector(self.btnChatRightMenuClicked(sender:)), for: UIControlEvents.touchUpInside)
//        if #available(iOS 11, *) {
//            addButton.translatesAutoresizingMaskIntoConstraints = false
//        }
        addButtonView.addSubview(addButtonImageView)
        addButtonView.addSubview(addButton)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = addButtonView
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let doneView = UIView()
//        doneView.backgroundColor = UIColor.gray
        doneView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let doneImageView = UIImageView()
        doneImageView.image = #imageLiteral(resourceName: "addChat")
        doneImageView.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
        let addButton1 = UIButton() //Custom back Button
        addButton1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        
//        let addButton1 = UIButton() //Custom Button
//        addButton1.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
//        addButton1 .setImage(UIImage(named: "addChat"), for: UIControlState.normal)
        addButton1 .addTarget(self, action: #selector(self.btnAddChatClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0//-5;
        
        doneView.addSubview(doneImageView)
        doneView.addSubview(addButton1)
        let rightBarButton2 = UIBarButtonItem()
        rightBarButton2.customView = doneView
        self.navigationItem.rightBarButtonItem = rightBarButton2
        self.navigationItem.setRightBarButtonItems([negativeSpacer, rightBarButton, rightBarButton2], animated: false)
        
    }
    
    func custumLeftTwoButtons(){
        if !shouldShowLogo {
            return
        }
        
        let menuView = UIView()
        menuView.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        let menuImageView = UIImageView()
        menuImageView.image = #imageLiteral(resourceName: "slideButtonIcon")
        menuImageView.frame = CGRect(x: 2, y: 13, width: 20, height: 14)
        
         let menuButton = UIButton() //Custom Button
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
//        addButton .setImage(#imageLiteral(resourceName: "slideButtonIcon"), for: UIControlState.normal)
        menuButton .addTarget(self, action: #selector(self.slideBtnAction), for: UIControlEvents.touchUpInside)
//        if #available(iOS 11, *) {
//            addButton.translatesAutoresizingMaskIntoConstraints = false
//        }
        let leftBarButton = UIBarButtonItem()
        menuView.addSubview(menuImageView)
        menuView.addSubview(menuButton)
        leftBarButton.customView = menuView
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let addButton1 = UIImageView() //Custom Button
        addButton1.frame = CGRect(x: 0, y: 0, width: 130, height: 28)
        addButton1.widthAnchor.constraint(equalToConstant: 130).isActive = true
        addButton1.heightAnchor.constraint(equalToConstant: 28).isActive = true
        addButton1.contentMode = .scaleAspectFit
        //addButton1 .setBackgroundImage(DataManager.school_logo, for: UIControlState.normal)
        addButton1.backgroundColor = UIColor.clear
        addButton1.sd_setImage(with: URL(string:"\(DataManager.school_logo!)"), placeholderImage: nil)
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0//-20;
        
        let leftBarButton2 = UIBarButtonItem()
        leftBarButton2.customView = addButton1
        self.navigationItem.leftBarButtonItem = leftBarButton2
        
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, leftBarButton, leftBarButton2], animated: false)
        
    }
    
   
    
    
}

extension BaseVC: CustomPickerViewDelegate {
    //MARK: - showImagePicker
    func showImagePicker(mediaType: MediaPickerType = .image) {
        self.view.endEditing(true)
        CustomImagePickerView.sharedInstace.delegate = self
        let alert  = UIAlertController(title: kSelectImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: kGallery, style: .default, handler: {action in
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        
                        if mediaType == .both {
                           CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = mediaType.rawValue.components(separatedBy: ",")
                        }else{
                           CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = [mediaType.rawValue]
                        }
                        CustomImagePickerView.sharedInstace.pickImageUsing(target: self, mode: .gallery)
                    } else {
                        self.showAlert(message: kCameraMessage, title: nil, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                            
                            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                        print("Settings opened: \(success)")
                                    })
                                } else {
                                    // Fallback on earlier versions
                                }
                            }
                        })
                        
                    }
                })
            }else {
                
                if mediaType == .both {
                    CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = mediaType.rawValue.components(separatedBy: ",")
                }else{
                    CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = [mediaType.rawValue]
                }
                CustomImagePickerView.sharedInstace.pickImageUsing(target: self, mode: .gallery)
            }
        }))
        alert.addAction(UIAlertAction(title: kCamera, style: .default, handler: {action in
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { response in
                if response {
                    
                    if mediaType == .both {
                        CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = mediaType.rawValue.components(separatedBy: ",")
                    }else{
                        CustomImagePickerView.sharedInstace.imagePicker.mediaTypes = [mediaType.rawValue]
                    }
                    
                    CustomImagePickerView.sharedInstace.pickImageUsing(target: self, mode: .camera)
                } else {
                    self.showAlert(message: kCameraMessage, title: nil, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                        
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)")
                                })
                            } else {
                                UIApplication.shared.openURL(settingsUrl)
                            }
                        }
                    })
                    
                }
            }
            
            
        }))
        alert.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func didImagePickerFinishPicking(_ image: UIImage) {
        imagePickerDelegate?.didPickImage(image)
    }
    
    func didVideoPickerFinishPicking(_ thumbnail: UIImage, videoUrl: URL) {
        imagePickerDelegate?.didPickVideo(thumbnail, videoUrl: videoUrl)
    }
    func didCancelImagePicking() {
        
    }
}



//MARK: Alert Methods
extension BaseVC {
    
    func showAlert(message: String?, title:String? = kAlert, otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = kOk, cancelAction: ((UIAlertAction)-> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorMessage(error: NSError?) {
        if error != nil {
            let alert = UIAlertController(title: error!.domain, message: error!.userInfo[kMessageAlert] as? String ?? kUnableRequestMsg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: kOk, style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: kAlert, message: kUnableRequestMsg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: kOk, style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
}

//MARK: Set Pickers
extension BaseVC: UIPickerViewDelegate {
    
    //MARK: Custom Picker
    func setPicker(textField: UITextField, array: [String], defaultIndex: Int = 0) {
        pickerView.delegate = self
        pickerArray = array
        
        //Set Toolbar for Picker View
        toolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 44.0))
        let item = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done,
                                   target: self, action: #selector(BaseVC.doneAction))
        //Set Picker View UI
        item.tintColor = UIColor.white
        toolbar.setItems([item], animated: true)
        pickerView.backgroundColor = UIColor.lightGray
        toolbar.barTintColor = UIColor.LegasiColor.blue.colorWith(alpha: 1.0)
        textField.inputAccessoryView = toolbar
        
        //Set Picker View to Textfield
        textField.inputView = pickerView
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(defaultIndex, inComponent: 0, animated: false)
    }
    func doneAction() {
        pickerDelegate?.didClickOnDoneButton?()
        view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerDelegate?.didSelectPickerViewAtIndex?(index: row) //Call Protocol Delegate
    }
    
    //MARK: Custom Date Picker
    func setDatePicker(textField: UITextField) {
        
        datePickerView.datePickerMode = .date
        datePickerView.timeZone = TimeZone.current
        textField.inputView = datePickerView
        datePickerView.backgroundColor = UIColor.lightGray
        datePickerView.addTarget(self, action: #selector(self.didPickerViewValueChanged(sender:)), for: .valueChanged)
    }
    
    func didPickerViewValueChanged(sender: UIDatePicker) {
        pickerDelegate?.didSelectDatePicker?(date: sender.date)
    }
}

//MARK: LeftSideBar
extension BaseVC {
    
    //MARK: Custom Slide Button
    func customSlideButton(){
        let menuView = UIView()
        menuView.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        let menuImageView = UIImageView()
        menuImageView.image = #imageLiteral(resourceName: "slideButtonIcon")
        menuImageView.frame = CGRect(x: 2, y: 13, width: 20, height: 14)
        
        let slideButton = UIButton() //Custom Button
        slideButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        slideButton .addTarget(self, action: #selector(self.slideBtnAction), for: UIControlEvents.touchUpInside)
//        if #available(iOS 11, *) {
//            slideButton.translatesAutoresizingMaskIntoConstraints = false
//        }
        let leftBarButton = UIBarButtonItem()
        menuView.addSubview(menuImageView)
        menuView.addSubview(slideButton)
        leftBarButton.customView = menuView
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0//-20;
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, leftBarButton], animated: false)
    }
    
    //MARK: Slide Button Action
    func slideBtnAction(){
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
        
    }
    
    //MARK : LINKEDIN DATA
    func getLinkedInData(finish: @escaping APIServiceSuccessCallback){
        
        if let accessToken = DataManager.linkedInAccessToken {
            // Specify the URL string that we'll get the profile info from.
            
           var request = URLRequest(url: URL(string: TARGET_URL_STRING)!)
            // Indicate that this is a GET request.
            request.httpMethod = "GET"
            
            // Add the access token as an HTTP header field.
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: kAuthorization)
            
            
            // Initialize a NSURLSession object.
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            // Make the request.
            let task: URLSessionDataTask = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                // Get the HTTP status code of the request.
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        print(dataDictionary)
                        
                        DataManager.linkedin_id = dataDictionary[kId] as? String
                        
                       DispatchQueue.main.async(execute: { () -> Void in
                        
                       finish(true)

                            //                            self.btnOpenProfile.setTitle(profileURLString, for: UIControlState())
                            //                            self.btnOpenProfile.isHidden = false
                            
                        })
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }else {
                    print("Error with status code",statusCode)
                }
            })
            
            task.resume()
        }
    }

}
