//
//  LeftSideViewController.swift
//  DemoTabBarWithSlideMenu
//
//  Created by 123 on 01/11/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import UIKit

class LeftSideBarVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userTypeLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var sliderTableView: UITableView!
    @IBOutlet weak var centerView: UIView!
    
    @IBOutlet weak var likeImageView: UIImageView!
    var imageArray = [#imageLiteral(resourceName: "jobsIcon"), #imageLiteral(resourceName: "myProfileIcon"), #imageLiteral(resourceName: "myPostIcon"), #imageLiteral(resourceName: "settingsIcon"), #imageLiteral(resourceName: "helpIcon"), #imageLiteral(resourceName: "logoutIcon")]
    var titleArray = ["Jobs", "My Profile", "My Posts", "Settings", "Help", "Logout"]
    
    var adminImageArray = [#imageLiteral(resourceName: "jobsIcon"), #imageLiteral(resourceName: "myProfileIcon"), #imageLiteral(resourceName: "myPostIcon"), #imageLiteral(resourceName: "settingsIcon"), #imageLiteral(resourceName: "logoutIcon")]
    var adminTitleArray = ["Jobs", "My Profile", "My Posts", "Settings", "Logout"]
    
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        self.navigationController?.isNavigationBarHidden = true
        sliderTableView.tableFooterView = UIView()
        
         profileImageView.setRadius(radius: 5.0)
         likeImageView.setRadius(radius: likeImageView.frame.width/2)
        
        
        self.profileImageView.sd_setImage(with: URL(string:"\(DataManager.user_image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        self.likeImageView.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), placeholderImage: #imageLiteral(resourceName: "likeBlue"))
        self.yearLabel.text = DataManager.graduationYear
        self.nameLabel.text = DataManager.user_name
       
        if DataManager.role == 1 {
            // graduate or student
            if DataManager.userType == "Student"{
                self.userTypeLabel.text = DataManager.userType
            }else{
                self.userTypeLabel.text = DataManager.nameForGraduate!
            }
        }else {     //in case 2 or 3
            //admin
            self.userTypeLabel.text = "School admin"
        }
        
        self.likeCountLabel.text = "\(DataManager.totalLikes!)"
        centerView.setRadius(radius: 5.0, borderColor: UIColor.LegasiColor.darkBlue.colorWith(alpha: 1.0), borderWidth: 2.0)
    }
}


//MARK: UITableView DataSource
extension LeftSideBarVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DataManager.role != 1{
            return adminImageArray.count
        }else{
            return imageArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kSliderCell)
        if DataManager.role != 1{
            (cell!.viewWithTag(1) as! UIImageView).image = adminImageArray[indexPath.row]
            (cell!.viewWithTag(2) as! UILabel).text = adminTitleArray[indexPath.row]
        }else{
            (cell!.viewWithTag(1) as! UIImageView).image = imageArray[indexPath.row]
            (cell!.viewWithTag(2) as! UILabel).text = titleArray[indexPath.row]
        }
        
        return cell!
    }
    
}

extension LeftSideBarVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if DataManager.role != 1 {
            switch indexPath.row {
            case 0: //Jobs VC
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kJobsVC) as! JobsVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
            break //kMyProfileVC
            case 1:
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let profilevc = storyboard.instantiateViewController(withIdentifier: kMyProfileVC) as! MyProfileVC
                let navigationController  = UINavigationController(rootViewController: profilevc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
            case 2: //My Posts
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kMyPostsVC) as! MyPostsVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
            case 3: //Settings
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kSettingVC) as! SettingVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
            case 4: //Logout
                self.showAlert(message: "Are you sure you want to logout?", title: "LOGOUT", otherButtons: ["No":{ (action) in
                    
                    }], cancelTitle: "Yes", cancelAction: { (action) in
                        self.logoutMethod()
                })
                break
            default:
                break
                
                
            }
        } else{
            switch indexPath.row {
            case 0: //Jobs VC
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kJobsVC) as! JobsVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
            break //kMyProfileVC
            case 1: //MyProfileVC
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let profilevc = storyboard.instantiateViewController(withIdentifier: kMyProfileVC) as! MyProfileVC
                let navigationController  = UINavigationController(rootViewController: profilevc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
                
            case 2: //My Posts
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kMyPostsVC) as! MyPostsVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
            case 3: //Settings
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let jobvc = storyboard.instantiateViewController(withIdentifier: kSettingVC) as! SettingVC
                let navigationController  = UINavigationController(rootViewController: jobvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break  //HelpVC
                
            case 4: //Help
                let storyboard = UIStoryboard(storyboard: .SlideMenu)
                let helpvc = storyboard.instantiateViewController(withIdentifier: kHelpVC) as! HelpVC
                let navigationController  = UINavigationController(rootViewController: helpvc)
                appDelegate.drawerContainer?.setCenterView(navigationController, withCloseAnimation: true, completion: nil)
                break
                
                
            case 5: //Logout
                self.showAlert(message: "Are you sure you want to logout?", title: "LOGOUT", otherButtons: ["No":{ (action) in
                    
                    }], cancelTitle: "Yes", cancelAction: { (action) in
                       self.logoutMethod()
                })
                break
            default:
                break
                
            }
        }
    }
    
    
    func logoutMethod(){
        
        UserLoginVM.sharedInstance.Logout(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                DataManager.isUserLoggedIn = false
                DataManager.user_image = nil
                DataManager.school_logo = nil
                DataManager.schoolId = nil
                DataManager.likeIcon_url = nil
                DataManager.unlikeIcon_url = nil
                DataManager.user_name = nil
                DataManager.role = nil
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.stopLocationService()
                let storyboard = UIStoryboard(storyboard: .Main)
                let vc = storyboard.instantiateViewController(withIdentifier: kLoginVc) as! LoginVC
                let navigationController = UINavigationController(rootViewController: vc)
                UIApplication.shared.keyWindow?.rootViewController = navigationController
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
}
