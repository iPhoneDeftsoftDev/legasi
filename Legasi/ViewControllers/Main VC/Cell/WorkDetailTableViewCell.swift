//
//  WorkDetailTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class WorkDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var currentJobButton: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var jobTitleTextfield: UITextField!
    
    @IBOutlet weak var jobLocationButton: UIButton!
    @IBOutlet weak var jobDescTextView: UITextView!
    @IBOutlet weak var companyNameTextField: UITextField!
    
    @IBOutlet weak var jobLocationTextfield: UITextField!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var industryTextField: UITextField!
    var vc: BaseVC!
    
    var selectedTextField = UITextField()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //MARK: Load data for Cell
    func loadData(viewcontroller: BaseVC, at indexPath: IndexPath) {
        
        vc = viewcontroller
        
        //Set TextField Placeholder
        
        jobTitleTextfield.setPlaceholder(placholder: kJobTitlePlaceHolder, color: .darkGray, size: 16.0)
        companyNameTextField.setPlaceholder(placholder: kCompanyNamePlaceHolder, color: .darkGray, size: 16.0)
        industryTextField.setPlaceholder(placholder: kIndustryTypePlaceHolder, color: .darkGray, size: 16.0)
        jobLocationTextfield.setPlaceholder(placholder: kJobLocationPlaceHolder, color: .darkGray, size: 16.0)
        
        jobTitleTextfield.tag = indexPath.row
        companyNameTextField.tag = indexPath.row
        industryTextField.tag = indexPath.row
        jobLocationTextfield.tag = indexPath.row
        jobDescTextView.tag = indexPath.row
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
