//
//  EducationTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class EducationTableViewCell: UITableViewCell {

    @IBOutlet weak var graduationYearTextfield: UITextField!
    
    @IBOutlet weak var collegeNameTextField: UITextField!
   
    @IBOutlet weak var degreeTypeTextfield: UITextField!
    
    @IBOutlet weak var majorDetailTextField: UITextField!
    var vc: BaseVC!
    
    var selectedTextField = UITextField()
    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: Load data for Cell
    func loadData(viewcontroller: BaseVC, at indexPath: IndexPath) {
        
        vc = viewcontroller
        
    //Set TextField Placeholder
        
        graduationYearTextfield.setPlaceholder(placholder: kGraducationYearPlaceHoler, color: .darkGray, size: 16.0)
        collegeNameTextField.setPlaceholder(placholder: kCollegeNamePlaceHolder, color: .darkGray, size: 14.0)
        degreeTypeTextfield.setPlaceholder(placholder: kDegreeTypePlaceHolder, color: .darkGray, size: 16.0)
        majorDetailTextField.setPlaceholder(placholder: kMajorDetailPlaceHolder, color: .darkGray, size: 16.0)
        
        degreeTypeTextfield.minimumFontSize = 10
        graduationYearTextfield.tag = indexPath.row
        collegeNameTextField.tag = indexPath.row
        degreeTypeTextfield.tag = indexPath.row
        majorDetailTextField.tag = indexPath.row
      }


}
