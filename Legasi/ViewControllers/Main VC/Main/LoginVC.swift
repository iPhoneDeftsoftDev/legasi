//
//  LoginVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit
import CoreLocation
class LoginVC: BaseVC,CLLocationManagerDelegate{

    //MARK: IBOutlets
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var coordinates = CLLocationCoordinate2D()
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customiseUI()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        loginButton.layer.cornerRadius = self.loginButton.frame.height/2
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: kLoginViewTitle)
        self.userNameTextfield.setPlaceholder()
        self.passwordTextfield.setPlaceholder()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()

        
        LocationManager.sharedManager.askPermissionsAndFetchLocationWithCompletion(isPlacemarkRequired: true, shouldInvokePermissionFailureCallback: true) { (location, placemark, errorReason) in
            // Creates a marker in the center of the map.
             self.coordinates = location?.coordinate ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            self.latitude = self.coordinates.latitude
            self.longitude = self.coordinates.longitude
           
        }
    }
    
    //MARK: IBActions
    @IBAction func forgotButtonAction(_ sender: Any) {
        let forgotvc = self.storyboard?.instantiateViewController(withIdentifier: kForgotPasswordVc) as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotvc, animated: false)
    }

    @IBAction func loginButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        if !userNameTextfield.isEmpty {
            if !passwordTextfield.isEmpty {
            
                self.loginMethod()
               
            }
            else {
                self.showAlert(message: kPasswordMessage)
          }
       }
       else {
           self.showAlert(message: kUserNameOrEmailMessage)
        }
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
       let signupvc = self.storyboard?.instantiateViewController(withIdentifier: kSignUpVc) as! SignUpVC
       self.navigationController?.pushViewController(signupvc, animated: false)
    }
    
}
//MARK: LoginVC Extension For Webservices Method
extension LoginVC {

    func loginMethod(){
        var deviceToken = DataManager.deviceToken
        if(deviceToken == kEmptyString || deviceToken == nil){
            deviceToken = kEmptyString
        }else {
            deviceToken = DataManager.deviceToken
        }
        UserLoginVM.sharedInstance.loginByEmail(email: userNameTextfield.text!, password: passwordTextfield.text!, lat: latitude, long: longitude, device_type: kIphone, device_token: deviceToken!) { (success, message, error) in
            if(success == 1)
            {
                    self.userNameTextfield.text = kEmptyString
                    self.passwordTextfield.text = kEmptyString
                    DataManager.isUserLoggedIn = true
                    self.loadLeftSideMenu()
               
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}
//MARK: UITextField delegate
extension LoginVC :  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
