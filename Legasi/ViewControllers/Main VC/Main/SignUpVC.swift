//
//  SignUpVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class SignUpVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var countryCodeTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var tickButton: UIButton!
 //MARK: Variables
    var imageDict = [String: Data]()
    var isTerms = true
//MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tickButton.setImage(#imageLiteral(resourceName: "terms_unselected"), for: .normal)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
//MARK: Private Methods
    private func customiseUI() {
        setTitle(title: kSignUpViewTitle)
        setBackButton()
      //SetPlaceHolders
        nameTextfield.setPlaceholder()
        userNameTextfield.setPlaceholder()
        countryCodeTextfield.setPlaceholder()
        phoneTextfield.setPlaceholder()
        emailTextfield.setPlaceholder()
        passwordTextfield.setPlaceholder()
        confirmPasswordTextfield.setPlaceholder()
        tickButton.layer.cornerRadius = 10
        self.view.endEditing(true)
    }
 //MARK: Storyboard Actions
    @IBAction func userImageButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePickerDelegate = self
        self.showImagePicker()
    }
    
    @IBAction func termsButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kTermsConditionVC) as!TermsConditionVC
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    
    @IBAction func tickButtonAction(_ sender: Any) {
        
        if isTerms == true{
            tickButton.setImage(#imageLiteral(resourceName: "terms"), for: .normal)
            isTerms = false
        }else{
            tickButton.setImage(#imageLiteral(resourceName: "terms_unselected"), for: .normal)
            isTerms = true
        }
        
        
        
    }
    //MARK: IBActions
    @IBAction func registerButtonAction(_ sender: Any) {
        
        var phonenumberlength: String = phoneTextfield.text!
        let length = phonenumberlength.characters.count
        var passwordlength: String = passwordTextfield.text!
        let length1 = passwordlength.characters.count
        
        if nameTextfield.text == kEmptyString || ((nameTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kNameValidMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if userNameTextfield.text == kEmptyString || ((userNameTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kUserNameMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if countryCodeTextfield.text == kEmptyString || ((countryCodeTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kCountryCodeMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if length  > 14 || length < 7  && length > 0
        {
            self.showAlert(message: kPhoneMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        }
        else if emailTextfield.text == kEmptyString || ((emailTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString)
        {
            self.showAlert(message: kEmailValidMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if !emailTextfield.isValidEmail
        {
            self.showAlert(message: kEmailValidMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }else if passwordTextfield.text == kEmptyString || length1  < 6  || ((passwordTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString)
        {
            self.showAlert(message: kPasswordLimitMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        }  else if confirmPasswordTextfield.text == kEmptyString
        {
            self.showAlert(message: kConformPasswordMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        }else if confirmPasswordTextfield.text == kEmptyString || confirmPasswordTextfield.text != passwordTextfield.text || ((confirmPasswordTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString)
        {
            self.showAlert(message: KMatchPasswordMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        } else if isTerms == true
        {
            self.showAlert(message: kTermsCondMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else{
             signUpMethod()
           
            
        }
    }
}
//MARK: Custom Image Picker Delegates
extension SignUpVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {  //Get Image from Picker and use the image
        imageDict[APIKeys.kProfileImage] = image.jpegData(.medium)
        userImageView.image = image
        
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
        imageDict[APIKeys.kProfileImage] = thumbnail.jpegData(.medium)
        userImageView.image = thumbnail
        print(videoUrl)
    }
    
}

//MARK: UITextField Delegates Methods
extension SignUpVC :  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextfield{
            if countryCodeTextfield.text == kEmptyString{
                countryCodeTextfield.text  = "+"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == countryCodeTextfield {
            
            if range.location == 0 && string == " " {
                return false
            }
            let characterLength = textField.text!.utf16.count + (string.utf16).count - range.length
            
            if characterLength > 15 {
                return false
            }
            if textField.text!.characters.count >= 1 && string == "+"{
                return false
            }
            let aSet = CharacterSet(charactersIn:"0123456789+").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: kEmptyString)
            return string == numberFiltered
        }else if textField == self.userNameTextfield || textField == self.nameTextfield {
            return true
        }
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: kEmptyString)
        
        return (string == filtered)

    }
}
//MARK: WEBSERIVES METHOD
extension SignUpVC {
    

    func signUpMethod(){
       
        UserLoginVM.sharedInstance.checkUserAccountExist(email: emailTextfield.text!,userName:userNameTextfield.text!) { (success, message, error) in
            if(success == 1)
            {
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kAddressDetailVc) as!AddressDetailVC
                nextObj.signUpDict = [APIKeys.kFullname:self.nameTextfield.text!, APIKeys.kUsername:self.userNameTextfield.text!, APIKeys.kCountryCode:self.countryCodeTextfield.text!, APIKeys.kPhoneNumber: self.phoneTextfield.text!,APIKeys.kEmail:self.emailTextfield.text!, APIKeys.kPassword:self.passwordTextfield.text!,APIKeys.kProfileImage:self.imageDict]
                self.navigationController?.pushViewController(nextObj, animated: false)
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
    }
}
