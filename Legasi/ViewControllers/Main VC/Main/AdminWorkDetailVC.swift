//
//  AdminWorkDetailVC.swift
//  Legasi
//
//  Created by ios28 on 14/02/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AdminWorkDetailVC: BaseVC {
    //MARK: - IBOutlets
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
//    @IBOutlet weak var industryTypeTextField: UITextField!
//    @IBOutlet weak var jobLocationTextField: UITextField!
    @IBOutlet weak var jobDescriptionTextView: UITextView!
    @IBOutlet weak var sinceYearTextField: UITextField!
    
    //MARK: - Variables
    var workDetail = WorkDetails()
    var workDetailArray = NSMutableArray()
    var industryTypeArray = ["Information technology","Business","Quality Analysis","Textile","Mechanical","Accountancy and banking","Creative arts and design","Engineering and manufacturing","Healthcare","Law","Law enforcement and security","Leisure, sport and tourism","Marketing, advertising and PR","Media and Internet","Property and construction","Social care","Teacher training and education"]
    var yearArray = [String]()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customiseUI()
        self.initializeYearArray()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeYearArray() {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let year = Int(dateFormatter.string(from: now))!
        yearArray.removeAll()
        for i in (1900...year).reversed() {
            yearArray.append("\(i)")
        }
    }
    
    //MARK: - private method
    private func customiseUI() {
        jobTitleTextField.setPlaceholder(placholder: kJobTitlePlaceHolder, color: .darkGray, size: 16.0)
        companyNameTextField.setPlaceholder(placholder: kCompanyNamePlaceHolder, color: .darkGray, size: 16.0)
        sinceYearTextField.setPlaceholder(placholder: kSinceYearPlaceHolder, color: .darkGray, size: 16.0)
//        jobLocationTextField.setPlaceholder(placholder: kJobLocationPlaceHolder, color: .darkGray, size: 16.0)
        
        setBackButton()
        setTitle(title: "WORK DETAILS")
        self.pickerDelegate = self
        
        self.jobTitleTextField.text = workDetail.jobTitle
        self.companyNameTextField.text = DataManager.schoolName
        self.companyNameTextField.isEnabled = false
        self.sinceYearTextField.text = workDetail.sinceYear
//        self.jobLocationTextField.text = workDetail.jobLocation
        if workDetail.jobDiscription != nil {
            self.jobDescriptionTextView.text = workDetail.jobDiscription
            self.jobDescriptionTextView.textColor = UIColor.black
        }
    }
    
    //MARK: - Action

    @IBAction func jobLocationAction(_ sender: Any) {
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
        locationVC.delegate = self
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if self.checkValidation() {
            
            workDetailArray.removeAllObjects()
            let data = workDetail.jsonRepresentation
            workDetailArray.add(data)
            
            if let objectData = try? JSONSerialization.data(withJSONObject: workDetailArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                let objectString = String(data: objectData, encoding: .utf8)
                
                EditWorkDetail(WorkDetail: objectString!)
                
            }
        }
    }
    
    func checkValidation() -> Bool {
        if self.jobTitleTextField.text!.isEmpty || self.companyNameTextField.text!.isEmpty || self.sinceYearTextField.text!.isEmpty ||  self.jobDescriptionTextView.text.isEmpty {
            self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            return false
        }
        workDetail.jobTitle = self.jobTitleTextField.text!
        workDetail.companyName = self.companyNameTextField.text!
        workDetail.sinceYear = self.sinceYearTextField.text!
//        workDetail.industryType = self.industryTypeTextField.text!
//        workDetail.jobLocation = self.jobLocationTextField.text!
        workDetail.jobDiscription = self.jobDescriptionTextView.text!
        workDetail.workDetailId = 0
        workDetail.isCurrentJob = "Yes"
        return true
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

//MARK: UITextfield & UITextview Delegates
extension AdminWorkDetailVC: UITextFieldDelegate, UITextViewDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField == sinceYearTextField) {
            sinceYearTextField.text = yearArray[0]
            setPicker(textField: textField, array:  yearArray)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == jobDescriptionTextView {
            if(jobDescriptionTextView.text == "Job Description"){
                jobDescriptionTextView.text = kEmptyString
                jobDescriptionTextView.textColor = UIColor.black
            }
        }
        textView.becomeFirstResponder()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars < 300;
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count == 0 {
            jobDescriptionTextView.text = "Job Description"
        }
    }
    
    
}
//MARK: Custom Picker Delegates
extension AdminWorkDetailVC: LegasiPickerDelegate{
    
    func didSelectPickerViewAtIndex(index: Int) {
        sinceYearTextField.text = yearArray[index]
    }
}

//MARK: Custom Picker Delegate
extension AdminWorkDetailVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
//        jobLocationTextField.text = address
//        workDetail.lat = "\(latitude)"
//        workDetail.long = "\(longitude)"
    }
}
extension AdminWorkDetailVC {
    
    func EditWorkDetail(WorkDetail:String){
        SettingsVM.sharedInstance.UpdateWorkDetail(userId: DataManager.user_id!, workDetail: WorkDetail) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
