//
//  WorkDetailVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class WorkDetailVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var workDetailTableView: UITableView!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet var skipButton: UIButton!
    
    @IBOutlet weak var addMoreButton: UIButton!
    //MARK: Variables
    var workDetailDict = [WorkDetails]()
    var selectedIndexPath: IndexPath!
    var selectedTextField = UITextField()
    var workDetailArray = NSMutableArray()
    var signUpDict = [String:Any]()
    var previousRectTextView = CGRect.zero
    var industryTypeArray = ["Information technology","Business","Quality Analysis","Textile","Mechanical","Accountancy and banking","Creative arts and design","Engineering and manufacturing","Healthcare","Law","Law enforcement and security","Leisure, sport and tourism","Marketing, advertising and PR","Media and Internet","Property and construction","Social care","Teacher training and education"]
    var isEdit = false
    var index = 1
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        self.customiseUI()
    }
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        setTitle(title: "WORK DETAILS")
        workDetailTableView.estimatedRowHeight = 200
        workDetailTableView.rowHeight = UITableViewAutomaticDimension
        self.pickerDelegate = self
        if isEdit == true{
          // index = workDetailDict.count
            workDetailTableView.reloadData()
           skipButton.setTitle("CANCEL", for: .normal)
           registerButton.setTitle("SAVE", for: .normal)
        }
    }
    //MARK: IBActions
    @IBAction func registerButtonAction(_ sender: Any) {
        self.view.endEditing(true)
         if workDetailDict.count == 0 {
                self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            } else{
                var isEmpty = false
                for value in workDetailDict {
                    if value.jobTitle.isEmpty || value.companyName.isEmpty || value.industryType.isEmpty || value.jobLocation.isEmpty || value.jobDiscription.isEmpty || value.jobDiscription == "Job Description" {
                        
                        isEmpty = true
                        break
                    }
                }
                if isEmpty {
                    self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }else{
                    workDetailArray.removeAllObjects()
                    for value in workDetailDict {
                        let data = value.jsonRepresentation
                        workDetailArray.add(data)
                    }
                    
                    if let objectData = try? JSONSerialization.data(withJSONObject: workDetailArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                        let objectString = String(data: objectData, encoding: .utf8)
                        
                        if isEdit != true{
                          registerationDataMethod(workDetailData: objectString!)
                        }else{
                           EditWorkDetail(WorkDetail: objectString!)
                        }
                        
                    }
                    
                    
                
            }
        }
    }
    
    
    @IBAction func skipButtonAction(_ sender: Any) {
        if isEdit != true{
            registerationDataMethod(workDetailData:"")
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    
    @IBAction func addMoreButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        //1 = graduate or student, 2 & 3 = admin
        if DataManager.role! > 1 && workDetailDict.count >= 1 {
            return
        }
       
            if workDetailDict.count > 0{
                
                if (workDetailDict.last?.jobTitle.isEmpty)! || (workDetailDict.last?.companyName.isEmpty)! || (workDetailDict.last?.industryType.isEmpty)! || (workDetailDict.last?.jobLocation.isEmpty)! || (workDetailDict.last?.jobDiscription.isEmpty)! {
                    
                    self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }else{
                    index += 1
                    workDetailTableView.reloadData()
                    
                }
            }else {
                if index == 0 {
                    index += 1
                    workDetailTableView.reloadData()
                }else {
                    self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }
            }
            self.view.layoutIfNeeded()
        
    }
}
//MARK: Webservices Methods
extension WorkDetailVC {
    
    func registerationDataMethod(workDetailData:String){
        var deviceToken = DataManager.deviceToken
        if(deviceToken == kEmptyString || deviceToken == nil){
            deviceToken = kEmptyString
        }else {
            deviceToken = DataManager.deviceToken
        }
        
        UserLoginVM.sharedInstance.signUp(fullName: signUpDict[APIKeys.kFullname]as!String, username: signUpDict[APIKeys.kUsername]as!String, profileImage:signUpDict[APIKeys.kProfileImage] as! [String : Data] , countryCode: signUpDict[APIKeys.kCountryCode] as! String, phoneNumber: signUpDict[APIKeys.kPhoneNumber]as!String, email: signUpDict[APIKeys.kEmail]as!String, password: signUpDict[APIKeys.kPassword]as!String, addressLine1: signUpDict[APIKeys.kAddressLine1]as!String, latitude: signUpDict[APIKeys.KLatitude]as!Double, longitude: signUpDict[APIKeys.KLongitude]as!Double,  schoolId: "\(signUpDict[APIKeys.kSchoolId]as!Int)", userType: signUpDict[APIKeys.kUserType]as!String, dob: signUpDict[APIKeys.kDob]as!String, linkedInId: signUpDict[APIKeys.kLinkedInId]as!String, facebookId: signUpDict[APIKeys.kFacebookId]as!String, deviceType: kIphone, deviceToken: deviceToken!, graduationYear: signUpDict[APIKeys.kGraduationYear]as!String, educationDetails: signUpDict[APIKeys.kEducationDetail] as! String, workDetails: workDetailData,skills:signUpDict[APIKeys.kSkills] as!String) { (success, message, error) in
            if(success == 1)
            {
                DataManager.linkedInAccessToken = nil
                
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kPendingRequestVC) as!PendingRequestVC
//                    self.navigationController?.pushViewController(nextObj, animated: false)
                    let navigationController = UINavigationController(rootViewController: nextObj)
                    UIApplication.shared.keyWindow?.rootViewController = navigationController
                }
                
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}
//MARK: UITableView Datasource & Delegates
extension WorkDetailVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kWorkDetailCell, for: indexPath)as!WorkDetailTableViewCell
        cell.selectionStyle = .none
        cell.jobDescTextView.tag = indexPath.row
        if indexPath.row < workDetailDict.count{
            cell.jobDescTextView.text = workDetailDict[indexPath.row].jobDiscription
            cell.jobTitleTextfield.text = workDetailDict[indexPath.row].jobTitle
            cell.companyNameTextField.text = workDetailDict[indexPath.row].companyName
            cell.industryTextField.text = workDetailDict[indexPath.row].industryType
            cell.jobLocationTextfield.text = workDetailDict[indexPath.row].jobLocation
            if workDetailDict[indexPath.row].isCurrentJob == "Yes" {
                cell.currentJobButton.setImage(#imageLiteral(resourceName: "SelectedTick"), for: .normal)
            }else {
                cell.currentJobButton.setImage(#imageLiteral(resourceName: "UnselectedTick"), for: .normal)
            }
            cell.jobDescTextView.textColor = UIColor.black
        }
            
        else {
            cell.jobDescTextView.text = "Job Description"
            cell.jobDescTextView.textColor = UIColor.darkGray
            cell.jobTitleTextfield.text = kEmptyString
            cell.companyNameTextField.text = kEmptyString
            cell.industryTextField.text = kEmptyString
            cell.jobLocationTextfield.text = kEmptyString
            cell.currentJobButton.setImage(#imageLiteral(resourceName: "UnselectedTick"), for: .normal)
        }
        cell.loadData(viewcontroller: self, at: indexPath)
        cell.jobTitleTextfield.delegate = self
        cell.companyNameTextField.delegate = self
        cell.industryTextField.delegate = self
        cell.jobLocationTextfield.delegate = self
        cell.jobDescTextView.delegate = self
        //Set Shadow to View
        cell.mainView.layer.cornerRadius = 7
        cell.mainView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.mainView.layer.shadowOffset = CGSize.zero
        cell.mainView.layer.shadowRadius = 2
        cell.mainView.layer.shadowOpacity = 0.3
        cell.crossButton.tag = indexPath.row
        cell.crossButton.addTarget(self, action: #selector(self.crossButtonAction(sender:)), for: .touchUpInside)
        cell.jobLocationButton.addTarget(self, action: #selector(self.locationButtonAction(sender:)), for: .touchUpInside)
        cell.jobLocationButton.tag = indexPath.row
        cell.currentJobButton.addTarget(self, action: #selector(handleCurrentJob), for: .touchUpInside)
        cell.currentJobButton.tag = indexPath.row
        
        return cell
        
    }
    
    @objc private func handleCurrentJob(sender: UIButton) {
        var currentJob = ""
        if sender.currentImage == #imageLiteral(resourceName: "UnselectedTick") {
            sender.setImage(#imageLiteral(resourceName: "SelectedTick"), for: .normal)
            currentJob = "Yes"
        }else {
            sender.setImage(#imageLiteral(resourceName: "UnselectedTick"), for: .normal)
            currentJob = "No"
        }
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        selectedIndexPath = indexPath
//        let cell = workDetailTableView.cellForRow(at: indexPath)as!WorkDetailTableViewCell
        
        if selectedIndexPath.row < workDetailDict.count{
            workDetailDict[sender.tag].isCurrentJob = currentJob
        }
        else {
            let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: kEmptyString, jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString,workDetailId:0, isCurrentJob: currentJob, sinceYear: kEmptyString)
        workDetailDict.append(data)
        }
    }
    
    func locationButtonAction(sender: UIButton){
        self.view.endEditing(true)
        let indexPath = IndexPath(row: sender.tag, section: 0)
        selectedIndexPath = indexPath
        let cell = workDetailTableView.cellForRow(at: indexPath) as! WorkDetailTableViewCell
        selectedTextField = cell.jobLocationTextfield
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
        locationVC.delegate = self
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    func crossButtonAction(sender: UIButton){
        self.view.endEditing(true)
        
        if index > 1 {
            index -= 1
            if workDetailDict.count > sender.tag {
                workDetailDict.remove(at: sender.tag)
            }
            workDetailTableView.reloadData()
            
            self.view.layoutIfNeeded()
        }
    }
}
//MARK: UITextfield & UITextview Delegates
extension WorkDetailVC: UITextFieldDelegate, UITextViewDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        selectedIndexPath = indexPath
        let cell = workDetailTableView.cellForRow(at: indexPath) as! WorkDetailTableViewCell
        if (textField == cell.industryTextField) {
            cell.industryTextField.text = industryTypeArray[0]
            setPicker(textField: textField, array:  industryTypeArray)
        }
        
        selectedTextField = textField
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = workDetailTableView.cellForRow(at: indexPath) as! WorkDetailTableViewCell
        if textField == cell.jobTitleTextfield {
            if selectedIndexPath.row < workDetailDict.count {
                workDetailDict[textField.tag].jobTitle = textField.text!
            }
            else {
                let data = WorkDetails(jobTitle: textField.text!, companyName: kEmptyString, industryType: kEmptyString, jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: "",workDetailId:0, isCurrentJob: "No", sinceYear: kEmptyString)
                workDetailDict.append(data)
            }
        }else if textField == cell.companyNameTextField {
            if selectedIndexPath.row < workDetailDict.count {
                workDetailDict[textField.tag].companyName = textField.text!
            }
            else {
                let data =  WorkDetails(jobTitle: kEmptyString, companyName: textField.text!, industryType: kEmptyString, jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString,workDetailId:0, isCurrentJob: "No", sinceYear: kEmptyString)
                workDetailDict.append(data)
            }
        }else if textField == cell.industryTextField {
            if selectedIndexPath.row < workDetailDict.count {
                workDetailDict[textField.tag].industryType = textField.text!
            }
            else {
                let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: textField.text!, jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString,workDetailId:0, isCurrentJob: "No", sinceYear: kEmptyString)
                workDetailDict.append(data)
                
            }
        }
        //        else if textField == cell.jobLocationTextfield {
        //            if selectedIndexPath.row < workDetailDict.count {
        //                workDetailDict[textField.tag].jobLocation = textField.text!
        //            }
        //            else {
        //                let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: kEmptyString, jobLocation: textField.text!, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString)
        //                workDetailDict.append(data)
        //
        //            }
        //        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        let indexPath = IndexPath(row: textView.tag, section: 0)
        selectedIndexPath = indexPath
        let cell = workDetailTableView.cellForRow(at: indexPath)as!WorkDetailTableViewCell
        if textView == cell.jobDescTextView {
            if selectedIndexPath.row < workDetailDict.count{
                workDetailDict[textView.tag].jobDiscription = textView.text!
            }
            else {
                
                let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: kEmptyString, jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: textView.text!,workDetailId:0, isCurrentJob: "No", sinceYear: kEmptyString)
                workDetailDict.append(data)
                
                
            }
            
        }
        
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let indexPath = IndexPath(row: textView.tag, section: 0)
        selectedIndexPath = indexPath
        let cell = workDetailTableView.cellForRow(at: indexPath)as!WorkDetailTableViewCell
        
        if textView == cell.jobDescTextView {
            if(cell.jobDescTextView.text == "Job Description"){
                cell.jobDescTextView.text = kEmptyString
                cell.jobDescTextView.textColor = UIColor.black
            }
            
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.setAnimationsEnabled(false)
        workDetailTableView.beginUpdates()
        workDetailTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        //        var frame = textView.frame
        //        frame.size.height = textView.contentSize.height
        //        textView.frame = frame
        let pos = textView.endOfDocument
        let currentRect = textView.caretRect(for: pos)
        if(currentRect.origin.y > previousRectTextView.origin.y){
            //new line reached.
            let index = IndexPath(row: textView.tag, section: 0)
            workDetailTableView.scrollToRow(at: index, at: UITableViewScrollPosition.bottom, animated: false)
        }
        previousRectTextView = currentRect
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 300;
    }
    
    
}
//MARK: Custom Picker Delegates
extension WorkDetailVC: LegasiPickerDelegate{
    
    func didSelectPickerViewAtIndex(index: Int) {
        let cell = workDetailTableView.cellForRow(at: selectedIndexPath) as! WorkDetailTableViewCell
        
        if(selectedTextField == cell.industryTextField) {
            cell.industryTextField.text = industryTypeArray[index]
            if selectedIndexPath.row < workDetailDict.count{
                workDetailDict[selectedIndexPath.row].industryType = industryTypeArray[index]
            }
            else {
                
                let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: industryTypeArray[index], jobLocation: kEmptyString, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString, workDetailId: 0, isCurrentJob: "No", sinceYear: kEmptyString)
                workDetailDict.append(data)
            }
        }
    }
}

//MARK: Custom Picker Delegate
extension WorkDetailVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        let cell = workDetailTableView.cellForRow(at: selectedIndexPath) as! WorkDetailTableViewCell
        if !address.isEmpty {
           // cell.jobLocationTextfield.text = address
            
            if(selectedTextField == cell.jobLocationTextfield) {
                cell.jobLocationTextfield.text = address
                if selectedIndexPath.row < workDetailDict.count{
                    workDetailDict[selectedIndexPath.row].jobLocation = address
                }
                else {
                    
                    let data =  WorkDetails(jobTitle: kEmptyString, companyName: kEmptyString, industryType: kEmptyString, jobLocation: address, lat: kEmptyString, long: kEmptyString, jobDiscription: kEmptyString,workDetailId:0, isCurrentJob: "No", sinceYear: kEmptyString)
                    workDetailDict.append(data)
                }
            }
            // descriptionTextView.becomeFirstResponder()
        }
    }
}
extension WorkDetailVC {
    
    func EditWorkDetail(WorkDetail:String){
        SettingsVM.sharedInstance.UpdateWorkDetail(userId: DataManager.user_id!, workDetail: WorkDetail) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
