//
//  PendingRequestVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 23/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PendingRequestVC: BaseVC {
//MARK: Class Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: kPendingRequestTitle)
    }
    
  //MARK: IBActions  
    @IBAction func loginButtonAction(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let loginObj = storyboard.instantiateViewController(withIdentifier: kLoginVc) as! LoginVC
        let nav = UINavigationController(rootViewController: loginObj)
        appdelegate.window!.rootViewController = nav
     }

}
