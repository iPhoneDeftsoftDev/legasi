//
//  SkillsVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 29/01/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SkillsVC: BaseVC {

    @IBOutlet var skipButton: UIButton!
    @IBOutlet var skillsButton: UIButton!
    @IBOutlet var skillsTextView: UITextView!
    var signUpDict = [String:Any]()
    var isEdit = false
    var skills = [skillsDetailData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setTitle(title: "Skills")
        setBackButton()
        var skillsArray = [String]()
        if isEdit == true{
            for skill in skills{
                skillsArray.append(skill.jobTitle)
            }
            skillsTextView.text = skillsArray.joined(separator: ", ")
            skipButton.setTitle("CANCEL", for: .normal)
            skillsButton.setTitle("UPDATE", for: .normal)
            skillsTextView.textColor = UIColor.black
        }else{
            skipButton.setTitle("SKIP", for: .normal)
            skillsButton.setTitle("NEXT", for: .normal)
            skillsTextView.textColor = UIColor.darkGray
        }
        skillsTextView.setRadius(radius: 10.0, borderColor: UIColor.black, borderWidth: 1.0)
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func skillsButtonAction(_ sender: Any) {
        
        if skillsTextView.text == kEmptyString || ((skillsTextView.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString || skillsTextView.text == "Enter skills here..") {
            self.showAlert(message: "Enter Skills", title: kAlert, otherButtons: nil, cancelTitle: kOk)
        } else{
            if isEdit != true{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let nxtObj = storyboard.instantiateViewController(withIdentifier: kWorkDetailVc)as!WorkDetailVC
                self.signUpDict[APIKeys.kSkills] = skillsTextView.text!
                nxtObj.signUpDict = self.signUpDict
                self.navigationController?.pushViewController(nxtObj, animated: true)
            }else{
                self.skillsUpdateMethod()
            }
            
        }
       
    }
   
    @IBAction func skipButtonAction(_ sender: Any) {
        if isEdit != true{
            let storyboard = UIStoryboard.storyboard(storyboard: .Main)
            let nxtObj = storyboard.instantiateViewController(withIdentifier: kWorkDetailVc)as!WorkDetailVC
            self.signUpDict[APIKeys.kSkills] = ""
            nxtObj.signUpDict = self.signUpDict
            self.navigationController?.pushViewController(nxtObj, animated: true)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
}

extension SkillsVC: UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Enter skills here.."
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Enter skills here.."){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        //        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        //        let numberOfChars = newText.characters.count // for Swift use count(newText)
        //        return numberOfChars < 150;
        return true
    }
}

extension SkillsVC {
    func skillsUpdateMethod(){
        var skills = skillsTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if skills.last == "," {
            skills = String(skills.dropLast())
        }
        SettingsVM.sharedInstance.SkillsUpdate(userId: DataManager.user_id!, skills: skills) { (success, message, error) in
            if(success == 1)
            {
               self.navigationController?.popViewController(animated: false)
               
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}
