//
//  LinkAccountVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SystemConfiguration

class LinkAccountVC: BaseVC {

//MARK: IBOutlets
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var topConstraintView: NSLayoutConstraint!
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var linkedInLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var linkedInButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    //MARK: Variables
    var isEdit = false
    var dictFB : NSDictionary!
    var signUpDict = [String:Any]()
    var facebookId = String()
    var linkedInId = String()
    var accountType: String!
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
          }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        setTitle(title: kLinkAccountTitle)
        
        let screen = UIScreen.main.bounds.size
        if screen.height <= 568 {
            topConstraintView.constant = 100
        }else if screen.height >= 667 {
             topConstraintView.constant = 140
        }
        
        if DataManager.linkedInAccessToken != nil {
            if DataManager.facebook_id != nil {
                linkedInLabel.text = kConnectedWith
                facebookLabel.text = kConnectedWith
                facebookId = DataManager.facebook_id!
                linkedInId = DataManager.linkedin_id!
            }else
            {
                linkedInLabel.text = kConnectedWith
                linkedInId = DataManager.linkedin_id!
                facebookLabel.text = kConnectWith
            }
            
            
          }else{
            if DataManager.facebook_id != nil {
                linkedInLabel.text = kConnectWith
                facebookLabel.text = kConnectedWith
                facebookId = DataManager.facebook_id!
            }else
            {
                linkedInLabel.text = kConnectWith
                facebookLabel.text = kConnectWith
            }
            
        }
        
        if isEdit == true{
            skipButton.isHidden = true
            nextButton.setTitle(kSaveButtonTitle, for: .normal)
            if facebookId != ""{
                facebookLabel.text = kConnectedWith
                DataManager.facebook_id = facebookId
            }else{
                facebookLabel.text = kConnectWith
            }
            if linkedInId != ""{
                DataManager.linkedin_id = linkedInId
                linkedInLabel.text = kConnectedWith
            }else{
                linkedInLabel.text = kConnectWith
            }
        
        }

      
    }

    override func backButtonAction() {
        DataManager.linkedInAccessToken = nil
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: Storyboard Actions
    @IBAction func nextButtonAction(_ sender: Any) {
        if isEdit != true{
            let educationvc = self.storyboard?.instantiateViewController(withIdentifier: kEducationDetailVc) as!EducationDetailVC
            signUpDict[APIKeys.kLinkedInId] = DataManager.linkedin_id ?? kEmptyString
            signUpDict[APIKeys.kFacebookId] = DataManager.facebook_id ?? kEmptyString
            educationvc.signUpDict = signUpDict
            self.navigationController?.pushViewController(educationvc, animated: false)
        } else{
           EditLinkAccountDetail()
         }
        
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
        if isEdit != true{
            let educationvc = self.storyboard?.instantiateViewController(withIdentifier: kEducationDetailVc) as!EducationDetailVC
            educationvc.signUpDict = signUpDict
            signUpDict[APIKeys.kLinkedInId] =  kEmptyString
            signUpDict[APIKeys.kFacebookId] =  kEmptyString
            educationvc.signUpDict = signUpDict
            self.navigationController?.pushViewController(educationvc, animated: false)
        }
    }
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        
        self.linkedInButton.isEnabled = false
        self.nextButton.isEnabled = false
        self.skipButton.isEnabled = false
       let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        
        fbLoginManager.logIn(withReadPermissions: [APIKeys.kPublicProfile, APIKeys.kEmail], from: self) { (result, error) in
            if error == nil {
                self.linkedInButton.isEnabled = true
                self.nextButton.isEnabled = true
                self.skipButton.isEnabled = true
                if result?.grantedPermissions != nil {
                    self.getFBUserData()
                }
            }
            else {
               
                self.linkedInButton.isEnabled = true
                self.nextButton.isEnabled = true
                self.skipButton.isEnabled = true
            }
        }
       
    }
    
    @IBAction func linkdinButtonAction(_ sender: Any) {
        
        if ConnectionCheck.isConnectedToNetwork(){
            
            checkForExistingAccessToken()
            }else {
            
                self.showAlert(message: kInternetConnectionMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }
        
        
        
        
    }
//MARK: Private Methods
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: kMe, parameters: [kFields: "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dictFB = result as? NSDictionary
                    print(result!)
                    self.facebookLabel.text = kConnectedWith
                    self.accountType = "FB"
                    DataManager.facebook_id = self.dictFB[kId] as? String
                    self.facebookLabel.text = kConnectedWith
                } else {
                    
                    self.linkedInButton.isEnabled = true
                    self.nextButton.isEnabled = true
                    self.skipButton.isEnabled = true
                }
            })
        }
    }

    func checkForExistingAccessToken() {
        if DataManager.linkedInAccessToken != nil {
            DataManager.linkedInAccessToken = nil
        }
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kWebVC) as!WebViewController
        self.navigationController?.pushViewController(nextObj, animated: false)
    }

}

extension LinkAccountVC {
    
    func EditLinkAccountDetail(){
        SettingsVM.sharedInstance.LinkAccount(userId: DataManager.user_id!, accountTypeFB: DataManager.facebook_id!, accountTypeLink: DataManager.linkedin_id!) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
