//
//  ForgotPasswordVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var emailTextfield: UITextField!
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        setTitle(title: kForgotPasswordTitle)
    }
    
    //MARK: IBActions
    @IBAction func resetPasswordButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        if emailTextfield.isValidEmail {
            forgotPasswordMethod()
        }else {
            self.showAlert(message: kEmailValidMessage)
        }
        
    }
}
//MARK: Webservice Method
extension ForgotPasswordVC {
    
    func forgotPasswordMethod(){
        
        UserLoginVM.sharedInstance.forgotPassword(email: emailTextfield.text!) { (success, message, error) in
            if(success == 1)
            {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                   self.backButtonAction()
                })
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}
//MARK: UITextField delegates
extension ForgotPasswordVC :  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: kEmptyString)
        
        return (string == filtered)
    }

}
