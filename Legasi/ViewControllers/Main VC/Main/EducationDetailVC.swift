//
//  EducationDetailVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class EducationDetailVC: BaseVC {

    //MARK: IBOutlets
    @IBOutlet weak var educationTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet var skipButton: UIButton!
    @IBOutlet weak var addMoreButton: UIButton!
    var educationDict = [EducationDetails]()
    var index = 1
    var isEdit = false
    var selectedIndexPath: IndexPath!
    var selectedTextField = UITextField()
    var educationArray = NSMutableArray()
    var signUpDict = [String:Any]()
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
  
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        setTitle(title: kEducationTitle)
        educationTableView.estimatedRowHeight = 200
        educationTableView.rowHeight = UITableViewAutomaticDimension
        self.pickerDelegate = self
       
        if isEdit != true{
            if signUpDict[APIKeys.kUserType]as!String == kStudent{
                nextButton.isHidden = false
                skipButton.isHidden = false
            }else{
                nextButton.isHidden = false
                skipButton.isHidden = true
            }
        }else{
            //index = educationDict.count
            educationTableView.reloadData()
            skipButton.setTitle("CANCEL", for: .normal)
            nextButton.setTitle("SAVE", for: .normal)
        }
    }
    
    //MARK: IBActions
    @IBAction func nextButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        if educationDict.count == 0 {
            self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        } else{
            var isEmpty = false
            
            for value in educationDict {
                if value.collegeName.isEmpty || value.degreeType.isEmpty || value.graduationYear.isEmpty || value.majorDetails.isEmpty {
                    
                    isEmpty = true
                    break
                }
                
            }
            if isEmpty {
                self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else{
                educationArray.removeAllObjects()
                for value in educationDict {
                    let data = value.jsonRepresentation
                    educationArray.add(data)
                }
                if let objectData = try? JSONSerialization.data(withJSONObject: educationArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                    
                    let objectString = String(data: objectData, encoding: .utf8)
                    
                    if isEdit != true{
                        
                       // kSkillsVC
                        
                        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                        let nxtObj = storyboard.instantiateViewController(withIdentifier: kSkillsVC)as!SkillsVC
                        self.signUpDict[APIKeys.kEducationDetail] = objectString
                        nxtObj.signUpDict = self.signUpDict
                        self.navigationController?.pushViewController(nxtObj, animated: true)
                       
                    }else {
                        self.EditEducationDetail(educationDetail:objectString!)
                    }
                    
                }
                
            }
            
        }
        
    }

    @IBAction func skipButtonAction(_ sender: Any) {
        if isEdit != true{
            let storyboard = UIStoryboard.storyboard(storyboard: .Main)
            let nxtObj = storyboard.instantiateViewController(withIdentifier: kSkillsVC)as!SkillsVC
            self.signUpDict[APIKeys.kEducationDetail] = ""
            nxtObj.signUpDict = self.signUpDict
            self.navigationController?.pushViewController(nxtObj, animated: true)
            
        } else{
            self.navigationController?.popViewController(animated: false)
        }
        
        
    }
    
    @IBAction func addMoreButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        if educationDict.count > 0{
            
            if (educationDict.last?.collegeName.isEmpty)! || (educationDict.last?.degreeType.isEmpty)! || (educationDict.last?.graduationYear.isEmpty)! || (educationDict.last?.majorDetails.isEmpty)! {
                
                self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else{
                index += 1
                educationTableView.reloadData()

            }
        } else {
            if index == 0 {
                index += 1
                educationTableView.reloadData()
            }else {
                self.showAlert(message: kEnterDetailsMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }
        }
        
        self.view.layoutIfNeeded()
   
    }
}

//MARK: TableView Datasource & Delegates
extension EducationDetailVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kEducationDetailCell, for: indexPath)as!EducationTableViewCell
        cell.selectionStyle = .none
        cell.loadData(viewcontroller: self, at: indexPath)
        
        if indexPath.row < educationDict.count{
            cell.collegeNameTextField.text = educationDict[indexPath.row].collegeName
            cell.graduationYearTextfield.text = educationDict[indexPath.row].graduationYear
            cell.degreeTypeTextfield.text = educationDict[indexPath.row].degreeType
            cell.majorDetailTextField.text = educationDict[indexPath.row].majorDetails
        }
            
        else {
            cell.collegeNameTextField.text = kEmptyString
            cell.graduationYearTextfield.text = kEmptyString
            cell.degreeTypeTextfield.text = kEmptyString
            cell.majorDetailTextField.text = kEmptyString
            
        }
        cell.collegeNameTextField.delegate = self
        cell.graduationYearTextfield.delegate = self
        cell.degreeTypeTextfield.delegate = self
        cell.majorDetailTextField.delegate = self
        //Set Shadow to View
        cell.mainView.layer.cornerRadius = 7
        cell.mainView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.mainView.layer.shadowOffset = CGSize.zero
        cell.mainView.layer.shadowRadius = 2
        cell.mainView.layer.shadowOpacity = 0.3
        cell.crossButton.tag = indexPath.row

        cell.crossButton.addTarget(self, action: #selector(self.crossButtonAction(sender:)), for: .touchUpInside)
        

        
        return cell
        
    }
    
   func crossButtonAction(sender: UIButton){
    self.view.endEditing(true)
        if index > 1 {
        index -= 1
        if educationDict.count > sender.tag {
            educationDict.remove(at: sender.tag)
        }
        educationTableView.reloadData()
        
        self.view.layoutIfNeeded()
        }
    }
}
//MARK: UITextfield Delegates
extension EducationDetailVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        selectedIndexPath = indexPath
        let cell = educationTableView.cellForRow(at: indexPath) as! EducationTableViewCell
        if (textField == cell.graduationYearTextfield) {
            let yearArray = UserLoginVM.sharedInstance.getYearList()
            let yearArrayString = yearArray.map {
                String($0)
            }
            
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let index = yearArrayString.index(of: "\(year)")
            cell.graduationYearTextfield.text = yearArrayString[index!]
            setPicker(textField: textField, array:  yearArrayString, defaultIndex: index ?? 0)
        }
        selectedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = educationTableView.cellForRow(at: indexPath) as! EducationTableViewCell
        if textField == cell.graduationYearTextfield {
            if textField.tag < educationDict.count {
                educationDict[textField.tag].graduationYear = textField.text!
            }
            else {
                let data = EducationDetails(graduationYear: textField.text!, collegeName: kEmptyString, degreeType: kEmptyString, majorDetails: kEmptyString,educationDetailId:0)
                educationDict.append(data)
            }
        }else if textField == cell.collegeNameTextField {
            if textField.tag < educationDict.count {
                educationDict[textField.tag].collegeName = textField.text!
            }
            else {
                let data = EducationDetails(graduationYear: kEmptyString, collegeName: textField.text!, degreeType: kEmptyString, majorDetails: kEmptyString,educationDetailId:0)
                educationDict.append(data)
            }
        }else if textField == cell.degreeTypeTextfield {
            if textField.tag < educationDict.count {
                educationDict[textField.tag].degreeType = textField.text!
            }
            else {
                let data = EducationDetails(graduationYear: kEmptyString, collegeName: kEmptyString, degreeType: textField.text!, majorDetails: kEmptyString,educationDetailId:0)
                educationDict.append(data)
            }
        }else if textField == cell.majorDetailTextField {
            if textField.tag < educationDict.count {
                educationDict[textField.tag].majorDetails = textField.text!
            }
            else {
                let data = EducationDetails(graduationYear: kEmptyString, collegeName: kEmptyString, degreeType: kEmptyString, majorDetails: textField.text!,educationDetailId:0)
                educationDict.append(data)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: kEmptyString)
        
        return (string == filtered)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
//MARK: Custom Picker Delegates
extension EducationDetailVC: LegasiPickerDelegate{
    
    func didSelectPickerViewAtIndex(index: Int) {
        let cell = educationTableView.cellForRow(at: selectedIndexPath) as! EducationTableViewCell
        
        if(selectedTextField == cell.graduationYearTextfield) {
            cell.graduationYearTextfield.text = "\(UserLoginVM.sharedInstance.getYearList()[index])"
            if selectedIndexPath.row < educationDict.count {
                educationDict[selectedIndexPath.row].graduationYear = "\(UserLoginVM.sharedInstance.getYearList()[index])"
            }
            else {
                let data = EducationDetails(graduationYear:"\(UserLoginVM.sharedInstance.getYearList()[index])", collegeName: kEmptyString, degreeType: kEmptyString, majorDetails: kEmptyString,educationDetailId:0)
                educationDict.append(data)
                
            }
        }
    }
}

extension EducationDetailVC {
    
    func EditEducationDetail(educationDetail:String){
        SettingsVM.sharedInstance.UpdateEducationDetail(userId: DataManager.user_id!, educationDetail: educationDetail) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
