//
//  TermsConditionVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 23/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class TermsConditionVC: BaseVC {
//MARK: IBOutlets
   // @IBOutlet weak var termsTextView: UITextView!
    
    @IBOutlet var termsWebView: UIWebView!
    
   //MARK: Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
       
        
        
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"DOC1" ofType:@"docx"];
//        NSURL *targetURL = [NSURL fileURLWithPath:path];
//        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
//        [mywebView loadRequest:request];
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        setTitle(title: kTermsAndConditionTitle)
        termsWebView.contentMode = .scaleAspectFill
        termsWebView.generateShadowUsingBezierPath(radius: 1.0, frame: CGRect(x: 20, y: termsWebView.frame.origin.y, width: self.view.frame.width-40, height: termsWebView.frame.height))
         termsWebView.scalesPageToFit = true
        let path = Bundle.main.path(forResource: "Legasi Terms of Service", ofType: "doc")!
        let url = URL.init(fileURLWithPath: path)
        let request = URLRequest(url: url)
        termsWebView.loadRequest(request)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       //termsTextView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    

}
