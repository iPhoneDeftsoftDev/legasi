//
//  AddressDetailVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class AddressDetailVC: BaseVC {

    //MARK: IBOutlets
    @IBOutlet weak var AddressLine1Textfield: UITextField!
   
    @IBOutlet weak var nextButton: UIButton!
   //MARK: Variables
    var isEdit = false
    var signUpDict = [String:Any]()
    var selectedAddressLatitude : Double?
    var selectedAddressLongitude : Double?
    var address = String()
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
 //MARK: Private Method
    private func customiseUI() {
        AddressLine1Textfield.setPlaceholder()
        
        self.setTitle(title: kAddressDetailTitle)
        self.setBackButton()
        
        if isEdit == true{
            AddressLine1Textfield.text = address
            nextButton.setTitle(kSaveButtonTitle, for: .normal)
        }
    }
    
    //MARK: IBActions
    @IBAction func registerButtonAction(_ sender: Any) {
       
       if AddressLine1Textfield.text == kEmptyString || ((AddressLine1Textfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kAddressLineMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        
      else{
            if isEdit != true{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kSchoolDetailVc) as!SchoolDetailVC
                signUpDict[APIKeys.kAddressLine1] = AddressLine1Textfield.text!
                signUpDict[APIKeys.KLatitude] = selectedAddressLatitude!
                signUpDict[APIKeys.KLongitude] = selectedAddressLongitude!
                nextObj.addressDict = signUpDict
                self.navigationController?.pushViewController(nextObj, animated: false)
            }else{
                EditAddressDetail()
            }
            
            
        }
    }
  
}
//MARK: UITextField delegate
extension AddressDetailVC :  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == AddressLine1Textfield{
            textField.resignFirstResponder()
             let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
            let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
            locationVC.delegate = self
            self.navigationController?.pushViewController(locationVC, animated: true)
                
            }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
}
//MARK: Custom Picker Delegate
extension AddressDetailVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        if !address.isEmpty {
            AddressLine1Textfield.text = address
            selectedAddressLatitude = latitude
            selectedAddressLongitude = longitude
            // descriptionTextView.becomeFirstResponder()
        }
    }
}

extension AddressDetailVC {
    
    func EditAddressDetail(){
        SettingsVM.sharedInstance.UpdateAddressDetail(userId: DataManager.user_id!, addressLine1: AddressLine1Textfield.text!, latitude: selectedAddressLatitude!, longitude: selectedAddressLongitude!) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
