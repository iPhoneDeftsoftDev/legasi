//
//  SchoolDetailVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class SchoolDetailVC: BaseVC {

    //MARK: IBOutlets
    @IBOutlet weak var schoolTextfield: UITextField!
    @IBOutlet weak var graduationYearTextfield: UITextField!
    @IBOutlet weak var dobTextfield: UITextField!
    
    @IBOutlet weak var userTypeSegment: UISegmentedControl!
    
    @IBOutlet weak var nextButton: UIButton!
    //MARK: Variables
    var schoolArray = NSMutableArray()
    var selectedTextField: UITextField!
    var yearArray = NSMutableArray()
    let currentDate = Date()
    var isEdit = false
    var userType: String!
    var addressDict = [String:Any]()
    var schoolListArray = [schoolListData]()
    var schoolDictData = [String:Any]()
    var school_id = Int()
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userTypeSegment.setRadius(radius: userTypeSegment.frame.height/2, borderColor: UIColor.black, borderWidth: 1)
        userTypeSegment.selectedSegmentIndex = 0
        userType = kStudent
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
//MARK: Private Methods
    private func customiseUI() {
        schoolTextfield.setPlaceholder()
        graduationYearTextfield.setPlaceholder()
        dobTextfield.setPlaceholder()
        setTitle(title: kSchoolDetailTitle)
        setBackButton()
        self.pickerDelegate = self
        

        if isEdit == true{
            schoolTextfield.text = schoolDictData["schoolName"] as? String
            graduationYearTextfield.text = schoolDictData["passoutYear"] as? String
            dobTextfield.text = schoolDictData["dob"] as? String
            if schoolDictData["userType"] as? String == "Graduate"{
                userTypeSegment.selectedSegmentIndex = 1
                userType = kGraduate
            }else{
                userTypeSegment.selectedSegmentIndex = 0
                userType = kStudent
            }
            let name = schoolDictData["nameOfGraduate"] as? String
            self.userTypeSegment.setTitle(name!.isEmpty ? "Graduate" : name, forSegmentAt: 1)
           
            
            school_id = schoolDictData["school_id"] as! Int
            nextButton.setTitle(kSaveButtonTitle, for: .normal)
            
        }
        schoolListDataMethod()
        
    }
 //MARK: Storyboard Actions   
    @IBAction func userTypeSegmentAction(_ sender: Any) {
        
        if(userTypeSegment.selectedSegmentIndex == 0){
            userType = kStudent
        }else{
            userType = kGraduate
        }
    }
    
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if schoolTextfield.text == kEmptyString || ((schoolTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kSchoolNameMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if graduationYearTextfield.text == kEmptyString || ((graduationYearTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kGraduationYearMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if dobTextfield.text == kEmptyString || ((dobTextfield.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kDOBMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else{
            if isEdit != true{
                var schoolId:Int!
                for dict in schoolListArray {
                     if schoolTextfield.text == dict.name {
                        schoolId = dict.schoolId
                        break
                    }
                }
                //role 1 is for student or graduate.
//                DataManager.role = 1
                
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kLinkAccountVc) as!LinkAccountVC
                addressDict[APIKeys.kSchoolId] = schoolId!
                addressDict[APIKeys.kUserType] = userType
                addressDict[APIKeys.kDob] = dobTextfield.text!
                addressDict[APIKeys.kGraduationYear] = graduationYearTextfield.text!
                nextObj.signUpDict = addressDict
                self.navigationController?.pushViewController(nextObj, animated: false)
            } else{
                
                for dict in schoolListArray {
                    if schoolTextfield.text == dict.name {
                        school_id = dict.schoolId!
                        break
                    }
                }
                EditSchoolDetail()
            }
            
            
        }
    }
}
//MARK: Webservices Method
extension SchoolDetailVC {
    func schoolListDataMethod(){
        
        UserLoginVM.sharedInstance.schoolList { (success, message, error) in
            if(success == 1)
            {
                self.schoolListArray = UserLoginVM.sharedInstance.schoolListArray
                
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}
//MARK: UITextField Delegates
extension SchoolDetailVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == schoolTextfield) {
            schoolArray.removeAllObjects()
            for dict in schoolListArray {
               schoolArray.add(dict.name ?? kEmptyString)
            }
            
            schoolTextfield.text = schoolArray[0] as? String
            let name = schoolListArray[0].nameOfGraduate
            self.userTypeSegment.setTitle(name!.isEmpty ? "Graduate" : name, forSegmentAt: 1)
            setPicker(textField: textField, array: schoolArray as! [String])
        }
        else if(textField == graduationYearTextfield) {
            let yearArray = UserLoginVM.sharedInstance.getYearList()
            let yearArrayString = yearArray.map {
                String($0)
            }
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let index = yearArrayString.index(of: "\(year)")
            graduationYearTextfield.text = yearArrayString[index!]
            setPicker(textField: textField, array:  yearArrayString, defaultIndex: index ?? 0)
            
            
        }
        else if(textField == dobTextfield) {
            setDatePicker(textField: textField)
            datePickerView.maximumDate = NSDate() as Date
        }
        selectedTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 50 // Bool
    }
}

//MARK: Custom Picker Delegates
extension SchoolDetailVC: LegasiPickerDelegate {

    func didSelectDatePicker(date: Date) {
        if(selectedTextField == dobTextfield) {
            dobTextfield.text = date.stringFromDate(format: .mdyDate, type: .local)
         }
    }
    
    func didSelectPickerViewAtIndex(index: Int) {
        if(selectedTextField == schoolTextfield ) {
            schoolTextfield.text = schoolArray[index] as? String
            let name = schoolListArray[index].nameOfGraduate
            self.userTypeSegment.setTitle(name!.isEmpty ? "Graduate" : name, forSegmentAt: 1)
        }
        else if(selectedTextField == graduationYearTextfield) {
            graduationYearTextfield.text = "\(UserLoginVM.sharedInstance.getYearList()[index])"
        }
    }
}
extension SchoolDetailVC {
    
    func EditSchoolDetail(){
        SettingsVM.sharedInstance.UpdateSchoolDetail(userId: DataManager.user_id!, schoolId: school_id, userType: userType, dob: dobTextfield.text!,graduationYear:graduationYearTextfield.text!) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
