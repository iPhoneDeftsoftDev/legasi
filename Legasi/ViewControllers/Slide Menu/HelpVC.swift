//
//  HelpVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/11/17.
// Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class HelpVC: BaseVC {
//MARK: - IBOutlets
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var attachmentButton: UIButton!
    @IBOutlet weak var problemTextField: UITextField!
    @IBOutlet weak var subjectTextField: UITextField!
//MARK: - Variables
    var selectedTextField: UITextField!
    var helpArray = ["Problem 1","Problem 2","Problem 3","Problem 4","Problem 5"]
    var imageDict = [String:Data]()
    
//MARK: - Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
//MARK: - Private Methods
    private func customiseUI() {
        setTitle(title: "HELP")
        setBackButton()
        self.pickerDelegate = self
        problemTextField.setPlaceholder()
        subjectTextField.setPlaceholder()
        attachmentButton.layer.cornerRadius = attachmentButton.frame.width/2
        descriptionTextView.textColor = UIColor.darkGray
    }
    override func backButtonAction() {
        self.loadLeftSideMenu()
    }
    
//MARK:- IBActions
    @IBAction func attachmentButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePickerDelegate = self
        self.showImagePicker()
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        
        if problemTextField.text == kEmptyString || ((problemTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: "Select type of problem.", title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if subjectTextField.text == kEmptyString || ((subjectTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: "Enter subject", title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if descriptionTextView.text == kEmptyString || ((descriptionTextView.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: "Enter description", title: kAlert, otherButtons: nil, cancelTitle: kOk)
        } else{
            self.helpQuerySubmitMethod()
        }
   }
    
}
//MARK: Custom Image Picker Delegates
extension HelpVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {  //Get Image from Picker and use the image
        imageDict[APIKeys.kMedia] = image.jpegData(.medium)
        attachmentButton.setImage(image, for: .normal)
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
        imageDict[APIKeys.kMedia] = thumbnail.jpegData(.medium)
        attachmentButton.setImage(thumbnail, for: .normal)
        print(videoUrl)
    }
    
}
//MARK: - UITextFieldDelegate & UITextViewDelegate
extension HelpVC : UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == problemTextField{
            problemTextField.text = helpArray[0]
            setPicker(textField: textField, array: helpArray)
        }
        selectedTextField = textField
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Description"
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description"){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.characters.count // for Swift use count(newText)
//        return numberOfChars < 150;
           return true
    }
    
    
}
//MARK: - Custom Picker Delegate
extension HelpVC: LegasiPickerDelegate{
    
    func didSelectPickerViewAtIndex(index: Int) {
        
        if(selectedTextField == problemTextField) {
            
            problemTextField.text = helpArray[index]
            
        }
    }
}
//MARK:- Webservice Method
extension HelpVC {
    func helpQuerySubmitMethod(){
        
        SettingsVM.sharedInstance.HelpQueryMethod(userId: DataManager.user_id!, typeOfProblem: problemTextField.text!, subject: subjectTextField.text!, description: descriptionTextView.text!, media: imageDict) { (success, message , error) in
            if(success == 1) {
               self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                // self.loadLeftSideMenu()
                
               })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
}
