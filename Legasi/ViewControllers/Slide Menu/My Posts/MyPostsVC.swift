//
//  MyPostsVC.swift
//  Legasi
//
//  Created by Apple on 10/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import EventKit

class MyPostsVC: BaseVC {
    
    //MARK: IBOUtlets
   
    @IBOutlet weak var noRecordLabel: UILabel!
    @IBOutlet weak var postsTableView: UITableView!
    var pageNumber = 1
    var apiRunning = false
    var pollsId = 0
    var startEventDate = Date()
    var endEventDate = Date()
    var isTop = false
    let store = EKEventStore()
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         customiseUI()
    }
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "MY POSTS")
        setBackButton()
        isTop = true
        postsTableView.estimatedRowHeight = 300
        postsTableView.rowHeight = UITableViewAutomaticDimension
        self.pageNumber = 1
        NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
        self.myPostDataMethod()
    }
    
    //MARK: Override Base VC
    override func backButtonAction() {
        self.loadLeftSideMenu()
    }
}

//MARK: UITableView DataSource
extension MyPostsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsFeedVM.sharedInstance.newsFeedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].type
        
        if type == NewsFeedType.normal {
            let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
            post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Message
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title)?.fromBase64()
            if(decodeMsg == nil){
                post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
                post1TableCell.titleNameLabel.text = decodeMsg
            }
            
           
            post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            
            post1TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.profileDetailButton.tag = indexPath.row
            
            
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post1TableCell.delegate = self
            post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            post1TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            post1TableCell.pollDelegate = self
            
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                // DataManager.likeIcon_url
                
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                
            } else{
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                
            }
            post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.likeButton.tag = indexPath.row
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post1TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post1TableCell.imageArray = imageArray
            }
            
            post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            post1TableCell.locationLabel.sizeToFit()
            post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList.count == 0 {
                post1TableCell.pollViewHeight.constant = 0
            }
            post1TableCell.loadData()
            post1TableCell.commentButton.isUserInteractionEnabled = false
            return post1TableCell
        }else if type == NewsFeedType.job {
            
            let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            jobCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Message
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobName!).fromBase64()
            if(decodeMsg == nil){
               jobCell.titleLabel.text = "Job Title: \(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobName!)"
            }else{
                jobCell.titleLabel.text = "Job Title: \(decodeMsg ?? "")"
            }
            
            
            
            jobCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location
            
            jobCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            jobCell.profileDetailButton.tag = indexPath.row
            
            jobCell.companyName.text = "Company: \(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].companyName!)"
            jobCell.skillsLabel.text = ""
            jobCell.skillsTopConstraint.constant = 0
            
            var type = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].jobType
            if type == "1"{
                type = "Full Time Job"
            }else if type == "2"{
                type = "Part Time Job"
            }else if type == "3"{
                type = "Paid Internship Job"
            }else {
                type = "Unpaid Internship Job"
            }
            jobCell.jobTypeLabel.text = "\(type!)"
            jobCell.likeLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            jobCell.isJobVC = false
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].status == 1{
                jobCell.availableView.backgroundColor = UIColor.black
                jobCell.availableLabel.text = "Available"
            }else{
                jobCell.availableView.backgroundColor = UIColor.darkGray
                jobCell.availableLabel.text = "Unavailable"
            }
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }
            
            
            
            jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            jobCell.likeButton.tag = indexPath.row
            jobCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            
            jobCell.decriptionLabel.text = ""
            jobCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            jobCell.locationLabel.sizeToFit()
            jobCell.titleLabel.sizeToFit()
            jobCell.companyName.sizeToFit()
            jobCell.skillsLabel.sizeToFit()
            jobCell.commentButton.isUserInteractionEnabled = false
            jobCell.loadData()
            return jobCell
        }else if type == NewsFeedType.pollSubmit {
            
            let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
            post1TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Message
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title).fromBase64()
            if(decodeMsg == nil){
                 post1TableCell.titleNameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
                post1TableCell.titleNameLabel.text = decodeMsg
            }
            
            post1TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.profileDetailButton.tag = indexPath.row
            
            post1TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post1TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post1TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            post1TableCell.timeLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            post1TableCell.pollDelegate = self
            post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.likeButton.tag = indexPath.row
            
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                // DataManager.likeIcon_url
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }
            
            post1TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post1TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post1TableCell.imageArray = imageArray
            }
            
            post1TableCell.delegate = self
            post1TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            post1TableCell.pollSubmitButton.addTarget(self, action: #selector(self.tapPollSubmitButton(sender:)), for: UIControlEvents.touchUpInside)
            post1TableCell.pollSubmitButton.tag = indexPath.row
            post1TableCell.locationLabel.sizeToFit()
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList.count == 0 {
                post1TableCell.pollViewHeight.constant = 0
                
            }else{
                post1TableCell.pollArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].pollList
                //polled by me
                if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isPolled == "Yes"{
                    post1TableCell.barGraphTypeHeight.constant = 0
                    post1TableCell.submitButtonHeight.constant = 0
                    post1TableCell.pollViewHeight.constant = 200
                }else {//polled by other
                    post1TableCell.barGraphTypeHeight.constant = 197
                    post1TableCell.submitButtonHeight.constant = 90
                    if post1TableCell.pollArray.count == 5 {
                        post1TableCell.pollViewHeight.constant = 484 + 30
                    }else{
                        post1TableCell.pollViewHeight.constant = 484
                    }
                }
            }
            post1TableCell.commentButton.isUserInteractionEnabled = false
            post1TableCell.loadData()
            return post1TableCell
            
        } else {
            let post2TableCell : Post2TableCell = UITableViewCell.fromNib()
            
            post2TableCell.nameLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].name
            //Decode Message
            let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title).fromBase64()
            if(decodeMsg == nil){
                 post2TableCell.titleLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
            }else{
                 post2TableCell.titleLabel.text = decodeMsg
            }
            
            post2TableCell.descriptionLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].Description
            
            post2TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post2TableCell.profileDetailButton.tag = indexPath.row
            
            
            post2TableCell.likesLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].likes!)"
            post2TableCell.commentLabel.text = "\(NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].comments!)"
            let image = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].profilePic
            post2TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            post2TableCell.dateLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].time
            if NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].isLike == 1{
                // DataManager.likeIcon_url
                post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
            } else{
                post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
            }
            
            post2TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            post2TableCell.likeButton.tag = indexPath.row
            post2TableCell.dummyArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].newsFeedTagList
            let imageArray = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].mediaListData!
            if imageArray.count == 0{
                post2TableCell.imageCollectionViewHeight.constant = 0
            }else{
                post2TableCell.imageArray = imageArray
            }
            post2TableCell.submitRSVPButton.addTarget(self, action: #selector(self.tapRSVPButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            
            post2TableCell.submitRSVPButton.tag = indexPath.row
            
            post2TableCell.locationLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].location!
            post2TableCell.delegate = self
            post2TableCell.commentButton.isUserInteractionEnabled = false
            post2TableCell.loadData()
            return post2TableCell
        }
        
    }
    
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func tapRSVPButtonAction(sender:UIButton){
        let dateStr = NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].date
        startEventDate = (dateStr?.dateFromString(format: .shortDMYDate))!
        submitRSVPMethod(event_id: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, value:1,eventName: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].title)
    }
    func tapLikeButtonAction(sender:UIButton){
        if NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].isLike == 1{
            
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, type: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].type.rawValue)
        }else{
            self.likeOnNewsFeedMethod(likeValue: 1,eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId, type: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].type.rawValue)
        }
    }
    
    func tapPollSubmitButton(sender:UIButton){
        pollSubmitData(pollId: pollsId, eventId: NewsFeedVM.sharedInstance.newsFeedArray[sender.tag].eventId)
    }
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            Indicator.isEnabledIndicator = false
            
            if !apiRunning {
                Indicator.sharedInstance.showIndicator()
                self.myPostDataMethod()
            }
        }
    }
    
}

//MARK: Custom Delegates
extension MyPostsVC : VideoPlayerDelegates, VideoPlayersDelegates, PollSubmitDelegates{
    
    func getVideoDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    
    func getVideosDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    func pollSubmitMethod(pollId: Int) {
        pollsId = pollId
    }
}
//MARK: UITableView Delegates
extension MyPostsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
        nextObj.isThroughMyPost = true
        nextObj.newsFeedId = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].eventId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
}

//MARK: Webservices Method
extension MyPostsVC{
    
    func myPostDataMethod(){
        
        apiRunning = true
        NewsFeedVM.sharedInstance.GetOwnNewsFeed(userId: DataManager.user_id!, page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.pageNumber += 1
                self.noRecordLabel.isHidden = true
                self.postsTableView.isHidden = false
                self.postsTableView.reloadData()
                if self.isTop == true{
                    self.postsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    self.isTop = false
                }
                
            }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                            self.noRecordLabel.isHidden = false
                            self.postsTableView.isHidden = true
                       
                    }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            self.postsTableView.reloadData()
            self.apiRunning = false
        }
    }
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int,type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.myPostDataMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func pollSubmitData(pollId: Int, eventId:Int){
        NewsFeedVM.sharedInstance.voteOnPoll(userId: DataManager.user_id!, eventId: eventId, pollId: pollId) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.myPostDataMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func submitRSVPMethod(event_id:Int, value:Int, eventName:String){
        var value = value
        if value == 0{
            value = 2
        }
        NewsFeedVM.sharedInstance.SubmitRSVPRequest(userId: DataManager.user_id!, eventId: event_id, value: value) { (success, message , error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.myPostDataMethod()
                self.createEventinTheCalendar(with: eventName, forDate: self.startEventDate, toDate: self.startEventDate)
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func createEventinTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    self.showAlert(message: "failed to save event with error : \(error)", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                    
                }
                
            } else {
                //we have error in getting access to device calnedar
                self.showAlert(message: error?.localizedDescription, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                
            }
        }
    }

}
