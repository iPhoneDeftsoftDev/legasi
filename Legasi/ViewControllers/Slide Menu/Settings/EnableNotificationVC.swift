//
//  EnableNotificationVC.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class EnableNotificationVC: BaseVC {
   
//MARK: IBOutlet
    @IBOutlet var enableNotificationTableView: UITableView!
    
//Variables
    var newPostbyschool:Int!
    var newPostbyconnectedUser:Int!
    var schooleventhappening:Int!
    var newmessage:Int!
    var newconnectionrequest:Int!
    var userreplytomypost:Int!
    var birthdayonconnecteduser:Int!
    var newjobupdatedbyconnecteduser:Int!
    let listArray = ["New post by school ","New post by connected user","School event is happening near me \n(30 miles/50km)","New message","New Connection request","User replies to my post","Birthday of connected user (based on the timezone of the birthday person)","New job update by connected user "]
  
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CustomiseUI()
    }
    //MARK: Private Function
    private func CustomiseUI() {
        
        setTitle(title: "NOTIFICATIONS")
        setBackButton()
        
        //Set Table View Height Automatic
        enableNotificationTableView.estimatedRowHeight = 60.0
        
        enableNotificationTableView.rowHeight = UITableViewAutomaticDimension
        enableNotificationTableView.tableFooterView = UIView()
    }
}

//MARK: UITableView Delegates
extension EnableNotificationVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kEnableNotificationCell, for: indexPath)as!EnableNotificationTableViewCell
        cell.notificationtypeLabel.text = self.listArray[indexPath.row]
        
        if indexPath.row == 0{
            if newPostbyschool == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        
        if indexPath.row == 1{
            if newPostbyconnectedUser == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        if indexPath.row == 2{
            if schooleventhappening == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
               cell.notificationSwitchButton.isOn = false
            }
        }

        if indexPath.row == 3{
            if newmessage == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        if indexPath.row == 4{
            if newconnectionrequest == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }

        if indexPath.row == 5{
            if userreplytomypost == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        if indexPath.row == 6{
            if birthdayonconnecteduser == 1{
               cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        if indexPath.row == 7{
            if newjobupdatedbyconnecteduser == 1{
                cell.notificationSwitchButton.isOn = true
            }else{
                cell.notificationSwitchButton.isOn = false
            }
        }
        cell.notificationSwitchButton.addTarget(self, action: #selector(self.tapSwitchAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.notificationSwitchButton.tag = indexPath.row
        return cell
    }
    //TableView Cell Button Action
    func tapSwitchAction(sender: UISwitch){
        sender.isOn = !sender.isOn
        
        
        switch sender.tag {
        case 0:
            newPostbyschool = newPostbyschool==1 ? 2: 1
            break
        case 1 :
            newPostbyconnectedUser = newPostbyconnectedUser==1 ? 2: 1
            break
        case 2 :
            schooleventhappening = schooleventhappening==1 ? 2: 1
            break
        case 3 :
            newmessage = newmessage==1 ? 2: 1
            break
        case 4 :
            newconnectionrequest = newconnectionrequest==1 ? 2: 1
            break
        case 5 :
            userreplytomypost = userreplytomypost==1 ? 2: 1
            break
        case 6 :
            birthdayonconnecteduser = birthdayonconnecteduser==1 ? 2: 1
            break
        case 7 :
            newjobupdatedbyconnecteduser = newjobupdatedbyconnecteduser==1 ? 2: 1
            break
        default :
            break
        }
        if (sender.isOn == true){
           SetNotificationData(value:2,type: sender.tag + 1)
        }
        else{
          SetNotificationData(value:1,type:sender.tag + 1)
        }
        
    }
    
}

//MARK: Webservice Method
extension EnableNotificationVC{
    
    func SetNotificationData(value:Int,type:Int){
        SettingsVM.sharedInstance.NotificationSettings(userId: DataManager.user_id!, value: value, notificationType: type) { (success, message , error) in
            if(success == 1) {
               self.enableNotificationTableView.reloadData()
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
}
