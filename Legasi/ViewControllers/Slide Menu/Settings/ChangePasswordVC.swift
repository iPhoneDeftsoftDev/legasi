//
//  ChangePasswordVC.swift
//  Legasi
//
//  Created by ios28 on 12/02/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseVC {

    // MARK: - IBOutlets
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customiseUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "CHANGE PASSWORD")
        setBackButton()
    }
    
    // MARK: - Actions
    @IBAction func submitButtonAction(_ sender: Any) {
        if oldPasswordTextField.isEmpty {
            self.showAlert(message: kEnterOldPassword)
        }else if oldPasswordTextField.text!.count < 6 {
            self.showAlert(message: kPasswordLimitMessage)
        }else if newPasswordTextField.isEmpty {
            self.showAlert(message: kEnterNewPassword)
        }else if newPasswordTextField.text!.count < 6 {
            self.showAlert(message: kNewPasswordLimitMessage)
        }else if confirmPasswordTextField.isEmpty {
            self.showAlert(message: kEnterConfirmPassword)
        }else if newPasswordTextField.text != confirmPasswordTextField.text {
            self.showAlert(message: KMatchNewPasswordMessage)
        }else {
            self.changePassword()
        }
    }
    

}

//MARK: - API methods
extension ChangePasswordVC {
    func changePassword(){
        SettingsVM.sharedInstance.changePassword(userId: DataManager.user_id!, oldPassword: self.oldPasswordTextField.text!, newPassword: self.newPasswordTextField.text!) { (success, message, error) in
            if(success == 1)
            {
                super.showAlert(message: message, cancelAction: { (action) in
                    let sideMenu = LeftSideBarVC()
                    sideMenu.logoutMethod()
                })
            }
            else
            {
                if(message != nil)
                {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }else {
                    self.showErrorMessage(error: error)
                }
                
            }
        }
        
    }
}

