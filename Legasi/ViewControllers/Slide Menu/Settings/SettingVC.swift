//
//  SettingVC.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SettingVC: BaseVC {
 //MARK: - Variables
    let titleArray = ["Privacy", "Notifications", "Change Password"]
//    let adminTitleArray = ["Change Password"]
   
    //MARK: IBOutlet
    @IBOutlet var settingTableView: UITableView!
    
    //Variable
    var dict = ProfileDetailData()
   
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CustomiseUI()
    }
    
    //MARK: Private Function
    private func CustomiseUI() {
        
        setTitle(title: "SETTINGS")
        setBackButton()
        
        //Set Table View Height Automatic
        settingTableView.tableFooterView = UIView()
        getProfileData()
    }
    
    //MARK: Override BAse VC Methods
    override func backButtonAction() {
        self.loadLeftSideMenu()
    }
}

//MARK: UITableViewDelegates
extension SettingVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if DataManager.role != 1 {
//            return adminTitleArray.count
//        }else {
            return titleArray.count
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kSettingCell, for: indexPath)
//        if DataManager.role != 1 {
//            (cell.viewWithTag(1) as! UILabel).text = self.adminTitleArray[indexPath.row]
//            }else {
                (cell.viewWithTag(1) as! UILabel).text = self.titleArray[indexPath.row]
//            }
        return cell
    }
}

//MARK: UITableView Delegates
extension SettingVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if DataManager.role != 1 {
//            if indexPath.row == 0 { //Change Password
//                let changePassword = self.storyboard?.instantiateViewController(withIdentifier: kChangePasswordVC) as! ChangePasswordVC
//                self.navigationController?.pushViewController(changePassword, animated: true)
//            }
//        } else {
            if indexPath.row == 0 { //Privacy
                let privacyvc = self.storyboard?.instantiateViewController(withIdentifier: kPrivacyVC) as! PrivacyVC
                privacyvc.shareLocation = dict.shareLocation
                privacyvc.studentStatus = dict.student
                privacyvc.graduateStatus = dict.graduate
                privacyvc.usersStatus = dict.connectedUsers
                privacyvc.schoolStatus = dict.school
                privacyvc.postSeenStatus = dict.postSeen
                self.navigationController?.pushViewController(privacyvc, animated: true)
            }
            else if indexPath.row == 1 {
                let privacyvc = self.storyboard?.instantiateViewController(withIdentifier: kEnableNotificationVC) as! EnableNotificationVC
                privacyvc.newPostbyschool = dict.newPostbyschool
                privacyvc.newPostbyconnectedUser = dict.newPostbyconnectedUser
                privacyvc.schooleventhappening = dict.schooleventhappening
                privacyvc.newmessage = dict.newmessage
                privacyvc.newconnectionrequest = dict.newconnectionrequest
                privacyvc.userreplytomypost = dict.userreplytomypost
                privacyvc.birthdayonconnecteduser = dict.birthdayonconnecteduser
                privacyvc.newjobupdatedbyconnecteduser = dict.newjobupdatedbyconnecteduser
                self.navigationController?.pushViewController(privacyvc, animated: true)
            }else {
                let changePassword = self.storyboard?.instantiateViewController(withIdentifier: kChangePasswordVC) as! ChangePasswordVC
                self.navigationController?.pushViewController(changePassword, animated: true)
            }
//        }
    }
}

//MARK: Webservice Method
extension SettingVC {
    
    func getProfileData(){
        
        SettingsVM.sharedInstance.GetOwnProfile(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
               self.dict = SettingsVM.sharedInstance.profileData

             }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
        
        
    }
    

}
