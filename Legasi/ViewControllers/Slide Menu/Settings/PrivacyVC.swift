//
//  PrivacyVC.swift
//  Legasi
//
//  Created by Apple on 10/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PrivacyVC: BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var publicTextField: UITextField!
    @IBOutlet weak var locationSwitchButton: UISwitch!
    @IBOutlet weak var userButton: UIButton!
    @IBOutlet weak var graduateButton: UIButton!
    @IBOutlet weak var schoolButton: UIButton!
    @IBOutlet weak var studentButton: UIButton!
  
    //MARK: - Variabels
    var selectedTextField: UITextField!
    var helpArray = ["public","student","school","\(DataManager.nameForGraduate!)"]
    var shareLocation = Int()
    var studentStatus = Int()
    var graduateStatus = Int()
    var usersStatus = Int()
    var schoolStatus = Int()
    var postSeenStatus = Int()
    var isStudent = false
    var isSchool = false
    var isGraduate = false
    var isUsers = false
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "PRIVACY")
        setBackButton()
        self.pickerDelegate = self
        publicTextField.delegate = self
        publicTextField.setPlaceholder()
       
        if shareLocation == 1{
            locationSwitchButton.isOn = true
        }else{
            locationSwitchButton.isOn = false
        }
        if studentStatus == 1{
            isStudent = true
            studentButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
        }else{
            isStudent = false
            studentButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
        }
        if graduateStatus == 1{
            isGraduate = true
            graduateButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
        }else{
            isGraduate = false
            graduateButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
        }
        if schoolStatus == 1{
            isSchool = true
            schoolButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
        }else{
            isSchool = false
            schoolButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
        }
        if usersStatus == 1{
            isUsers = true
            userButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
        }else{
            isUsers = false
            userButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
        }
        
        if postSeenStatus == 1{
            publicTextField.text = "public"
        }else if postSeenStatus == 2{
            publicTextField.text = "student"
        }else if postSeenStatus == 3{
            publicTextField.text = "school"
        }else{
            publicTextField.text = "\(DataManager.nameForGraduate!)"
        }
        
        
    }
//MARK: IBActions
    @IBAction func studentButtonAction(_ sender: Any) {
        if isStudent == true{
            studentButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
            studentStatus = 2
            isStudent = false
        }else{
            studentStatus = 1
            studentButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
            isStudent = true
        }
        
    }
    
    @IBAction func schoolButtonAction(_ sender: Any) {
        if isSchool == true{
            schoolButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
            schoolStatus = 2
            isSchool = false
        }else{
            schoolStatus = 1
            schoolButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
            isSchool = true
        }
    }
    
    @IBAction func graduateButtonAction(_ sender: Any) {
        if isGraduate == true{
            graduateButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
            graduateStatus = 2
            isGraduate = false
        }else{
            graduateStatus = 1
            graduateButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
            isGraduate = true
        }
    }
    
    @IBAction func userButtonAction(_ sender: Any) {
        if isUsers == true{
            userButton.setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
            usersStatus = 2
            isUsers = false
        }else{
            usersStatus = 1
            userButton.setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
            isUsers = true
        }
    }
    
    
    @IBAction func locationSitchAction(_ sender: UISwitch) {
        if (sender.isOn == true){
            shareLocation = 1
        }
        else{
            shareLocation = 2
        }
        
    }
    
    @IBAction func updateButtonAction(_ sender: Any) {
        SetPrivacyData()
    }
    
}


//MARK: - UITextField Delegate
extension PrivacyVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == publicTextField{
            publicTextField.text = helpArray[0]
            setPicker(textField: textField, array: helpArray)
        }
        selectedTextField = textField
    }
}


//chatCell
//MARK: - Custom Picker Delegate
extension PrivacyVC: LegasiPickerDelegate{
    
    func didSelectPickerViewAtIndex(index: Int) {
        
        if(selectedTextField == publicTextField) {
            
            publicTextField.text = helpArray[index]
            postSeenStatus = index + 1
        }
    }
}

//MARK:- Webservice Method
extension PrivacyVC{
    
    func SetPrivacyData(){
        SettingsVM.sharedInstance.PrivacySetting(userId: DataManager.user_id!, shareMylocation: shareLocation, student: studentStatus, graduates: graduateStatus, school: schoolStatus, connectedUsers: usersStatus, postSeen: postSeenStatus) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
}
