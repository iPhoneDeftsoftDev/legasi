//
//  EnableNotificationTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 24/01/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EnableNotificationTableViewCell: UITableViewCell {

    @IBOutlet var notificationSwitchButton: UISwitch!
    @IBOutlet var notificationtypeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
