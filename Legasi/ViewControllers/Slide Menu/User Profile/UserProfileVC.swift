//
//  UserProfileVC.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import MessageUI
import EventKit
import MapKit
class UserProfileVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet var menuView: UIView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var screenTopConstriant: NSLayoutConstraint!
    
    @IBOutlet var tableMainView: UIView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var groupCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userTypeLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var tabSegment: UISegmentedControl!
    @IBOutlet weak var detailsTableView: UITableView!
    
    //Variables
    var isFollow = false
    var otherUserId = 0
    var pageNumber = 1
    var apiRunning = false
    var pollsId = 0
    var isFollowing:Int!
    let store = EKEventStore()
    var startEventDate = Date()
    var userPhoneNumber: Int!
    var userName: String!

    //Class Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.PopViewTapped))
        self.menuView.addGestureRecognizer(tapGesture)
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name(rawValue: "UserProfileNotify"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchUserData(notification:)), name: NSNotification.Name(rawValue: "UserProfileNotify"), object: nil)
    }
    //MARK: NSNOTIFICATION METHOD
    func fetchUserData(notification: NSNotification){
        print(notification)
        let postDict = notification.userInfo as NSDictionary!
        otherUserId = postDict?["otherUserId"]as!Int
        self.getOtherUserProfile(otherUserId: otherUserId)
    }
    @objc private func PopViewTapped() {
        menuView.removeFromSuperview()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        detailsTableView.beginUpdates()
        //        detailsTableView.endUpdates()
    }
    
//Private Methods
    private func customiseUI() {
        if UIScreen.main.bounds.height >= 712 {
            screenTopConstriant.constant = 55
        }
        self.navigationController?.isNavigationBarHidden = true
        pageNumber = 1
        followButton.setRadius(radius: 5.0, borderColor: UIColor.white, borderWidth: 1.5)
        profileImageView.setRadius(radius: 5.0)
        
        let attr = NSDictionary(object: UIFont.LegasiFont.regular.fontWithSize(size: 17.0), forKey: NSFontAttributeName as NSCopying)
        tabSegment.setTitleTextAttributes(attr as? [AnyHashable : Any], for: .normal)
        
        //Set Table View Height Automatic
        detailsTableView.estimatedRowHeight = 40.0
        detailsTableView.rowHeight = UITableViewAutomaticDimension
        tabSegment.selectedSegmentIndex = 0
        self.view.backgroundColor = UIColor.white
        self.tableMainView.backgroundColor = UIColor.white
        if otherUserId == DataManager.user_id{
            menuButton.isHidden = true
            followButton.isHidden = true
        }else{
           menuButton.isHidden = false
           followButton.isHidden = false
        }
        
        
        if otherUserId != 0 {
            self.getOtherUserProfile(otherUserId: otherUserId)
        }
        likeImageView.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), placeholderImage: #imageLiteral(resourceName: "likeBlue"))
    }
    
    //MARK: IBActions
    
    func tapLikeButtonAction(sender:UIButton){

        if UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].isLike == 1 {
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].eventId, type: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].type!.rawValue)
        }else {
            self.likeOnNewsFeedMethod(likeValue: 1, eventId: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].eventId, type: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].type!.rawValue)
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.backButtonAction()
    }
    
    //MARK: - MAIL BUTTON ACTION
    @IBAction func mailButtonAction(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["abc@gmail.com"])
        composeVC.setSubject("Legasi")
        composeVC.setMessageBody("Hey Josh! This is testing mail.", isHTML: false)
        
        if MFMailComposeViewController.canSendMail() {
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    //MARK: - CHAT BUTTON ACTION
    @IBAction func chatButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
        nextObj.titleStr = UserLoginVM.sharedInstance.userProfileData!.name
        nextObj.friendId = UserLoginVM.sharedInstance.userProfileData!.userId
        nextObj.chatListId = UserLoginVM.sharedInstance.userProfileData!.chatListId
        nextObj.otherUserProfilePic = UserLoginVM.sharedInstance.userProfileData!.profileImageUrl
        nextObj.otherUserName = UserLoginVM.sharedInstance.userProfileData.name
        self.navigationController?.pushViewController(nextObj, animated: false)
 
    }
    
    @IBAction func blockButtonAction(_ sender: Any) {
        self.showAlert(message: "Are you sure you want to block this user?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
               
                self.blockUnblockMethod(friendId:self.otherUserId, status:1)
         })
        
       
    }
    //MARK: - CALL BUTTON ACTION
    @IBAction func callButtonAction(_ sender: Any) {
        if self.userPhoneNumber == 0 {
             self.showAlert(message: nil, title: "\(self.userName ?? "") Phone Number isn't Available", otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            return
        }
        if let url = URL(string: "tel://\(self.userPhoneNumber!)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func followButtonAction(_ sender: UIButton) {
        if isFollowing == 1{
            self.followUserMethod(type:2)
        }
        else {
            self.followUserMethod(type:1)
        }
    }
    
    @IBAction func tabSergmentAction(_ sender: UISegmentedControl) {
        if tabSegment.selectedSegmentIndex == 0 {
            self.view.backgroundColor = UIColor.white
            self.tableMainView.backgroundColor = UIColor.white
            self.detailsTableView.backgroundColor = UIColor.white
        }else{
            self.view.backgroundColor = UIColor.LegasiColor.lightGray.colorWith(alpha: 1.0)
            self.tableMainView.backgroundColor = UIColor.LegasiColor.lightGray.colorWith(alpha: 1.0)
            self.detailsTableView.backgroundColor = UIColor.LegasiColor.lightGray.colorWith(alpha: 1.0)
            
        }
        self.tabSegment.backgroundColor = UIColor.white
        detailsTableView.reloadData()
    }
    
    @IBAction func menuButtonAction(_ sender: Any) {
        
        menuView.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height)
        self.view.addSubview(menuView)
        
    }
}


//MARK: UITableView Datasource and Delegtes
extension UserProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabSegment.selectedSegmentIndex == 0 {
            if UserLoginVM.sharedInstance.userProfileData != nil {
//                if DataManager.role != 1 {
//                    // for admin
                    if UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0 && UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 {
                        return 4
                    }
                    if UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0 || UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 {
                        return 3
                    }
                
                    return 2
//                }
//                if UserLoginVM.sharedInstance.userProfileData.workDetail.count > 0 {
//                    return 3
//                }
//                return 2
                //i returned 3 becoz past jobs are not available otherwise return 5
            }
            else {
                return 0
            }
        }
        else {
            return UserLoginVM.sharedInstance.otherUserPostDataArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tabSegment.selectedSegmentIndex == 0 {
            let cell: UserMainCell = UITableViewCell.fromNib() //User Details Cell
            cell.role = self.userTypeLabel.text!
            cell.selectionStyle = .none
            if UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0 && indexPath.row == 0 {
                cell.titleView.text = "CURRENT JOBS"
                let workData = UserLoginVM.sharedInstance.userProfileData.currentWorkDetail
                cell.loadData(currentWorkData: workData, pastWorkData: nil, collegeData: nil, type: "current job")
                
                return cell
            }else if UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 && indexPath.row == 0 {
                cell.titleView.text = "PAST JOBS"
                let workData = UserLoginVM.sharedInstance.userProfileData.pastWorkDetail
                cell.loadData(currentWorkData: nil, pastWorkData: workData, collegeData: nil, type: "past job")
                
                return cell
            }else if UserLoginVM.sharedInstance.userProfileData.educationDetail.count > 0 && indexPath.row == 0 {
                cell.titleView.text = "COLLEGE INFO"
                let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
                cell.loadData(currentWorkData: nil, pastWorkData: nil, collegeData: educationData, type: "college info")
                
                return cell
            }else if UserLoginVM.sharedInstance.userProfileData.skillDetail.count > 0 && indexPath.row == 0{
                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
                cell.loadData(data: skillsData)
                return cell
            }else if (UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 && UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0) && indexPath.row == 1 {
                cell.titleView.text = "PAST JOBS"
                let workData = UserLoginVM.sharedInstance.userProfileData.pastWorkDetail
                cell.loadData(currentWorkData: nil, pastWorkData: workData, collegeData: nil, type: "past job")
                return cell
            }else if
                ((UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 || UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0) && (UserLoginVM.sharedInstance.userProfileData.educationDetail.count > 0  && indexPath.row == 1)){
                cell.titleView.text = "COLLEGE INFO"
                let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
                cell.loadData(currentWorkData: nil, pastWorkData: nil, collegeData: educationData, type: "college info")
                
                return cell
            }else if (((UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 || UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0) || UserLoginVM.sharedInstance.userProfileData.educationDetail.count > 0) && (UserLoginVM.sharedInstance.userProfileData.skillDetail.count > 0  && indexPath.row == 1)){
                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
                cell.loadData(data: skillsData)
                return cell
            }
            else if ((UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0 && UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0) && (UserLoginVM.sharedInstance.userProfileData.educationDetail.count > 0 && indexPath.row == 2)) {
                cell.titleView.text = "COLLEGE INFO"
                let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
                cell.loadData(currentWorkData: nil, pastWorkData: nil, collegeData: educationData, type: "college info")

                return cell
            }
            else if ((UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 && UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0) &&  ((UserLoginVM.sharedInstance.userProfileData.educationDetail.count == 0 && UserLoginVM.sharedInstance.userProfileData.skillDetail.count > 0)  && indexPath.row == 2)){
                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
                cell.loadData(data: skillsData)
                return cell
                
            }else if ((UserLoginVM.sharedInstance.userProfileData.pastWorkDetail.count > 0 || UserLoginVM.sharedInstance.userProfileData.currentWorkDetail.count > 0) &&  ((UserLoginVM.sharedInstance.userProfileData.educationDetail.count > 0 && UserLoginVM.sharedInstance.userProfileData.skillDetail.count > 0)  && indexPath.row == 2)){
                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
                cell.loadData(data: skillsData)
                return cell
                
            }
//            else {
//                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
//                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
//                cell.loadData(data: skillsData)
//                return cell
//            }
            
//            if indexPath.row == 2 {
//                let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
//                let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
//                cell.loadData(data: skillsData)
//                return cell
//            }
//            else {
//                let cell: UserMainCell = UITableViewCell.fromNib() //User Details Cell
//                /*if DataManager.role != 1 && indexPath.row == 0 {
//                    cell.titleView.text = "COLLEGE INFO"
//                    let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
//                    cell.loadData(workData: nil, collegeData: educationData, type: "college info")
//                }
//                else */
//
//                if UserLoginVM.sharedInstance.userProfileData.workDetail.count > 0 && indexPath.row == 0 {
//                    let workData = UserLoginVM.sharedInstance.userProfileData.workDetail
//                    cell.loadData(workData: workData, collegeData: nil, type: "current job")
//
//                }else if UserLoginVM.sharedInstance.userProfileData.workDetail.count == 0 && indexPath.row == 0 {
//                    cell.titleView.text = "COLLEGE INFO"
//                    let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
//                    cell.loadData(workData: nil, collegeData: educationData, type: "college info")
//                }
//
//                if UserLoginVM.sharedInstance.userProfileData.workDetail.count > 0 && indexPath.row == 1 {
//                    cell.titleView.text = "COLLEGE INFO"
//                    let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
//                    cell.loadData(workData: nil, collegeData: educationData, type: "college info")
//                }else if UserLoginVM.sharedInstance.userProfileData.workDetail.count == 0 && indexPath.row == 1 {
//                    let cell: SkillCell = UITableViewCell.fromNib() //Skill Cell
//                    let skillsData = UserLoginVM.sharedInstance.userProfileData.skillDetail
//                    cell.loadData(data: skillsData)
//                    return cell
//                }
//
//
//                if indexPath.row == 1 {
//                    cell.titleView.text = "COLLEGE INFO"
//                    let educationData = UserLoginVM.sharedInstance.userProfileData.educationDetail
//                    cell.loadData(workData: nil, collegeData: educationData, type: "college info")
//                }else{
//                    let workData = UserLoginVM.sharedInstance.userProfileData.workDetail
//                    cell.loadData(workData: workData, collegeData: nil, type: "current job")
//                }
//                return cell
//            }
        }
        else {
            let type = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].type
            
            if type == NewsFeedType.normal {
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].name
                post1TableCell.linkButtonHeight.constant = 0
                post1TableCell.linkTopConstraint.constant = 0
                let decodeMsg = (UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                    post1TableCell.titleNameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].title
                }else{
                   post1TableCell.titleNameLabel.text = decodeMsg
                }
                
                post1TableCell.descriptionLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].Description
                post1TableCell.descriptionLabel.numberOfLines = 2
                post1TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.nameButton.tag = indexPath.row
                let image = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].profilePic
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post1TableCell.delegate = self
                post1TableCell.timeLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].time
                post1TableCell.dummyArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].newsFeedTagList
                post1TableCell.pollDelegate = self
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userRole == 1{
                    if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                let imageArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }
                
                post1TableCell.likesLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].likes!)"
                post1TableCell.commentLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].comments!)"
                let isAllowLocation = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowLocation
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].location!
                }

                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                
                post1TableCell.loadData()
                
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row
                post1TableCell.commentButton.isUserInteractionEnabled = false
                return post1TableCell
            }else if type == NewsFeedType.job {
                
                let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
                let image = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].profilePic
                
                jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                jobCell.nameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].name
                let concatStr:NSMutableAttributedString = NSMutableAttributedString()
                let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributedTitle = NSAttributedString(string: "Job Title:" , attributes: myAttributeBlackColor)
                
                let decodeMsg = (UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].jobName!).fromBase64()
                if(decodeMsg == nil){
                    let attributedStringTitle = NSAttributedString(string: " \(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].jobName!)" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }else{
                    let attributedStringTitle = NSAttributedString(string: " \(decodeMsg ?? "")" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }
                
                jobCell.titleLabel.attributedText = concatStr
                
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userRole == 1{
                    if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userType == "Student"{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                
                let concat1Str:NSMutableAttributedString = NSMutableAttributedString()
                let myAttribute1BlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttribute1GrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributed1Title = NSAttributedString(string: "Company:" , attributes: myAttribute1BlackColor)
                let attributed1StringTitle = NSAttributedString(string: " \(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].companyName!)" , attributes: myAttribute1GrayColor)
                concat1Str.append(myAttributed1Title)
                concat1Str.append(attributed1StringTitle)
                jobCell.companyName.attributedText = concat1Str
                let isAllowLocation = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowLocation
                if isAllowLocation != 1{
                    jobCell.locationButtonHeight.constant = 0
                    jobCell.locationLabel.text = ""
                } else{
                    jobCell.locationLabel.sizeToFit()
                    jobCell.locationLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].location
                }
               
                jobCell.skillsLabel.text = ""
                jobCell.skillsTopConstraint.constant = 0
                jobCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.nameButton.tag = indexPath.row
                var type = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].jobType
                if type == "1"{
                    type = "Full Time Job"
                }else if type == "2"{
                    type = "Part Time Job"
                }else if type == "3"{
                    type = "Paid Internship Job"
                }else {
                    type = "Unpaid Internship Job"
                }
                jobCell.jobTypeLabel.text = "\(type!)"
                jobCell.isJobVC = false
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowComment == 1 {
                    jobCell.commentButton.isHidden = false
                    jobCell.commentLabel.isHidden = false
                }else{
                    jobCell.commentButton.isHidden = true
                    jobCell.commentLabel.isHidden = true
                }
                
                jobCell.likeLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].likes!)"
                jobCell.commentLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].comments!)"
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].status == 1{
                    jobCell.availableView.backgroundColor = UIColor.black
                    jobCell.availableLabel.text = "Available"
                }else{
                    jobCell.availableView.backgroundColor = UIColor.darkGray
                    jobCell.availableLabel.text = "Unavailable"
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                jobCell.decriptionLabel.text = ""
                jobCell.timeLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].time
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                jobCell.locationLabel.isUserInteractionEnabled = true
                jobCell.locationLabel.tag = indexPath.row
                jobCell.locationLabel.addGestureRecognizer(tapGesture)
                

                jobCell.locationLabel.sizeToFit()
                jobCell.titleLabel.sizeToFit()
                jobCell.companyName.sizeToFit()
                jobCell.skillsLabel.sizeToFit()
                jobCell.loadData()
                
                jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.likeButton.tag = indexPath.row
                jobCell.commentButton.isUserInteractionEnabled = false
                return jobCell
            }else if type == NewsFeedType.pollSubmit {
                
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].name
                post1TableCell.linkButtonHeight.constant = 0
                post1TableCell.linkTopConstraint.constant = 0
              
                let decodeMsg = (UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                    post1TableCell.titleNameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].title
                }else{
                    post1TableCell.titleNameLabel.text = decodeMsg
                }
                
                post1TableCell.descriptionLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].Description
                post1TableCell.descriptionLabel.numberOfLines = 2
                let image = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].profilePic
                
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userRole == 1{
                    if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                post1TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.nameButton.tag = indexPath.row
                post1TableCell.likesLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].likes!)"
                post1TableCell.commentLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].comments!)"
                post1TableCell.timeLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].time
                post1TableCell.pollDelegate = self
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                post1TableCell.dummyArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].newsFeedTagList
                let imageArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                post1TableCell.delegate = self
                let isAllowLocation = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowLocation
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].location!
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }
                post1TableCell.pollSubmitButton.addTarget(self, action: #selector(self.tapPollSubmitButton(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.pollSubmitButton.tag = indexPath.row
                post1TableCell.locationLabel.sizeToFit()
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                    
                }else{
                    post1TableCell.pollArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].pollList
                    //polled by me
                    if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isPolled == "Yes"{
                        post1TableCell.barGraphTypeHeight.constant = 0
                        post1TableCell.submitButtonHeight.constant = 0
                        post1TableCell.pollViewHeight.constant = 200
                    }else {//polled by other
                        post1TableCell.barGraphTypeHeight.constant = 197
                        post1TableCell.submitButtonHeight.constant = 90
                        if post1TableCell.pollArray.count == 5 {
                            post1TableCell.pollViewHeight.constant = 484 + 30
                        }else{
                            post1TableCell.pollViewHeight.constant = 484
                        }
                        
                    }
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                

                post1TableCell.loadData()
                
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row
                post1TableCell.commentButton.isUserInteractionEnabled = false
                return post1TableCell
                
            } else {
                let post2TableCell : Post2TableCell = UITableViewCell.fromNib()
                
                post2TableCell.nameLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].name
                
                let decodeMsg = (NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                    post2TableCell.titleLabel.text = NewsFeedVM.sharedInstance.newsFeedArray[indexPath.row].title
                }else{
                    post2TableCell.titleLabel.text = decodeMsg
                }
                
                post2TableCell.descriptionLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].Description
                post2TableCell.descriptionLabel.numberOfLines = 2
               
                post2TableCell.linkButtonHeight.constant = 0
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userRole == 1{
                    if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].userType == "Student"{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowComment == 1 {
                    post2TableCell.commentButton.isHidden = false
                    post2TableCell.commentLabel.isHidden = false
                }else{
                    post2TableCell.commentButton.isHidden = true
                    post2TableCell.commentLabel.isHidden = true
                }
                post2TableCell.likesLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].likes!)"
                post2TableCell.commentLabel.text = "\(UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].comments!)"
                let image = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].profilePic
                post2TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post2TableCell.nameButton.tag = indexPath.row
                post2TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post2TableCell.dateLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].time
                post2TableCell.dummyArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].newsFeedTagList
                
                let imageArray = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post2TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post2TableCell.imageArray = imageArray
                }
                if UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                let isAllowLocation = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].isAllowLocation
                if isAllowLocation != 1{
                    post2TableCell.locationButtonHeight.constant = 0
                    post2TableCell.locationLabel.text = ""
                } else{
                    post2TableCell.locationLabel.sizeToFit()
                    post2TableCell.locationLabel.text = UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].location!
                }
                 post2TableCell.rsvpViewHeightConstraint.constant = 0
                
               
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post2TableCell.locationLabel.isUserInteractionEnabled = true
                post2TableCell.locationLabel.tag = indexPath.row
                post2TableCell.locationLabel.addGestureRecognizer(tapGesture)
                
                post2TableCell.commentButton.isUserInteractionEnabled = false
                post2TableCell.delegate = self
                post2TableCell.loadData()
                return post2TableCell
            }
        }
         let cell = UITableViewCell()
         cell.selectionStyle = .none
         return cell
    }
//TableView Cell Button Action
    func lblClick(tapGesture:UITapGestureRecognizer){
        if ConnectionCheck.isConnectedToNetwork(){
            LocationManager.sharedManager.getCoordinates(address: UserLoginVM.sharedInstance.otherUserPostDataArray[(tapGesture.view?.tag)!].location!) { (coordinates) in
                let latitude: CLLocationDegrees = coordinates!.latitude
                let longitude: CLLocationDegrees = coordinates!.longitude
                
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = UserLoginVM.sharedInstance.otherUserPostDataArray[(tapGesture.view?.tag)!].location!
                mapItem.openInMaps(launchOptions: options)
            }
        }else {
            
            self.showAlert(message: kInternetConnectionMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }
        
    }

    func tapRSVPButtonAction(sender:UIButton){
        let dateStr = UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].date
        startEventDate = (dateStr?.dateFromString(format: .shortDMYDate))!
        submitRSVPMethod(event_id: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].eventId, value: 1, eventName: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].title)
    }

    func tapLinkButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kLinkWebViewVC)as!LinkWebViewVC
        nextObj.linkUrl = UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].link
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tabSegment.selectedSegmentIndex == 0 {
            return
        }
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
        nextObj.newsFeedId =  UserLoginVM.sharedInstance.otherUserPostDataArray[indexPath.row].eventId
        nextObj.isThroughMyPost = true
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].userId!
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        if UserLoginVM.sharedInstance.userProfileData != nil {
        //            if tabSegment.selectedSegmentIndex == 0 {
        //                if indexPath.row == 2 {
        //                    (cell as! SkillCell).collectionHeightConstraint.constant = (cell as! SkillCell).skillCollectionView.contentSize.height+70.0
        //                    (cell as! SkillCell).updateConstraints()
        //                    self.view.layoutIfNeeded()
        //                }
        //                else {
        //                    (cell as! UserMainCell).tableHeightConstraint.constant = (cell as! UserMainCell).descriptionTableView.contentSize.height+70.0
        //                    (cell as! UserMainCell).updateConstraints()
        //                    self.view.layoutIfNeeded()
        //                }
        //            }
        //        }
    }
    
    func tapPollSubmitButton(sender:UIButton){
        pollSubmitData(pollId: pollsId, eventId: UserLoginVM.sharedInstance.otherUserPostDataArray[sender.tag].eventId)
    }
    
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.tabSegment.selectedSegmentIndex == 1 {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                Indicator.isEnabledIndicator = false
                if !apiRunning {
                    self.getOtherUserPost()
                }
            }
        }
    }
    
}
//MARK: Custom Delegate Method
extension UserProfileVC : VideoPlayerDelegates, VideoPlayersDelegates, PollSubmitDelegates{
    
    func getVideoDetail(videoUrl: String, name: String) {
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func getVideosDetail(videoUrl: String, name: String) {
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func pollSubmitMethod(pollId: Int) {
        pollsId = pollId
    }
}

//MARK: - WEBSERVICES
extension UserProfileVC {
    //MARK: -= GET OTHER USER PROFILE DATA
    func getOtherUserProfile(otherUserId: Int){
        UserLoginVM.sharedInstance.getOtherUserProfile(otherUserId: otherUserId) { (success, message , error) in
            if(success == 1) {
                self.detailsTableView.reloadData()
                self.getOtherUserPost()
                
                DispatchQueue.main.async{
                    let data = UserLoginVM.sharedInstance.userProfileData!
                    DataManager.totalLikes = data.totalLike
                    self.likeCountLabel.text = String(data.totalLike)
                    self.groupCountLabel.text = String(data.groupParticipationCount)
                    if data.profileImageUrl != ""{
                        self.profileImageView.sd_setImage(with: URL(string: data.profileImageUrl)!)
                    }
                    if data.coverImageUrl != ""{
                        self.coverImageView.sd_setImage(with: URL(string: data.coverImageUrl)!)
                    }
                    self.nameLabel.text = data.name
                    self.userPhoneNumber = data.phoneNumber
                    self.userName = data.name
                    self.isFollowing = data.isFollowing
                    if self.isFollowing == 1{
                        self.followButton.setTitle("UNFOLLOW", for: .normal)
                    }else{
                        self.followButton.setTitle("FOLLOW", for: .normal)
                    }
                    if data.role == 1 {
                        // graduate or student
                        if data.userType == "Student"{
                            self.userTypeLabel.text = data.userType
                        }else{
                            self.userTypeLabel.text = DataManager.nameForGraduate!
                        }
                    }else {     //in case 2 or 3
                        //admin
                        self.userTypeLabel.text = "School admin"
                    }
                    
                    
                    self.yearLabel.text = data.graduationYear
                }
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    //MARK: Follow/Unfollow WS
    func pollSubmitData(pollId: Int, eventId:Int){
        NewsFeedVM.sharedInstance.voteOnPoll(userId: DataManager.user_id!, eventId: eventId, pollId: pollId) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                UserLoginVM.sharedInstance.otherUserPostDataArray = [NewsFeedData]()
                self.getOtherUserPost()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func followUserMethod(type:Int){
        UserLoginVM.sharedInstance.followUnfollowUser(userId: DataManager.user_id!, friendId: self.otherUserId, value: type) { (success, message , error) in
            if(success == 1) {
                
                self.getOtherUserProfile(otherUserId: self.otherUserId)
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int, type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                
                self.pageNumber = 1
                ConnectionsVM.sharedInstance.groupDetailArray = [NewsFeedData]()
                self.getOtherUserPost()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    //MARK: - OTHER USER POST WS
    func getOtherUserPost(){
        apiRunning = true
        UserLoginVM.sharedInstance.otherUserPost(otherUserId: self.otherUserId,page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.pageNumber += 1
                self.detailsTableView.reloadData()
                
            }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                        //self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                    }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.detailsTableView.reloadData()
            }
            
            self.apiRunning = false
        }
    }
    func blockUnblockMethod(friendId:Int, status:Int){
        self.menuView.removeFromSuperview()
        InboxVM.sharedInstance.BlockUnblockUser(userId: DataManager.user_id!, otherUserId: friendId, value: status) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func submitRSVPMethod(event_id:Int, value:Int, eventName:String){
        var value = value
        if value == 0{
            value = 2
        }
        NewsFeedVM.sharedInstance.SubmitRSVPRequest(userId: DataManager.user_id!, eventId: event_id, value: value) { (success, message , error) in
            if(success == 1) {
                self.getOtherUserPost()
                self.createEventinTheCalendar(with: eventName, forDate: self.startEventDate, toDate: self.startEventDate)
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func createEventinTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    self.showAlert(message: "failed to save event with error : \(error)", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                    
                }
                
            } else {
                //we have error in getting access to device calnedar
                self.showAlert(message: error?.localizedDescription, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                
            }
        }
    }
}
//MARK: MAIL Delegates
extension UserProfileVC : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        print("Error while sending message.")
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}
