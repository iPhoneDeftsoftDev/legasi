//
//  UserMainCell.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class UserMainCell: UITableViewCell {
    
    //MARK: IBOUtlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var descriptionTableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    var currentWorkDataArray = [workDetailData]()
    var pastWorkDataArray = [workDetailData]()
    var collegeDataArray = [educationDetailData]()
    var dataType = ""
    var role = String()

    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        layoutIfNeeded()
    }
    
    
    
    
    
    //MARK: Load Data
    func loadData(currentWorkData: [workDetailData]?, pastWorkData: [workDetailData]?, collegeData : [educationDetailData]?, type : String) {
        
        dataType = type
        if currentWorkData != nil{
            currentWorkDataArray = currentWorkData!
        }
        
        if pastWorkData != nil{
            pastWorkDataArray = pastWorkData!
        }
        
        if collegeData != nil{
            collegeDataArray = collegeData!
        }
        
        //Set Shadow to View
        shadowView.layer.cornerRadius = 7
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 2
        shadowView.layer.shadowOpacity = 0.3
        
        descriptionTableView.dataSource = self
        descriptionTableView.delegate = self
        
        //Set Table View Height Automatic
        descriptionTableView.estimatedRowHeight = 40.0
        descriptionTableView.rowHeight = UITableViewAutomaticDimension
    }
    
}

extension UserMainCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataType == "current job"{
            return currentWorkDataArray.count
            
        }else if dataType == "past job"{
            return pastWorkDataArray.count
            
        }else{
            return collegeDataArray.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserDetailCell = UITableViewCell.fromNib() //User Details Cell
        if dataType == "current job"{
            let data = currentWorkDataArray[indexPath.row]
            if indexPath.row == currentWorkDataArray.count - 1 {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            }
            cell.title1Label.text = "Job Title"
            cell.description1Label.text = data.jobTitle
            if role == "School admin"{
                cell.title2Label.text = "School Name"
                cell.titleIndustryLabel.text = "Year"
                cell.industryLabel.text = "Since \(data.sinceYear!)"
                cell.locationview.isHidden = true
                
            
            }else{
                cell.title2Label.text = "Company Name"
                cell.titleIndustryLabel.text = "Industry Type"
                cell.industryLabel.text = data.industryType
                
                cell.titleLocationLabel.text = "Location"
                cell.locationLabel.text = data.jobLocation
            }
            cell.description2Label.text = data.companyName
            
            cell.title3Label.text = "Job Description"
            cell.description3Label.text = data.jobDiscription
            
           
            
        }else if dataType == "past job"{
            let data = pastWorkDataArray[indexPath.row]
            if indexPath.row == pastWorkDataArray.count - 1 {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            }
            cell.title1Label.text = "Job Title"
            cell.description1Label.text = data.jobTitle
            
            cell.title2Label.text = "Company Name"
            cell.description2Label.text = data.companyName
            
            cell.title3Label.text = "Job Description"
            cell.description3Label.text = data.jobDiscription
           
            cell.titleIndustryLabel.text = "Industry Type"
            cell.industryLabel.text = data.industryType
            
            cell.titleLocationLabel.text = "Location"
            cell.locationLabel.text = data.jobLocation
            
            
            
        }else{
            let data = collegeDataArray[indexPath.row]
            if indexPath.row == collegeDataArray.count - 1 {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            }
            cell.title1Label.text = "College Name"
            cell.description1Label.text = data.collegeName
            
            cell.title2Label.text = "Degree"
            cell.description2Label.text = data.degreeType
            
            cell.title3Label.text = "Year"
            cell.description3Label.text = data.graduationYear
            
            cell.stackLocationView.isHidden = true
        }
        
        return cell
    }
}

extension UserMainCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      
        if dataType == "current job"{
//            if currentWorkDataArray.count > 1{
                tableHeightConstraint.constant = descriptionTableView.contentSize.height + 20
//            }else{
//                tableHeightConstraint.constant = descriptionTableView.contentSize.height
//            }
        }else if dataType == "past job"{
//            if pastWorkDataArray.count > 1{
                tableHeightConstraint.constant = descriptionTableView.contentSize.height + 20
//            }else{
//                tableHeightConstraint.constant = descriptionTableView.contentSize.height
//            }
        } else{
            if collegeDataArray.count > 1{
                tableHeightConstraint.constant = descriptionTableView.contentSize.height + 20
            } else{
               tableHeightConstraint.constant = descriptionTableView.contentSize.height
            }
        }
        
    }
}
