//
//  SkillCollectionCell.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SkillCollectionCell: UICollectionViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
