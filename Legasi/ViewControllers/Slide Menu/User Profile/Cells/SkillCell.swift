//
//  SkillCell.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SkillCell: UITableViewCell {
    
    //MARK: IBOutelts
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var skillCollectionView: UICollectionView!
    
    @IBOutlet weak var noRecordLabel: UILabel!
    
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    var skillsDataArray = [skillsDetailData]()
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        layoutIfNeeded()
    }
    
    //MARK: Load Data
    func loadData(data : [skillsDetailData]) {
        
        skillsDataArray = data
        //Set Shadow to View
        shadowView.layer.cornerRadius = 7
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 2
        shadowView.layer.shadowOpacity = 0.3
      
        if skillsDataArray.count == 0 {
            skillCollectionView.isHidden = true
            noRecordLabel.isHidden = false
        }else{
             skillCollectionView.isHidden = false
            noRecordLabel.isHidden = true

            //Set Collectionview Flow Layout
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top:10, left: 10, bottom: 0, right: 0)
            layout.minimumLineSpacing = 0.0
            layout.minimumInteritemSpacing = 0.0
            layout.itemSize = CGSize(width:(skillCollectionView.frame.width/2) - 30, height: 50.0)
            
            skillCollectionView.dataSource = self
            skillCollectionView.delegate = self
            skillCollectionView.bounces = false
            skillCollectionView.isPagingEnabled = false
            skillCollectionView.showsVerticalScrollIndicator = false
            skillCollectionView.showsHorizontalScrollIndicator = false
            skillCollectionView.collectionViewLayout = layout
            skillCollectionView.register(UINib(nibName: "SkillCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SkillCollectionCell")
        }
         titleLabel.text = "SKILLS"
      }
}

extension SkillCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return skillsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillCollectionCell", for: indexPath) as! SkillCollectionCell
        cell.titleLabel.text = "skill ".appending(String(indexPath.row + 1))
        cell.descriptionLabel.text = skillsDataArray[indexPath.row].jobTitle
        return cell
    }
}
//MARK: Collection View Delegates
extension SkillCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:(skillCollectionView.frame.width/2)-30, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        collectionHeightConstraint.constant = collectionView.contentSize.height
    }
}

