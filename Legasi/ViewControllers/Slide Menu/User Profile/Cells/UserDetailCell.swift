//
//  UserDetailCell.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class UserDetailCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var stackLocationView: UIStackView!
    @IBOutlet weak var description1Label: UILabel!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var description2Label: UILabel!
    
    @IBOutlet weak var locationview: UIView!
    @IBOutlet weak var title3Label: UILabel!
    @IBOutlet weak var description3Label: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var titleLocationLabel: UILabel!
    @IBOutlet weak var industryLabel: UILabel!
    @IBOutlet weak var titleIndustryLabel: UILabel!
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
