//
//  EditProfileVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class EditProfileVC: BaseVC {
//MARK: Class Life Cycle Method
   
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var coverImageUploadButton: UIButton!
    @IBOutlet weak var userImageUploadButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
   
    //MARK: Variables
    var profileDict = ProfileDetailData()
    var imageDict = [String:Data]()
    var imageName = String()
  
//MARK: Class Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK: Private Method
    private func customiseUI() {
        setTitle(title: "EDIT PROFILE")
        setBackButton()
        coverImageUploadButton.layer.borderWidth = 1.0
         coverImageUploadButton.layer.cornerRadius = 10.0
        coverImageUploadButton.layer.borderColor = UIColor.white.cgColor
      //Set Profile Data
        DispatchQueue.main.async{
            
            self.userImageView.sd_setImage(with: URL(string:"\(self.profileDict.profileImage!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            
            self.coverImageView.sd_setImage(with: URL(string:"\(self.profileDict.coverImage!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
           
            self.nameLabel.text = self.profileDict.userName
            self.nameTextField.text = self.profileDict.fullName
            self.userNameTextField.text = self.profileDict.userName
            self.countryCodeTextField.text = self.profileDict.countryCode
            self.emailTextField.text = self.profileDict.emailAddress
            self.phoneNumberTextField.text = self.profileDict.phoneNumber
            
        }
        
    }
  //MARK: IBActions
    @IBAction func userImageUploadAction(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePickerDelegate = self
        self.imageName = "profileImage"
        self.showImagePicker()

    }
    @IBAction func coverImageUploadAction(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePickerDelegate = self
        self.imageName = "coverImage"
        self.showImagePicker()
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        
        var phonenumberlength: String = phoneNumberTextField.text!
        let length = phonenumberlength.characters.count
        
        
        if nameTextField.text == kEmptyString || ((nameTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kNameValidMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if userNameTextField.text == kEmptyString || ((userNameTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kUserNameMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if countryCodeTextField.text == kEmptyString || ((countryCodeTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            self.showAlert(message: kCountryCodeMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if length  > 14 || length < 7 && length > 0
        {
            self.showAlert(message: kPhoneMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
            
        }
        else{
            editProfileMethod()
            
        }
    }

}
//MARK: Custom Image Picker Delegates
extension EditProfileVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {  //Get Image from Picker and use the image
        imageDict[imageName] = image.jpegData(.medium)
        
        if imageName == "profileImage" {
            userImageView.image = image
        }else{
            coverImageView.image = image
            
            
        }
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
        imageDict[imageName] = thumbnail.jpegData(.medium)
        if imageName == "profileImage" {
            userImageView.image = thumbnail
        }else{
            coverImageView.image = thumbnail
            
            
        }
    }
    
}

//MARK: Webservice Method
extension EditProfileVC {
    
    func editProfileMethod(){
        SettingsVM.sharedInstance.UpdateProfileMethod(userId: DataManager.user_id!, fullName: nameTextField.text!, userName: userNameTextField.text!, countryCode: countryCodeTextField.text!, phoneNumber: phoneNumberTextField.text!, email: emailTextField.text!, image: imageDict) { (success, message , error) in
                if(success == 1) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                        self.navigationController?.popViewController(animated: false)
                    })
                    
                }
                else {
                    if(message != nil) {
                        self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                    }
                    else {
                        self.showErrorMessage(error: error)
                    }
                }
            }
            
        

        
    }
}

//MARK: UITextFieldDelegate
extension EditProfileVC : UITextFieldDelegate{
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextField{
            if countryCodeTextField.text == kEmptyString{
                countryCodeTextField.text  = "+"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == countryCodeTextField {
            
            if range.location == 0 && string == " " {
                return false
            }
            let characterLength = textField.text!.utf16.count + (string.utf16).count - range.length
            
            if characterLength > 15 {
                return false
            }
            if textField.text!.characters.count >= 1 && string == "+"{
                return false
            }
            let aSet = CharacterSet(charactersIn:"0123456789+").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: kEmptyString)
            return string == numberFiltered
        }
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: kEmptyString)
        
        return (string == filtered)
        
    }

}
