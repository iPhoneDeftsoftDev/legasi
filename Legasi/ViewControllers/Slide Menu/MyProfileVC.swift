//
//  MyProfileVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class MyProfileVC: BaseVC {
//MARK: IBOutlet
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: Variables
    var listArray = ["VIEW AND UPDATE PROFILE", "UPDATE ADDRESS","UPDATE EDUCATION DETAILS","UPDATE SCHOOL DETAILS","LINK ACCOUNTS","UPDATE WORK HISTORY","UPDATE SKILLS"]
  
    var adminListArray = ["VIEW AND UPDATE PROFILE", "UPDATE EDUCATION DETAILS", "UPDATE WORK HISTORY", "UPDATE SKILLS"]
    
    //MARK: Class Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customiseUI()
    }
    
   //MARK: Private Methods
    private func customiseUI() {
        DataManager.linkedInAccessToken = nil
        setTitle(title: "MY PROFILE")
        setBackButton()
        profileTableView.tableFooterView = UIView()
        getProfileData()
    }
    override func backButtonAction() {
        self.loadLeftSideMenu()
    }

   
}
//MARK: UITableView Delegates and Datasources
extension MyProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DataManager.role != 1 {
            return adminListArray.count
        }else {
            return listArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        if DataManager.role != 1 {
            cell.nameLabel.text = adminListArray[indexPath.row]
        }else {
            cell.nameLabel.text = listArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DataManager.role != 1 {
            if indexPath.row == 0{
                let profileVc = self.storyboard?.instantiateViewController(withIdentifier: kEditProfileVC)as!EditProfileVC
                profileVc.profileDict = SettingsVM.sharedInstance.profileData
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
            else if indexPath.row == 1{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let educationVc = storyboard.instantiateViewController(withIdentifier: kEducationDetailVc)as!EducationDetailVC
                educationVc.isEdit = true
                educationVc.educationDict = SettingsVM.sharedInstance.profileData.educationDetail
                self.navigationController?.pushViewController(educationVc, animated: true)
            }else if indexPath.row == 2{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let workVc = storyboard.instantiateViewController(withIdentifier: kAdminWorkDetailVc)as!AdminWorkDetailVC
                if let detail = SettingsVM.sharedInstance.profileData.WorkDetails.first {
                    workVc.workDetail = detail
                }
                self.navigationController?.pushViewController(workVc, animated: true)
            }else if indexPath.row == 3{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let workVc = storyboard.instantiateViewController(withIdentifier: kSkillsVC)as!SkillsVC
                workVc.skills = SettingsVM.sharedInstance.profileData.skillDetail
                workVc.isEdit = true
                self.navigationController?.pushViewController(workVc, animated: true)
            }
        }else {
            if indexPath.row == 0{
                let profileVc = self.storyboard?.instantiateViewController(withIdentifier: kEditProfileVC)as!EditProfileVC
                profileVc.profileDict = SettingsVM.sharedInstance.profileData
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
            else if indexPath.row == 1{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let addressVc = storyboard.instantiateViewController(withIdentifier: kAddressDetailVc)as!AddressDetailVC
                addressVc.address = SettingsVM.sharedInstance.profileData.address
                addressVc.selectedAddressLatitude = SettingsVM.sharedInstance.profileData.lat
                addressVc.selectedAddressLongitude = SettingsVM.sharedInstance.profileData.long
                addressVc.isEdit = true
                self.navigationController?.pushViewController(addressVc, animated: true)
            }else if indexPath.row == 2{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let educationVc = storyboard.instantiateViewController(withIdentifier: kEducationDetailVc)as!EducationDetailVC
                educationVc.isEdit = true
                educationVc.educationDict = SettingsVM.sharedInstance.profileData.educationDetail
                self.navigationController?.pushViewController(educationVc, animated: true)
            }else if indexPath.row == 3{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let schoolVc = storyboard.instantiateViewController(withIdentifier: kSchoolDetailVc)as!SchoolDetailVC
                schoolVc.schoolDictData = ["schoolName":SettingsVM.sharedInstance.profileData.schoolName.name ?? "","school_id":SettingsVM.sharedInstance.profileData.schoolName.schoolId ?? 0,"userType":SettingsVM.sharedInstance.profileData.userType,"passoutYear":SettingsVM.sharedInstance.profileData.passoutYear,"dob":SettingsVM.sharedInstance.profileData.dob,"nameOfGraduate":SettingsVM.sharedInstance.profileData.schoolName.nameOfGraduate ?? ""]
                schoolVc.isEdit = true
                self.navigationController?.pushViewController(schoolVc, animated: true)
            }else if indexPath.row == 4{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let linkVc = storyboard.instantiateViewController(withIdentifier: kLinkAccountVc)as!LinkAccountVC
                linkVc.facebookId = SettingsVM.sharedInstance.profileData.facebookId
                linkVc.linkedInId = SettingsVM.sharedInstance.profileData.linkedInId
                linkVc.isEdit = true
                self.navigationController?.pushViewController(linkVc, animated: true)
            }else if indexPath.row == 5{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let workVc = storyboard.instantiateViewController(withIdentifier: kWorkDetailVc)as!WorkDetailVC
                workVc.workDetailDict = SettingsVM.sharedInstance.profileData.WorkDetails
                workVc.isEdit = true
                self.navigationController?.pushViewController(workVc, animated: true)
            }else if indexPath.row == 6{
                let storyboard = UIStoryboard.storyboard(storyboard: .Main)
                let workVc = storyboard.instantiateViewController(withIdentifier: kSkillsVC)as!SkillsVC
                workVc.skills = SettingsVM.sharedInstance.profileData.skillDetail
                workVc.isEdit = true
                self.navigationController?.pushViewController(workVc, animated: true)
            }
        }
    }
}

//MARK: Webservice Methods
extension MyProfileVC {
    
    func getProfileData(){
        
        SettingsVM.sharedInstance.GetOwnProfile(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                DispatchQueue.main.async{
                    let data = SettingsVM.sharedInstance.profileData
                    self.userImageView.sd_setImage(with: URL(string:"\(data.profileImage!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                   
                    self.coverImageView.sd_setImage(with: URL(string:"\(data.coverImage!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
                    
                    self.nameLabel.text = data.userName
                    DataManager.user_image = data.profileImage
                    DataManager.user_name =  data.userName
                    DataManager.graduationYear = data.passoutYear
                    DataManager.userType = data.userType
                    DataManager.likeIcon_url = data.likeIcon_url
                    DataManager.unlikeIcon_url = data.unlikeIcon_url
                }

            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
        
        
    }
    
}
