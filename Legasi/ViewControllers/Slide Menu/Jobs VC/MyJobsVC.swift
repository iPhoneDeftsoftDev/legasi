//
//  MyJobsVC.swift
//  Legasi
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class MyJobsVC: BaseVC {
    
    //MARK: IBOUtlets
    @IBOutlet weak var myJobTableView: UITableView!
    
    @IBOutlet weak var noRecordLabel: UILabel!
    //Variables
    var jobsListArray = [JobsListData]()
    var pageNumber = 1
    var apiRunning = false
    var isTop = false
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customiseUI()
        
    }
    //MARK: Private Methods
    private func customiseUI() {
        //Set Table View Height Automatic
        myJobTableView.estimatedRowHeight = 340.0
        myJobTableView.rowHeight = UITableViewAutomaticDimension
        jobsListArray = [JobsListData]()
        setBackButton()
        pageNumber = 1
        noRecordLabel.isHidden = true
        myJobTableView.isHidden = false
        isTop = true
        setTitle(title: "MY JOB POSTS")
        getMyJobListMethod()
    }
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            Indicator.isEnabledIndicator = false
            
            if !apiRunning {
                Indicator.sharedInstance.showIndicator()
                self.getMyJobListMethod()
            }
        }
    }
    
}

//MARK: UITableView Datasource
extension MyJobsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
        let image = jobsListArray[indexPath.row].profileImageUrl
        jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        jobCell.nameLabel.text = jobsListArray[indexPath.row].name
        
        let concatStr:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle = NSAttributedString(string: "Job Title: " , attributes: myAttributeBlackColor)
        
        //Decode Message
        let decodeMsg = (jobsListArray[indexPath.row].title!).fromBase64()
        if(decodeMsg == nil){
            let attributedStringTitle = NSAttributedString(string: " \(jobsListArray[indexPath.row].title!)" , attributes: myAttributeGrayColor)
            concatStr.append(myAttributedTitle)
            concatStr.append(attributedStringTitle)
        }else{
            let attributedStringTitle = NSAttributedString(string: " \(decodeMsg ?? "")" , attributes: myAttributeGrayColor)
            concatStr.append(myAttributedTitle)
            concatStr.append(attributedStringTitle)
        }
        jobCell.titleLabel.attributedText = concatStr
        
//        jobCell.titleLabel.text = "Job Title: \(jobsListArray[indexPath.row].title!)"
        jobCell.locationLabel.text = jobsListArray[indexPath.row].location
        
        jobCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.profileDetailButton.tag = indexPath.row
        
        let concatStr1:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor1 = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor1 = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle1 = NSAttributedString(string: "Company: " , attributes: myAttributeBlackColor1)
        let attributedStringTitle1 = NSAttributedString(string: " \(jobsListArray[indexPath.row].companyName!)" , attributes: myAttributeGrayColor1)
        concatStr1.append(myAttributedTitle1)
        concatStr1.append(attributedStringTitle1)
        jobCell.companyName.attributedText = concatStr1
//        jobCell.companyName.text = "companyName: \(jobsListArray[indexPath.row].companyName!)"
        let concatStr2:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor2 = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor2 = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle2 = NSAttributedString(string: "Skills: " , attributes: myAttributeBlackColor2)
        let attributedStringTitle2 = NSAttributedString(string: " \(jobsListArray[indexPath.row].skills!)" , attributes: myAttributeGrayColor2)
        concatStr2.append(myAttributedTitle2)
        concatStr2.append(attributedStringTitle2)
        jobCell.skillsLabel.attributedText = concatStr2
//        jobCell.skillsLabel.text = "Skills: \(jobsListArray[indexPath.row].skills!)"
        
        var type = jobsListArray[indexPath.row].jobType
        if type == "1"{
            type = "Full Time Job"
        }else if type == "2"{
            type = "Part Time Job"
        }else if type == "3"{
            type = "Paid Internship Job"
        }else {
            type = "Unpaid Internship Job"
        }
        jobCell.jobTypeLabel.text = "\(type!)"
        jobCell.likeLabel.text = "\(jobsListArray[indexPath.row].likes!)"
        if jobsListArray[indexPath.row].jobStatus == 1{
            jobCell.availableView.backgroundColor = UIColor.black
            jobCell.availableLabel.text = "Available"
        }else{
            jobCell.availableView.backgroundColor = UIColor.darkGray
            jobCell.availableLabel.text = "Unavailable"
        }
        if jobsListArray[indexPath.row].isLike == 1{
            jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
        } else{
            jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
        }
        
        if jobsListArray[indexPath.row].userRole == 1{
            if jobsListArray[indexPath.row].userType == "Student"{
                jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
            }else{
                jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
            }
        }else{
            jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
        }
        
        jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.likeButton.tag = indexPath.row
        jobCell.commentLabel.text = "\(jobsListArray[indexPath.row].comments!)"
        
        jobCell.decriptionLabel.text = ""
        jobCell.timeLabel.text = jobsListArray[indexPath.row].date
        jobCell.locationLabel.sizeToFit()
        jobCell.titleLabel.sizeToFit()
        jobCell.companyName.sizeToFit()
        jobCell.skillsLabel.sizeToFit()
        jobCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.commentButton.tag = indexPath.row
        
        jobCell.isJobVC = true
        jobCell.loadData()
        return jobCell

        
        
        
        
//        let cell : MyJobsCell = UITableViewCell.fromNib() //My Jobs Cell
//        cell.decriptionLabel.text = myJobListArray[indexPath.row].description
//        cell.titleLabel.text = myJobListArray[indexPath.row].title
//        cell.timeLabel.text = myJobListArray[indexPath.row].date
//        cell.likeLabel.text = "\(myJobListArray[indexPath.row].likes!)"
//        cell.commentLabel.text = "\(myJobListArray[indexPath.row].comments!)"
//        if myJobListArray[indexPath.row].isLike == 1{
//            cell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
//        } else{
//            cell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
//        }
//        cell.likeButton.layer.cornerRadius = cell.likeButton.frame.width/2
//        cell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
//        cell.likeButton.tag = indexPath.row
//        if myJobListArray[indexPath.row].jobStatus == 1{
//            cell.avilableView.backgroundColor = UIColor.black
//            cell.availableLabel.text = "Available"
//        }else{
//            cell.avilableView.backgroundColor = UIColor.darkGray
//            cell.availableLabel.text = "Unavailable"
//        }
//        cell.loadData()
        
    }
    
    //MARK:- TableView Cell Button Actions
    func tapCommentButtonAction(sender:UIButton){
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
        nextObj.isThroughMyPost = true
        nextObj.feedIndex = sender.tag
        nextObj.newsFeedId = jobsListArray[sender.tag].eventId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }


    func tapLikeButtonAction(sender:UIButton){
        if jobsListArray[sender.tag].isLike == 1{
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: jobsListArray[sender.tag].eventId, type: 2)
        }else{
            self.likeOnNewsFeedMethod(likeValue: 1,eventId: jobsListArray[sender.tag].eventId, type: 2)
        }
    }
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = jobsListArray[sender.tag].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
}

//MARK: UITableView Delegate
extension MyJobsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let jobvc = self.storyboard?.instantiateViewController(withIdentifier: kAddJobVC) as! AddJobVC
        jobvc.screenType = .myJob
        jobvc.eventId = jobsListArray[indexPath.row].eventId
        self.navigationController?.pushViewController(jobvc, animated: true)
    }
}
//MARK:- Webservice Method
extension MyJobsVC {
    
    func getMyJobListMethod(){
        apiRunning = true
        AddJobPostVM.sharedInstance.GetMyJobPost(userId: DataManager.user_id!, page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.pageNumber += 1
                self.noRecordLabel.isHidden = true
                self.jobsListArray = AddJobPostVM.sharedInstance.jobListArray
                self.myJobTableView.isHidden = false
                self.myJobTableView.reloadData()
                if self.isTop == true{
                    self.myJobTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    self.isTop = false
                }
            }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                        self.noRecordLabel.isHidden = false
                        self.myJobTableView.isHidden = true
                    }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.myJobTableView.reloadData()
            }
            
            self.apiRunning = false
        }
        
        
    }
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int,type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                self.jobsListArray = [JobsListData]()
                self.getMyJobListMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}
