//
//  JobsCell.swift
//  Legasi
//
//  Created by Apple on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class JobsCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var profileDetailButton: UIButton!
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var availableView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var locationButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    
    @IBOutlet var userTypeImageView: UIImageView!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var shadowViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var skillsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var shadowViewleadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var companyName: UILabel!
    var isJobVC = false
    //MARK: Cell Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadData() {
        self.customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        //Set Shadow to View
//        if isJobVC == true{
//            shadowView.layer.cornerRadius = 7
//            shadowView.layer.shadowColor = UIColor.black.cgColor
//            shadowView.layer.shadowOffset = CGSize.zero
//            shadowView.layer.shadowRadius = 2
//            shadowView.layer.shadowOpacity = 0.3
//            //shadowView.frame = CGRect(x: 10, y: 10, width: shadowView.frame.size.width - 20, height: shadowView.frame.size.height)
//            shadowViewleadingConstraint.constant = 10
//            shadowViewTrailingConstraint.constant = 10
//        }
        
        //Set Radius to image view
        self.profileImageView.layer.cornerRadius = 22.0
        self.likeButton.layer.cornerRadius = self.likeButton.frame.width/2
    }

    
}
