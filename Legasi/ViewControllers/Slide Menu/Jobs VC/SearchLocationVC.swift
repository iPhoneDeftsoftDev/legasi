//
//  SearchLocationVC.swift
//  Legasi
//
//  Created by 123 on 22/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol SearchLocationDelegates {
    func getAddress(address: String, latitude: Double, longitude: Double)
}

class SearchLocationVC: BaseVC{
//MARK: - IBOutlets
    @IBOutlet var textFieldLocation: UITextField!
    @IBOutlet weak var labelNoDataFound: UILabel!
    @IBOutlet weak var tableSearch: UITableView!
//MARK: - Variables
    var delegate : SearchLocationDelegates?
    var addressType : String = String()
    var placeNameArray = NSMutableArray()
    var placeIDArray =  NSMutableArray()
    var placeID: String = String()
    var searchAddress : String = String()
    var cleanString : String = String()
    var selectedAddressLatitude : String = String()
    var selectedAdderessLongitude : String = String()
    var addressArray = [AddressDetails]()

    //MARK: - CLASS LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        setTitle(title: "LOCATION")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableSearch.estimatedRowHeight = 100.0
        tableSearch.rowHeight = UITableViewAutomaticDimension
        tableSearch.isHidden = true
        tableSearch.backgroundColor = .clear
        self.title = addressType
        self.tableSearch.setShadow()
        self.tableSearch.setRadius(radius: 4, borderColor: UIColor.clear, borderWidth: 1.0)
        textFieldLocation.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //To hide IQKeyboard and Done Button
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        self.textFieldLocation.becomeFirstResponder()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //To Show IQKeyboard and Done Button
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        self.view.endEditing(true)
    }
    
    
    
    //MARK: - BACK BUTTON ACTION
    @IBAction func actionPopView(_ sender: AnyObject) {
        delegate?.getAddress(address: textFieldLocation.text!, latitude: Double(selectedAddressLatitude)!, longitude: Double(selectedAdderessLongitude)!)
        //show animation while push
        self.navigationController!.popViewController(animated: false)
    }
    
  }
 //MARK: - TableView Delegates and Data Sources
extension SearchLocationVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.selectionStyle = .none
        let addressTitleLabel = cell.viewWithTag(100) as! UILabel
        let addressDetailsLabel = cell.viewWithTag(101) as! UILabel
        addressTitleLabel.text = self.addressArray[indexPath.row].name
        addressDetailsLabel.text = self.addressArray[indexPath.row].address
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        LocationManager.sharedManager.getCoordinates(address: self.addressArray[indexPath.row].name!) { (coordinates) in
            DispatchQueue.main.async {
                self.delegate?.getAddress(address: self.addressArray[indexPath.row].address, latitude: (coordinates?.latitude) ?? 0.0, longitude: (coordinates?.longitude) ?? 0.0)
            }
            
        }
        self.navigationController!.popViewController(animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - TEXTFIELDS DELEGATES
extension SearchLocationVC: UITextFieldDelegate {
   
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.isEmpty {
            tableSearch.isHidden = true
        }
        else {
            tableSearch.isHidden = false
            LocationManager.sharedManager.autoCompleteAddress(text: textField.text!) { (addressDetails) in
                print(addressDetails)
                DispatchQueue.main.async {
                    self.addressArray = addressDetails
                    self.tableSearch.isHidden = false
                    self.tableSearch.reloadData()
                }
            }
        }
    }
}
//MARK: UIScrollView Delegate
extension SearchLocationVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

