//
//  JobsVC.swift
//  Legasi
//
//  Created by Apple on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class JobsVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var jobsTableView: UITableView!
    @IBOutlet weak var pickerTableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pickerTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet var addJobButton: UIButton!
    @IBOutlet weak var pickerTextField: UITextField!
    @IBOutlet var popView: UIView!
    
    @IBOutlet var popUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noRecordLabel: UILabel!
    
    //MARK: Variables
    var filterArray = ["All", "Full Time Jobs", "Part Time Jobs", "Paid Internship", "Unpaid Internship"]
    var isPopUp = false
    var jobsListArray = [JobsListData]()
    var searchType:Int!
    var pageNumber = 1
    var apiRunning = false
    var isTop = false
    var isCommentPost: Bool!
    var eventId = Int()
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customiseUI()
    }
    //MARK: Private Methods
    private func customiseUI() {
        
        //Set Table View Height Automatic
        jobsTableView.estimatedRowHeight = 340.0
        jobsTableView.rowHeight = UITableViewAutomaticDimension
        
        //Set Radius
        backView.setRadius(radius: 5.0)
        pickerTextField.setRadius(radius: 5.0)
        pickerTextField.tintColor = UIColor.clear
        isTop = true
        //Set height
        pickerTableHeightConstraint.constant = 0.0
        pageNumber = 1
        noRecordLabel.isHidden = true
        setBackButton()
        setMoreButton()
        setTitle(title: "JOBS")
        searchType = 5
        self.jobsListArray = [JobsListData]()
        getJobListMethod()
        //Set Tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.PopViewTapped))
        self.popView.addGestureRecognizer(tapGesture)
    }
    
    //MARK: Override BaseVC
    override func moreButtonAction() {
        if isPopUp {
            popView.removeFromSuperview()
        }
        else {
             popView.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height)
            self.view.addSubview(popView)
            
            if DataManager.role == 1{
                if DataManager.userType == "Student"{
                    popUpViewHeightConstraint.constant = 40
                    addJobButton.isHidden = true
                }else{
                    addJobButton.isHidden = false
                }
            }else{
                addJobButton.isHidden = false
            }
        }
        isPopUp = !isPopUp
    }
    
    override func backButtonAction() {
        self.loadLeftSideMenu()
    }
    
    @objc private func PopViewTapped() {
        popView.removeFromSuperview()
    }
    
    //MARK: Set Picker table
    func showHidePicker() {
        if pickerTableHeightConstraint.constant == 0 {
            pickerTableHeightConstraint.constant = 230.0
        }
        else {
            pickerTableHeightConstraint.constant = 0.0
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: IBActions
    @IBAction func filterButtonAction(_ sender: UIButton) {
    }
    
    
    
   
    //MARK: PopView Actions
    @IBAction func addJobAction(_ sender: UIButton) {
        isPopUp = false
        popView.removeFromSuperview()
        let jobvc = self.storyboard?.instantiateViewController(withIdentifier: kAddJobVC) as! AddJobVC
        jobvc.screenType = .addJob
        jobvc.groupId = 0
        self.navigationController?.pushViewController(jobvc, animated: true)
    }
    
    @IBAction func myJobAction(_ sender: UIButton) {
        isPopUp = false
        popView.removeFromSuperview()
        let myjobvc = self.storyboard?.instantiateViewController(withIdentifier: kMyJobsVC) as! MyJobsVC
        self.navigationController?.pushViewController(myjobvc, animated: true)
    }
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            Indicator.isEnabledIndicator = false
            
            if !apiRunning {
                Indicator.sharedInstance.showIndicator()
                self.getJobListMethod()
            }
        }
    }
    
}

//MARK: UITextField Delegates
extension JobsVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.showHidePicker()
    }
}


//MARK: UITableView Datasource
extension JobsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == pickerTableView {
            return filterArray.count
        }
        return jobsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == pickerTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: kPickerCell)!
            cell.textLabel?.textColor = UIColor.white
            cell.textLabel?.font = UIFont.LegasiFont.regular.fontWithSize(size: 13.0)
            cell.textLabel?.text = filterArray[indexPath.row]
            return cell
        }
        
        let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
        let image = jobsListArray[indexPath.row].profileImageUrl
        jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        jobCell.nameLabel.text = jobsListArray[indexPath.row].name
    
        let concatStr:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle = NSAttributedString(string: "Job Title:" , attributes: myAttributeBlackColor)
        
        //Decode Message
        let decodeMsg = (jobsListArray[indexPath.row].title!).fromBase64()
        if(decodeMsg == nil){
            let attributedStringTitle = NSAttributedString(string: " \(jobsListArray[indexPath.row].title!)" , attributes: myAttributeGrayColor)
            concatStr.append(myAttributedTitle)
            concatStr.append(attributedStringTitle)
        }else{
            let attributedStringTitle = NSAttributedString(string: " \(decodeMsg!)" , attributes: myAttributeGrayColor)
            concatStr.append(myAttributedTitle)
            concatStr.append(attributedStringTitle)
        }
        jobCell.titleLabel.attributedText = concatStr
        
//        jobCell.titleLabel.text = "Job Title: \(jobsListArray[indexPath.row].title!)"
        jobCell.locationLabel.text = jobsListArray[indexPath.row].location
        
        jobCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.profileDetailButton.tag = indexPath.row
        if jobsListArray[indexPath.row].userRole == 1{
            if jobsListArray[indexPath.row].userType == "Student"{
                jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
            }else{
                jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
            }
        }else{
            jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
        }
        
        let concatStr1:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor1 = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor1 = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle1 = NSAttributedString(string: "Company: " , attributes: myAttributeBlackColor1)
        let attributedStringTitle1 = NSAttributedString(string: " \(jobsListArray[indexPath.row].companyName!)" , attributes: myAttributeGrayColor1)
        concatStr1.append(myAttributedTitle1)
        concatStr1.append(attributedStringTitle1)
        jobCell.companyName.attributedText = concatStr1
        
//        jobCell.companyName.text = "Company: \(jobsListArray[indexPath.row].companyName!)"
        let concatStr2:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor2 = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
        let myAttributeGrayColor2 = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedTitle2 = NSAttributedString(string: "Skills: " , attributes: myAttributeBlackColor2)
        let attributedStringTitle2 = NSAttributedString(string: " \(jobsListArray[indexPath.row].skills!)" , attributes: myAttributeGrayColor2)
        concatStr2.append(myAttributedTitle2)
        concatStr2.append(attributedStringTitle2)
        jobCell.skillsLabel.attributedText = concatStr2
//        jobCell.skillsLabel.text = "Skills: \(jobsListArray[indexPath.row].skills!)"
        
        var type = jobsListArray[indexPath.row].jobType
        if type == "1"{
            type = "Full Time Job"
        }else if type == "2"{
            type = "Part Time Job"
        }else if type == "3"{
            type = "Paid Internship Job"
        }else {
            type = "Unpaid Internship Job"
        }
        jobCell.jobTypeLabel.text = "\(type!)"
        jobCell.likeLabel.text = "\(jobsListArray[indexPath.row].likes!)"
        if jobsListArray[indexPath.row].jobStatus == 1{
            jobCell.availableView.backgroundColor = UIColor.black
            jobCell.availableLabel.text = "Available"
        }else{
            jobCell.availableView.backgroundColor = UIColor.darkGray
            jobCell.availableLabel.text = "Unavailable"
        }
        if jobsListArray[indexPath.row].isLike == 1{
            jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
        } else{
            jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
        }
        
        jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.likeButton.tag = indexPath.row
        jobCell.commentLabel.text = "\(jobsListArray[indexPath.row].comments!)"
        
        jobCell.decriptionLabel.text = ""
        jobCell.timeLabel.text = jobsListArray[indexPath.row].date
        jobCell.locationLabel.sizeToFit()
        jobCell.titleLabel.sizeToFit()
        jobCell.companyName.sizeToFit()
        jobCell.skillsLabel.sizeToFit()
        jobCell.isJobVC = true
        
        jobCell.commentButton.addTarget(self, action: #selector(self.tapCommentButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        jobCell.commentButton.tag = indexPath.row
        
        jobCell.loadData()
        return jobCell
    }
    
    func tapCommentButtonAction(sender:UIButton){
        
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
        nextObj.isThroughMyPost = true
        nextObj.feedIndex = sender.tag
        nextObj.newsFeedId = jobsListArray[sender.tag].eventId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = jobsListArray[sender.tag].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
    func tapLikeButtonAction(sender:UIButton){
        if jobsListArray[sender.tag].isLike == 1{
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: jobsListArray[sender.tag].eventId, type: 2)
        }else{
            self.likeOnNewsFeedMethod(likeValue: 1,eventId: jobsListArray[sender.tag].eventId, type: 2)
        }
    }
}

//MARK: UITableView Delegates
extension JobsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == pickerTableView {
            pickerTextField.text = filterArray[indexPath.row]
            pickerTextField.resignFirstResponder()
            if indexPath.row == 0{
                searchType = 5
            }else{
                searchType = indexPath.row
            }
            pageNumber = 1
            self.jobsListArray = [JobsListData]()
            self.getJobListMethod()
            self.showHidePicker()
        }
        else {
            
            let jobvc = self.storyboard?.instantiateViewController(withIdentifier: kAddJobVC) as! AddJobVC
            jobvc.screenType = .jobDetails
            jobvc.eventId = jobsListArray[indexPath.row].eventId
            self.navigationController?.pushViewController(jobvc, animated: true)
        }
    }
}

extension JobsVC {
    
    func getJobListMethod(){
        apiRunning = true
        AddJobPostVM.sharedInstance.GetAllJobPost(userId: DataManager.user_id!, searchType: searchType, page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.pageNumber += 1
                self.noRecordLabel.isHidden = true
                self.jobsListArray = AddJobPostVM.sharedInstance.jobListArray
                self.jobsTableView.isHidden = false
                self.jobsTableView.reloadData()
                if self.isTop == true{
                    self.jobsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    self.isTop = false
                }
             }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                        self.noRecordLabel.isHidden = false
                        self.jobsTableView.isHidden = true
                     }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            self.jobsTableView.reloadData()
            self.apiRunning = false
        }
        
        
    }
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int,type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                self.jobsListArray = [JobsListData]()
                self.getJobListMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
}
