//
//  AddJobVC.swift
//  Legasi
//
//  Created by Apple on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

enum JobScreenType: String {
    case addJob = "ADD JOB POST"
    case myJob = "MY JOB DETAILS"
    case jobDetails = "JOB DETAILS"
    case editJob = "EDIT JOB POST"
}


import UIKit

class AddJobVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet weak var jobAvailableViewHeight: NSLayoutConstraint!
    @IBOutlet var jobAvailabilitySwitch: UISwitch!
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var availableView: UIView!
    @IBOutlet var titleLabelCollection: [UILabel]!
    @IBOutlet var textFieldCollection: [UITextField]!
    @IBOutlet weak var jobNameTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var skillsTextField: UITextField!
    @IBOutlet weak var jobTypeTextField: UITextField!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var descriptionTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet var popView: UIView!
   
    
    //MARK: Variable
    var industryTypeArray = ["FullTime Job","PartTime Job","Paid Internship","Unpaid Internship"]
    var selectedTextField: UITextField!
    var selectedAddressLatitude : Double?
    var selectedAddressLongitude : Double?
    var selectedJobType : Int?
    var imageDict = [String: Data]()
    var pollArray = String()
    //MARK: Variables
    var screenType: JobScreenType = .addJob
    var eventId = Int()
    var isPopUp = false
    var groupId:Int!
    var jobStatus = 1
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.customiseUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: screenType.rawValue)
        self.pickerDelegate = self
        setBackButton()
        jobAvailabilitySwitch.addTarget(self, action: #selector(jobAvailabilityChangeEvent(switchState:)), for: .valueChanged)
        for textField in textFieldCollection {
            textField.setPlaceholder()
        }
        
        //Set Screen TextFields based on screen type
        if screenType == .addJob {
            for label in titleLabelCollection {
                label.isHidden = true
            }
            postButton.isHidden = false
            jobAvailableViewHeight.constant = 0
            availableView.isHidden = true
            scrollBottomConstraint.constant = 105.0
            arrowImage.isHidden = false
        }
        else {
            arrowImage.isHidden = true
            for label in titleLabelCollection {
                label.isHidden = false
            }
            if screenType == .editJob {
                postButton.isHidden = true
                availableView.isHidden = true
                scrollBottomConstraint.constant = 105.0
            }
            else {
                postButton.isHidden = true
                jobAvailableViewHeight.constant = 0
                scrollBottomConstraint.constant = 30.0
                
                for textField in textFieldCollection {
                    textField.isUserInteractionEnabled = false
                }
                descriptionTextView.isUserInteractionEnabled = false
                
            }
            getJobDetailMethod()
        }
        
        if screenType == .myJob {
            setMoreButton()
            postButton.isHidden = true
            locationButton.isUserInteractionEnabled = false
        }
        if screenType == .editJob {
            arrowImage.isHidden = false
            setDoneButton()
            postButton.isHidden = true
        }
        
        //Set Tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.PopViewTapped))
        self.popView.addGestureRecognizer(tapGesture)
    }
    
    @objc private func PopViewTapped() {
        popView.removeFromSuperview()
    }
    
    
    
    //MARK: Override BaseVC
    override func moreButtonAction() {
        if isPopUp {
            popView.removeFromSuperview()
        }
        else {
            popView.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height)
            self.view.addSubview(popView)
        }
        isPopUp = !isPopUp
    }
   //MARK: IBActions
    @IBAction func locationButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        let locationVC = self.storyboard?.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
        locationVC.delegate = self
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    override func doneButtonAction() {
        self.EditJobPost()
    }
    
    //MARK: - Availability Switch Change Event
    @IBAction func jobAvailabilityChangeEvent(switchState: UISwitch) {
        if switchState.isOn {
            jobStatus = 1
        } else {
            jobStatus = 2
        }
    }
    
    //MARK: - POST BUTTON ACTION
    
    @IBAction func postButtonAction(_ sender: UIButton) {
        
        //check if all textfields contains data
        if (jobNameTextField.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0{
            if (companyNameTextField.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0{
                if (locationTextField.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0{
                    if (descriptionTextView.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0 && descriptionTextView.text != "Description"{
                        if (skillsTextField.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0{
                            if (jobTypeTextField.text?.trimmingCharacters(in: .whitespaces).characters.count)! > 0{
                                
                                //call job post webservice
                                self.addJobPost(jobName: jobNameTextField.text!, companyName: companyNameTextField.text!, location: locationTextField.text!, description: descriptionTextView.text!, skills: skillsTextField.text!, jobType: self.selectedJobType!)
                                
                            }else{
                                self.showAlert(message: "Please select job type.")
                            }
                        }else{
                            self.showAlert(message: "Please enter skills")
                        }
                    }else{
                        self.showAlert(message: "Please enter description.")
                    }
                }else{
                    self.showAlert(message: "Please enter location")
                }
            }else{
                self.showAlert(message: "Please enter company name.")
            }
            
        }else{
            self.showAlert(message: "Please enter job name.")
        }
        
    }
    
    //MARK: - PopView Actions
    @IBAction func editButtonAction(_ sender: UIButton) {
        isPopUp = false
        popView.removeFromSuperview()
        let jobvc = self.storyboard?.instantiateViewController(withIdentifier: kAddJobVC) as! AddJobVC
        jobvc.screenType = .editJob
        jobvc.eventId = eventId
        self.navigationController?.pushViewController(jobvc, animated: true)
    }
    
    //MARK: -  Delete Button Action
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        isPopUp = false
        popView.removeFromSuperview()
        self.showAlert(message: "Are you sure you want to delete this job?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
                self.deleteJobMethod()
        })
    }
}

//MARK: - WEBSERVICE EXTENSION
extension AddJobVC {
    func getJobDetailMethod(){
        AddJobPostVM.sharedInstance.GetEachJobDetail(userId: DataManager.user_id!, eventId: eventId) { (success, message , error) in
            if(success == 1) {
                self.setJobDetailData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func setJobDetailData(){
        
        //Decode Message
        let decodeMsg = (AddJobPostVM.sharedInstance.jobDetail.title!).fromBase64()
        if(decodeMsg == nil){
            jobNameTextField.text = AddJobPostVM.sharedInstance.jobDetail.title
        }else{
           jobNameTextField.text = decodeMsg
        }
        companyNameTextField.text = AddJobPostVM.sharedInstance.jobDetail.companyName
        locationTextField.text = AddJobPostVM.sharedInstance.jobDetail.location
        descriptionTextView.text = AddJobPostVM.sharedInstance.jobDetail.description
        skillsTextField.text = AddJobPostVM.sharedInstance.jobDetail.skills
        var type = AddJobPostVM.sharedInstance.jobDetail.jobType
        if type == "1"{
            type = "Full Time Job"
        }else if type == "2"{
            type = "Part Time Job"
        }else if type == "3"{
            type = "Paid Internship Job"
        }else {
            type = "Unpaid Internship Job"
        }
        jobTypeTextField.text = type
        if AddJobPostVM.sharedInstance.jobDetail.jobStatus == 1{
            availableView.backgroundColor = UIColor.black
            availableLabel.text = "Available"
            jobAvailabilitySwitch.isOn = true
            jobStatus = 1
        }else{
            availableView.backgroundColor = UIColor.darkGray
            availableLabel.text = "Unavailable"
            jobAvailabilitySwitch.isOn = false
            jobStatus = 2
        }
        
        self.descriptionTextView.textColor = UIColor.black
        self.selectedAddressLongitude = AddJobPostVM.sharedInstance.jobDetail.long
        self.selectedAddressLatitude = AddJobPostVM.sharedInstance.jobDetail.lat
        self.selectedJobType = Int(AddJobPostVM.sharedInstance.jobDetail.jobType!)
        
        let size = descriptionTextView.sizeThatFits(CGSize(width: descriptionTextView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        if size.height != descriptionTextViewHeightConstraint.constant && size.height > descriptionTextView.frame.size.height || size.height < descriptionTextView.frame.size.height && descriptionTextViewHeightConstraint.constant > 47 {
            
            descriptionTextViewHeightConstraint.constant = size.height
            descriptionTextView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func EditJobPost(){
        
        AddJobPostVM.sharedInstance.EditOwnJob(userId: DataManager.user_id!, eventId: eventId, jobName: jobNameTextField.text!, companyName: companyNameTextField.text!, location: locationTextField.text!, description: descriptionTextView.text!, skillsNeeded: skillsTextField.text!, lat: self.selectedAddressLatitude!, long: self.selectedAddressLongitude!, jobType: self.selectedJobType!,jobStatus:jobStatus) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (success) in
                        self.navigationController?.popViewController(animated: false)
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    
    }

    //MARK: - ADD JOB POST WS
    func addJobPost(jobName: String,companyName: String,location : String,description: String,skills: String,jobType:Int){
        let encodedMsg :String = jobName.trimmingCharacters(in: .whitespaces).toBase64()
        // newsFeedType for JOB POST is 2.
        AddJobPostVM.sharedInstance.addNewJobPost(userId: DataManager.user_id!, newsFeedType: 2, jobName: encodedMsg, companyName: companyName, location: location, description: description, skills: skills, jobType: jobType, lat: self.selectedAddressLatitude!, long: self.selectedAddressLongitude!, title: "", body: "", privacy: "", allowComment: "", shareLocation: "", tags: "", mediaType: "", pollOption: pollArray, shareLink: "", mediaName: nil, vedioArray: nil, thumbNailName: nil, mediaArray: nil, mimeType: .image, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (success) in
                    //pop to back view controller
                    self.backButtonAction()
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    
    
    func deleteJobMethod(){
        
        AddJobPostVM.sharedInstance.DeleteNewfeed(userId: DataManager.user_id!, newfeedId: eventId) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (success) in
                    //pop to back view controller
                    self.backButtonAction()
                })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
}

//MARK: - UITextViewDelegates Delegates
extension AddJobVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Description"
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description"){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.characters.count // for Swift use count(newText)
//        return numberOfChars < 150;
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        if size.height != descriptionTextViewHeightConstraint.constant && size.height > textView.frame.size.height || size.height < textView.frame.size.height && descriptionTextViewHeightConstraint.constant > 47 {
           
            descriptionTextViewHeightConstraint.constant = size.height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
}

//MARK: UITextField Delegates
extension AddJobVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == jobTypeTextField{
            jobTypeTextField.text = industryTypeArray[0]
            selectedJobType = 1
            //for 0 index job type is 1
            setPicker(textField: textField, array: industryTypeArray)
        }

        selectedTextField = textField
    }
}
//MARK: Custom Picker Delegates
extension AddJobVC: LegasiPickerDelegate {
    
    func didSelectPickerViewAtIndex(index: Int) {
        if(selectedTextField == jobTypeTextField ) {
            jobTypeTextField.text = industryTypeArray[index]
            selectedJobType = index + 1
            //pass index path incremented by 1 for selected job type
        }
    }
}
//MARK: Custom Picker Delegate
extension AddJobVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        if !address.isEmpty {
            locationTextField.text = address
            selectedAddressLatitude = latitude
            selectedAddressLongitude = longitude
            // descriptionTextView.becomeFirstResponder()
        }
    }
}
