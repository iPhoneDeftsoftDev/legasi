
//
//  ConnectionsVC.swift
//  Legasi
//
//  Created by 123 on 02/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class ConnectionsVC: BaseVC {
//MARK: IBOutlets
    @IBOutlet weak var connectionTableView: UITableView!
    @IBOutlet weak var connectionSegment: UISegmentedControl!
    
    @IBOutlet weak var noResultFound: UILabel!
   //MARK: Variables
    var nameArray = [followerUserData]()
    var groupArray = [userGroupListData]()
   
   //MARK: Class Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        connectionSegment.selectedSegmentIndex = 0
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "CONNECTIONS")
        customSlideButton()
         noResultFound.isHidden = true
        if GroupNameVC.isAddMemberPush == "Yes"{
            connectionSegment.selectedSegmentIndex = 2
            GroupNameVC.isAddMemberPush = "No"
        }
        if connectionSegment.selectedSegmentIndex == 0 {
            getFollowerListData()
        }else if connectionSegment.selectedSegmentIndex == 1 {
            getFollowingListData()
        }else{
            getGroupListData()
        }
        
        connectionSegment.setTitleTextAttributes([NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 15)], for: .normal)
        connectionTableView.estimatedRowHeight = 83
        connectionTableView.rowHeight = UITableViewAutomaticDimension
        connectionTableView.tableFooterView = UIView()
        connectionSegment.setRadius(radius: 10, borderColor: UIColor.black, borderWidth: 1)
    }
    //MARK: IBActions
    @IBAction func connectionSegmentAction(_ sender: Any) {
        self.navigationItem.rightBarButtonItem = nil
        ConnectionsVM.sharedInstance.followerUserArray = [followerUserData]()
        if connectionSegment.selectedSegmentIndex == 0 {
            self.navigationItem.rightBarButtonItem = nil
            getFollowerListData()
        }else if connectionSegment.selectedSegmentIndex == 1 {
            self.navigationItem.rightBarButtonItem = nil
            getFollowingListData()
        }else{
            customRightAddButton()
            getGroupListData()
        }
        
    }
    //MARK: - Set Add Button
    func customRightAddButton(){
        self.navigationController?.navigationBar.barTintColor = UIColor.LegasiColor.blue.colorWith(alpha: 1.0)
        
        let doneView = UIView()
//                doneView.backgroundColor = UIColor.gray
        doneView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let doneImageView = UIImageView()
        doneImageView.image = #imageLiteral(resourceName: "addChat")
        if #available(iOS 11, *) {
            doneImageView.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
        }else {
            doneImageView.frame = CGRect(x: -8, y: 0, width: 30, height: 30)
        }
        let addButton = UIButton() //Custom back Button
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
//        let addButton = UIButton()
//        addButton.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        addButton.layer.cornerRadius = 20
        if connectionSegment.selectedSegmentIndex == 2 {
//            addButton .setImage(UIImage(named: "addChat"), for: UIControlState.normal)
            addButton .addTarget(self, action: #selector(self.btnAddGroupClicked(sender:)), for: UIControlEvents.touchUpInside)
        }else {
            addButton .setImage(UIImage(named: ""), for: UIControlState.normal)
            
        }
        let rightBarButton = UIBarButtonItem()
        doneView.addSubview(doneImageView)
        doneView.addSubview(addButton)
        rightBarButton.customView = doneView
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -15;
        self.navigationItem.setRightBarButtonItems([negativeSpacer, rightBarButton], animated: false)
        
    }
    func btnAddGroupClicked(sender: UIButton) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kAddGroupVC)as!AddGroupVC
        nextObj.isEdit = true
        nextObj.groupId = 0
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }

}
//MARK: UITableview delegate & datasources
extension ConnectionsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if connectionSegment.selectedSegmentIndex == 2 {
            return groupArray.count
        }else{
            return nameArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.lightGray, borderWidth: 0.5)
        cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
        if connectionSegment.selectedSegmentIndex == 2 {
            cell.nameLabel.text = groupArray[indexPath.row].groupName
            cell.connectionsLabel.text = "\(groupArray[indexPath.row].totalConnection!) Mutual Connection"
            let image = groupArray[indexPath.row].groupImageUrl
            cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        }else{
            cell.nameLabel.text = nameArray[indexPath.row].friendName
            //cell.connectionsLabelHeight.constant = 0
            cell.connectionsLabel.text = ""
            let image = nameArray[indexPath.row].friendProfileImageUrl
            cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        }
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.width/2
        cell.userImageView.clipsToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if connectionSegment.selectedSegmentIndex == 2 {
            let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
            let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupNameVC)as!GroupNameVC
            nextObj.titleName = groupArray[indexPath.row].groupName
            nextObj.groupId = groupArray[indexPath.row].groupId
            nextObj.isAdmin = groupArray[indexPath.row].isAdmin
            self.navigationController?.pushViewController(nextObj, animated: false)
            
        }else{
           
            let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
            let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
            if nameArray[indexPath.row].userId == DataManager.user_id{
                nextObj.otherUserId = nameArray[indexPath.row].friendId
            }else{
                nextObj.otherUserId = nameArray[indexPath.row].userId
            }
            
            self.navigationController?.pushViewController(nextObj, animated: false)
            
        }
        
    }
}

//MARK:- Webservice Method
extension ConnectionsVC {
    
    func getFollowingListData(){
        ConnectionsVM.sharedInstance.userFollowingList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
               
                self.noResultFound.isHidden = true
                self.connectionTableView.isHidden = false
                self.nameArray = ConnectionsVM.sharedInstance.followerUserArray
                self.connectionTableView.reloadData()
            }
            else {
                self.noResultFound.isHidden = false
                self.connectionTableView.isHidden = true
                if(message != nil) {
//                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    func getFollowerListData(){
        
        ConnectionsVM.sharedInstance.userFollowersList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.nameArray = ConnectionsVM.sharedInstance.followerUserArray
                self.noResultFound.isHidden = true
                self.connectionTableView.isHidden = false
                self.connectionTableView.reloadData()
            }
            else {
               
                if(message != nil) {
                    
                   // self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.noResultFound.isHidden = false
                self.connectionTableView.isHidden = true
            }
        }
    }
    func getGroupListData(){
        ConnectionsVM.sharedInstance.userGroupList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.groupArray = ConnectionsVM.sharedInstance.userGroupListArray
                self.noResultFound.isHidden = true
                self.connectionTableView.isHidden = false
                self.connectionTableView.reloadData()
            }
            else {
                if(message != nil) {
                   // self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.noResultFound.isHidden = false
                self.connectionTableView.isHidden = true
            }
        }
    }

}
