//
//  GroupDetailVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class GroupDetailVC: BaseVC {
//IBOutlets
    @IBOutlet weak var groupMenuView: UIView!
    
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var groupDescLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    
    @IBOutlet weak var membersCountLabel: UILabel!
    @IBOutlet weak var groupMenuTableView: UITableView!
    @IBOutlet var groupMenuMainView: UIView!
    @IBOutlet weak var groupDetailTableView: UITableView!
    @IBOutlet weak var groupMenuHeightConstraint: NSLayoutConstraint!
   //Variables
   var isMenuTap = false
    var groupMenuArray = [String]()
    var groupId:Int!
    var groupMemberArray = [groupMemberData]()
   //MARK: Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        groupMenuMainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        groupMenuView.setRadius(radius: 10)
        groupDetailTableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       

        customiseUI()
       
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "GROUP DETAILS")
        setBackButton()
        setMoreButton()
        getGroupDetailMethod()
        groupDetailTableView.estimatedRowHeight = 87
        groupDetailTableView.rowHeight = UITableViewAutomaticDimension
    }
    override func moreButtonAction() {
        if isMenuTap == false {
            self.view.addSubview(groupMenuMainView)
            isMenuTap = true
        }else{
            groupMenuMainView.removeFromSuperview()
            isMenuTap = false
        }
        groupMenuHeightConstraint.constant = groupMenuTableView.contentSize.height
    }


}

//MARK: UITableView Delegate & Datasources
extension GroupDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == groupDetailTableView {
//            if ConnectionsVM.sharedInstance.groupDetailInfo.memberArray != nil{
//                return ConnectionsVM.sharedInstance.groupDetailInfo.memberArray.count
//            }
            return groupMemberArray.count
         }else{
            return groupMenuArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == groupDetailTableView {
            let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
            cell.selectionStyle = .none
            cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.lightGray, borderWidth: 0.5)
            cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
            let image = ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].userImageUrl! ?? ""
            cell.userImageView.sd_setImage(with: URL(string:"\(image)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            cell.userImageView.layer.cornerRadius = cell.userImageView.frame.width/2
            cell.userImageView.clipsToBounds = true
            cell.nameLabel.text = ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].name
           let admin = ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].userType
            if admin == "Admin"{
                cell.dateLabel.text = admin
            }else{
                cell.dateLabel.text = ""
            }
            
            
            return cell
        } else{
            let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
            cell.selectionStyle = .none
            cell.nameLabel.text = groupMenuArray[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != groupDetailTableView {
          
            if  ConnectionsVM.sharedInstance.groupDetailInfo.isAdmin == "No"{
                
                if indexPath.row == 0 {
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kAddGroupVC)as!AddGroupVC
                    nextObj.groupDetailDict = ConnectionsVM.sharedInstance.groupDetailInfo
                    nextObj.isEdit = false
                    nextObj.groupId = groupId
                    self.navigationController?.pushViewController(nextObj, animated: false)
                }else if indexPath.row == 1{
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    leaveGroupMethod()
                
                }
                
            }else{
              if indexPath.row == 0 {
                groupMenuMainView.removeFromSuperview()
                isMenuTap = false
                let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kAddMemberVC)as!AddMemberVC
                nextObj.groupId = groupId
                self.navigationController?.pushViewController(nextObj, animated: false)
            }else if indexPath.row == 1{
                 groupMenuMainView.removeFromSuperview()
                isMenuTap = false
                let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kAddGroupVC)as!AddGroupVC
                nextObj.groupDetailDict = ConnectionsVM.sharedInstance.groupDetailInfo
                nextObj.isEdit = false
                nextObj.groupId = groupId
                self.navigationController?.pushViewController(nextObj, animated: false)
                
            }else if indexPath.row == 2{
                groupMenuMainView.removeFromSuperview()
                isMenuTap = false
                leaveGroupMethod()
                
            }else if indexPath.row == 3{
                groupMenuMainView.removeFromSuperview()
                isMenuTap = false
                deleteGroupMethod()
                
            }
        }
    } else{
            
            if ConnectionsVM.sharedInstance.groupDetailInfo.isAdmin != "No" {
                 let admin = ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].userType
                let alert  = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
                if admin != "Admin"{
                   
                    alert.addAction(UIAlertAction(title: "Make Group Admin", style: .default, handler: { (action) in
                            self.makeGroupAdminMethod(otherUser_id:(ConnectionsVM.sharedInstance.groupDetailInfo.memberArray[indexPath.row].memberId)!)
                    }))
                }
                    alert.addAction(UIAlertAction(title: "Remove \(ConnectionsVM.sharedInstance.groupDetailInfo.memberArray[indexPath.row].name.capitalized)", style: .default, handler: { (action) in
                        self.removerUserFromGroup(otherUser_id:(ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].memberId)!)
                    }))
                    alert.addAction(UIAlertAction(title: "View \(ConnectionsVM.sharedInstance.groupDetailInfo.memberArray[indexPath.row].name.capitalized) Profile", style: .default, handler: { (action) in
                    
                    let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
                    nextObj.otherUserId = (ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].memberId)!
                    self.navigationController?.pushViewController(nextObj, animated: false)
                    }))

                    alert.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            } else{
                    let alert  = UIAlertController(title: kSelectImage, message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "View \(ConnectionsVM.sharedInstance.groupDetailInfo.memberArray[indexPath.row].name.capitalized) Profile", style: .default, handler: { (action) in
                    
                    let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
                    nextObj.otherUserId = (ConnectionsVM.sharedInstance.groupDetailInfo.memberArray?[indexPath.row].memberId)!
                    self.navigationController?.pushViewController(nextObj, animated: false)
                    }))
                    alert.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            }
         }
    }
    
}

//MARK:- Webservice Method
extension GroupDetailVC {
    
    func getGroupDetailMethod(){
        ConnectionsVM.sharedInstance.GetEachGroupDetail(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                
                self.setGroupDetailData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
     }
    func setGroupDetailData(){
        coverImageView.sd_setImage(with: URL(string:"\(ConnectionsVM.sharedInstance.groupDetailInfo.groupCoverImageUrl!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
        userImageView.sd_setImage(with: URL(string:"\(ConnectionsVM.sharedInstance.groupDetailInfo.groupImageUrl!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        groupNameLabel.text = ConnectionsVM.sharedInstance.groupDetailInfo.groupName
        membersCountLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailInfo.memberCount!) MEMBERS"
        groupDescLabel.text = ConnectionsVM.sharedInstance.groupDetailInfo.groupDesc
        groupMemberArray = ConnectionsVM.sharedInstance.groupDetailInfo.memberArray
        if ConnectionsVM.sharedInstance.groupDetailInfo.isAdmin == "No"{
            groupMenuArray = ["Edit Group Info","Leave Group"]
            
        } else{
            groupMenuArray = ["Add Members","Edit Group Info","Leave Group","Delete Group"]
        }
        groupMenuTableView.reloadData()
        groupDetailTableView.reloadData()
        
    }
    func makeGroupAdminMethod(otherUser_id:Int){
        ConnectionsVM.sharedInstance.MakeGroupAdmin(userId: DataManager.user_id!, groupId: groupId,otherUserId:otherUser_id) { (success, message , error) in
            if(success == 1) {
                 self.getGroupDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }

        
    }
    func removerUserFromGroup(otherUser_id:Int){
        ConnectionsVM.sharedInstance.RemoveGroupUser(userId: DataManager.user_id!, groupId: groupId,otherUserId:otherUser_id) { (success, message , error) in
            if(success == 1) {
                self.getGroupDetailMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
        
    }
    func deleteGroupMethod(){
        ConnectionsVM.sharedInstance.DeleteGroup(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                     self.navigationController?.popToRootViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            
        }
    }
    
    func leaveGroupMethod(){
        ConnectionsVM.sharedInstance.LeaveGroupMethod(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popToRootViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            
        }
    }

}
