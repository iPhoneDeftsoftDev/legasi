//
//  GroupNameVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import EventKit
import MapKit
class GroupNameVC: BaseVC {
    //IBOutlets
    @IBOutlet weak var groupMenuTableView: UITableView!
    @IBOutlet var groupMenuMainView: UIView!
    @IBOutlet weak var groupMenuView: UIView!
    @IBOutlet weak var groupMenuViewHeight: NSLayoutConstraint!
    @IBOutlet weak var groupNameTableView: UITableView!
    @IBOutlet var addPostView: UIView!
    
    @IBOutlet var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var addJobPostView: UIView!
    @IBOutlet var roundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noResultLabel: UILabel!
   //Variables
    var titleName:String!
    var groupId:Int!
    var isMenuTap = false
    var pageNumber = 1
    var apiRunning = false
    var isMember = String()
    var pollsId = 0
    var isAdmin = String()
    var groupMenuArray = [String]()
    let store = EKEventStore()
    var startEventDate = Date()
    static var isAddMemberPush = ""
  
    //MARK: Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        groupMenuMainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        groupMenuView.setRadius(radius: 10)
        addPostView.frame = (UIApplication.shared.keyWindow?.frame)!
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name(rawValue: "GroupPostList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchGroupData(notification:)), name: NSNotification.Name(rawValue: "GroupPostList"), object: nil)
    }
    
    //MARK: NSNOTIFICATION METHOD
    func fetchGroupData(notification: NSNotification){
        print(notification)
        let postDict = notification.userInfo as NSDictionary!
        groupId = postDict?["groupId"]as!Int
        isAdmin = postDict?["isAdmin"]as!String
        titleName = postDict?["titleName"]as!String
        pageNumber = 1
        ConnectionsVM.sharedInstance.groupDetailArray = [NewsFeedData]()
        groupFeedsDetailData()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
        
    }

    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: titleName)
        setBackButton()
        pageNumber = 1
        custumRightTwoButtons()
        noResultLabel.isHidden = true
        if isAdmin == "No"{
            if isMember == "No"{
                groupMenuArray = ["Join Group"]
            } else{
                groupMenuArray = ["Group Info","Leave Group"]
            }
            
            
        } else{
            groupMenuArray = ["Add Members","Group Info","Leave Group","Delete Group"]
        }

        //Set Table View Height Automatic
        groupNameTableView.estimatedRowHeight = 340.0
        groupNameTableView.rowHeight = UITableViewAutomaticDimension
        ConnectionsVM.sharedInstance.groupDetailArray = [NewsFeedData]()
        groupFeedsDetailData()
        
    }
    override func custumRightTwoButtons() {
      if isAdmin == "No"{
        if isMember == "No"{
            let addButtonView = UIView()
            addButtonView.frame = CGRect(x: 0, y: 0, width: 19, height: 30)
            let addButtonImageView = UIImageView()
            addButtonImageView.image = #imageLiteral(resourceName: "moreIcon")
            addButtonImageView.contentMode = .scaleAspectFit
            addButtonImageView.frame = CGRect(x: 0, y: 3, width: 24, height: 24)
            let addButton = UIButton()
            addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            addButton .addTarget(self, action: #selector(self.btnChatRightMenuClicked(sender:)), for: UIControlEvents.touchUpInside)
            let rightBarButton = UIBarButtonItem()
            addButtonView.addSubview(addButtonImageView)
            addButtonView.addSubview(addButton)
            rightBarButton.customView = addButtonView
            self.navigationItem.rightBarButtonItem = rightBarButton
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = 0;
            
            self.navigationItem.setRightBarButtonItems([rightBarButton, negativeSpacer], animated: false)
        }else{
            let addButtonView = UIView()
            addButtonView.frame = CGRect(x: 0, y: 0, width: 19, height: 30)
            let addButtonImageView = UIImageView()
            addButtonImageView.image = #imageLiteral(resourceName: "moreIcon")
            addButtonImageView.contentMode = .scaleAspectFit
            addButtonImageView.frame = CGRect(x: 0, y: 3, width: 24, height: 24)
            let addButton = UIButton()
            addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            addButton .addTarget(self, action: #selector(self.btnChatRightMenuClicked(sender:)), for: UIControlEvents.touchUpInside)
            
            let rightBarButton = UIBarButtonItem()
            addButtonView.addSubview(addButtonImageView)
            addButtonView.addSubview(addButton)
            rightBarButton.customView = addButtonView
            self.navigationItem.rightBarButtonItem = rightBarButton
            
            let addButtonView1 = UIView()
            addButtonView1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let addButtonImageView1 = UIImageView()
            addButtonImageView1.image = #imageLiteral(resourceName: "addChat")
            if #available(iOS 11, *) {
                addButtonImageView1.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
            }else {
                addButtonImageView1.frame = CGRect(x: -8, y: 0, width: 30, height: 30)
            }
            let addButton1 = UIButton() //Custom back Button
            addButton1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            addButton1 .addTarget(self, action: #selector(self.btnAddChatClicked(sender:)), for: UIControlEvents.touchUpInside)
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = 0//-5;
            
            let rightBarButton2 = UIBarButtonItem()
            addButtonView1.addSubview(addButtonImageView1)
            addButtonView1.addSubview(addButton1)
            rightBarButton2.customView = addButtonView1
            self.navigationItem.rightBarButtonItem = rightBarButton2
            self.navigationItem.setRightBarButtonItems([negativeSpacer, rightBarButton, rightBarButton2], animated: false)
        }

        
      }else{
        let addButtonView = UIView()
        addButtonView.frame = CGRect(x: 0, y: 0, width: 19, height: 30)
        let addButtonImageView = UIImageView()
        addButtonImageView.image = #imageLiteral(resourceName: "moreIcon")
        addButtonImageView.contentMode = .scaleAspectFit
        addButtonImageView.frame = CGRect(x: 0, y: 3, width: 24, height: 24)
        let addButton = UIButton()
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addButton .addTarget(self, action: #selector(self.btnChatRightMenuClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        addButtonView.addSubview(addButtonImageView)
        addButtonView.addSubview(addButton)
        rightBarButton.customView = addButtonView
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let addButtonView1 = UIView()
        addButtonView1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let addButtonImageView1 = UIImageView()
        addButtonImageView1.image = #imageLiteral(resourceName: "addChat")
        addButtonImageView1.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
        let addButton1 = UIButton() //Custom back Button
        addButton1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addButton1 .addTarget(self, action: #selector(self.btnAddChatClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0//-5;
        
        let rightBarButton2 = UIBarButtonItem()
        addButtonView1.addSubview(addButtonImageView1)
        addButtonView1.addSubview(addButton1)
        rightBarButton2.customView = addButtonView1
        self.navigationItem.rightBarButtonItem = rightBarButton2
        self.navigationItem.setRightBarButtonItems([negativeSpacer, rightBarButton, rightBarButton2], animated: false)
        }
    }
    
    override func backButtonAction() {
        
        if GroupNameVC.isAddMemberPush == "Yes" {
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            // let vc = navVc.visibleViewController
            topViewController.selectedIndex = 1
            GroupNameVC.isAddMemberPush = "Yes"
            navVc.popViewController(animated: false)
            
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    //MARK: IBActions
    
    func tapLikeButtonAction(sender:UIButton){
        if ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].isLike == 1 {
            self.likeOnNewsFeedMethod(likeValue: 2, eventId: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].eventId, type: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].type!.rawValue)
        }else {
            self.likeOnNewsFeedMethod(likeValue: 1, eventId: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].eventId, type: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].type!.rawValue)
        }

    }
    
    override func btnAddChatClicked(sender: UIButton) {
        groupMenuMainView.removeFromSuperview()
        isMenuTap = false
        UIApplication.shared.keyWindow?.addSubview(addPostView)
        
        if DataManager.role == 1{
            if DataManager.userType == "Student"{
                roundViewHeightConstraint.constant = 55
                stackViewHeightConstraint.constant = 55
                addJobPostView.isHidden = true
            }else{
                roundViewHeightConstraint.constant = 110
                stackViewHeightConstraint.constant = 110
                addJobPostView.isHidden = false
            }
        }else{
            roundViewHeightConstraint.constant = 110
            stackViewHeightConstraint.constant = 110
            addJobPostView.isHidden = false
        }
       
    }
    
    @IBAction func addPostButtonAction(_ sender: Any) {
       // RemovePopUpView
        removePopUpView()
       
        let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kAddPostVc) as! AddPostVC
        nextObj.groupId = groupId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
    
    @IBAction func crossAction(_ sender: Any) {
        removePopUpView()
    }
    //MARK: RemovePopUpView
    private func removePopUpView() {
        self.addPostView.removeFromSuperview()
    }
    @IBAction func addJobPostButtonAction(_ sender: Any) {
        // RemovePopUpView
        removePopUpView()
        
        let storyboard =  UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kAddJobVc) as! AddJobVC
        nextObj.groupId = groupId
        self.navigationController?.pushViewController(nextObj, animated: true)
    }
    
    override func btnChatRightMenuClicked(sender: UIButton) {
        if isMenuTap == false {
            self.view.addSubview(groupMenuMainView)
            
            isMenuTap = true
        }else{
            groupMenuMainView.removeFromSuperview()
            isMenuTap = false
        }
        groupMenuViewHeight.constant = groupMenuTableView.contentSize.height
    }
    

}
//MARK: UITableView DataSource & Delegates
extension GroupNameVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if tableView == groupNameTableView {
             return ConnectionsVM.sharedInstance.groupDetailArray.count
       
        }else{
            return groupMenuArray.count
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == groupNameTableView {
            let type = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].type
            
            if type == NewsFeedType.normal {
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].name
                post1TableCell.linkButtonHeight.constant = 0
                post1TableCell.linkTopConstraint.constant = 0
                
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row

                //Decode Comment
                
                let decodeMsg = (ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                     post1TableCell.titleNameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title
                }else{
                     post1TableCell.titleNameLabel.text  = decodeMsg
                }
                post1TableCell.descriptionLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].Description
                post1TableCell.descriptionLabel.numberOfLines = 2
                post1TableCell.profileDetailButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.profileDetailButton.tag = indexPath.row
                let image = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].profilePic
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post1TableCell.delegate = self
                post1TableCell.timeLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].time
                post1TableCell.dummyArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].newsFeedTagList
                post1TableCell.pollDelegate = self
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                let imageArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userRole == 1{
                    if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }

                
                post1TableCell.likesLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].likes!)"
                post1TableCell.commentLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].comments!)"
                let isAllowLocation = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowLocation!
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].location!
                }
                
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                
                post1TableCell.loadData()
                
                return post1TableCell
            }else if type == NewsFeedType.job {
                
                let jobCell : JobsCell = UITableViewCell.fromNib() //Jobs Cell
                let image = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].profilePic
                jobCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                jobCell.nameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].name
                
                let concatStr:NSMutableAttributedString = NSMutableAttributedString()
                let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributedTitle = NSAttributedString(string: "Job Title:" , attributes: myAttributeBlackColor)
                
                
                
                //Decode Comment
                
                let decodeMsg = (ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].jobName!).fromBase64()
                if(decodeMsg == nil){
                    let attributedStringTitle = NSAttributedString(string: " \(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].jobName!)" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }else{
                    let attributedStringTitle = NSAttributedString(string: " \(decodeMsg ?? "")" , attributes: myAttributeGrayColor)
                    concatStr.append(myAttributedTitle)
                    concatStr.append(attributedStringTitle)
                }
                jobCell.titleLabel.attributedText = concatStr
                
                jobCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.likeButton.tag = indexPath.row
                
                let concat1Str:NSMutableAttributedString = NSMutableAttributedString()
                let myAttribute1BlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13)]
                let myAttribute1GrayColor = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
                let myAttributed1Title = NSAttributedString(string: "Company:" , attributes: myAttribute1BlackColor)
                let attributed1StringTitle = NSAttributedString(string: " \(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].companyName!)" , attributes: myAttribute1GrayColor)
                concat1Str.append(myAttributed1Title)
                concat1Str.append(attributed1StringTitle)
                jobCell.companyName.attributedText = concat1Str

                let isAllowLocation = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowLocation!
                if isAllowLocation != 1{
                    jobCell.locationButtonHeight.constant = 0
                    jobCell.locationLabel.text = ""
                } else{
                    jobCell.locationLabel.sizeToFit()
                    jobCell.locationLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].location
                }
                
                jobCell.skillsLabel.text = ""
                jobCell.skillsTopConstraint.constant = 0
                jobCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                jobCell.nameButton.tag = indexPath.row
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowComment == 1 {
                    jobCell.commentButton.isHidden = false
                    jobCell.commentLabel.isHidden = false
                }else{
                    jobCell.commentButton.isHidden = true
                    jobCell.commentLabel.isHidden = true
                }

                
                var type = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].jobType
                if type == "1"{
                    type = "Full Time Job"
                }else if type == "2"{
                    type = "Part Time Job"
                }else if type == "3"{
                    type = "Paid Internship Job"
                }else {
                    type = "Unpaid Internship Job"
                }
                jobCell.jobTypeLabel.text = "\(type!)"
                jobCell.likeLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].likes!)"
                jobCell.commentLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].comments!)"
                jobCell.isJobVC = false

                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].status == 1{
                    jobCell.availableView.backgroundColor = UIColor.black
                    jobCell.availableLabel.text = "Available"
                }else{
                    jobCell.availableView.backgroundColor = UIColor.darkGray
                    jobCell.availableLabel.text = "Unavailable"
                }
                jobCell.decriptionLabel.text = ""
                jobCell.timeLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].time
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    jobCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userRole == 1{
                    if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userType == "Student"{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        jobCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    jobCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                jobCell.locationLabel.isUserInteractionEnabled = true
                jobCell.locationLabel.tag = indexPath.row
                jobCell.locationLabel.addGestureRecognizer(tapGesture)
                
                jobCell.locationLabel.sizeToFit()
                jobCell.titleLabel.sizeToFit()
                jobCell.companyName.sizeToFit()
                jobCell.skillsLabel.sizeToFit()
                jobCell.loadData()
                return jobCell
            }else if type == NewsFeedType.pollSubmit {
                
                let post1TableCell : Post1TableCell = UITableViewCell.fromNib()
                post1TableCell.nameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].name
                //Decode Comment
                
                let decodeMsg = (ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                    post1TableCell.titleNameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title
                }else{
                    post1TableCell.titleNameLabel.text = decodeMsg
                }
                
                
                
                post1TableCell.linkButtonHeight.constant = 0
                post1TableCell.linkTopConstraint.constant = 0
                post1TableCell.descriptionLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].Description
                post1TableCell.descriptionLabel.numberOfLines = 2
                let image = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].profilePic
                post1TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userRole == 1{
                    if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userType == "Student"{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post1TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                
                post1TableCell.likeButton.addTarget(self, action: #selector(self.tapLikeButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.likeButton.tag = indexPath.row
                
                post1TableCell.likesLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].likes!)"
                post1TableCell.commentLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].comments!)"
                post1TableCell.timeLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].time
                post1TableCell.pollDelegate = self
                post1TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.nameButton.tag = indexPath.row
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post1TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowComment == 1 {
                    post1TableCell.commentButton.isHidden = false
                    post1TableCell.commentLabel.isHidden = false
                }else{
                    post1TableCell.commentButton.isHidden = true
                    post1TableCell.commentLabel.isHidden = true
                }

                post1TableCell.dummyArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].newsFeedTagList
                let imageArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post1TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post1TableCell.imageArray = imageArray
                }
                
                post1TableCell.delegate = self
                let isAllowLocation = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowLocation!
                if isAllowLocation != 1{
                    post1TableCell.locationButtonHeight.constant = 0
                    post1TableCell.locationLabel.text = ""
                } else{
                    post1TableCell.locationLabel.sizeToFit()
                    post1TableCell.locationLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].location!
                }
                
                post1TableCell.pollSubmitButton.addTarget(self, action: #selector(self.tapPollSubmitButton(sender:)), for: UIControlEvents.touchUpInside)
                post1TableCell.pollSubmitButton.tag = indexPath.row
                post1TableCell.locationLabel.sizeToFit()
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].pollList.count == 0 {
                    post1TableCell.pollViewHeight.constant = 0
                    
                }else{
                    post1TableCell.pollArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].pollList
                    //polled by me
                    if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isPolled == "Yes"{
                        post1TableCell.barGraphTypeHeight.constant = 0
                        post1TableCell.submitButtonHeight.constant = 0
                        post1TableCell.pollViewHeight.constant = 200
                    }else {//polled by other
                        post1TableCell.barGraphTypeHeight.constant = 197
                        post1TableCell.submitButtonHeight.constant = 90
                        if post1TableCell.pollArray.count == 5 {
                            post1TableCell.pollViewHeight.constant = 484 + 30
                        }else{
                            post1TableCell.pollViewHeight.constant = 484
                        }
                        
                    }
                }
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post1TableCell.locationLabel.isUserInteractionEnabled = true
                post1TableCell.locationLabel.tag = indexPath.row
                post1TableCell.locationLabel.addGestureRecognizer(tapGesture)
                post1TableCell.loadData()
                return post1TableCell
                
            } else {
                let post2TableCell : Post2TableCell = UITableViewCell.fromNib()
                
                post2TableCell.nameLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].name
                //Decode Comment
               
                let decodeMsg = (ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title)?.fromBase64()
                if(decodeMsg == nil){
                    post2TableCell.titleLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].title
                }else{
                   post2TableCell.titleLabel.text = decodeMsg
                }
                
                post2TableCell.descriptionLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].Description
                post2TableCell.descriptionLabel.numberOfLines = 2
               
                post2TableCell.linkButtonHeight.constant = 0
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userRole == 1{
                    if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].userType == "Student"{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "studentIcon")
                    }else{
                        post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "graduateIcon")
                    }
                }else{
                    post2TableCell.userTypeImageView.image = #imageLiteral(resourceName: "adminIcon")
                }
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowComment == 1 {
                    post2TableCell.commentButton.isHidden = false
                    post2TableCell.commentLabel.isHidden = false
                }else{
                    post2TableCell.commentButton.isHidden = true
                    post2TableCell.commentLabel.isHidden = true
                }
                post2TableCell.likesLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].likes!)"
                post2TableCell.commentLabel.text = "\(ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].comments!)"
                post2TableCell.nameButton.addTarget(self, action: #selector(self.tapNameButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                post2TableCell.nameButton.tag = indexPath.row
                let image = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].profilePic
                post2TableCell.profileImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
                post2TableCell.dateLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].time
                post2TableCell.dummyArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].newsFeedTagList
                let imageArray = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].mediaListData!
                if imageArray.count == 0{
                    post2TableCell.imageCollectionViewHeight.constant = 0
                }else{
                    post2TableCell.imageArray = imageArray
                }
                
                let isAllowLocation = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isAllowLocation!
                if isAllowLocation != 1{
                    post2TableCell.locationButtonHeight.constant = 0
                    post2TableCell.locationLabel.text = ""
                } else{
                    post2TableCell.locationLabel.sizeToFit()
                    post2TableCell.locationLabel.text = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].location!
                }
                

                post2TableCell.delegate = self
                if ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].isLike == 1{
                    // DataManager.likeIcon_url
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.likeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeBlue"))
                } else{
                    post2TableCell.likeButton.sd_setImage(with: URL(string:"\(DataManager.unlikeIcon_url!)"), for: .normal,placeholderImage: #imageLiteral(resourceName: "likeWhite"))
                }
                
                 post2TableCell.rsvpViewHeightConstraint.constant = 0
                
                let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(lblClick(tapGesture:)))
                tapGesture.numberOfTapsRequired = 1
                post2TableCell.locationLabel.isUserInteractionEnabled = true
                post2TableCell.locationLabel.tag = indexPath.row
                post2TableCell.locationLabel.addGestureRecognizer(tapGesture)
                
                post2TableCell.loadData()
                return post2TableCell
            }
            
        }else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
            cell.selectionStyle = .none
            cell.nameLabel.text = groupMenuArray[indexPath.row]
            return cell
        }
      }
    
    
    func lblClick(tapGesture:UITapGestureRecognizer){
        if ConnectionCheck.isConnectedToNetwork(){
            LocationManager.sharedManager.getCoordinates(address: ConnectionsVM.sharedInstance.groupDetailArray[(tapGesture.view?.tag)!].location!) { (coordinates) in
                let latitude: CLLocationDegrees = coordinates!.latitude
                let longitude: CLLocationDegrees = coordinates!.longitude
                
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = ConnectionsVM.sharedInstance.groupDetailArray[(tapGesture.view?.tag)!].location!
                mapItem.openInMaps(launchOptions: options)
                
            }
            
        }else {
            
            self.showAlert(message: kInternetConnectionMsg, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }
        
    }
    func tapRSVPButtonAction(sender:UIButton){
        let dateStr = ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].date
        startEventDate = (dateStr?.dateFromString(format: .shortDMYDate))!
        submitRSVPMethod(event_id: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].eventId, value: 1, eventName: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].title)
    }
    
    
    func tapLinkButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kLinkWebViewVC)as!LinkWebViewVC
        nextObj.linkUrl = ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].link!
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    func tapNameButtonAction(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].userId
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == groupNameTableView {
            let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
            let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
            nextObj.isThroughMyPost = true
            nextObj.newsFeedId = ConnectionsVM.sharedInstance.groupDetailArray[indexPath.row].eventId
            self.navigationController?.pushViewController(nextObj, animated: true)
        
         }else{
             if isAdmin == "No"{
                if isMember == "No"{
                    if indexPath.row == 0 {
                       self.joinUserGroup()
                    }
               
                }else{
                    if indexPath.row == 0 {
                        groupMenuMainView.removeFromSuperview()
                        isMenuTap = false
                        let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                        let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupDetailVC)as!GroupDetailVC
                        nextObj.groupId = groupId
                        self.navigationController?.pushViewController(nextObj, animated: false)
                    }else if indexPath.row == 1{
                        groupMenuMainView.removeFromSuperview()
                        isMenuTap = false
                        leaveGroupMethod()
                    }
                }
                
               
                
            } else{
                
                if indexPath.row == 0 {
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kAddMemberVC)as!AddMemberVC
                    nextObj.groupId = groupId
                    self.navigationController?.pushViewController(nextObj, animated: false)
                }else if indexPath.row == 1{
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                    let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupDetailVC)as!GroupDetailVC
                    nextObj.groupId = groupId
                    self.navigationController?.pushViewController(nextObj, animated: false)
                    
                }else if indexPath.row == 2{
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    leaveGroupMethod()
                    
                }else if indexPath.row == 3{
                    groupMenuMainView.removeFromSuperview()
                    isMenuTap = false
                    deleteGroupMethod()
                    
                }
            }
        }
}
    
    func tapPollSubmitButton(sender:UIButton){
        pollSubmitData(pollId: pollsId, eventId: ConnectionsVM.sharedInstance.groupDetailArray[sender.tag].eventId)
    }
    //MARK: - PAGING CONCEPT
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            Indicator.isEnabledIndicator = false
            
            if !apiRunning {
                self.groupFeedsDetailData()
            }
        }
    }

}
//MARK: Custom Delegates
extension GroupNameVC : VideoPlayerDelegates, VideoPlayersDelegates, PollSubmitDelegates{
    
    func getVideoDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    
    func getVideosDetail(videoUrl: String, name: String) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = videoUrl
        nextObj.userName = name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
    func pollSubmitMethod(pollId: Int) {
        pollsId = pollId
    }
}
//MARK: APIMethods
extension GroupNameVC {
    func pollSubmitData(pollId: Int, eventId:Int){
        NewsFeedVM.sharedInstance.voteOnPoll(userId: DataManager.user_id!, eventId: eventId, pollId: pollId) { (success, message, error) in
            if(success == 1) {
                self.pageNumber = 1
                NewsFeedVM.sharedInstance.newsFeedArray = [NewsFeedData]()
                self.groupFeedsDetailData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }

    func groupFeedsDetailData(){
        apiRunning = true
        ConnectionsVM.sharedInstance.eachGroupNewfeedList(userId: DataManager.user_id!, groupId: groupId, page: pageNumber) { (success, message , error) in
            if(success == 1) {
                self.groupNameTableView.reloadData()
                self.pageNumber += 1
                self.noResultLabel.isHidden = true
                self.groupNameTableView.isHidden = false
            }
            else {
                if(message != nil) {
                    if self.pageNumber == 1{
                         self.groupNameTableView.isHidden = true
                         self.noResultLabel.isHidden = false
                     }
                    
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            self.groupNameTableView.reloadData()
            self.apiRunning = false
        }
    }
    
    func joinUserGroup(){
        ConnectionsVM.sharedInstance.JoinUserGroup(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            
        }
    }
    
    
    func deleteGroupMethod(){
        ConnectionsVM.sharedInstance.DeleteGroup(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            
        }
    }
    
    func leaveGroupMethod(){
        ConnectionsVM.sharedInstance.LeaveGroupMethod(userId: DataManager.user_id!, groupId: groupId) { (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
            
        }
    }
    
    func submitRSVPMethod(event_id:Int, value:Int, eventName:String){
        var value = value
        if value == 0{
            value = 2
        }
        NewsFeedVM.sharedInstance.SubmitRSVPRequest(userId: DataManager.user_id!, eventId: event_id, value: value) { (success, message , error) in
            if(success == 1) {
                self.groupFeedsDetailData()
                self.createEventinTheCalendar(with: eventName, forDate: self.startEventDate, toDate: self.startEventDate)
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    func createEventinTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    self.showAlert(message: "failed to save event with error : \(error)", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                    
                }
                
            } else {
                //we have error in getting access to device calnedar
                self.showAlert(message: error?.localizedDescription, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                
            }
        }
    }
    
    func likeOnNewsFeedMethod(likeValue:Int, eventId: Int, type: Int){
        NewsFeedVM.sharedInstance.LikeNewfeed(userId: DataManager.user_id!, eventId: eventId, newfeedType: type, value: likeValue) { (success, message, error) in
            if(success == 1) {
                
                self.pageNumber = 1
                ConnectionsVM.sharedInstance.groupDetailArray = [NewsFeedData]()
                self.groupFeedsDetailData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    

    
    
    
}
