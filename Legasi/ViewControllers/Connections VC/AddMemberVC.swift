//
//  AddMemberVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class AddMemberVC: BaseVC {

    //IBOutlets
    @IBOutlet weak var memberTableView: UITableView!
    
    @IBOutlet weak var noResultLabel: UILabel!
    
    @IBOutlet weak var addMemberButton: UIButton!
    
    
  //VARIABLES
    var selectedIndexPath = Int()
    var memberIdArray = [Int]()
    var groupId : Int!
    //MARK: Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        noResultLabel.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "ADD MEMBERS")
        setBackButton()
        memberTableView.estimatedRowHeight = 83
        memberTableView.rowHeight = UITableViewAutomaticDimension
        memberTableView.tableFooterView = UIView()
        memberListData()
    }
    
//MARK:- IBActions
    @IBAction func addMemberButtonAction(_ sender: Any) {
        var memberIdString = ""
        if memberIdArray.count > 0 {
            for id in memberIdArray{
                if memberIdString == "" {
                    memberIdString = "\(id)"
                } else {
                    memberIdString = "\(memberIdString),\(id)"
                }
            }
        }
        
        if memberIdString == ""{
            self.showAlert(message: "Please Select Atleast 1 Member.", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }else{
            addMemberMethod(memberIds: memberIdString)
        }
        
        
    }

}

//MARK: UITableViewDelegate & UITableViewDataSource
extension AddMemberVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ConnectionsVM.sharedInstance.memberListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.lightGray, borderWidth: 0.5)
        cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
        let image = ConnectionsVM.sharedInstance.memberListArray[indexPath.row].userImageUrl
        cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.width/2
        cell.userImageView.clipsToBounds = true
        cell.nameLabel.text = ConnectionsVM.sharedInstance.memberListArray[indexPath.row].name
        if memberIdArray.contains(where: {$0 == ConnectionsVM.sharedInstance.memberListArray[indexPath.row].memberId}) {
            cell.tickImageView.image = #imageLiteral(resourceName: "activeGreen")
        }else{
            cell.tickImageView.image = #imageLiteral(resourceName: "activeGray")
        }
        cell.tickImageView.clipsToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if !memberIdArray.contains(where: {$0 == ConnectionsVM.sharedInstance.memberListArray[indexPath.row].memberId}) {
           memberIdArray.append(ConnectionsVM.sharedInstance.memberListArray[indexPath.row].memberId!)
           
         }else{
            if let index = self.memberIdArray.index(where: {$0 == ConnectionsVM.sharedInstance.memberListArray[indexPath.row].memberId}) {
                self.memberIdArray.remove(at: index)
                
            }
            
         }
        
         memberTableView.reloadData()
        
    }
}

//MARK: Webservice Method
extension AddMemberVC{
    func memberListData(){
        ConnectionsVM.sharedInstance.MemberList(userId: DataManager.user_id!, schoolId: DataManager.schoolId!) { (success, message , error) in
            if(success == 1) {
                self.noResultLabel.isHidden = true
                self.addMemberButton.isHidden = false
                self.memberTableView.isHidden = false
                self.memberTableView.reloadData()
            }
            else {
                self.noResultLabel.isHidden = false
                self.addMemberButton.isHidden = true
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.memberTableView.isHidden = true
            }
        }
    }
    
    func addMemberMethod(memberIds:String){
        ConnectionsVM.sharedInstance.AddMemberInGroup(userId: DataManager.user_id!, memberId: memberIds, groupId: groupId) { (success, message , error) in
            if(success == 1) {
               self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                  self.navigationController?.popViewController(animated: false)
               })
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    

    
    
}
