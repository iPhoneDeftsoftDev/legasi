//
//  AddGroupVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreLocation

class AddGroupVC: BaseVC {
    //IBOutlets
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var updateGroupIconButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var imageUploadButton: UIButton!
    
    @IBOutlet weak var updateCoverButton: UIButton!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var locationSwitchButton: UISwitch!
    @IBOutlet weak var privateSwitchButton: UISwitch!

    //Variables
    var isEdit = Bool()
    var groupId: Int!
    var imageDict = [String : Data]()
    var visibilityType = String()
    var latitude: Double!
    var longitude:Double!
    var location: Int!
    var imageName: String!
    var isImageSelected = false
    var isEditImage = false
    var groupDetailDict = groupDetailData()
    var actionType = Int()
    //MARK: Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setBackButton()
        userImageView.setRadius(radius: 10)
        descriptionTextView.textColor = UIColor.darkGray
        groupNameTextField.setPlaceholder()
        if isEdit == true{
            if isImageSelected == false{
                coverImageView.backgroundColor = UIColor.LegasiColor.lightGray.colorWith(alpha: 1.0)
                userImageView.backgroundColor = UIColor.lightGray
                updateGroupIconButton.setTitleColor(UIColor.black, for: .normal)
                updateCoverButton.setTitleColor(UIColor.black, for: .normal)
                blurView.backgroundColor = UIColor.LegasiColor.lightGray.colorWith(alpha: 1.0)
                updateCoverButton.setRadius(radius: 5, borderColor: UIColor.black, borderWidth: 1)

            } else{
                updateGroupIconButton.setTitleColor(UIColor.white, for: .normal)
                updateCoverButton.setTitleColor(UIColor.white, for: .normal)
                blurView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                updateCoverButton.setRadius(radius: 5, borderColor: UIColor.white, borderWidth: 1)

            }
            setTitle(title: "CREATE GROUP")
            updateGroupIconButton.setTitle("Add Group Icon", for: .normal)
            updateCoverButton.setTitle("Add Group Image", for: .normal)
            actionType = 1
        }else{
            setTitle(title: "EDIT GROUP")
            if isEditImage == false{
                getGroupDetailMethod()
            }
            
            blurView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            updateGroupIconButton.setTitle("Update Group Icon", for: .normal)
            updateGroupIconButton.setTitleColor(UIColor.white, for: .normal)
            updateCoverButton.setTitleColor(UIColor.white, for: .normal)
            updateCoverButton.setRadius(radius: 5, borderColor: UIColor.white, borderWidth: 1)
            actionType = 2
           
            
        }
            self.visibilityType = "Private"
            self.location = 1
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //set current location latitude and longitude
            self.latitude = appdelegate.latitude
            self.longitude = appdelegate.longitude
    }
    func getGroupDetailMethod(){
        
        groupNameTextField.text = groupDetailDict.groupName
        descriptionTextView.text = groupDetailDict.groupDesc
        coverImageView.sd_setImage(with: URL(string:"\(String(describing: groupDetailDict.groupCoverImageUrl!))"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
        userImageView.sd_setImage(with: URL(string:"\(String(describing: groupDetailDict.groupImageUrl!))"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        let privacy = groupDetailDict.visibleType
        if privacy == "Private"{
            privateSwitchButton.setOn(true, animated: false)
            self.visibilityType = "Private"
        }else{
            privateSwitchButton.setOn(false, animated: false)
            self.visibilityType = "Public"
        }
        
        let location = groupDetailDict.groupLocation
        if location == "1"{
            locationSwitchButton.setOn(true, animated: false)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //set current location latitude and longitude
            self.latitude = appdelegate.latitude
            self.longitude = appdelegate.longitude
            self.location = 1

        }else{
            locationSwitchButton.setOn(false, animated: false)
            self.latitude = 0.0
            self.longitude = 0.0
            self.location = 2
        }
        actionType = 2
        imageDict = [String : Data]()
    }


    //MARK: IBActions
    @IBAction func updateGroupIconButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        isEditImage = true
        self.imagePickerDelegate = self
        self.imageName = APIKeys.kGroupIconImage
        self.showImagePicker()
    }
    @IBAction func imageUploadButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        isEditImage = true
        self.imagePickerDelegate = self
        self.imageName = APIKeys.kGroupIconImage
        self.showImagePicker()
    }
    
    @IBAction func updateCoverImageButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        isEditImage = true
        self.imagePickerDelegate = self
        self.imageName = APIKeys.kGroupImage
        self.showImagePicker()
    }
    
    @IBAction func updateButtonAction(_ sender: Any) {
        updateButton.isUserInteractionEnabled = false
        if  groupNameTextField.text == kEmptyString || ((groupNameTextField.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            updateButton.isUserInteractionEnabled = true
            self.showAlert(message: kJobTitleMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }
        else if descriptionTextView.text == kEmptyString || descriptionTextView.text == "Description" || ((descriptionTextView.text?.trimmingCharacters(in: .whitespaces)) == kEmptyString) {
            updateButton.isUserInteractionEnabled = true
            self.showAlert(message: kLocationMessage, title: kAlert, otherButtons: nil, cancelTitle: kOk)
        }else{
            addGroupInfoMethod()
        }
     
    }
    @IBAction func switchButtonAction(_ sender: Any) {
        
        if privateSwitchButton.isOn == true{
            self.visibilityType = "Private"
        }
        else{
            self.visibilityType = "Public"
        }
        
    }
    
    @IBAction func locationSwitchButtonAction(_ sender: Any) {
        if locationSwitchButton.isOn == true{
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //set current location latitude and longitude
            self.latitude = appdelegate.latitude
            self.longitude = appdelegate.longitude
            self.location = 1
        } else{
            self.latitude = 0.0
            self.longitude = 0.0
            self.location = 2
        }
    }
    
}
//MARK: Custom Image Picker Delegates
extension AddGroupVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {  //Get Image from Picker and use the image
        imageDict[imageName] = image.jpegData(.medium)
       
        if imageName == APIKeys.kGroupIconImage {
            userImageView.image = image
        }else{
             coverImageView.image = image
             isImageSelected = true

        }
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
       imageDict[imageName] = thumbnail.jpegData(.medium)
        if imageName == APIKeys.kGroupIconImage {
            userImageView.image = thumbnail
        }else{
            coverImageView.image = thumbnail
        }
    }
    
}
//MARK:  UITextFieldDelegate & UITextViewDelegate
extension AddGroupVC :  UITextFieldDelegate , UITextViewDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Description"
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description"){
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
    func textViewDidChange(_ textView: UITextView) {
        var frame = textView.frame
        frame.size.height = textView.contentSize.height
        textView.frame = frame
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
       let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
      return numberOfChars < 300;
        return true
    }
    
    
}
//MARK: APIMethods
extension AddGroupVC {
    
    func addGroupInfoMethod(){
        
        ConnectionsVM.sharedInstance.AddEditGroupMethod(userId: DataManager.user_id!, actionType: actionType, schoolId: DataManager.schoolId!, groupName: groupNameTextField.text!, groupDescription:descriptionTextView.text!, visibleType: visibilityType, location: location, groupLat: latitude, groupLong: longitude, groupId: groupId, image: imageDict) { (success, message , error) in
            if(success == 1) {
                self.updateButton.isUserInteractionEnabled = true
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
            }
            else {
                self.updateButton.isUserInteractionEnabled = true
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
         
    }
}
