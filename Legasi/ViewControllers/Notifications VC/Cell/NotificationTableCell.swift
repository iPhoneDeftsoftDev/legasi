//
//  NotificationTableCell.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class NotificationTableCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet var profileView: UIView!
    @IBOutlet var messageLabel: UILabel!
    
    @IBOutlet var rejectViewButton: UIButton!
    @IBOutlet var AcceptButton: UIButton!
    @IBOutlet var acceptViewWidth: NSLayoutConstraint!
    @IBOutlet var lineView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    //MARK: Class Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.profileView.setRadius(radius: self.profileView.frame.height/2)
        self.profileView.layer.borderColor = UIColor.darkGray.cgColor
        self.profileView.layer.borderWidth = 0.5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
