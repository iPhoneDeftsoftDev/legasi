//
//  Notifications.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class NotificationsVC: BaseVC {
    
    //MARK: IBOutlet
    @IBOutlet var notificationTableView: UITableView!
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name(rawValue: "NotificationList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchNotificationData(notification:)), name: NSNotification.Name(rawValue: "NotificationList"), object: nil)
    }
    //MARK: NSNOTIFICATION METHOD
    func fetchNotificationData(notification: NSNotification){
        self.notificationListMethod()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CustomizeUI()
    }
    //NotificationList
    //MARK: Private Function
    private func CustomizeUI() {
        
        //MARK: Set Title 
        self.setTitle(title: "NOTIFICATIONS")
        self.customSlideButton()
        
        //Set Table View Height Automatic
        notificationTableView.estimatedRowHeight = 44
        notificationTableView.rowHeight = UITableViewAutomaticDimension
        self.notificationTableView.tableFooterView = UIView()
        self.notificationListMethod()
    }
}

//MARK: TableViewDataSource & TableViewDelegates
extension NotificationsVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationsVM.sharedInstance.notificationsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationsCell, for: indexPath) as! NotificationTableCell
        cell.selectionStyle = .none
        let concatStr:NSMutableAttributedString = NSMutableAttributedString()
        let myAttributeBlackColor = [NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.semiBold.fontWithSize(size: 16)]
        let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
        let myAttributedUsername = NSAttributedString(string: "\(NotificationsVM.sharedInstance.notificationsArray[indexPath.row].OtherUserName!)" , attributes: myAttributeBlackColor)
        
        if NotificationsVM.sharedInstance.notificationsArray[indexPath.row].notificationType == "8" {
            let attributedStringLiked = NSAttributedString(string: "\(NotificationsVM.sharedInstance.notificationsArray[indexPath.row].message!) " , attributes: myAttributeGrayColor)
            concatStr.append(attributedStringLiked)
            concatStr.append(myAttributedUsername)
        }else {
            let attributedStringLiked = NSAttributedString(string: " \(NotificationsVM.sharedInstance.notificationsArray[indexPath.row].message!)" , attributes: myAttributeGrayColor)
            concatStr.append(myAttributedUsername)
            concatStr.append(attributedStringLiked)
        }
        
        cell.messageLabel.attributedText = concatStr
        if NotificationsVM.sharedInstance.notificationsArray[indexPath.row].notificationType != "13"{
            cell.acceptViewWidth.constant = 0
            cell.lineView.backgroundColor = UIColor.white
        } else{
            cell.acceptViewWidth.constant = 90
            cell.lineView.backgroundColor = UIColor.lightGray
        }
        
        cell.AcceptButton.addTarget(self, action: #selector(self.tapAcceptAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.AcceptButton.tag = indexPath.row
        cell.rejectViewButton.addTarget(self, action: #selector(self.tapRejectAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.rejectViewButton.tag = indexPath.row
        cell.timeLabel.text = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].miliseconds
        let image = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].OtherUserProfileImage
        cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        return cell
    }
    
    func tapAcceptAction(sender:UIButton){
        let notifyId = NotificationsVM.sharedInstance.notificationsArray[sender.tag].notificationId
        acceptRequestAction(value: 1, notificationId: notifyId!)
    }
    func tapRejectAction(sender:UIButton){
        let notifyId = NotificationsVM.sharedInstance.notificationsArray[sender.tag].notificationId
        acceptRequestAction(value: 2, notificationId: notifyId!)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].notificationType!
        if type == "1" || type == "2" || type == "5" ||  type == "6" || type == "7" || type == "9" || type == "10" || type == "12"{
            let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
            let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
            nextObj.isThroughMyPost = true
            nextObj.newsFeedId = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].otherId
            self.navigationController?.pushViewController(nextObj, animated: true)
        } else if  type == "3" || type == "4"{
            let storyBoard = UIStoryboard.storyboard(storyboard: .SlideMenu)
            let nextObj = storyBoard.instantiateViewController(withIdentifier: kUserProfileVC) as! UserProfileVC
            nextObj.otherUserId = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].otherUserId
            self.navigationController?.pushViewController(nextObj, animated: true)
       
        }
        else if  type == "11" || type == "14" {
            let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
            let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupNameVC)as!GroupNameVC
            nextObj.titleName = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].groupName!
            nextObj.groupId = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].otherId
            nextObj.isAdmin = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].isAdmin!
            self.navigationController?.pushViewController(nextObj, animated: false)
            
        }
            
        else if type == "8"{
          
            let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
            let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
            nextObj.titleStr = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].OtherUserName
            nextObj.friendId = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].otherUserId
            nextObj.chatListId = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].otherId
            nextObj.otherUserProfilePic = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].OtherUserProfileImage!
            nextObj.otherUserName = NotificationsVM.sharedInstance.notificationsArray[indexPath.row].OtherUserName
            self.navigationController?.pushViewController(nextObj, animated: false)
        }
        
    }
}
//MARK: APIMethods
extension NotificationsVC {
    func notificationListMethod(){
        NotificationsVM.sharedInstance.NotificationList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.notificationTableView.isHidden = false
                self.notificationTableView.reloadData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.notificationTableView.isHidden = true
            }
        }
    }
    func acceptRequestAction(value:Int, notificationId:Int){
        NotificationsVM.sharedInstance.AcceptJoinRequest(userId: DataManager.user_id!, notificationId: notificationId, value: value) { (success, message , error) in
            if(success == 1) {
                self.notificationListMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.notificationTableView.isHidden = true
            }
        }
    }
}
