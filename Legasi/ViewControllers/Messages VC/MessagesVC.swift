//
//  MessagesVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift



class MessagesVC: BaseVC {
    
    // IBOutlets
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chatTextField: UITextView!
    @IBOutlet weak var attachmentButton: UIButton!
    @IBOutlet var profileView: UIView!
   
    @IBOutlet weak var bottomConstraintTextView: NSLayoutConstraint!
    //MARK: Variables
    var titleStr: String!
    var isPopUp = false
    var friendId:Int!
    var chatListId:Int!
    var otherUserName = String()
    var otherUserProfilePic = String()
    var chatListArray = [userInboxData]()
    var messageType = String()
    var mediaName = String()
    var thumbnail = ""
    //Image Or message
    var isImage = false
    //data for image
    var imageDict = [String:Data]()
    var videoDict = [String:Data]()
    let imageArray = NSMutableArray()
    var isMessageReceived = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        chatDetailMethod()
        NotificationCenter.default.removeObserver(self,name:NSNotification.Name(rawValue: "ChatList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchChatData(notification:)), name: NSNotification.Name(rawValue: "ChatList"), object: nil)
        
    }
    //MARK: NSNOTIFICATION METHOD
    func fetchChatData(notification: NSNotification){
        print(notification)
        isMessageReceived = true
        let postDict = notification.userInfo as NSDictionary!
        titleStr = postDict?["otherUserName"]as!String
        friendId = postDict?["friendId"]as!Int
        chatListId = postDict?["chatListId"]as!Int
        otherUserName = postDict?["otherUserName"]as!String
        otherUserProfilePic = postDict?["otherUserProfilePic"]as!String
        chatDetailMethod()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: titleStr)
        setBackButton()
        setMoreButton()
        messageType = "1"
        self.chatTableView.estimatedRowHeight = 55.0
        self.chatTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    override func backButtonAction() {
        let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
        let navVc = topViewController.selectedViewController as!UINavigationController
        // let vc = navVc.visibleViewController
        topViewController.selectedIndex = 2
        navVc.popViewController(animated: false)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    //MARK: Notification methods for keyboard present and dismiss
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        //set constraints of bottom of text view
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraintTextView.constant = keyboardFrame.size.height + 8
        })
        
        //dispatch time to scroll at last
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            if(self.chatListArray.count > 0){
                self.scrollAtLast()
            }
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        //set constraints of bottom of text view
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraintTextView.constant = 8;
        })
        
        //dispatch time to scroll at last
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            if(self.chatListArray.count > 0){
                self.scrollAtLast()
            }
        })
    }
    func scrollAtLast(){//scroll at last index
       
        let path = IndexPath(row: chatListArray.count - 1, section: 0)
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            if path.row > 0 {
                self.chatTableView.scrollToRow(at: path,
                                               at: UITableViewScrollPosition.bottom, animated: true)
            }
        })
     }
    

    override func moreButtonAction() {
        if isPopUp {
            profileView.removeFromSuperview()
        }else {
            profileView.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height)
            self.view.addSubview(profileView)
        }
        isPopUp = !isPopUp
    }
    //MARK: - IBActions
    @IBAction func blockUserButtonAction(_ sender: Any) {
        self.showAlert(message: "Are you sure you want to block this user?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
                
                self.blockUnblockMethod(friendId:self.friendId, status:1)
        })
        
        
    }
    @IBAction func profileButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = friendId
        self.navigationController?.pushViewController(nextObj, animated: false)
  
    
    }
    @IBAction func sendButtonAction(_ sender: Any) {
        isMessageReceived = false
        self.view .endEditing(true)
        let trimmingString = chatTextField.text.trimmingCharacters(in: .whitespaces)
        if !trimmingString.isEmpty  && trimmingString != "Enter Your Message Here"{
            isImage = false
            chatTextField.isUserInteractionEnabled = false
            //Method for sending message
            sendMessageMethod()
        }
    }
    @IBAction func attachmentButtonAction(_ sender: Any) {
        self.view .endEditing(true)
        //Create Actionsheet for getting image
        self.imagePickerDelegate = self
        self.showImagePicker(mediaType: .both)
        
      }
    func setTextViewIntialStage (){//set textview at initial stage
        chatTextField.isScrollEnabled = false
        chatTextField.text = "Enter Your Message Here"
        chatTextField.textColor = UIColor.darkGray
        //TODO:
        //        let point = CGPoint(x: 0,y :arrForData.count-1) // CGFloat, Double, Int
        //        tableVwChat.setContentOffset(point, animated: true)
    }

    
}
//MARK: Imagepicker Delegate
extension MessagesVC: CustomImagePickerDelegate{
    func didPickImage(_ image: UIImage) {//Get Image from Picker and use the image
        imageDict["0"] = image.jpegData(.medium)
        mediaName = "image"
        messageType = "2"
        isImage = true
        //Method for sending image
        sendMessageMethod()
//        chatTextField.text = ""
//        dismiss(animated: true, completion: { () -> Void in
//            self.chatTableView .reloadData()
//            if(self.chatListArray.count > 0){
//                self.scrollAtLast()
//            }
//            self.setTextViewIntialStage()
//        })
    }
    
    func didPickVideo(_ thumbnail: UIImage, videoUrl: URL) {
        imageArray.add(thumbnail)
        mediaName = "video"
        messageType = "3"
        do {
            let data =  try Data(contentsOf: videoUrl, options: .mappedIfSafe)
            imageDict["0"]  = data
        }catch {
            print(error)
            return
        }
        
        sendMessageMethod()
        
    }

}


//MARK: - UITableView Delegate & Datasources
extension MessagesVC: UITableViewDataSource, UITableViewDelegate{
    //MARK: UITABLEVIEW DELEGATES & DATASOURCES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return chatListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let msgType = chatListArray[indexPath.row].msgType
        
        
         //Current User Text Msg
        if chatListArray[indexPath.row].sendFrom != DataManager.user_id! && msgType == MsgType.nothing {
            let cell = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
            cell.selectionStyle = .none
            cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
            cell.userImageView.setRadius(radius: cell.userImageView.frame.width/2)
            cell.userImageView.clipsToBounds = true
            cell.userImageView.sd_setImage(with: URL(string:"\(otherUserProfilePic)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
           
            cell.userImageView.clipsToBounds = true
       //Message Decode
            let decodeMsg = (chatListArray[indexPath.row].msg)?.fromBase64()
            if(decodeMsg == nil){
                cell.msgLabel.text = chatListArray[indexPath.row].msg
            }else{
                cell.msgLabel.text = decodeMsg
            }
            cell.nameLabel.text = otherUserName
            cell.dateLabel.text = chatListArray[indexPath.row].miliseconds
            return cell
            
            
        }else if(chatListArray[indexPath.row].sendFrom == DataManager.user_id! && msgType == MsgType.nothing){//For reciever and text message
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)as!chatSenderTableViewCell
            cell.selectionStyle = .none
            cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
            cell.userImageView.setRadius(radius: cell.userImageView.frame.width/2)
            cell.userImageView.clipsToBounds = true
            
            cell.userImageView.sd_setImage(with: URL(string:"\(DataManager.user_image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            cell.userImageView.clipsToBounds = true
            //Message Decode
            let decodeMsg = (chatListArray[indexPath.row].msg)?.fromBase64()
            if(decodeMsg == nil){
                 cell.senderLabel.text = chatListArray[indexPath.row].msg
            }else{
                 cell.senderLabel.text = decodeMsg
            }
            cell.nameLabel.text = DataManager.user_name!
            cell.dateLabel.text = chatListArray[indexPath.row].miliseconds
            return cell
            
        }else if(chatListArray[indexPath.row].sendFrom == DataManager.user_id! && (msgType == MsgType.image || msgType == MsgType.video)){//current user and image(attachment)
            let cell = tableView.dequeueReusableCell(withIdentifier: kSenderImageCell, for: indexPath)as!SenderImageTableViewCell
            cell.selectionStyle = .none
            cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
            cell.userImageView.sd_setImage(with: URL(string:"\(DataManager.user_image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            
            cell.userImageView.setRadius(radius: cell.userImageView.frame.width/2)
            cell.userImageView.clipsToBounds = true
            
            let msgImage = chatListArray[indexPath.row].image
            cell.msgImageView.sd_setImage(with: URL(string:"\(msgImage!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
            cell.msgImageView.clipsToBounds = true
            cell.msgImageView.layer.cornerRadius = 10.0
            cell.blurView.layer.cornerRadius = 10.0
            let media =  chatListArray[indexPath.row].mediaListData
            if media?[0].videoUrl == ""{
                cell.blurView.isHidden = true
            }else{
                cell.blurView.isHidden = false
                cell.videoButton.addTarget(self, action: #selector(self.tapVideoButton(sender:)), for: UIControlEvents.touchUpInside)
                cell.videoButton.tag = indexPath.row
            }
            
            
            
            return cell
            
        }else if(chatListArray[indexPath.row].sendFrom != DataManager.user_id! && (msgType == MsgType.image || msgType == MsgType.video)){//For reciever and text message
            let cell = tableView.dequeueReusableCell(withIdentifier: kReceiverImageCell, for: indexPath)as!ReceiverImageTableViewCell
            cell.selectionStyle = .none
            cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
            cell.userImageView.sd_setImage(with: URL(string:"\(otherUserProfilePic)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
            
            cell.userImageView.setRadius(radius: cell.userImageView.frame.width/2)
            cell.userImageView.clipsToBounds = true
            
            let msgImage = chatListArray[indexPath.row].image
            cell.msgImageView.sd_setImage(with: URL(string:"\(msgImage!)"), placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
            cell.msgImageView.clipsToBounds = true
            cell.msgImageView.layer.cornerRadius = 10.0
            cell.blurView.layer.cornerRadius = 10.0
            let media =  chatListArray[indexPath.row].mediaListData
            if media?[0].videoUrl == ""{
                cell.blurView.isHidden = true
            }else{
                cell.blurView.isHidden = false
                cell.videoButton.addTarget(self, action: #selector(self.tapVideoButton(sender:)), for: UIControlEvents.touchUpInside)
                cell.videoButton.tag = indexPath.row
            }
            return cell
            
        }
        let cell = UITableViewCell()
        return cell
    }
    
    func tapVideoButton(sender:UIButton){
        let storyboard = UIStoryboard.storyboard(storyboard: .NewsFeed)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kVideoPlayerVC)as!VideoPlayerVC
        nextObj.videoUrl = chatListArray[sender.tag].mediaListData[0].videoUrl
        nextObj.userName = chatListArray[sender.tag].name
        self.navigationController?.pushViewController(nextObj, animated: false)
    }
    
}
//MARK: UITextViewDelegates Delegates
extension MessagesVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView.text == ""){
            textView.text = "Enter Your Message Here"
            textView.textColor = UIColor.darkGray
        }
        
        textView.resignFirstResponder()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        DispatchQueue.main.async {
            if(textView.text == "Enter Your Message Here"){
                textView.text = ""
                textView.textColor = UIColor.black
            }
            textView.becomeFirstResponder()
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        //text view will get height till four lines
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            let numLines = textView.contentSize.height / (textView.font?.lineHeight)!;
            if(numLines > 4){
                textView.isScrollEnabled = true;
            }else{
                textView.isScrollEnabled = false;
            }
        })
        
        //check for whether table has data then sc
//        if(chatListArray.count > 1){
//            scrollAtLast()
//        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
   }


//MARK: Webservice Method
extension MessagesVC{
    func chatDetailMethod(){
        InboxVM.sharedInstance.EachChatDetail(userId:  DataManager.user_id!, chatListId: chatListId) { (success, message , error) in
            if(success == 1) {
                self.chatListArray = InboxVM.sharedInstance.chatListArray
                self.chatTableView.reloadData()
                self.scrollAtLast()
                if !self.isMessageReceived {
                    self.setTextViewIntialStage()
                }
            }
            else {
                
                if(message != nil) {
                   // self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
    
    
    
    func sendMessageMethod(){
        
       var array = [Data]()
        for (_,value) in imageDict {
            array.append(value)
        }
        var thumbArray = [Data]()
        var mimeType: MimeType = .image
        if messageType == "3" {
            mimeType = .video
            for image in imageArray {
                thumbArray.append((image as! UIImage).jpegData(.medium)!)
                thumbnail = "image"
            }
        }
        //Encode Message
       let encodedMsg :String = chatTextField.text.trimmingCharacters(in: .whitespaces).toBase64()
        
     InboxVM.sharedInstance.SendMsgMethod(userId: DataManager.user_id!, chatListId: chatListId, text: encodedMsg, friendId: friendId, msgType: messageType, mediaName: mediaName, vedioArray: thumbArray, thumbNailName: thumbnail, mediaArray: array, mimeType: mimeType) { (success, message , error) in
            if(success == 1) {
                self.chatListId = InboxVM.sharedInstance.chatListId!
                self.chatDetailMethod()
                
             }
            else {
                
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        self.chatTextField.isUserInteractionEnabled = true
        }
        
    }
    
    func blockUnblockMethod(friendId:Int, status:Int){
        self.profileView.removeFromSuperview()
        InboxVM.sharedInstance.BlockUnblockUser(userId: DataManager.user_id!, otherUserId: friendId, value: status) { (success, message , error) in
            if(success == 1) {
                
                self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                    self.navigationController?.popViewController(animated: false)
                })
                
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}
