//
//  BlockListVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BlockListVC: BaseVC {
//MARK: IBOutlets
    @IBOutlet weak var blockTableView: UITableView!
    @IBOutlet weak var noRecordLabel: UILabel!
    
    //MARK: Variables
    var nameArray = [blockListData]()
   
    //MARK: - Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "BLOCK LIST")
        setBackButton()
        blockTableView.estimatedRowHeight = 83
        blockTableView.rowHeight = UITableViewAutomaticDimension
        blockTableView.tableFooterView = UIView()
        noRecordLabel.isHidden = true
        blockListMethod()
    }

}
//MARK: - UITableView Delegate & Datasources
extension BlockListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.lightGray, borderWidth: 0.5)
        cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
        let image = nameArray[indexPath.row].profileImage
        cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))

        cell.nameLabel.text = nameArray[indexPath.row].name
        cell.block_btn.setRadius(radius: 5, borderColor: UIColor.black, borderWidth: 1)
        cell.block_btn.addTarget(self, action: #selector(self.tapBlockButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.block_btn.tag = indexPath.row
        return cell
    }
    
    func tapBlockButtonAction(sender:UIButton){
        self.showAlert(message: "Are you sure you want to unblock this user?", title: kAlert, otherButtons: ["No":{ (action) in
            
            }], cancelTitle: "Yes", cancelAction: { (action) in
                
                self.blockUnblockMethod(friendId: self.nameArray[sender.tag].userId, status: 2)
        })
        
        
        
    }
 }
//MARK:- Webservice Method
extension BlockListVC {
    
    func blockListMethod(){
        InboxVM.sharedInstance.GetBlockList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.nameArray = InboxVM.sharedInstance.blockListArray
                self.noRecordLabel.isHidden = true
                self.blockTableView.isHidden = false
                self.blockTableView.reloadData()
            }
            else {
                self.noRecordLabel.isHidden = false
                self.blockTableView.isHidden = true
                if(message != nil) {
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
      }
    func blockUnblockMethod(friendId:Int, status:Int){
        InboxVM.sharedInstance.BlockUnblockUser(userId: DataManager.user_id!, otherUserId: friendId, value: status) { (success, message , error) in
            if(success == 1) {
                self.blockListMethod()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }

    
}
