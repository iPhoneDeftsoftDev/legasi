//
//  chatSenderTableViewCell.swift
//  CourierServiceCustomer
//
//  Created by 123 on 20/06/17.
//  Copyright © 2017 DEFTSOFT. All rights reserved.
//

import UIKit

class chatSenderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet var senderLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
