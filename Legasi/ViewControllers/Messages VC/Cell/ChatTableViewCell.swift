//
//  ChatTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    
    @IBOutlet weak var block_btn: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var connectionsLabelHeight: NSLayoutConstraint!
    @IBOutlet var connectionsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var tickImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
