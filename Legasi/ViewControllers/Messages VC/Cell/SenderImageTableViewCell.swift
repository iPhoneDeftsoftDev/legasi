//
//  SenderImageTableViewCell.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SenderImageTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var blurView: UIView!
    
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var msgImageView: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
