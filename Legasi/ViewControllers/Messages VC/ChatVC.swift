//
//  ChatVC.swift
//  Legasi
//
//  Created by 123 on 02/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

class ChatVC: BaseVC {

   //MARK: - IBOutlets
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var blockListMainView: UIView!
    @IBOutlet var blockListView: UIView!
    @IBOutlet weak var noRecordLabel: UILabel!
    //MARK: - Variables
    var isBlockListTap = false
    var nameArray = [userInboxData]()
    //MARK: - Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        blockListMainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blockListView.setRadius(radius: 10)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
     
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "MESSAGES")
        customSlideButton()
        custumRightTwoButtons()
        chatTableView.tableFooterView = UIView()
        chatTableView.estimatedRowHeight = 87
        chatTableView.rowHeight = UITableViewAutomaticDimension
        noRecordLabel.isHidden = true
        inboxListMethod()
    }
    override func btnChatRightMenuClicked(sender: UIButton) {
        if isBlockListTap == true{
            self.blockListMainView.removeFromSuperview()
            isBlockListTap = false
        }else{
          self.view.addSubview(blockListMainView)
            isBlockListTap = true
        }
    }
   //MARK: - IBActions
    @IBAction func blockListButtonAction(_ sender: Any) {
        self.blockListMainView.removeFromSuperview()
        isBlockListTap = false
        let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kBlockListVC)
        self.navigationController?.pushViewController(nextObj, animated: false)
        
        
    }
    

}
 //MARK: - UITableView Delegate & Datasources
extension ChatVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
        cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
        let image = nameArray[indexPath.row].profileImage
        cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        
        let myAttributeBlackColor = [ NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName: UIFont.LegasiFont.semiBold.fontWithSize(size: 16)]
        let myAttributeGrayColor = [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.LegasiFont.regular.fontWithSize(size: 13) ]
       let myAttributedUsername = NSAttributedString(string: nameArray[indexPath.row].name , attributes: myAttributeBlackColor)
       
        var message: String!
        if nameArray[indexPath.row].msgType == MsgType.image || nameArray[indexPath.row].msgType == MsgType.video{
            if nameArray[indexPath.row].sendFrom == DataManager.user_id {
                 message = "media file send"
            }else{
                 message = "media file receive"
            }
           
        }else{
            //Message Decode
            let decodeMsg = (nameArray[indexPath.row].msg)?.fromBase64()
            if(decodeMsg == nil){
                message = nameArray[indexPath.row].msg
            }else{
                message = decodeMsg
            }
        }
        let attributedStringLiked = NSAttributedString(string: " \n\n\(message!)" , attributes: myAttributeGrayColor)
        let concatStr:NSMutableAttributedString = NSMutableAttributedString()
        concatStr.append(myAttributedUsername)
        concatStr.append(attributedStringLiked)
        cell.nameLabel.attributedText = concatStr
        cell.nameLabel.sizeToFit()
        cell.dateLabel.text = nameArray[indexPath.row].miliseconds
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
        nextObj.titleStr = nameArray[indexPath.row].name
        if nameArray[indexPath.row].sendFrom == DataManager.user_id {
            nextObj.friendId = nameArray[indexPath.row].sendTo
        }else{
          nextObj.friendId = nameArray[indexPath.row].sendFrom
        }
        nextObj.chatListId = nameArray[indexPath.row].chatListId
        nextObj.otherUserProfilePic = nameArray[indexPath.row].profileImage
        nextObj.otherUserName = nameArray[indexPath.row].name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
}

//MARK:- Webservice Method
extension ChatVC {
    
    func inboxListMethod(){
        InboxVM.sharedInstance.ChatInboxList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.nameArray = InboxVM.sharedInstance.inboxArray
                self.noRecordLabel.isHidden = true
                self.chatTableView.isHidden = false
                self.chatTableView.reloadData()
            }
            else {
                self.noRecordLabel.isHidden = false
                self.chatTableView.isHidden = true
                if(message != nil) {
                     self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
        
    }
}
