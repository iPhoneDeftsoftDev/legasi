//
//  ChatUserVC.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ChatUserVC: BaseVC {
  //MARK: - IBOutlets
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var noRecordLabel: UILabel!
    //MARK: - Variables
    var nameArray = [MemberDetail]()
   //MARK: -Class Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customiseUI()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "SELECT")
        setBackButton()
        userTableView.estimatedRowHeight = 83
        userTableView.rowHeight = UITableViewAutomaticDimension
        userTableView.tableFooterView = UIView()
        noRecordLabel.isHidden = true
        ChatUserListMethod()
    }
    
}
//MARK: - UITableView Delegate & Datasources
extension ChatUserVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: kChatCell, for: indexPath)as!ChatTableViewCell
        cell.selectionStyle = .none
        cell.mainView.setRadius(radius: cell.mainView.layer.frame.width/2, borderColor: UIColor.clear, borderWidth: 0.0)
        cell.userImageView.setRadius(radius: cell.userImageView.layer.frame.width/2)
        let image = nameArray[indexPath.row].userImageUrl
        cell.userImageView.sd_setImage(with: URL(string:"\(image!)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        cell.nameLabel.text = nameArray[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
        nextObj.titleStr = nameArray[indexPath.row].name
        nextObj.friendId = nameArray[indexPath.row].memberId
        nextObj.chatListId = 0
        nextObj.otherUserProfilePic = nameArray[indexPath.row].userImageUrl
        nextObj.otherUserName = nameArray[indexPath.row].name
        self.navigationController?.pushViewController(nextObj, animated: false)
        
    }
}

//MARK: Webservice Method
extension ChatUserVC {
    
    func ChatUserListMethod(){
        InboxVM.sharedInstance.UserFriendList(userId: DataManager.user_id!) { (success, message , error) in
            if(success == 1) {
                self.nameArray = InboxVM.sharedInstance.friendListArray
                self.noRecordLabel.isHidden = true
                self.userTableView.isHidden = false
                self.userTableView.reloadData()
            }
            else {
                self.noRecordLabel.isHidden = false
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
                self.userTableView.isHidden = true
            }
        }
    }
}
