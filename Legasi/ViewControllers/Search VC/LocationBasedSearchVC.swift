//
//  LocationBasedSearchVC.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class LocationBasedSearchVC: BaseVC {
    
    //MARK: IBOutlets
    @IBOutlet var profileCollectionView: UICollectionView!
    @IBOutlet var leftArrowButton: UIButton!
    @IBOutlet weak var topScreenConstraint: NSLayoutConstraint!
    @IBOutlet var rightArrowButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var studentsSwitchButton: UISwitch!
    @IBOutlet weak var graduatesSwitchButton: UISwitch!
    @IBOutlet var myLocationLabel: UILabel!
    
    @IBOutlet var graduateLabel: UILabel!
    @IBOutlet weak var checkInButton: UIButton!
    var locationManager = CLLocationManager()
    var latitude : Double?
    var longitude : Double?
    var currentLocationCoordinates : CLLocationCoordinate2D?
    var isStudent : String?
    var isGradutes : String?
    var locationMarker: GMSMarker?
    var locationBaseArray = [locationBaseUser]()
    var newLocationBaseArray = [locationBaseUser]()
    var selectedIndex = 0
    var isTapMarker = false
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchLocationData(notification:)), name: NSNotification.Name(rawValue: "locationUpdate"), object: nil)
        
    }
    //MARK: NSNOTIFICATION METHOD
    func fetchLocationData(notification: NSNotification){
        CustomizeUI()
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CustomizeUI()
    }
    
    //MARK: Private Function
    private func CustomizeUI() {
        if UIScreen.main.bounds.height >= 712 {
            topScreenConstraint.constant = 45
        }
        self.setBackButton()
        self.navigationController?.isNavigationBarHidden = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        self.myLocationLabel.text = ""
        graduateLabel.text = DataManager.nameForGraduate!.uppercased()
        checkInButton.isUserInteractionEnabled = false
        getCurrentLocation()
        loadCollectionView()
        self.leftArrowButton.isEnabled = false
    }
    
    //MARK: - GET CURRENT LOCATION COORDINATES
    func getCurrentLocation(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        //set current location latitude and longitude
        self.latitude = appdelegate.latitude
        self.longitude = appdelegate.longitude
        
        self.currentLocationCoordinates = appdelegate.coordinates
        self.createCurrentLoactionMarker(locationCoordinates: appdelegate.coordinates)
        //reverse geocoading
        self.getLocationNameFromCoordinates(coordinate: appdelegate.coordinates)
        //call location base search webservice
        self.searchUsersByLocation()
        
    }
    
    //REVERSE GEOCOADING TO GET CURRENT LOCATION NAME
    func getLocationNameFromCoordinates(coordinate: CLLocationCoordinate2D){
        LocationManager.sharedManager.reverseGeoCoading(location: coordinate, complete: { (addressDetails) in
            DispatchQueue.main.async {
                if addressDetails.count > 0{
                    print(addressDetails)
                    print(addressDetails[0].locality)
                    if addressDetails[0].locality != nil{
                        
                        self.myLocationLabel.text = addressDetails[0].locality.uppercased()
                        self.checkInButton.isUserInteractionEnabled = true
                    }else{
                        self.myLocationLabel.text = ""
                    }
                }else{
                    self.myLocationLabel.text = ""
                }
                self.checkInButton.isUserInteractionEnabled = true
            }
        })
    }
    
    //MARK: - CREATE CURRENT LOCATION MARKER
    func createCurrentLoactionMarker(locationCoordinates : CLLocationCoordinate2D){
        let currentLoactionMarker: GMSMarker = GMSMarker(position: locationCoordinates)
        currentLoactionMarker.icon = #imageLiteral(resourceName: "currentLocationMarker")
        currentLoactionMarker.map = self.mapView
    }
    
    //location base search webservice
    
    func searchUsersByLocation(){
        //check on the bases of student and grauates
        if studentsSwitchButton.isOn == true{
            self.isStudent = "1"
        }
        else{
            self.isStudent = "2"
        }
        
        if graduatesSwitchButton.isOn == true{
            self.isGradutes = "1"
        }else{
            self.isGradutes = "2"
        }
        
        //call location base search webservice
        if self.latitude != nil{
            self.locationBaseSearch(student: self.isStudent!, graduate: self.isGradutes!, latitude: self.latitude!, longitude: self.longitude!)
        }else{
            self.showAlert(message: "No Current location found")
        }
    }
    
    //MARK: - MANAGE COLLECTION VIEW
    private func loadCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.itemSize = CGSize(width:self.profileCollectionView.frame.width, height: self.profileCollectionView.frame.height)
        layout.scrollDirection = .horizontal
        profileCollectionView.collectionViewLayout = layout
    }
    
    private func moveCollectionToFrame(contentOffset : CGFloat) {
        print(contentOffset)
        let frame: CGRect = CGRect(x : contentOffset ,y : self.profileCollectionView.contentOffset.y ,width : self.profileCollectionView.frame.width,height : self.profileCollectionView.frame.height)
        self.profileCollectionView.scrollRectToVisible(frame, animated: true)
        
    }
    
    //MARK: - BACK BUTTON ACTION
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - LEFT ARROW BUTTON ACTION
    @IBAction func LeftArrowButtonAction(_ sender: Any) {
        var index = profileCollectionView.indexPathsForVisibleItems.first
        index?.row -= 1
        if index?.row == 0 {
            self.leftArrowButton.isEnabled = false
        }else {
            self.leftArrowButton.isEnabled = true
        }
        self.rightArrowButton.isEnabled = true
        if (index?.row)! >= 0 {
            self.profileCollectionView.scrollToItem(at: index!, at: .left, animated: true)
            selectedIndex = (index?.row)!
            self.mapView.clear()
            self.createCurrentLoactionMarker(locationCoordinates: self.currentLocationCoordinates!)
            self.showMarkersOnMap(locationArray: self.newLocationBaseArray)
        }
    }
    
    //MARK: - RIGHT ARROW BUTTON ACTION
    @IBAction func RightArrowButtonAction(_ sender: Any) {
        var index = profileCollectionView.indexPathsForVisibleItems.first
        index?.row += 1
        if index?.row == self.newLocationBaseArray.count - 1 {
            self.rightArrowButton.isEnabled = false
        }else {
            self.rightArrowButton.isEnabled = true
        }
        self.leftArrowButton.isEnabled = true
        if (index?.row)! < self.newLocationBaseArray.count{
            self.profileCollectionView.scrollToItem(at: index!, at: .right, animated: true)
            selectedIndex = (index?.row)!
            self.mapView.clear()
            self.createCurrentLoactionMarker(locationCoordinates: self.currentLocationCoordinates!)
            self.showMarkersOnMap(locationArray: self.newLocationBaseArray)
        }
        
        
    }
    
    //MARK: - STUDENT BUTTON ACTION
    @IBAction func studentsSwitchAction(_ sender: Any) {
        if !graduatesSwitchButton.isOn && !studentsSwitchButton.isOn{
            graduatesSwitchButton.isOn = true
            
        }
        self.setupArray()
    }
    
    //MARK: - GRADDUATES BUTTON ACTION
    @IBAction func graduatesSwitchAction(_ sender: Any) {
        if !graduatesSwitchButton.isOn && !studentsSwitchButton.isOn{
            studentsSwitchButton.isOn = true
        }
        self.setupArray()
    }
    
    //MARK: - CHECK IN BUTTON ACTION
    @IBAction func checkInAction(_ sender: Any) {
        if self.myLocationLabel.text != "" && self.latitude != nil {
            self.checkInWebservice(latitude: self.latitude!, longitude: self.longitude!, currentLocation: self.myLocationLabel.text!)
        }else{
            
            self.showAlert(message: "Please turn on your location.", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    } else {
                        UIApplication.shared.openURL(settingsUrl)
                    }
                }
            })
        }
        
    }
    
    
    func setupArray() {
        self.newLocationBaseArray.removeAll()
        for (index,value) in self.locationBaseArray.enumerated(){
            if self.locationBaseArray[index].userType == "Graduate" && graduatesSwitchButton.isOn {
                self.newLocationBaseArray.append(value)
                self.profileCollectionView.reloadData()
            }
            if self.locationBaseArray[index].userType == "Student" && studentsSwitchButton.isOn {
                self.newLocationBaseArray.append(value)
                self.profileCollectionView.reloadData()
            }
        }
        self.showMarkersOnMap(locationArray: self.newLocationBaseArray)
    }
    
}

//MARK: - EXTENSION FOR WEBSERVICES
extension LocationBasedSearchVC {
    
    //MARK: - LOCATION BASE SEARCH WS
    func locationBaseSearch(student : String,graduate: String,latitude: Double, longitude: Double){
        SearchVM.sharedInstance.searchByLocation(student: student, graduate: graduate, lat: latitude, long: longitude) { (success, message , error) in
            if(success == 1) {
                
                //check if users are available
                if SearchVM.sharedInstance.locationBaseUserArray.count > 0{
                    self.profileCollectionView.reloadData()
                    self.locationBaseArray = SearchVM.sharedInstance.locationBaseUserArray
                    self.newLocationBaseArray = self.locationBaseArray
                    
                    if self.locationBaseArray.count == 1{
                        self.leftArrowButton.isEnabled = false
                        self.rightArrowButton.isEnabled = false
                    }
                    
                    self.showMarkersOnMap(locationArray: self.newLocationBaseArray)
                    
                }else{
//                    self.showAlert(message: "No users are available.", title: kAlert, otherButtons: nil, cancelTitle: "OK", cancelAction: { (sucess) in
//                        self.backButtonAction()
//                    })
                }
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
    
    //MARK: - SHOW MARKERS ON MAP VIEW
    func showMarkersOnMap(locationArray : [locationBaseUser]) {
        
        //IF both buttons are ON hen show complete data
        self.mapView.clear()
        self.createCurrentLoactionMarker(locationCoordinates: self.currentLocationCoordinates!)
        
        //add coodinates into array.
        let markerArray : NSMutableArray = NSMutableArray()
        for locationData in locationArray{
            var locationNameMarker : CLLocationCoordinate2D = CLLocationCoordinate2D()
            locationNameMarker.latitude = locationData.latitude
            locationNameMarker.longitude = locationData.longitude
            markerArray.add(locationNameMarker)
        }
        
        //create marker on the bases of student and graduates
        for (index, coordinates) in markerArray.enumerated(){
            if locationArray[index].userType == "Student"{
                if studentsSwitchButton.isOn {
                    locationMarker = GMSMarker(position: coordinates as! CLLocationCoordinate2D)
                    locationMarker?.appearAnimation = GMSMarkerAnimation.pop
                    locationMarker?.opacity = 0.75
                    let locationResult = locationArray[index]
                    if index == self.selectedIndex {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                            self.locationMarker?.title  = locationResult.name
                            self.mapView.selectedMarker = self.locationMarker
                        }
                    }
                    self.locationMarker?.icon = #imageLiteral(resourceName: "graduatesMarker")
                    self.locationMarker?.map = self.mapView
                    
                }
            }else if locationArray[index].userType == "Graduate"{
                if graduatesSwitchButton.isOn {
                    locationMarker = GMSMarker(position: coordinates as! CLLocationCoordinate2D)
                    locationMarker?.appearAnimation = GMSMarkerAnimation.pop
                    locationMarker?.opacity = 0.75
                    let locationResult = locationArray[index]
                    if index == self.selectedIndex {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                            self.locationMarker?.title  = locationResult.name
                            self.mapView.selectedMarker = self.locationMarker
                        }
                    }
                    self.locationMarker?.icon = #imageLiteral(resourceName: "graduatesMarker")
                    self.locationMarker?.map = self.mapView
                }
            }
        }
    }
    
    //MARK: -  CHECK IN WS
    func checkInWebservice(latitude : Double, longitude: Double,currentLocation: String){
        SearchVM.sharedInstance.checkIn(lat: latitude, long: longitude, location: self.myLocationLabel.text!){ (success, message , error) in
            if(success == 1) {
                self.showAlert(message: message)
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}


//MARK: - MAP VIEW DELEGATES AND DATA SOURCES
extension LocationBasedSearchVC : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        for i in 0..<self.newLocationBaseArray.count{
            if marker.layer.latitude == self.newLocationBaseArray[i].latitude{
                var index = profileCollectionView.indexPathsForVisibleItems.first
               index?.row = i
               self.isTapMarker = true
                if index?.row == 0 {
                    self.leftArrowButton.isEnabled = false
                }else {
                    self.leftArrowButton.isEnabled = true
                }
                self.rightArrowButton.isEnabled = true
                if (index?.row)! >= 0 {
                    
                    self.profileCollectionView.scrollToItem(at: index!, at: .left, animated: true)
                    selectedIndex = (index?.row)!
                   marker.title = self.newLocationBaseArray[i].name
                }
                
                if index?.row == self.newLocationBaseArray.count - 1 {
                    self.rightArrowButton.isEnabled = false
                }else {
                    self.rightArrowButton.isEnabled = true
                }
                self.leftArrowButton.isEnabled = true
                if (index?.row)! < self.newLocationBaseArray.count{
                    self.profileCollectionView.scrollToItem(at: index!, at: .right, animated: true)
                    selectedIndex = (index?.row)!
                   marker.title = self.newLocationBaseArray[i].name
                }
               
               break
                
                
            }
            
            
        }
        
        
        
        
        print("Tap on marker function called")
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("camera position changed.")
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("On clicking tap info info window.")
    }
    
    //  func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView?{
    // }
    
}

//MARK: - LOCATION MANAGER DELEGATES

extension LocationBasedSearchVC: CLLocationManagerDelegate {
    func locationManager( _ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = false
        }
    }
    
    func locationManager( _ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            //GO TO LOCATION WITH ANIMATION
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                CATransaction.setValue(Int(1), forKey: kCATransactionAnimationDuration)
                CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))
                self.mapView.animate(to: .init(target: location.coordinate, zoom: 10, bearing: 0, viewingAngle: 0)) //Bearing 270 before
            })
            CATransaction.commit()
            locationManager.stopUpdatingLocation()
            
        }
    }
}

//MARK: CollectionViewDataSources
extension LocationBasedSearchVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.newLocationBaseArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = profileCollectionView.dequeueReusableCell(withReuseIdentifier: kProfileCell, for: indexPath) as! ProfileCollectionViewCell
        
        if self.newLocationBaseArray[indexPath.row].profileImageUrl != ""{
            cell.profileImage.sd_setImage(with: URL(string: self.newLocationBaseArray[indexPath.row].profileImageUrl)!, placeholderImage: #imageLiteral(resourceName: "userImage"))
            
        } else{
            cell.profileImage.image = #imageLiteral(resourceName: "userImage")
        }
        cell.yearLabel.text = self.newLocationBaseArray[indexPath.row].graduationYear
        if self.newLocationBaseArray[indexPath.row].role == 1 {
            // graduate or student
            if self.newLocationBaseArray[indexPath.row].userType == "Student"{
                cell.nameLabel.text = "\(self.newLocationBaseArray[indexPath.row].name!) (\(self.newLocationBaseArray[indexPath.row].userType!))"
            }else{
                cell.nameLabel.text = "\(self.newLocationBaseArray[indexPath.row].name!) (\(DataManager.nameForGraduate!))"
            }
        }else {     //in case 2 or 3
            //admin
            cell.nameLabel.text = "\(self.newLocationBaseArray[indexPath.row].name!) (School admin)"
        }
//        if self.newLocationBaseArray[indexPath.row].userType! == "Student"{
//            cell.nameLabel.text = "\(self.newLocationBaseArray[indexPath.row].name!) (\(self.newLocationBaseArray[indexPath.row].userType!))"
//        }else{
//            cell.nameLabel.text = "\(self.newLocationBaseArray[indexPath.row].name!) (\(DataManager.nameForGraduate!))"
//        }
        if self.newLocationBaseArray[indexPath.row].coverImageUrl != ""{
            cell.coverImageView.sd_setImage(with: URL(string: self.newLocationBaseArray[indexPath.row].coverImageUrl)!, placeholderImage: #imageLiteral(resourceName: "dummy_placeholder"))
        }else{
            cell.profileImage.image = #imageLiteral(resourceName: "dummy_placeholder")
        }
        
        cell.profileImage.setRadius(radius: 7)
        selectedIndex = indexPath.row
        if isTapMarker != true{
            self.mapView.clear()
            self.createCurrentLoactionMarker(locationCoordinates: self.currentLocationCoordinates!)
            self.showMarkersOnMap(locationArray: self.newLocationBaseArray)
        }
        
        return cell
    }
}

//MARK: Collection View Delegates
extension LocationBasedSearchVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard.init(name: "SlideMenu", bundle: nil)
        let nextObj = storyBoard.instantiateViewController(withIdentifier: kUserProfileVC) as! UserProfileVC
        nextObj.otherUserId = self.newLocationBaseArray[indexPath.row].userId
        self.navigationController?.pushViewController(nextObj, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width:self.profileCollectionView.frame.width, height: self.profileCollectionView.frame.height)
        return cellSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
        
        switch pageNumber {
        case 0 :
            self.rightArrowButton.isEnabled = true
            self.leftArrowButton.isEnabled = false
            break
        case self.newLocationBaseArray.count-1 :
            self.rightArrowButton.isEnabled = false
            self.leftArrowButton.isEnabled = true
            break
        default:
            self.rightArrowButton.isEnabled = true
            self.leftArrowButton.isEnabled = true
        }
    }
    
    
    
}

