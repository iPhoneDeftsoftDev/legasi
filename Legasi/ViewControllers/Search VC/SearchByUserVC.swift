//
//  SearchByUserVC.swift
//  Legasi
//
//  Created by 123 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreLocation

protocol SearchUsersDelegates {
    func getUserDetail(requestDict: JSONDictionary,filterArray:[searchFilter])
}

class SearchByUserVC: BaseVC {
    
    //MARK: IBOutlet
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var milesTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!
    @IBOutlet weak var graduateButton: UIButton!
    @IBOutlet weak var studentButton: UIButton!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fromYearTextField: UITextField!
    @IBOutlet weak var toYearTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var graducationYearHeight: NSLayoutConstraint!
    @IBOutlet weak var groupsHeightConstraint: NSLayoutConstraint!
    @IBOutlet var searchUsersButton: UIButton!
    @IBOutlet var searchGroupsButton: UIButton!
    @IBOutlet var groupsCollectionVew: UICollectionView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var graduationView: UIView!
    @IBOutlet var groupsView: UIView!
    @IBOutlet var groupCollectionViewHeight: NSLayoutConstraint!
    
    //MARK: Variables
    var selectedTextField: UITextField!
    var delegate : SearchUsersDelegates?
    var coordinates = CLLocationCoordinate2D()
    var filterArray = [searchFilter]()
    var requestDict =  JSONDictionary()
    var userType = String()
    var groupIdArray = [Int]()
    var currentPickerIndex = 0
   //MARK: Static Variables
    static var location = String()
    static var zipCode = String()
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        locationView.generateShadowUsingBezierPath(radius: 5.0, frame: CGRect(x: 15, y: locationView.frame.origin.y, width: self.view.frame.width-30, height: locationView.frame.height))
        graduationView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: graduationView.frame.origin.y, width: self.view.frame.width-30, height: graduationView.frame.height))
        groupsView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: groupsView.frame.origin.y, width: self.view.frame.width-30, height: groupsView.frame.height))
        
        self.scrollViewHeight.constant = self.view.frame.size.height - 200
        self.scrollView.contentSize.height = locationView.frame.height + 215
        //UITextFieldDelegate
        zipcodeTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        locationTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customiseUI()
    }
    
       
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "SEARCH")
        setBackButton()
        
        //Set shadow to view
        self.pickerDelegate = self
        requestDict["searchType"] = "2"
        getGroupList()
        
        //Filter for Search Type
        if let index = filterArray.index(where: {$0.type == FilterType.searchType}) {
            let name = filterArray[index].title
            if name == "Users" {
                searchUsersButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
                searchGroupsButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            }
            else {
                searchUsersButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
                searchGroupsButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
                //SetScroll Acc to View Height
                groupsHeightConstraint.constant = 0
                graducationYearHeight.constant = 0
                scrollView.isScrollEnabled = false
                self.scrollViewHeight.constant = locationView.frame.height + 70
                graduationView.isHidden = true
                groupsView.isHidden = true
            }
        } 
        else {
//            let data = searchFilter(title: "Users", type: .searchType)
//            filterArray.append(data)
            searchUsersButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
            searchGroupsButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
        }
        
        //Location
        milesTextField.text = requestDict["miles"] as? String ?? ""
        locationTextField.text = SearchByUserVC.location
        zipcodeTextField.text = SearchByUserVC.zipCode
        
        //Graduation
        
        
        let dict = self.convertToDictionary(text: requestDict["graduationYear"] as? String ?? "")
        
        
        let title = (dict)?["userType"] as? String ?? ""
        let fromYear = (dict)?["from"] as? String ?? ""
         let toYear = (dict)?["to"] as? String ?? ""
                
        if title == "Student" {
            studentButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
            graduateButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            toYearTextField.isEnabled = true
            fromYearTextField.isEnabled = true
        }
        else if title == "Graduate" {
            studentButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            graduateButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
            toYearTextField.isEnabled = true
            fromYearTextField.isEnabled = true
        }
        else {
            studentButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            graduateButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            toYearTextField.isEnabled = false
            fromYearTextField.isEnabled = false
        }
        fromYearTextField.text = fromYear
        toYearTextField.text = toYear
       
        let idStr = requestDict["groupIds"] as? String
        let array = idStr?.components(separatedBy: ",")
        
        if filterArray.index(where: {$0.type == FilterType.groupIds}) != nil {
            
            if (array?.count)! > 0 {
                for id in array!{
                    groupIdArray.append(Int(id)!)
                }
                groupsCollectionVew.reloadData()
            }
            
        }

       
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
   // Get picker index with date
    func getPickerIndexWithDate(){
        let yearArray = UserLoginVM.sharedInstance.getYearList()
        let yearArrayString = yearArray.map {
            String($0)
        }
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let index = yearArrayString.index(of: "\(year)")
        currentPickerIndex = index ?? 0
    }
    
   //MARK: IBActions
    @IBAction func studentButtonAction(_ sender: Any) {
        
        
        if userType == "Student"{
            userType = ""
            toYearTextField.isEnabled = false
            fromYearTextField.isEnabled = false
            toYearTextField.text = ""
            fromYearTextField.text = ""
            getPickerIndexWithDate()
            graduateButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            studentButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
        }else{
            userType = "Student"
            toYearTextField.isEnabled = true
            fromYearTextField.isEnabled = true
            graduateButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            studentButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
        }
        
     }
    
    @IBAction func graduateButtonAction(_ sender: Any) {
        if(userType == "Graduate"){
            graduateButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            studentButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            toYearTextField.isEnabled = false
            fromYearTextField.isEnabled = false
            toYearTextField.text = ""
            fromYearTextField.text = ""
            getPickerIndexWithDate()
            userType = ""
        }else{
            graduateButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
            studentButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
            toYearTextField.isEnabled = true
            fromYearTextField.isEnabled = true
            userType = "Graduate"
        }
        
   
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func filterButtonAction(_ sender: Any) {
        
        if groupIdArray.count > 0 {
            var idString = ""
            for id in groupIdArray{
                if idString == "" {
                    idString = "\(id)"
                }
                else {
                    idString = "\(idString),\(id)"
                }
            }
            requestDict["groupIds"] = idString
            //            let data = searchFilter(title: "Groups", type: .groupIds)
            //            if let index  = filterArray.index(where: {$0.type == FilterType.groupIds}) {
            //                filterArray[index] = data
            //            }
            //            else {
            //                filterArray.append(data)
            //            }
        }
        if(userType != ""){
            if(fromYearTextField.text?.characters.count == 0){
                self.showAlert(message: "Please Select 'From' Year!", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else if(toYearTextField.text?.characters.count == 0){
                self.showAlert(message: "Please Select 'To' Year!", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }else{
                self.delegate?.getUserDetail(requestDict: requestDict, filterArray: filterArray)
                self.navigationController?.popViewController(animated: false)
            }
        }else{
            self.delegate?.getUserDetail(requestDict: requestDict, filterArray: filterArray)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    //MARK: IBActions
    @IBAction func searchUsersButtonAction(_ sender: Any) {
        locationTextField.resignFirstResponder()
        milesTextField.resignFirstResponder()
        zipcodeTextField.resignFirstResponder()
        //Update Button Images
        searchUsersButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
        searchGroupsButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
       
        requestDict["searchType"] = "2"
        
        //SetScroll Acc to View Height
        graducationYearHeight.constant = 215
       // self.groupCollectionViewHeight.constant = self.groupsCollectionVew.collectionViewLayout.collectionViewContentSize.height
        groupsHeightConstraint.constant = 200
        self.scrollViewHeight.constant = self.view.frame.size.height - 130
        self.scrollView.contentSize.height = locationView.frame.height + 215 + 130
        scrollView.isScrollEnabled = true
       //Set graduationView and groupsView hidden false
        graduationView.isHidden = false
        groupsView.isHidden = false
    }
    
    @IBAction func searchGroupsButtonAction(_ sender: Any) {
        locationTextField.resignFirstResponder()
        milesTextField.resignFirstResponder()
        zipcodeTextField.resignFirstResponder()
        //Update Button Images
        searchGroupsButton.setImage(#imageLiteral(resourceName: "enableBlue"), for: .normal)
        searchUsersButton.setImage(#imageLiteral(resourceName: "disableGray"), for: .normal)
        
        
        requestDict["searchType"] = "3"

        
        //SetScroll Acc to View Height
        groupsHeightConstraint.constant = 0
        graducationYearHeight.constant = 0
        scrollView.isScrollEnabled = false
        self.scrollViewHeight.constant = locationView.frame.height + 70
        graduationView.isHidden = true
        groupsView.isHidden = true
        
        
        
        
    }
    

}

//MARK: UITextField Delegates
extension SearchByUserVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == fromYearTextField) {
            let yearArray = UserLoginVM.sharedInstance.getYearList()
            let yearArrayString = yearArray.map {
                String($0)
            }
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let index = yearArrayString.index(of: "\(year)")
            currentPickerIndex = index ?? 0
//            fromYearTextField.text = yearArrayString[index!]
            setPicker(textField: textField, array:  yearArrayString, defaultIndex: index ?? 0)
        }
        else if(textField == toYearTextField) {
            let yearArray = UserLoginVM.sharedInstance.getYearList()
            let yearArrayString = yearArray.map {
                String($0)
            }
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let index = yearArrayString.index(of: "\(year)")
            currentPickerIndex = index ?? 0
//            toYearTextField.text = yearArrayString[index!]
            setPicker(textField: textField, array:  yearArrayString, defaultIndex: index ?? 0)
            
            
        }else if textField == zipcodeTextField{
                if (milesTextField.text?.isEmpty)!{
                    self.showAlert(message: "Please enter miles first", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }
        } else if textField == locationTextField{
                textField.resignFirstResponder()
                if (milesTextField.text?.isEmpty)!{
                    self.showAlert(message: "Please enter miles first", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }else{
                    let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                    let locationVC = storyboard.instantiateViewController(withIdentifier: kSearchLocationVC) as! SearchLocationVC
                    locationVC.delegate = self
                    self.navigationController?.pushViewController(locationVC, animated: true)
                    
                }
          }
        selectedTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == zipcodeTextField || textField == locationTextField) {
            if zipcodeTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty && locationTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
                if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray.remove(at: index)
                    
                }
            }
            else {
                LocationManager.sharedManager.getCoordinates(address: textField.text!, completed: { (coordinates) in
                    if coordinates != nil {
                        self.coordinates = coordinates!
                        
                        var title = textField.text!
                        if !self.milesTextField.text!.isEmpty {
                            title = "\(self.milesTextField.text!) miles from \(textField.text!)"
                        }
                        
                        let data = searchFilter(title: title, type: .address, id: nil)
                        if let index  = self.filterArray.index(where: {$0.type == FilterType.address}) {
                            self.filterArray[index] = data
                        }
                        else {
                            self.filterArray.append(data)
                        }
                        
                       

                        
                    }
                    else {
                        self.coordinates = CLLocationCoordinate2D()
                        if textField == self.zipcodeTextField {
                            self.showAlert(message: "Please enter valid zip code")
                        }
                        else {
                            self.showAlert(message: "Please enter valid address")
                        }
                    }
                })
            }
        }
        if textField == milesTextField {
            if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
                if let index = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray.remove(at: index)
                    
                }
            }
            else {
                var title = "\(textField.text!) miles"
                if !self.locationTextField.text!.isEmpty {
                    title = "\(title) from \(self.locationTextField.text!)"
                }
                let data = searchFilter(title: title, type: .address, id: nil)
                if let index  = self.filterArray.index(where: {$0.type == FilterType.address}) {
                    self.filterArray[index] = data
                }
                else {
                    self.filterArray.append(data)
                }
                
            }
        }
        requestDict["miles"] = milesTextField.text!
        requestDict["lat"] = coordinates.latitude
        requestDict["long"] = coordinates.longitude
        //SearchByUserVC.location = locationTextField.text!
        SearchByUserVC.zipCode = zipcodeTextField.text!
        
    }
    @objc func textFieldDidChange(textField: UITextField) {
        if(textField == zipcodeTextField){
            locationTextField.text = ""
        }
        else if(textField == locationTextField) {
            zipcodeTextField.text = ""
        }
        self.coordinates = CLLocationCoordinate2D()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 50 // Bool
    }
}
extension SearchByUserVC : SearchLocationDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getAddress(address: String, latitude: Double, longitude: Double) {
        if !address.isEmpty {
            locationTextField.text = address
            
            var title = selectedTextField.text!
            if !self.milesTextField.text!.isEmpty {
                title = "\(self.milesTextField.text!) miles from \(selectedTextField.text!)"
            }
            
            let data = searchFilter(title: title, type: .address, id: nil)
            if let index  = self.filterArray.index(where: {$0.type == FilterType.address}) {
                self.filterArray[index] = data
            }
            else {
                self.filterArray.append(data)
            }
            
            requestDict["lat"] = latitude
            requestDict["long"] = longitude
            SearchByUserVC.location = locationTextField.text!
            
            
        }
    }
}

//MARK: Custom Picker Delegates
extension SearchByUserVC: LegasiPickerDelegate {
    
    func didClickOnDoneButton(){
        var title = userType
        if(selectedTextField == fromYearTextField ) {
            
            fromYearTextField.text = "\(UserLoginVM.sharedInstance.getYearList()[currentPickerIndex])"
            if !(toYearTextField.text?.isEmpty)!{
                
                title = "\(title) from \(fromYearTextField.text!) to \(toYearTextField.text!)"
            }

        }
        else if(selectedTextField == toYearTextField) {
            
            toYearTextField.text = "\(UserLoginVM.sharedInstance.getYearList()[currentPickerIndex])"
            
            if !(fromYearTextField.text?.isEmpty)!{
                
                if !(toYearTextField.text?.isEmpty)!{
                    
                    title = "\(title) from \(fromYearTextField.text!) to \(toYearTextField.text!)"
                }
            }else{
                
//                self.showAlert(message: "Please Select From Year First!", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
            }
            
        }
        
        let data = searchFilter(title: title, type: .graduateYear, id: nil)
        if let index = self.filterArray.index(where: {$0.type == FilterType.graduateYear}) {
            self.filterArray[index] = data
        }
        else {
            self.filterArray.append(data)
        }
        
        let dict = ["userType":userType, "from": "\(fromYearTextField.text!)",
            "to":"\(toYearTextField.text!)"]
        if let objectData = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            requestDict["graduationYear"] = objectString
        }
        
    }
    
   func didSelectPickerViewAtIndex(index: Int) {
    
       var title = userType
       if(selectedTextField == fromYearTextField ) {
            currentPickerIndex = index
//           fromYearTextField.text = "\(UserLoginVM.sharedInstance.getYearList()[index])"
        
        }
        else if(selectedTextField == toYearTextField) {
            currentPickerIndex = index
//            toYearTextField.text = "\(UserLoginVM.sharedInstance.getYearList()[index])"
        
//        if !(fromYearTextField.text?.isEmpty)!{
//            
//            if !(toYearTextField.text?.isEmpty)!{
//                
//                 title = "\(title) from \(fromYearTextField.text!) to \(toYearTextField.text!)"
//            }
//        }else{
//            
//            self.showAlert(message: "Please Select From Year First!", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
//        }
        
    }
    
//    let data = searchFilter(title: title, type: .graduateYear, id: nil)
//    if let index = self.filterArray.index(where: {$0.type == FilterType.graduateYear}) {
//        self.filterArray[index] = data
//    }
//    else {
//        self.filterArray.append(data)
//    }
//   
//    let dict = ["userType":userType, "from": "\(fromYearTextField.text!)",
//        "to":"\(toYearTextField.text!)"]
//    if let objectData = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: 0)) {
//        let objectString = String(data: objectData, encoding: .utf8)
//        requestDict["graduationYear"] = objectString
//    }
}
}

//MARK: CollectionViewDataSource
extension SearchByUserVC : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SearchByNameVM.sharedInstance.listArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSearchGroupCell, for: indexPath)
        
        if !groupIdArray.contains(where: {$0 == (SearchByNameVM.sharedInstance.listArray[indexPath.row] as!NSDictionary)["groupId"]as?Int}) {
            (cell.viewWithTag(1) as!UIButton).setImage(#imageLiteral(resourceName: "activeGray"), for: .normal)
        }else{
            (cell.viewWithTag(1) as!UIButton).setImage(#imageLiteral(resourceName: "activeBlue"), for: .normal)
        }
        
        (cell.viewWithTag(2) as!UILabel).text = (SearchByNameVM.sharedInstance.listArray[indexPath.row] as!NSDictionary)["groupName"]as?String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let groupId = (SearchByNameVM.sharedInstance.listArray[indexPath.row] as!NSDictionary)["groupId"] as!Int
        
        if let index = filterArray.index(where: {$0.id == groupId}) {
            filterArray.remove(at: index)
            if let index1 = groupIdArray.index(where: {$0 == groupId}) {
                groupIdArray.remove(at: index1)
            }
        }
        else {
            let name = (SearchByNameVM.sharedInstance.listArray[indexPath.row] as!NSDictionary)["groupName"]as?String
            let data = searchFilter(title: name!, type: .groupIds, id: groupId)
            filterArray.append(data)
            groupIdArray.append(groupId)
        }
        groupsCollectionVew.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGSize = ((SearchByNameVM.sharedInstance.listArray[indexPath.row] as!NSDictionary)["groupName"]as?String)!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        return  CGSize(width:size.width + 50.0, height: 20.0)
        
    }

}
//MARK: APIMethods
extension SearchByUserVC{
    func getGroupList(){
        
        SearchByNameVM.sharedInstance.groupListMethod(userId: DataManager.user_id!,schoolId:DataManager.schoolId!) { (success, message, error) in
            if(success == 1) {
                
                self.groupsCollectionVew.reloadData()
                self.groupsHeightConstraint.constant = self.groupsCollectionVew.collectionViewLayout.collectionViewContentSize.height + 75
                self.groupCollectionViewHeight.constant = self.groupsCollectionVew.collectionViewLayout.collectionViewContentSize.height
                self.groupsView.generateShadowUsingBezierPath(radius: 5.0,frame: CGRect(x: 15, y: self.groupsView.frame.origin.y, width: self.view.frame.width-30, height: self.groupsHeightConstraint.constant))
                self.view.layoutIfNeeded()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}
