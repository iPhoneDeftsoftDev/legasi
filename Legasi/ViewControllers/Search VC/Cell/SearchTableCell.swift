//
//  SearchTableCell.swift
//  Legasi
//
//  Created by 123 on 10/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet var profileView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet weak var connectionsLabel: UILabel!
    @IBOutlet weak var userNameLable: UILabel!
    @IBOutlet var typeImageView: UIImageView!
    //MARK: CLass Life Cycle Method
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Set Corner Radius and Shadow
        self.profileView.setRadius(radius: self.profileView.layer.frame.height/2, borderColor: UIColor.lightGray, borderWidth: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
