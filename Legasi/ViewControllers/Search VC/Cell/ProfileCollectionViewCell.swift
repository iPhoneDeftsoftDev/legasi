//
//  ProfileCollectionViewCell.swift
//  Legasi
//
//  Created by 123 on 09/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var yearLabel: UILabel!
   
    @IBOutlet weak var nameLabel: UILabel!
 
}
