//
//  SearchVC.swift
//  Legasi
//
//  Created by 123 on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SearchVC: BaseVC {

    //MARK: IBOutlet
    @IBOutlet var searchTableView: UITableView!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var searchByNameTextField: UITextField!
   //MARK: Variables
    var searchUserArray = [SearchUserData]()
    var searchFilterArray = [searchFilter]()
    var searchDict = JSONDictionary()
    
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        searchByNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customiseUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    //MARK: Private Methods
    private func customiseUI() {
        setTitle(title: "SEARCH")
        customSlideButton()
        
        //MARK: Set TableView Height
        self.searchTableView.estimatedRowHeight = 87
        self.searchTableView.rowHeight = UITableViewAutomaticDimension
        self.searchTableView.tableFooterView = UIView()
    }
  //MARK: IBActions
    @IBAction func FilterButtonAction(_ sender: Any) {
        if (searchByNameTextField.text?.isEmpty)!{
            self.showAlert(message: "Please Enter Search Text", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
        }else{
            let storyboard = UIStoryboard.storyboard(storyboard: .Search)
            let nextObj = storyboard.instantiateViewController(withIdentifier: kSearchByUserVC) as!SearchByUserVC
            nextObj.delegate = self
            nextObj.requestDict = searchDict
            nextObj.filterArray = searchFilterArray
            self.navigationController?.pushViewController(nextObj, animated: true)
        }
        
    }
    
    
    
}

//MARK: CollectionView DataSource
extension SearchVC : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchFilterArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSearchFilterCell, for: indexPath) as! TagCollectionViewCell
        cell.tagLabel.text =  searchFilterArray[indexPath.row].title
        cell.crossButton.addTarget(self, action: #selector(self.crossButtonAction(sender:)), for: .touchUpInside)
        cell.crossButton.tag = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGSize = searchFilterArray[indexPath.row].title.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        return  CGSize(width:size.width + 30.0, height: 20.0)
        
    }
    
    func crossButtonAction(sender: UIButton){
        if searchFilterArray[sender.tag].type == .address {
           searchDict.removeValue(forKey: "miles")
           searchDict.removeValue(forKey: "lat")
           searchDict.removeValue(forKey: "long")
            searchFilterArray.remove(at: sender.tag)
        }
        else if searchFilterArray[sender.tag].type == .searchType {
            searchDict.removeValue(forKey: "searchType")
            searchFilterArray.remove(at: sender.tag)
        }
        else if searchFilterArray[sender.tag].type == .graduateYear {
           searchDict.removeValue(forKey: "graduationYear")
            searchFilterArray.remove(at: sender.tag)
        }else if searchFilterArray[sender.tag].type == .groupIds {
            searchFilterArray.remove(at: sender.tag)
            
            let aray = searchFilterArray.filter({$0.type == .groupIds})
            
            var idString = ""
            for group in aray{
                if idString == "" {
                    idString = "\(group.id!)"
                }
                else {
                    idString = "\(idString),\(group.id!)"
                }
            }
            searchDict["groupIds"] = idString
        }
        
        filterCollectionView.reloadData()
        
        if searchFilterArray.count == 0{
            searchDict["searchType"] = "1"
        }
        
        self.getSearchResult()
        

    }
}

//MARK: TableView DataSource
extension SearchVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchUserArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kSearchTableCell, for: indexPath)as!SearchTableCell
        cell.userNameLable.text = searchUserArray[indexPath.row].name
        let image = searchUserArray[indexPath.row].profileImageUrl ?? ""
        cell.profileImageView.sd_setImage(with: URL(string:"\(image)"), placeholderImage: #imageLiteral(resourceName: "userImage"))
        cell.connectionsLabel.text = "\(searchUserArray[indexPath.row].totalConnection!) Mutual Connection"
        if searchUserArray[indexPath.row].type == "User"{
            cell.typeImageView.image = UIImage(named: "user_icon.png")
        }else if searchUserArray[indexPath.row].type == "Group"{
            cell.typeImageView.image = UIImage(named: "group_icon.png")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     self.view.endEditing(true)
        if searchUserArray[indexPath.row].type == "User"{
        let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
        nextObj.otherUserId = searchUserArray[indexPath.row].searchId
        self.navigationController?.pushViewController(nextObj, animated: false)
      }else if searchUserArray[indexPath.row].type == "Group"{
        
        let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
        let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupNameVC)as!GroupNameVC
        nextObj.titleName = searchUserArray[indexPath.row].name
        nextObj.groupId = searchUserArray[indexPath.row].searchId
        nextObj.isAdmin = searchUserArray[indexPath.row].isAdmin
        nextObj.isMember = searchUserArray[indexPath.row].isMember
        self.navigationController?.pushViewController(nextObj, animated: false)
        }
        
        
        
    }
    

}

//MARK: UITextfield Delegates
extension SearchVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
     }
    
    @objc func textFieldDidChange(textField: UITextField) {
        if(textField == searchByNameTextField){
            if (textField.text!.characters.count) > 0 {
                searchDict["searchType"] = "1"
                getSearchResult()
            }
        }
       
    }

    
    
    
}
//MARK: Custom Delegates
extension SearchVC : SearchUsersDelegates{
    //delegate method to pass selected address and Coordinates from Search Location Class
    func getUserDetail(requestDict: JSONDictionary,filterArray:[searchFilter]) {
       
        searchFilterArray = filterArray
        searchDict = requestDict
        filterCollectionView.reloadData()
        getSearchResult()
    }
}
//MARK: APIMethods
extension SearchVC {
    
    func getSearchResult(){
        
        SearchByNameVM.sharedInstance.searchByName(userId: DataManager.user_id!, searchText: searchByNameTextField.text!, requestDict: searchDict) { (success, message, error) in
            if(success == 1) {
                self.searchUserArray = SearchByNameVM.sharedInstance.searchUserArray
                if self.searchUserArray.count == 0 {
                   self.showAlert(message: "No Result Match", title: kAlert, otherButtons: nil, cancelTitle: kOk, cancelAction: nil)
                }
                self.searchTableView.reloadData()
            }
            else {
                if(message != nil) {
                    self.showAlert(message: message, title: kAlert, otherButtons: nil, cancelTitle: kOk)
                }
                else {
                    self.showErrorMessage(error: error)
                }
            }
        }
        
    }
}
