//
//  AppDelegate.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKLoginKit
import CoreLocation
import GoogleMaps
import BRYXBanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, CLLocationManagerDelegate {
//Variables
    var window: UIWindow?
    var custDict:NSDictionary!
    var banner: Banner!
    var message: String!
    var notifyKey: String?
    var dict: NSDictionary!
    var navigationController = UINavigationController()
    var drawerContainer: MMDrawerController?
    var locationManager: CLLocationManager?
    var latitude = Double()
    var tabbar :TabBarVC!

    var longitude = Double()
    var coordinates = CLLocationCoordinate2D()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        DataManager.linkedInAccessToken = nil
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        GMSServices.provideAPIKey(Google_Map_Key)
  //Check User Login Or Not
        if(DataManager.isUserLoggedIn == false)
        {
            let storyboard = UIStoryboard(storyboard: .Main)
            let vc = storyboard.instantiateViewController(withIdentifier: kLoginVc) as! LoginVC
            navigationController.pushViewController(vc, animated: false)
            navigationController.setNavigationBarHidden(false, animated: false)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window!.rootViewController = navigationController
            self.window!.makeKeyAndVisible()
        }else {
            enableMapLocation()
            loadLeftSideMenu()
        }
        registerForRemoteNotification()
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    //MARK: Notification setting.
    func registerForRemoteNotification() {
        //code for accessing device token
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound,.badge,.alert], completionHandler: { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            })
        } else {
            let settings  = UIUserNotificationSettings(types: [UIUserNotificationType.alert , UIUserNotificationType.badge , UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func enableMapLocation(){
        //Location Manager set up
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.allowsBackgroundLocationUpdates = false
        locationManager?.activityType = .automotiveNavigation
        locationManager?.startUpdatingLocation()
    }
 //MARK: CLLocation Delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
        if let location = locations[0] as CLLocation? {
            if myRequest == nil {
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
            self.coordinates = location.coordinate
            self.updateLocationMethod(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude))
            }
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedWhenInUse || status == .authorizedAlways) {
            // User has granted autorization to location, get location
            locationManager?.startUpdatingLocation()
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "locationUpdate"), object: nil)
        
        
        }
        
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        DataManager.deviceToken = deviceTokenString
        print("APNs device token: \(deviceTokenString)")
        
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    //MARK:
    //MARK: Receive notification before IOS 10
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        print("Push notification received: \(userInfo)")
        let notificationDict = userInfo["aps"] as! NSDictionary!
        //Call NoticationData
        getNotificationData(dataDict: notificationDict!)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("Push notification received: \(userInfo)")
        let notifyDict =  userInfo["aps"] as! NSDictionary!
        getNotificationData(dataDict: notifyDict!)
        completionHandler([])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("Push notification received: \(userInfo)")
        let notifyDict =  userInfo["aps"] as! NSDictionary!
        getNotificationData(dataDict: notifyDict!)
        completionHandler()
    }
//    {
//    Notifykey = "new_message";
//    alert = "has been sent a new message to you";
//    "msgsender_id" =     {
//    message = "has been sent a new message to you";
//    notificationType = 8;
//    notifykey = "new_message";
//    otherId = 5;
//    otherUserId = 3;
//    };
//    sound = default;
//    }
    //MARK: Notifications SetUp
    func getNotificationData(dataDict: NSDictionary){
        print(dataDict)
        let state = UIApplication.shared.applicationState
        if (state == .active||state == .background||state == .inactive){
            //Decode Comment
            
            let decodeMsg = (dataDict["alert"]as! String).fromBase64()
            if(decodeMsg == nil){
                 message = dataDict["alert"]as! String
            }else{
                message = decodeMsg
            }
            dict = dataDict["msgsender_id"]as!NSDictionary
            notifyKey = dict["notifykey"]as?String!
            
            if(notifyKey == "new_job" || notifyKey == "add_newsfeed" || notifyKey == "comment_on_post"||notifyKey == "reply_on_comment" || notifyKey == "reply_on_post" || notifyKey == "new_event_by_admin" || notifyKey == "new_job_by_admin" || notifyKey == "like_post"){
                if(DataManager.isUserLoggedIn){
                    let topViewController = UIApplication.topViewController()?.childViewControllers[0] as? TabBarVC
                    let navVc = topViewController?.selectedViewController as? UINavigationController ?? UINavigationController()
                    let vc = navVc.visibleViewController
                    if(vc != nil && vc!.isKind(of: PostDetailVC.self)) {
                        let postDict :[AnyHashable : Any] = ["newsFeedId":self.dict["otherId"]as?Int ?? 0]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PostNotify"), object: nil, userInfo: postDict)
                    }else {
                        
                        if(state == .active){
                            
                            addNotificationButtonToWindow(title: message as NSString)
                            
                        }else {
                            let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
                            let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
                            nextObj.isThroughMyPost = false
                            nextObj.newsFeedId = self.dict["otherId"]as?Int
                            navVc.pushViewController(nextObj, animated: false)
                            
                        }
                    }
                }
            } else if(notifyKey == "group_join_request"){
                if(DataManager.isUserLoggedIn){
                    let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
                    let navVc = topViewController.selectedViewController as!UINavigationController
                    let vc = navVc.visibleViewController
                    if(vc?.isKind(of: NotificationsVC.self))!{
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationList"), object: nil, userInfo: nil)
                        
                        
                    } else {
                        
                        if(state == .active){
                            
                            addNotificationButtonToWindow(title: message as NSString)
                            
                        }else {
                            topViewController.selectedIndex = 4
                            
                        }
                    }
                }
            }else if(notifyKey == "add_member_in_group" || notifyKey == "group_join_request_action" || notifyKey == "add_newsfeed_group" || notifyKey == "new_job_group"){
                if(DataManager.isUserLoggedIn){
                    let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
                    let navVc = topViewController.selectedViewController as!UINavigationController
                    let vc = navVc.visibleViewController
                    if(vc?.isKind(of: GroupNameVC.self))!{
                        
                        let postDict :[AnyHashable : Any] = ["groupId":self.dict["otherId"]as?Int ?? 0,"isAdmin":self.dict["isAdmin"]as?String ?? "", "titleName":self.dict["groupName"]as?String ?? ""]
                        GroupNameVC.isAddMemberPush = "Yes"
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GroupPostList"), object: nil, userInfo: postDict)
                        
                        
                    } else {
                        
                        if(state == .active){
                            
                            addNotificationButtonToWindow(title: message as NSString)
                            
                        }else {
                            let storyboard = UIStoryboard.storyboard(storyboard: .Connections)
                            let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupNameVC)as!GroupNameVC
                            nextObj.groupId = self.dict["otherId"] as! Int
                            nextObj.titleName = self.dict["groupName"] as! String
                            nextObj.isAdmin = self.dict["isAdmin"] as! String
                            GroupNameVC.isAddMemberPush = "Yes"
                            navVc.pushViewController(nextObj, animated: false)
                            
                        }
                    }
                }
            }else if(notifyKey == "check_in" || notifyKey == "follow" || notifyKey == "birthday"){
                if(DataManager.isUserLoggedIn){
                    let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
                    let navVc = topViewController.selectedViewController as!UINavigationController
                    let vc = navVc.visibleViewController
                    if(vc?.isKind(of: UserProfileVC.self))!{
                        
                        let postDict :[AnyHashable : Any] = ["otherUserId":self.dict["otherUserId"]as?Int ?? 0]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfileNotify"), object: nil, userInfo: postDict)
                        
                        
                    } else {
                        
                        if(state == .active){
                            
                            addNotificationButtonToWindow(title: message as NSString)
                            
                        }else {
                            let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                            let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
                            nextObj.otherUserId = self.dict["otherId"] as! Int
                            navVc.pushViewController(nextObj, animated: false)
                            
                        }
                    }
                }
            } else if(notifyKey == "new_message"){
                if(DataManager.isUserLoggedIn){
                    let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
                    let navVc = topViewController.selectedViewController as!UINavigationController
                    let vc = navVc.visibleViewController
                    if(vc?.isKind(of: MessagesVC.self))!{
                        
                        let postDict :[AnyHashable : Any] = ["friendId":self.dict["otherUserId"]as?Int ?? 0, "chatListId":self.dict["otherId"] as?Int ?? 0, "otherUserProfilePic": self.dict["profileImageUrl"] as? String ?? "","otherUserName": self.dict["name"]as?String ?? ""]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChatList"), object: nil, userInfo: postDict)
                       
                        
                    } else {
                        
                        if(state == .active){
                            
                            addNotificationButtonToWindow(title: message as NSString)
                            
                        }else {
                            let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
                            let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
                            nextObj.titleStr = self.dict["name"] as! String
                            nextObj.friendId = self.dict["otherUserId"] as! Int
                            nextObj.chatListId = self.dict["otherId"] as! Int
                            nextObj.otherUserProfilePic = self.dict["profileImageUrl"] as! String
                            nextObj.otherUserName = self.dict["name"] as! String
                            navVc.pushViewController(nextObj, animated: false)
                        }
                    }
                }
            } else{
                
            }
        }
    }
    //Set notification on button
    func addNotificationButtonToWindow(title: NSString)  {
        let image = #imageLiteral(resourceName: "logo")
        banner = Banner(title: "Legasi", subtitle: title as String, image: image, backgroundColor: UIColor.darkGray.withAlphaComponent(1.0))
        banner.springiness = .slight
        banner.position = .top
        banner.setRadius(radius: 3.0)
        banner.show(duration: 5.0)
        if let data = self.dict["countList"] as? NSDictionary{
            if let school = data.object(forKey: "school_detail") as? NSDictionary{
                
                if let likeIconUrl = school.object(forKey: "likeIconUrl") as? String{
                    DataManager.likeIcon_url = likeIconUrl
                }
                if let unlikeIconUrl = school.object(forKey: "unlikeIconUrl") as? String{
                    DataManager.unlikeIcon_url = unlikeIconUrl
                }
                if let schoolLogo = school.object(forKey: "smallLogoIconUrl") as? String{
                    DataManager.school_logo = schoolLogo
                }
                if let name = school.object(forKey: "nameforGraduates") as? String{
                    
                    DataManager.nameForGraduate = name.isEmpty ? "Graduate": name
                }
                
            }
            if let likes = data.object(forKey: "totalLikes") as? Int{
                DataManager.totalLikes = likes
            }
            
        }
        
        //setTabBadgeCount(data: self.dict["countList"]as!NSDictionary)
        
        
        banner.didTapBlock = {
            self.buttonNotificationAction()
        }
     }
    //Notification Button action Api
    func buttonNotificationAction() {
        
       
        if(notifyKey == "new_job" || notifyKey == "add_newsfeed" || notifyKey == "comment_on_post"||notifyKey == "reply_on_comment" || notifyKey == "reply_on_post" || notifyKey == "new_event_by_admin" || notifyKey == "new_job_by_admin"  || notifyKey == "like_post"){
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as? TabBarVC
            let navVc = topViewController?.selectedViewController as? UINavigationController ?? UINavigationController()
            let vc = navVc.visibleViewController
            if(vc != nil && vc!.isKind(of: PostDetailVC.self)) {
                let postDict :[AnyHashable : Any] = ["newsFeedId":self.dict["otherId"]as?Int ?? 0]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PostNotify"), object: nil, userInfo: postDict)
            } else{

                let storyboard =  UIStoryboard.storyboard(storyboard: .NewsFeed)
                let nextObj = storyboard.instantiateViewController(withIdentifier: KPostDetailVC) as! PostDetailVC
                nextObj.isThroughMyPost = false
                nextObj.newsFeedId = self.dict["otherId"]as?Int
                navVc.pushViewController(nextObj, animated: false)
                
            }
            
        } else if(notifyKey == "group_join_request"){
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            let vc = navVc.visibleViewController
            if(vc?.isKind(of: NotificationsVC.self))!{
               
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationList"), object: nil, userInfo: nil)
                
            } else{
                topViewController.selectedIndex = 4
            }
            
            
        }else if(notifyKey == "add_member_in_group" || notifyKey == "group_join_request_action" || notifyKey == "add_newsfeed_group" || notifyKey == "new_job_group" ){
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            let vc = navVc.visibleViewController
            if(vc?.isKind(of: GroupNameVC.self))!{
                let postDict :[AnyHashable : Any] = ["groupId":self.dict["otherId"]as?Int ?? 0,"isAdmin":self.dict["isAdmin"]as?String ?? "", "titleName":self.dict["groupName"]as?String ?? ""]
                GroupNameVC.isAddMemberPush = "Yes"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GroupPostList"), object: nil, userInfo: postDict)
                
            } else{
                
                let storyboard =  UIStoryboard.storyboard(storyboard: .Connections)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kGroupNameVC) as! GroupNameVC
                
                nextObj.groupId = self.dict["otherId"] as!Int
                nextObj.titleName = self.dict["groupName"] as! String
                nextObj.isAdmin = (self.dict["isAdmin"] as! String)
                GroupNameVC.isAddMemberPush = "Yes"
                navVc.pushViewController(nextObj, animated: false)
                
            }
            
       
        }else if(notifyKey == "check_in" || notifyKey == "follow" || notifyKey == "birthday"){
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            let vc = navVc.visibleViewController
            if(vc?.isKind(of: UserProfileVC.self))!{
                let postDict :[AnyHashable : Any] = ["otherUserId":self.dict["otherId"]as?Int ?? 0]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfileNotify"), object: nil, userInfo: postDict)
            } else{
                let storyboard = UIStoryboard.storyboard(storyboard: .SlideMenu)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kUserProfileVC)as!UserProfileVC
                nextObj.otherUserId = self.dict["otherUserId"] as! Int
                navVc.pushViewController(nextObj, animated: false)
            }
            
        } else if(notifyKey == "new_message"){
            let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
            let navVc = topViewController.selectedViewController as!UINavigationController
            let vc = navVc.visibleViewController
            if(vc?.isKind(of: MessagesVC.self))!{
               
               let postDict :[AnyHashable : Any] = ["friendId":self.dict["otherUserId"]as?Int ?? 0, "chatListId":self.dict["otherId"] as! String, "otherUserProfilePic": self.dict["profileImageUrl"] as? String ?? "","otherUserName": self.dict["name"]as?String ?? ""]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChatList"), object: nil, userInfo: postDict)
            } else{
                let storyboard = UIStoryboard.storyboard(storyboard: .Chat)
                let nextObj = storyboard.instantiateViewController(withIdentifier: kMessagesVC)as!MessagesVC
                nextObj.titleStr = self.dict["name"] as! String
                nextObj.friendId = self.dict["otherUserId"] as! Int
                nextObj.chatListId = self.dict["otherId"] as! Int
                nextObj.otherUserProfilePic = self.dict["profileImageUrl"] as! String
                nextObj.otherUserName = self.dict["name"] as! String
                navVc.pushViewController(nextObj, animated: false)
            }
            
        }else{
             banner.removeFromSuperview()
        }
        
    }
   
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
          notificationCount()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
//MARK: UpdateLocation Method
    func updateLocationMethod(latitude: Double, longitude:Double){
        
        if myRequest == nil {
            
            Indicator.isEnabledIndicator = false
            UserLoginVM.sharedInstance.UpdateLatLong(userId: DataManager.user_id!, lat: latitude, long: longitude) { (success, message, error) in
                Indicator.isEnabledIndicator = true
                if(success == 1)
                {
                    
                    
                } else if(success == 2){
                    
                }
                else
                {
                    if(message != nil)
                    {
//                        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: .alert)
//                        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                        alert.addAction(alertAction)
//                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                    }else {
                        //self.showErrorMessage(error: error)
                    }
                    
                }
            }
        }
    }
    //MARK: Notification Count Webservice
    func notificationCount(){
        Indicator.isEnabledIndicator = false
        NotificationsVM.sharedInstance.NotificationCount(userId: DataManager.user_id!) { (success, message , error) in
           Indicator.isEnabledIndicator = true
            if(success == 1) {
                let data = NotificationsVM.sharedInstance.infoDict
                DataManager.totalLikes = data?.object(forKey: "totalLikes") as? Int
                DataManager.school_logo = (data?.object(forKey: "school_detail") as? NSDictionary)?["smallLogoIconUrl"]as? String
                DataManager.likeIcon_url = (data?.object(forKey: "school_detail") as? NSDictionary)?["likeIconUrl"]as? String
                DataManager.unlikeIcon_url = (data?.object(forKey: "school_detail") as? NSDictionary)?["unlikeIconUrl"]as? String
                let name = (data?.object(forKey: "school_detail") as? NSDictionary)?["nameforGraduates"]as? String
                
                DataManager.nameForGraduate = name!.isEmpty ? "Graduate": name
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "schoolLogoUpdated"), object: nil)
               
                
              //  self.setTabBadgeCount(data: NotificationsVM.sharedInstance.infoDict["totalLikes"])
            }
            else {
//                if(message != nil) {
//                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: .alert)
//                    let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                    alert.addAction(alertAction)
//                    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
//                }
//                else {
//                   // self.showErrorMessage(error: error)
//                }
            }
        }
    }

    func setTabBadgeCount(data:NSDictionary){
       let topViewController = UIApplication.topViewController()?.childViewControllers[0] as!TabBarVC
        
        if let tabItems = topViewController.tabBar.items as NSArray!
        {
            let followers = data["follwers"] as!Int
            let message = data["message"] as!Int
            let notification = data["notification"] as!Int
            if  followers > 0 {
                let tabItem = tabItems[1] as! UITabBarItem
                tabItem.badgeValue = "\(followers)"
            }
            if  message > 0 {
                let tabItem = tabItems[2] as! UITabBarItem
                tabItem.badgeValue = "\(message)"
            }
            if  notification > 0 {
                let tabItem = tabItems[4] as! UITabBarItem
                tabItem.badgeValue = "\(notification)"
            }
            
        }

        
        
    }
    
    
 ///Stop Location Updation
    func stopLocationService(){
        
        if(locationManager != nil){
            locationManager?.stopUpdatingLocation()
            locationManager = CLLocationManager.init()
            locationManager?.delegate = nil
            
        }
    }

    //MARK: Load Left Side Menu
    func loadLeftSideMenu() {
        
            let mainStoryBoard:UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
            let mainPage:TabBarVC = mainStoryBoard.instantiateViewController(withIdentifier: kTabBarVC) as! TabBarVC
            let leftSideMenu:LeftSideBarVC = mainStoryBoard.instantiateViewController(withIdentifier: kLeftSideBarVC) as! LeftSideBarVC
            let leftSideMenuNav = UINavigationController(rootViewController:leftSideMenu)
            drawerContainer = MMDrawerController(center: mainPage, leftDrawerViewController: leftSideMenuNav)
            drawerContainer!.showsShadow = false
            drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.bezelPanningCenterView
            drawerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window!.rootViewController = self.drawerContainer
            self.window!.makeKeyAndVisible()
    }

}

