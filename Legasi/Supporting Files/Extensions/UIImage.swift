//
//  UIImage.swift
//  Roasted
//
//  Created by ABC on 23/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.27
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    var pngData: Data? {
        return UIImagePNGRepresentation(self)
    }
    
    func jpegData(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
