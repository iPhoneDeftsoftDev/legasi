//
//  UIColor.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    enum LegasiColor  {
        
        case blue
        case darkBlue
        case lightGray
        func colorWith(alpha: CGFloat) -> UIColor {
            var colorToReturn:UIColor?
            switch self {
            case .blue:
                colorToReturn = UIColor(red: 33/255, green: 170/255, blue: 221/255, alpha: alpha)
                
            case .darkBlue:
                colorToReturn = UIColor(red: 13/255, green: 37/255, blue: 54/255, alpha: alpha)
           
            case .lightGray:
            colorToReturn = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: alpha)
        }
        
            return colorToReturn!
        }
    }
}
