//
//  UIView.swift
//  Legasi
//
//  Created by Apple on 08/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit



@IBDesignable  class RoundView: UIView {
    
    //MARK: Set Corner Radius of View
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
}

extension UIView {
    
    func setRadius(radius: CGFloat, borderColor: UIColor = UIColor.clear, borderWidth: CGFloat = 0.0) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func setShadow() {
        self.layer.cornerRadius = 4
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.3
    }
    
    func generateShadowUsingBezierPath(radius: CGFloat, frame: CGRect? = nil)  {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
        
        let shadow = UIView()
        if frame != nil {
            shadow.frame = frame!
        }
        else {
            self.removeFromSuperview()
            shadow.frame = self.frame
        }
        shadow.backgroundColor = .white
        shadow.isUserInteractionEnabled = false
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOffset = .zero
        
        shadow.layer.shadowRadius = 2.0
        shadow.layer.masksToBounds = false
        shadow.layer.cornerRadius = self.layer.cornerRadius
        shadow.layer.shadowOpacity = 0.3
        self.superview?.addSubview(shadow)
        self.superview?.sendSubview(toBack: shadow)
    }
    func fadeIn(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    func fadeInForIndicator(duration: TimeInterval = 0.50) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.8
        })
    }
    
    func fadeOut(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    func fadeOutForIndicator(duration: TimeInterval = 0.50) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    

}
