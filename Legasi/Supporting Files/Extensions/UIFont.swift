//
//  UIFont.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    enum LegasiFont: String {
        case regular = "Montserrat-Regular"
        case light = "Montserrat-Light"
        case bold = "Montserrat-Bold"
        case semiBold = "Montserrat-Medium"
        
        
        func fontWithSize(size: CGFloat) -> UIFont {
            return UIFont(name: rawValue, size: size)!
        }
        
    }
    
}
