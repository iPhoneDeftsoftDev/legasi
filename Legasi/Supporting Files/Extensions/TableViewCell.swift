//
//  TableViewCell.swift
//  Legasi
//
//  Created by 123 on 06/11/17.
//  Copyright © 2017 MiniMAC22. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class func fromNib<T : UITableViewCell>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?[0] as! T
    }
}


extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            UIView.animate(withDuration: 0.4, animations: {
                self.scrollToRow(at: IndexPath(row: rows - 1, section:  sections - 1), at: .bottom, animated: animated)
            })
        }
    }
}
