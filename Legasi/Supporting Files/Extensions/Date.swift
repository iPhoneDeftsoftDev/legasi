//
//  Date.swift
//  CustomCalender
//
//  Created by 123 on 14/02/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation


// MARK: Enumerations
enum WeekDay: String {
    case Mo
    case Tu
    case We
    case Th
    case Fr
    case Sa
    case Su
    static let weekArray = [Mo, Tu, We, Th, Fr, Sa, Su]
}

enum TimeZoneType {
    case gmt
    case utc
    case local
}

enum DateFormat: String {
    case dateTime = "yyyy-MM-dd HH:mm:ss"
    case dateTime12 = "yyyy-MM-dd hh:mm:ss"
    case date12HourTime = "yyyy-MM-dd h:mm a"
    case mdyDate = "MM-dd-yyyy"
    case ymdDate = "yyyy-MM-dd"
    case dmyDate = "dd-MM-yyyy"
    case shortMDYDate = "MMM dd, yyyy"
    case shortDMYDate = "dd MMM, yyyy"
    case longMDYDate = "MMMM dd, yyyy"
    case longDMYData = "EEEE dd MMM yyyy"
    case time = "h:mm a"
    case longTime = "HH:mm:ss"
    case longTime12 = "hh:mm:ss"
    case weekDay = "EEEE"
}

extension Date {
    
    static func getTimeWithUTCZone(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        if let date = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = DateFormat.time.rawValue
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    func timeAgoSinceDate(date: Date) -> String {
        let earliest = (self as NSDate).earlierDate(date)
        let latest =   Date()
        let components:DateComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
      
        if components.year! > 0 {
            return "\(components.year!) years ago"
        }
        if components.month! > 0 {
            return "\(components.month!) months ago"
        }
        if components.weekOfYear! > 0 {
            return "\(components.weekOfYear!) weeks ago"
        }
        if components.day! > 0 {
            return "\(components.day!) days ago"
        }
        if components.hour! > 0 {
            return "\(components.hour!) hours ago"
        }
        if components.minute! > 0 {
            return "\(components.minute!) mintues ago"
        }
        if components.second! > 0 {
            return "\(components.second!) seconds ago"
        }
        return "Just Now"
    }
    
    
    func stringFromDate(format: DateFormat, type: TimeZoneType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        if type == .utc {
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        }
        else if type == .gmt {
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone!
        }
        else {
            dateFormatter.timeZone = TimeZone.current
        }
        return dateFormatter.string(from: self)
    }
        
    func dateByAddingDays(inDays:NSInteger)->Date {
        return Calendar.current.date(byAdding: .day, value: inDays, to: self)!
    }
    
    func dateByAddingWeek(inDays:NSInteger)->Date {
        return Calendar.current.date(byAdding: .weekOfYear, value: inDays, to: self)!
    }
    
    var millisecondsSince1970: Double {
        return self.timeIntervalSince1970 * 1000.0
    }
    
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000.0))
    }
    
    func addHoursInDate(hoursToAdd:Int) -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .hour, value: hoursToAdd, to: self)!
    }
    
    func addMinutesInDate(minutesToAdd:Int) -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .minute, value: minutesToAdd, to: self)!
    }
    
    func addSecondsInDate(secondsToAdd:Int) -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .second, value: secondsToAdd, to: self)!
    }
    
    func addDaysinDate(daysToAdd:Int) -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .day, value: daysToAdd, to: self)!
    }
    
    func getMonth(month: Int) -> String {
        switch month {
        case 1:
            return "Jan"
        case 2:
            return "Feb"
        case 3:
            return "Mar"
        case 4:
            return "Apr"
        case 5:
            return "May"
        case 6:
            return "Jun"
        case 7:
            return "Jul"
        case 8:
            return "Aug"
        case 9:
            return "Sep"
        case 10:
            return "Oct"
        case 11:
            return "Nov"
        case 12:
            return "Dec"
        default:
            return ""
        }
    }
    
}

extension Double {
    
    var dateFromMiliSeconds: Date {
        return Date(timeIntervalSince1970: TimeInterval(self / 1000))
    }
    
}

extension Date {
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> (String, String) {
        if years(from: date)   > 0 { return ("\(years(from: date)) years before", "Starts in \(years(from: date)) years")   }
        if months(from: date)  > 0 { return ("\(months(from: date)) months before","Starts in \(months(from: date)) months")  }
        if weeks(from: date)   > 0 { return ("\(weeks(from: date)) weeks before","Starts in \(weeks(from: date)) weeks")   }
        if days(from: date)    > 0 { return ("\(days(from: date)) days before", "Starts in \(days(from: date)) days")    }
        if hours(from: date)   > 0 { return ("\(hours(from: date)) hours before", "Starts in \(hours(from: date)) hours")   }
        if minutes(from: date) > 0 { return ("\(minutes(from: date)) minutes before","Starts in \(minutes(from: date)) minutes"
        )}
        if seconds(from: date) > 0 { return ("\(seconds(from: date)) seconds before", "Starts in \(seconds(from: date)) seconds") }
        return ("", "")
    }
    func compareTo(date: Date, toGranularity: Calendar.Component ) -> ComparisonResult  {
        var cal = Calendar.current
        return cal.compare(self, to: date, toGranularity: toGranularity)
    }
}
