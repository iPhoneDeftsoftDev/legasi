//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }

    func dateFromString(format: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        //debugPrint("End date in date from string method \(dateFormatter.date(from: self))")
        return dateFormatter.date(from: self)
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F:
                //                 // Emoticons
                //            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                //            0x1F680...0x1F6FF, // Transport and Map
                //            0x2600...0x26FF,   // Misc symbols
                //            0x2700...0x27BF,   // Dingbats
                //            0xFE00...0xFE0F:   // Variation Selectors
                return true
            default:
                continue
            }
        }
        return false
    }
    public func isImageType() -> Bool {
        // image formats which you want to check
        let imageFormats = ["jpg", "png", "gif"]
        
        if URL(string: self) != nil  {
            
            let extensi = (self as NSString).pathExtension
            
            return imageFormats.contains(extensi)
        }
        return false
    }
    
//    //: ### Base64 encoding a string
//    func base64Encoding() -> String? {
//        
//        if let data = self.data(using: .utf8) {
//            let encodedStr =  data.base64EncodedString(options: .init(rawValue: 3)).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//            return encodedStr?.replacingOccurrences(of: "+", with: "%2b")
//        }
//        
//        return nil
//    }
//    
//    //: ### Base64 decoding a string
//    func base64Decoding() -> String? {
//        if let data = Data.init(base64Encoded: self, options: .init(rawValue: 3)) {
//            return String(data: data, encoding: .utf8)
//        }
//        return nil
//    }
}
