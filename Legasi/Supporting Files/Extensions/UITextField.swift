//
//  UITextField.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    
    //MARK: Set Placeholder
    func setPlaceholder(placholder: String? = nil, color: UIColor = UIColor.darkGray, size: CGFloat = 16.0, style: UIFont.LegasiFont = .regular) {
        
        let myPlaceholder = placholder ?? self.placeholder!
        let attributedString = NSAttributedString(string: myPlaceholder, attributes:[NSForegroundColorAttributeName: color, NSFontAttributeName: style.fontWithSize(size: size)])
        self.attributedPlaceholder = attributedString
        
        self.font = style.fontWithSize(size: size)
    }
    
    // ***** Validations *****
    
    //MARK: Email Validation
    var isValidEmail: Bool {
        let emailRegEx = kEmailValidation
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!.lowercased())
    }
    //MARK: Link Validation
    var isValidUrl:Bool{
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        
        let validTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        return validTest.evaluate(with: self.text!)
    }
    var isEmpty: Bool {
        return self.text!.trimmingCharacters(in: .whitespaces).isEmpty
    }
}

@IBDesignable class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}

