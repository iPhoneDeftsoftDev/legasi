//
//  APIServices+Users.swift
//  OnCall
//
//  Created by 123 on 26/10/16.
//  Copyright © 2016 developer. All rights reserved.
//

enum UserAPIServices: APIService {
//Login Api
    case LoginByEmail(email: String, password: String,lat:Double,long:Double, device_type:String, device_token: String)
   
    case SignUp(fullName:String, username:String,countryCode:String, phoneNumber:String, email:String, password:String, addressLine1:String, latitude:Double, longitude:Double, schoolId:String, userType :String, dob:String, linkedInId :String,facebookId :String,deviceType:String,deviceToken :String, graduationYear:String, educationDetails:String,workDetails:String,skills:String)
   
    case ForgotPassword(email:String)
    case SchoolList()
    case CheckUserAccountExist(email: String,userName:String)
    case UpdateLatLong(userId:Int, lat:Double, long:Double)
    //other user Profile
    case OtherUserProfile(userId : Int, otherUserID: Int)
    case OtherUserPosts(userId : Int, otherUserID: Int,page:Int)
    case FollowUnfollowUser(userId: Int, friendId: Int, value: Int)
    //Logout
    case Logout(userId: Int)
    
    
    var path: String {
        var path = ""
        switch self {
 //Login Api
        case .LoginByEmail:
            path = BASE_API_URL.appending("login")
        case .SignUp:
            path = BASE_API_URL.appending("signup")
        case .ForgotPassword:
            path = BASE_API_URL.appending("forgotPassword")
        case .SchoolList:
            path = BASE_API_URL.appending("schoolList")
        case .CheckUserAccountExist:
            path = BASE_API_URL.appending("checkUserAccountExist")
        
        case .UpdateLatLong:
             path = BASE_API_URL.appending("updateLatLong")
            
        //other user Profile
        case .OtherUserProfile:
            path = BASE_API_URL.appending("getOtherUserProfileDetail")
       
        case .OtherUserPosts:
            path = BASE_API_URL.appending("getOtherUserNewfeed")
       
        case .FollowUnfollowUser:
            path = BASE_API_URL.appending("followUnfollowUser")
       
        case .Logout:
            path = BASE_API_URL.appending("logout")
            
        }
        return path
    }
    
  var resource: Resource {
        var resource: Resource!
        switch self {
//Login Api
      case let .LoginByEmail(email,password,lat,long,device_type,device_token):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kEmail] = email
            parametersDict[APIKeys.kPassword] = password
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            parametersDict[APIKeys.kDeviceType] = device_type
            parametersDict[APIKeys.kDeviceId] = device_token //TODO: Add Device ID
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .SignUp(fullName, username, countryCode, phoneNumber, email, password, addressLine1,latitude,longitude, schoolId, userType, dob, linkedInId,facebookId ,deviceType,deviceToken, graduationYear, educationDetails, workDetails,skills):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kFullname] = fullName
            parametersDict[APIKeys.kUsername] = username
            parametersDict[APIKeys.kCountryCode] = countryCode
            parametersDict[APIKeys.kPhoneNumber] = phoneNumber
            parametersDict[APIKeys.kEmail] = email
            parametersDict[APIKeys.kPassword] = password
            parametersDict[APIKeys.kAddressLine1] = addressLine1
            parametersDict[APIKeys.KLatitude] = "\(latitude)"
            parametersDict[APIKeys.KLongitude] = "\(longitude)"
            parametersDict[APIKeys.kSchoolId] = schoolId
            parametersDict[APIKeys.kUserType] = userType
            parametersDict[APIKeys.kDob] = dob
            parametersDict[APIKeys.kLinkedInId] = linkedInId
            parametersDict[APIKeys.kFacebookId] = facebookId
            parametersDict[APIKeys.kDeviceType] = deviceType
            parametersDict[APIKeys.kDeviceId] = deviceToken //TODO: Add Device ID
            parametersDict[APIKeys.kGraduationYear] = graduationYear
            parametersDict[APIKeys.kEducationDetail] = educationDetails
            parametersDict[APIKeys.kWorkDetail] = workDetails
            parametersDict[APIKeys.kSkills] = skills
           resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        case let .ForgotPassword(email):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kEmail] = email
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .CheckUserAccountExist(email,userName):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kEmail] = email
            parametersDict[APIKeys.kUsername] = userName
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)

        case .SchoolList():
             resource = Resource(method: .post, parameters: nil, headers: nil)
   
        case let .UpdateLatLong(userId, lat, long):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
    
        case let .OtherUserProfile(userId,otherUserID):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kFriendId] = otherUserID
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .OtherUserPosts(userId,otherUserID,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kOtherUserId] = otherUserID
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .FollowUnfollowUser(userId,friendId, value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kFriendId] = friendId
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .Logout(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
    
    }
    return resource

  }
}
extension APIManager {
    //Login Api
    
    class func loginBYEmail(email: String, password: String,lat:Double,long:Double, device_type:String, device_token: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.LoginByEmail(email: email, password: password, lat: lat,long: long, device_type: device_type,device_token:device_token).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
}
    
    
    class func signUp(fullName:String, username:String, profileImage:[String:Data], countryCode:String, phoneNumber:String, email:String, password:String, addressLine1:String, latitude:Double,longitude:Double, schoolId:String, userType :String, dob:String, linkedInId :String,facebookId :String,deviceType:String,deviceToken :String, graduationYear:String,educationDetails:String, workDetails:String,skills:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.SignUp(fullName: fullName, username: username, countryCode: countryCode, phoneNumber: phoneNumber, email: email, password: password, addressLine1: addressLine1, latitude: latitude, longitude: longitude, schoolId: schoolId, userType: userType, dob: dob, linkedInId: linkedInId, facebookId: facebookId, deviceType: deviceType, deviceToken: deviceToken, graduationYear: graduationYear,educationDetails:educationDetails, workDetails:workDetails,skills:skills).uploadMultiple(imageDict: profileImage, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func forgotPassword(email: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.ForgotPassword(email: email).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func CheckUserAccountExist(email: String,userName:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.CheckUserAccountExist(email: email,userName:userName).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

    class func schoolList(successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.SchoolList().requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func UpdateLatLong(userId: Int,lat:Double, long:Double, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.UpdateLatLong(userId: userId, lat: lat, long: long).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //other user Profile
    class func otherUserProfile(userId:Int,otherUserId: Int,successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.OtherUserProfile(userId: userId, otherUserID: otherUserId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func otherUserPostData(userId:Int,otherUserId: Int,page:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.OtherUserPosts(userId: userId, otherUserID: otherUserId,page:page).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func followUnfollowUser(userId:Int,friendId: Int,value: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.FollowUnfollowUser(userId: userId, friendId: friendId, value: value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

    class func Logout(userId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        UserAPIServices.Logout(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

    
    
}
