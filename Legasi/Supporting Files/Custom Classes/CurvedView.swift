//
//  CurvedView.swift
//  UIView Curve Demo
//
//  Created by Apple on 10/10/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CurvedView: UIView {
    
    //MARK: IBInspectable
    @IBInspectable var startPoint: CGFloat = 40 //Start of Curve
    @IBInspectable var endPoint: CGFloat = 20 //Mid Point of Curve
    @IBInspectable var backColor: UIColor = UIColor.white //Mid Point of Curve
    
    
    //MARK: Draw Method
    override func draw(_ rect: CGRect) {
        let myBezier = UIBezierPath()
        myBezier.move(to: CGPoint(x: 0, y: startPoint))
        myBezier.addQuadCurve(to: CGPoint(x: rect.width, y: startPoint), controlPoint: CGPoint(x: rect.width / 2, y: endPoint))
        myBezier.addLine(to: CGPoint(x: rect.width, y: rect.height))
        myBezier.addLine(to: CGPoint(x: 0, y: rect.height))
        myBezier.close()
        let context = UIGraphicsGetCurrentContext()
        context!.setLineWidth(4.0)
        backColor.setFill()
        myBezier.fill()
    }
}
