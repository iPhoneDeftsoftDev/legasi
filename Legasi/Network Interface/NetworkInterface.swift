//
//  NetworkInterface.swift
//  Manish Gumbal
//
//  Created by Manish Gumbal on 26/10/2016.
//  Copyright © 2016 Manish Gumbal. All rights reserved.
//

import Foundation
import Alamofire

//MARK: Call Backs
typealias JSONDictionary = [String:Any]
typealias JSONArray = [JSONDictionary]
typealias APIServiceSuccessCallback = ((Any?) -> ())
typealias APIServiceFailureCallback = ((NetworkErrorReason?, NSError?) -> ())
typealias JSONArrayResponseCallback = ((JSONArray?) -> ())
typealias JSONDictionaryResponseCallback = ((JSONDictionary?) -> ())
typealias responseCallBack = ((Int, String?, NSError?) -> ())


//MARK: Constant
var kMessage = "message"
var myRequest: Alamofire.Request?

public enum NetworkErrorReason: Error {
    case FailureErrorCode(code: Int, message: String)
    case InternetNotReachable
    case UnAuthorizedAccess
    case Other
}

public enum MimeType: String {
    case image = "image/png"
    case video = "video/mp4"
}

struct Resource {
    let method: HTTPMethod
    let parameters: [String : Any]?
    let headers: [String:String]?
}

protocol APIService {
    var path: String { get }
    var resource: Resource { get }
}

extension APIService {
    
    /**
     Method which needs to be called from the respective model class.
     - parameter successCallback:   successCallback with the JSON response.
     - parameter failureCallback:   failureCallback with ErrorReason, Error description and Error.
     */
    
    //MARK: Request Method to Send Request except in Multipart
    func request(isURLEncoded: Bool = false, success: @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("************************************************************************************")
            
            var encoding: URLEncoding = .default
            if resource.method == .get || resource.method == .head || resource.method == .delete || isURLEncoded{
                encoding = .methodDependent
            }
            
            debugPrint("Request method: \(resource.method)")
            debugPrint("Request parameter:\(String(describing: resource.parameters))")
            debugPrint("Request encoding: \(encoding)")
            debugPrint("Request headers: \(String(describing: resource.headers))")
            
            
            Alamofire.request(path, method: resource.method, parameters: resource.parameters, encoding: encoding, headers: resource.headers).validate().responseJSON(completionHandler: { (response) in
                debugPrint("********************************* API Response *************************************")
                debugPrint("\(response.debugDescription)")
                debugPrint("************************************************************************************")
                if Indicator.isEnabledIndicator {
                    Indicator.sharedInstance.hideIndicator()
                }

                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let value):
                    success(value as AnyObject?)
                case .failure(let error):
                    self.handleError(response: response, error: error as NSError, callback: failure)
                }
            })
        }
    }
    
//MARK: Request Method to Upload Multipart
    func uploadMultiple(imageDict:[String: Data]?, success:  @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("image dictionary: \(String(describing: imageDict))")

            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let urlRequest = createRequestWithMultipleImages(urlString: path, parameters: resource.parameters, imageDict: imageDict)
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 180
            myRequest = manager.upload((urlRequest?.1)!, with: (urlRequest?.0)!).uploadProgress(closure: { (progress) in
                print(progress.localizedDescription)
              
            }).responseJSON(completionHandler: { (response) in
                debugPrint("********************************* API Response *************************************")
                debugPrint("\(response.debugDescription)")
                debugPrint("************************************************************************************")
                if Indicator.isEnabledIndicator {
                    Indicator.sharedInstance.hideIndicator()
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                myRequest = nil
                switch response.result {
                case .success(let value):
                    if response.response?.statusCode == 200 {
                        success(value as AnyObject?)
                    }else if response.response?.statusCode == 401{
                        let alert = UIAlertController(title: kAlert, message: "You are blocked by admin.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                            self.logutMethod()
                        })
                        alert.addAction(alertAction)
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                    }else if response.response?.statusCode == 402{
                        let alert = UIAlertController(title: kAlert, message: "Your school subscription approval is pending from school admin.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                            self.logutMethod()
                        })
                        alert.addAction(alertAction)
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                    }else{
                        
                        success(value as AnyObject?)
                    }

                case .failure(let error):
                    self.handleError(response: response, error: error as NSError, callback: failure)
                }
            })
        }
    }
    
    
    
    
    func uploadMultipleArray(imageArray:[Data]?, imageName: String?, thumbNailArray:[Data]?, thumbNailName: String?, mediaType: MimeType = .image, success:  @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("image Name: \(imageName ?? "")")
            debugPrint("image dictionary: \(String(describing: imageArray))")
            
            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let urlRequest = createArrayRequestWithMultipleImages(urlString: path, parameters: resource.parameters, imageArray: imageArray, imageName: imageName, mediaType: mediaType, thumbNailArray:thumbNailArray, thumbNailName: thumbNailName)
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 600
            myRequest = manager.upload((urlRequest?.1)!, with: (urlRequest?.0)!).uploadProgress(closure: { (progress) in
                print(progress.localizedDescription)
            }).responseJSON(completionHandler: { (response) in
                debugPrint("********************************* API Response *************************************")
                debugPrint("\(response.debugDescription)")
                debugPrint("************************************************************************************")
                if Indicator.isEnabledIndicator {
                    Indicator.sharedInstance.hideIndicator()
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                myRequest = nil
                switch response.result {
                case .success(let value):
                    if response.response?.statusCode == 200 {
                        success(value as AnyObject?)
                    }else if response.response?.statusCode == 401{
                        let alert = UIAlertController(title: kAlert, message: "You are blocked by admin.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                            self.logutMethod()
                        })
                        alert.addAction(alertAction)
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                    }else if response.response?.statusCode == 402{
                        let alert = UIAlertController(title: kAlert, message: "Your school subscription approval is pending from school admin.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                            self.logutMethod()
                        })
                        alert.addAction(alertAction)
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                    }else{
                        
                        success(value as AnyObject?)
                    }
                case .failure(let error):
                    self.handleError(response: response, error: error as NSError, callback: failure)
                }
            })
        }
    }
    
    func createArrayRequestWithMultipleImages(urlString:String, parameters:[String : Any]?, imageArray: [Data]?, imageName: String?, mediaType: MimeType, thumbNailArray:[Data]?, thumbNailName: String?) -> (URLRequestConvertible, Data)? {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        mutableURLRequest.httpMethod = resource.method.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        var uploadData = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        if imageArray != nil {
            
            var type = "png"
            if mediaType == .video {
                type = "mov"
            }
            
            for image in imageArray! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(imageName!)[]\"; filename=\"\(imageName!).\(type)\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: \(mediaType.rawValue)\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append(image)
                uploadData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        if thumbNailArray != nil {
            for image in thumbNailArray! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(thumbNailName!)[]\"; filename=\"\(thumbNailName!).png\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append(image)
                uploadData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        uploadData.append("--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        do {
            let result = try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            return (result, uploadData)
        }
        catch _ {
        }
        
        return nil
    }

    
    func createRequestWithMultipleImages(urlString:String, parameters:[String : Any]?, imageDict: [String: Data]?) -> (URLRequestConvertible, Data)? {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        mutableURLRequest.httpMethod = resource.method.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        var uploadData = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        if imageDict != nil {
            for (key, value) in imageDict! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).png\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append(value)
                uploadData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        uploadData.append("--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        do {
            let result = try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            return (result, uploadData)
        }
        catch _ {
        }
        
        return nil
    }
    
    //MARK: Request Method to Send Request except in Multipart with ContentType application/json
    
    func requestNew(isURLEncoded: Bool = false, success: @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("Request parameter:\(String(describing: resource.parameters)))")
            debugPrint("************************************************************************************")
            
            var encoding: URLEncoding = .default
            if resource.method == .get || resource.method == .head || resource.method == .delete || isURLEncoded {
                encoding = .methodDependent
            }
            
            debugPrint("Request method: \(resource.method)")
            debugPrint("Request parameter:\(String(describing: resource.parameters)))")
            debugPrint("Request encoding: \(encoding)")
            debugPrint("Request headers: \(String(describing: resource.headers)))")
            
            var request = URLRequest(url: URL(string: path)!)
            request.httpMethod = resource.method.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            if resource.headers != nil {
                for (key, value) in resource.headers! {
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
            
            if resource.parameters != nil {
                request.httpBody = try! JSONSerialization.data(withJSONObject: resource.parameters ?? "")
            }
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 180
            myRequest = manager.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    Indicator.sharedInstance.hideIndicator()
                    debugPrint("************************** Response Start *********************************************************")
                    debugPrint("Result \(response))")
                    debugPrint("************************** Response End *********************************************************")
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                     myRequest = nil
                    switch response.result {
                    case .success(let value):
                       
                        if response.response?.statusCode == 200 {
                             success(value as AnyObject?)
                        }else if response.response?.statusCode == 401{
                            let alert = UIAlertController(title: kAlert, message: "You are blocked by admin.", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                                self.logutMethod()
                            })
                            alert.addAction(alertAction)
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                        }else if response.response?.statusCode == 402{
                            let alert = UIAlertController(title: kAlert, message: "Your school subscription approval is pending from school admin.", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: kOk, style: .default, handler: { (action) in
                                self.logutMethod()
                            })
                            alert.addAction(alertAction)
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
                        }else{
                           
                           success(value as AnyObject?)
                        }
                     case .failure(let error):
                        self.handleError(response: response, error: error as NSError, callback: failure)
                    }
            }
        }
    }
    func logutMethod(){
        DataManager.isUserLoggedIn = false
        let storyboard = UIStoryboard(storyboard: .Main)
        let vc = storyboard.instantiateViewController(withIdentifier: kLoginVc) as! LoginVC
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
//MARK: Data Handling
// Convert from NSData to json object
    private func JSONFromData(data: NSData) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil
    }
    
    // Convert from JSON to nsdata
    private func nsdataFromJSON(json: AnyObject) -> NSData?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil;
    }
    
    //MARK: Error Handling
    private func handleError(response: DataResponse<Any>?, error: NSError, callback:APIServiceFailureCallback) {
        print(response?.response?.statusCode ?? "")
        if let errorCode = response?.response?.statusCode {
            guard let responseJSON = self.JSONFromData(data: (response?.data)! as NSData) else {
                callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message:""), error)
                debugPrint("Couldn't read the data")
                return
            }
            let message = (responseJSON as? NSDictionary)?["err"] as? String ?? "Something went wrong. Please try again."
            callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message: message), error)
        }
        else {
            let customError = NSError(domain: "Network Error", code: error.code, userInfo: error.userInfo)
            print((response?.result.error?.localizedDescription)! as String)
            if let errorCode = response?.result.error?.localizedDescription , errorCode == "The Internet connection appears to be offline." {
                callback(NetworkErrorReason.InternetNotReachable, customError)
            }
            else {
                callback(NetworkErrorReason.Other, customError)
            }
        }
    }
}

//MARK: API Manager Class
class APIManager {
    class func errorForNetworkErrorReason(errorReason: NetworkErrorReason) -> NSError {
        var error: NSError!
        
        switch errorReason {
        case .InternetNotReachable:
            error = NSError(domain: "No Internet", code: -1, userInfo: [kMessage : "The Internet connection appears to be offline."])
        case .UnAuthorizedAccess:
            error = NSError(domain: "Authorization Failed", code: 0, userInfo: [kMessage : "Please Re-login to the app."])
        case let .FailureErrorCode(code, message):
            switch code {
            case 500:
                error = NSError(domain: "Server Error", code: code, userInfo: [kMessage : message])
            default:
                error = NSError(domain: "Please try again!", code: code, userInfo: [kMessage : message])
            }
            
        case .Other:
            error = NSError(domain: "Please try again!", code: 0, userInfo: [kMessage : "Something went wrong!. Please check your internet connection."])
        }
        return error
    }
}

//MARK: Indicator Class
public class Indicator {
    
    public static let sharedInstance = Indicator()
    var blurImg = UIImageView()
    var vwIndicator = SCSkypeActivityIndicatorView()
    static var isEnabledIndicator = true
    
    private init() {
        
        blurImg.frame = UIScreen.main.bounds
        blurImg.backgroundColor = UIColor.black
        blurImg.alpha = 0
        blurImg.isUserInteractionEnabled = true
        
        
        vwIndicator?.frame = CGRect.init(x: 0, y: 0, width: 70, height: 70)
        vwIndicator?.backgroundColor =  UIColor.clear
        vwIndicator?.center = blurImg.center
    }
    
    func showIndicator(){
        
        DispatchQueue.main.async( execute: {
            UIApplication.shared.keyWindow?.addSubview(self.blurImg)
            self.blurImg.fadeInForIndicator()
            UIApplication.shared.keyWindow?.addSubview(self.vwIndicator!)
            self.vwIndicator?.startAnimating()
            self.vwIndicator?.fadeIn()
        })
    }
    
    func hideIndicator(){
        
        DispatchQueue.main.async( execute: {
            self.blurImg.fadeOutForIndicator()
            self.vwIndicator?.fadeOut()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.blurImg.removeFromSuperview()
                self.vwIndicator?.removeFromSuperview()
                self.vwIndicator?.stopAnimating()
            })
        })
    }
    
}


