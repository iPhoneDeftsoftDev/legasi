
//
//  SearchUserDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 30/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

struct  SearchUserData {
    var name: String?
    var searchId: Int!
    var profileImageUrl: String?
    var type: String?
    var isAdmin:String!
    var isMember:String!
    var totalConnection:Int?
}
extension SearchByNameVM {
    func parseSearchUserListData(responseDict: JSONDictionary) {
        self.searchUserArray.removeAll()
        if let searchUserArray = responseDict[APIKeys.kData] as? NSArray {
            for userList in searchUserArray {
                let name = (userList as? NSDictionary)? [kName] as? String
                let searchId = (userList as? NSDictionary)? [kSearchId] as? Int
                let totalConnection = (userList as? NSDictionary)? [kTotalConnection] as? Int
                let profileImageUrl = (userList as? NSDictionary)? [kProfileImageUrl] as? String
                let type = (userList as? NSDictionary)? [kType] as? String
                let isAdmin = (userList as? NSDictionary)? ["isAdmin"] as? String
                let isMember = (userList as? NSDictionary)? ["isMember"] as? String
                // call struct of countryData
                
                let data = SearchUserData(name: name, searchId: searchId, profileImageUrl: profileImageUrl, type: type,isAdmin:isAdmin,isMember:isMember,totalConnection:totalConnection)
                self.searchUserArray.append(data)
            }
        }
}
}
