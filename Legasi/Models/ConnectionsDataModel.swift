//
//  ConnectionsDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import CoreLocation

struct followerUserData {
   var followUnfollowId: Int!
    var friendId:Int!
    var value: Int!
    var friendName: String!
    var friendProfileImageUrl: String?
    var userId:Int!
    }

struct userGroupListData {
    var groupId: Int!
    var visibleType: String!
    var groupName: String!
    var groupImageUrl: String?
    var schoolId:Int!
    var isAdmin:String!
    var totalConnection:Int?
}
struct MemberDetail{
    var memberId: Int!
    var name: String!
    var userImageUrl: String!
    var memberFullName:String!

}
struct groupDetailData{
    var groupCoverImageUrl:String?
    var groupImageUrl:String?
    var groupName:String!
    var memberCount:Int!
    var groupDesc:String?
    var memberArray:[groupMemberData]!
    var isAdmin:String!
    var visibleType:String!
    var groupLocation:String!
 }
struct groupMemberData{
    var memberId: Int!
    var name: String!
    var userImageUrl: String!
    var userType:String!
    
}
extension ConnectionsVM {
    func parseFollowerUserData(responseDict: JSONDictionary){
        followerUserArray.removeAll()
        if let followerArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in followerArray {
                
                let user_id = (dict as? NSDictionary)?["userId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["friendName"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["friendProfileImageUrl"] as? String ?? ""
                let followUnfollowId = (dict as? NSDictionary)?["followUnfollowId"] as? Int ?? 0
                let value = (dict as? NSDictionary)?["value"] as? Int ?? 0
                let friendId = (dict as? NSDictionary)?["friendId"] as? Int ?? 0
                
                let data = followerUserData(followUnfollowId: followUnfollowId, friendId: friendId, value: value, friendName: name, friendProfileImageUrl: profileImageUrl, userId: user_id)
                followerUserArray.append(data)
            }
        }
    }
    func parseMemberListData(responseDict: JSONDictionary){
         memberListArray.removeAll()
         if let membersListArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in membersListArray {
                
                let user_id = (dict as? NSDictionary)?["userId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["name"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["profileImageUrl"] as? String ?? ""
                let fullName = (dict as? NSDictionary)?["userName"] as? String ?? "N/A"
                
                let data = MemberDetail(memberId: user_id, name: name, userImageUrl: profileImageUrl, memberFullName: fullName)
                memberListArray.append(data)
            }
        }
    }

    
    func parseUserGroupData(responseDict: JSONDictionary){
        userGroupListArray.removeAll()
        if let userGroupArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in userGroupArray {
                
                let groupId = (dict as? NSDictionary)?["groupId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["groupName"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["groupImageUrl"] as? String ?? ""
                let visibleType = (dict as? NSDictionary)?["visibleType"] as? String ?? ""
                let schoolId = (dict as? NSDictionary)?["schoolId"] as? Int ?? 0
                let isAdmin = (dict as? NSDictionary)?["isAdmin"] as? String ?? ""
                let totalConnection = (dict as? NSDictionary)?["totalConnection"] as? Int ?? 0
                let data = userGroupListData(groupId: groupId, visibleType: visibleType, groupName: name, groupImageUrl: profileImageUrl,schoolId:schoolId,isAdmin:isAdmin,totalConnection:totalConnection)
                userGroupListArray.append(data)
            }
        }
    }
    
    
    func parseGroupFeedListData(responseDict: JSONDictionary){
        // self.newsFeedArray.removeAll()
        
        if let feedDict = responseDict[APIKeys.kData] as? NSDictionary {
            if let feedArray = feedDict[APIKeys.kData] as? NSArray {
                for feed in feedArray {
                    
                    let isPoll = (feed as? NSDictionary)?["isPolled"] as? String ?? ""
                    let status = (feed as? NSDictionary)?["jobStatus"] as? Int ?? 0
                    let user_id = (feed as? NSDictionary)?["userId"] as? Int ?? 0
                    let eventId = (feed as? NSDictionary)?["eventid"] as? Int ?? 0
                    let rsvpStatus = (feed as? NSDictionary)?["rsvpStatus"] as? Int ?? 1
                    let numberofTicket = (feed as? NSDictionary)?["numberofTicket"] as? Int ?? 1
                    let name = (feed as? NSDictionary)?["userName"] as? String ?? "N/A"
                    let jobName = (feed as? NSDictionary)?["jobName"] as? String ?? "N/A"
                    let companyName = (feed as? NSDictionary)?["companyName"] as? String ?? "N/A"
                    let skills = (feed as? NSDictionary)?["skill"] as? String ?? "N/A"
                    let jobType = (feed as? NSDictionary)?["jobType"] as? String ?? "N/A"
                    let isAllowComment = (feed as? NSDictionary)?["allowComment"] as? Int ?? 1
                    let isAllowLocation = (feed as? NSDictionary)?["shareLocation"] as? Int ?? 1
                    let link = (feed as? NSDictionary)?["link"] as? String ?? ""
                    var coordinates: CLLocationCoordinate2D!
                    if let latitude = (feed as? NSDictionary)?["eventLatitude"] as? Double {
                        coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: (feed as! NSDictionary)["eventLongitude"] as! Double)
                    }
                    let userType = (feed as? NSDictionary)?["userType"] as? String ?? ""
                    let userRole = (feed as? NSDictionary)?["userRole"] as? Int ?? 1
                    let date =  (feed as? NSDictionary)?["postDateForMobile"] as? Double ?? 0.0
                    let myDate = Date(milliseconds: date)
                    let dateString = myDate.timeAgoSinceDate(date: myDate)
                    let startDate =  (feed as? NSDictionary)?["eventStartDateMilisecond"] as? Double ?? 0.0
                    let mystartDate = Date(milliseconds: startDate)
                    let mainStartDate = mystartDate.stringFromDate(format: .longMDYDate, type: .local)
                    let startTime = mystartDate.stringFromDate(format: .time, type: .local)
                    
                    let end_Date =  (feed as? NSDictionary)?["eventEndDateMilisecond"] as? Double ?? 0.0
                    let myEndDate = Date(milliseconds: end_Date)
                    let endDate = myEndDate.stringFromDate(format: .longMDYDate, type: .local)
                    let endTime = myEndDate.stringFromDate(format: .time, type: .local)

                    let title = (feed as? NSDictionary)?["title"] as? String ?? ""
                    var description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    if description == "" {
                        description = (feed as? NSDictionary)?["description"] as? String ?? ""
                    }else{
                        description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    }
                    var myTagArray = [TagListData]()
                    if let tagArray = (feed as? NSDictionary)?["newfeed_tag_list"] as? NSArray{
                        for tag in tagArray {
                            if let tagDict = (tag as? NSDictionary)?["tag_detail"] as? NSDictionary {
                                let id  = tagDict["tagId"] as? Int ?? 0
                                let title = tagDict["tagTitle"] as? String ?? ""
                                let data  = TagListData(name: title, tagId: id)
                                myTagArray.append(data)
                            }
                        }
                    }
                    let location = (feed as? NSDictionary)?["location"] as? String ?? ""
                    let mediaType = MediaType(rawValue: ((feed as? NSDictionary)?["mediaType"] as? Int ?? 3))
                    var myMediaArray = [mediaPlayer]()
                    if let mediaArray = (feed as? NSDictionary)?["newfeed_media"] as? NSArray {
                        for media in mediaArray {
                            if let mediaDict = media as? NSDictionary {
                                var url = mediaDict["mediaThumbnailUrl"] as? String
                                if url == ""{
                                    url = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: "")
                                    myMediaArray.append(data)
                                }else{
                                    url = mediaDict["mediaThumbnailUrl"] as? String ?? ""
                                    let videoUrl = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: videoUrl)
                                    myMediaArray.append(data)
                                }
                                
                            }
                        }
                    }
                    let likes = (feed as? NSDictionary)?["newfeed_like_list_count"] as? Int ?? 0  //TODO: Change Value
                     let isLikes = (feed as? NSDictionary)?["isLike"] as? Int ?? 0  //TODO: Change Value
                    let comments = (feed as? NSDictionary)?["newfeed_comments_list_count"] as? Int ?? 0  //TODO: Change Value
                    let type = NewsFeedType(rawValue: ((feed as? NSDictionary)?["newsfeedType"] as? Int ?? 3))
                    let profileImage = (feed as? NSDictionary)?["userImage"] as? String ?? ""
                    var myPollArray = [PollList]()
                    if let pollArray = (feed as? NSDictionary)?["newfeed_poll_list"] as? NSArray {
                        for poll in pollArray {
                            if let pollDict = poll as? NSDictionary {
                                let title = pollDict["pollOption"] as? String ?? ""
                                let id = pollDict["pollId"] as? Int ?? 0
                                let count = pollDict["totalPolls"] as? Int ?? 0
                                let data = PollList(title: title, id: id, pollCount: count)
                                myPollArray.append(data)
                            }
                        }
                    }
                    let data = NewsFeedData(isPolled:isPoll,userId: user_id, name: name, time: dateString, title: title, Description: description, newsFeedTagList: myTagArray, location: location, mediaListData: myMediaArray, likes: likes, comments: comments, type: type, profilePic: profileImage, pollList: myPollArray, mediaType: mediaType,date: mainStartDate,eventId:eventId, commentsArray:nil,status:status,isLike:isLikes,jobName:jobName,companyName:companyName,skills:skills,jobType:jobType,isAllowComment:isAllowComment,isAllowLocation:isAllowLocation,link:link,rsvpStatus:rsvpStatus,userType:userType,userRole:userRole, startTime:startTime,endDate:endDate,endTime:endTime,numberofTicket:numberofTicket,startDateTime:startDate,endDateTime:end_Date,coordinates:coordinates)
                    groupDetailArray.append(data)
                }
            }
        }
    }

    
    func parseEachGroupData(responseDict: JSONDictionary){
        if let memberData = responseDict[APIKeys.kData] as? NSDictionary {
            
            let coverImage = memberData["groupImageUrl"] as? String ?? ""
            let userImage = memberData["groupIconImageUrl"] as? String ?? ""
            let groupName = memberData["groupName"] as? String ?? ""
            let description = memberData["description"] as? String ?? ""
            let isAdmin = memberData["isAdmin"] as? String ?? ""
            let privacy = memberData["visibleType"] as? String ?? ""
            let location = memberData["groupLocation"] as? String ?? ""
            var memberListArray = [groupMemberData]()
            if let memberArray = memberData["groupMemberList"] as? NSArray{
                for member in memberArray {
                    if let member = member as? NSDictionary {
                        let memberName  = member["userName"] as? String ?? ""
                        let userId = member["userId"] as? Int ?? 0
                        let imageUrl  = member["userProfileImageUrl"] as? String ?? ""
                        let status  = member["userType"] as? String ?? ""
                        let data  = groupMemberData(memberId: userId, name: memberName, userImageUrl: imageUrl, userType: status)
                        memberListArray.append(data)
                    }
                }
            }
           
            groupDetailInfo = groupDetailData(groupCoverImageUrl: coverImage, groupImageUrl: userImage, groupName: groupName, memberCount: memberListArray.count, groupDesc: description, memberArray: memberListArray, isAdmin:isAdmin,visibleType:privacy,groupLocation:location)
            
            
        }
        
    }
    
}
