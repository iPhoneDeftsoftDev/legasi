//
//  SettingDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 09/01/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct ProfileDetailData {
    var userName: String!
    var coverImage:String!
    var profileImage: String!
    var fullName: String!
    var countryCode:String!
    var phoneNumber: String!
    var emailAddress: String!
    var address:String!
    var lat:Double!
    var long:Double!
    var educationDetail:[EducationDetails]!
    var WorkDetails:[WorkDetails]!
    var schoolName: schoolListData!
    var userType:String!
    var passoutYear:String!
    var dob:String!
    var facebookId:String!
    var linkedInId:String!
    var likeIcon_url:String!
    var unlikeIcon_url :String!
    
    var shareLocation :Int! //shareMylocation
    var student:Int!
    var graduate:Int!
    var connectedUsers:Int!
    var school:Int!
    var postSeen:Int!
    
    var newPostbyschool:Int!
    var newPostbyconnectedUser:Int!
    var schooleventhappening:Int!
    var newmessage:Int!
    var newconnectionrequest:Int!
    var userreplytomypost:Int!
    var birthdayonconnecteduser:Int!
    var newjobupdatedbyconnecteduser:Int!
    var skillDetail : [skillsDetailData]!
    
 }


extension SettingsVM {
    
    func parseUserProfileData(responseDict: JSONDictionary){
        if let feed = responseDict[APIKeys.kData] as? NSDictionary {
            
            let userName = feed["userName"] as? String ?? ""
            let coverImage = feed["coverImageUrl"] as? String ?? ""
            let profileImage = feed["profileImageUrl"] as? String ?? ""
            let fullName = feed["name"] as? String ?? ""
            let countryCode = feed["countryCode"] as? String ?? ""
            var phoneNumber = ""
            if let phone = feed["phoneNumber"] as? Int {
                phoneNumber = "\(phone)"
            }
//            let phoneNumber = feed["phoneNumber"] as? String ?? ""
            
            let shareLocation = feed["shareMylocation"] as? Int ?? 1
            let student = feed["student"] as? Int ?? 1
            let graduate = feed["graduates"] as? Int ?? 1
            let connectedUsers = feed["connectedUsers"] as? Int ?? 1
            let school = feed["school"] as? Int ?? 1
            let postSeen = feed["postSeen"] as? Int ?? 1
            
            let newPostbyschool = feed["newPostbyschool"] as? Int ?? 1
            let newPostbyconnectedUser = feed["newPostbyconnectedUser"] as? Int ?? 1
            let schooleventhappening = feed["schooleventhappening"] as? Int ?? 1
            let newmessage = feed["newmessage"] as? Int ?? 1
            let newconnectionrequest = feed["newconnectionrequest"] as? Int ?? 1
            let userreplytomypost = feed["userreplytomypost"] as? Int ?? 1
            let birthdayonconnecteduser = feed["birthdayonconnecteduser"] as? Int ?? 1
            let newjobupdatedbyconnecteduser = feed["newjobupdatedbyconnecteduser"] as? Int ?? 1
            
            
            
            let emailAddress = feed["email"] as? String ?? ""
            let address = feed["addressLine1"] as? String ?? ""
            let lat = feed["latitude"] as? Double ?? 0.0
            let long = feed["longitude"] as? Double ?? 0.0
            let date =  feed["dob"] as? String ?? ""
            var schoolData = schoolListData()
            var likeIconUrl = String()
            var unLikeIconUrl = String()
            if let schoolList = feed["school_detail"] as? NSDictionary{
                let schoolName = schoolList["schoolName"] as? String ?? ""
                let id = schoolList["id"] as? Int ?? 0
                let nameOfGraduate = schoolList["nameforGraduates"] as? String ?? ""
                likeIconUrl = schoolList["likeIconUrl"] as? String ?? ""
                unLikeIconUrl = schoolList["unlikeIconUrl"] as? String ?? ""
                
                schoolData  = schoolListData(name: schoolName, schoolId: id,nameOfGraduate:nameOfGraduate)
               
            }
            let userType = feed["userType"] as? String ?? ""
            let passOutYear = feed["graduationYear"] as? String ?? ""
            let facebookId = feed["facebookId"] as? String ?? ""
            let linkedInId = feed["linkedInId"] as? String ?? ""
            var educationDetail = [EducationDetails]()
            if let educationArray = feed["education_detail"] as? NSArray{
                for data in educationArray {
                    if let data = data as? NSDictionary {
                        let graduationYear  = data["graduationYear"] as? String ?? ""
                        let collegeName = data["collegeName"] as? String ?? ""
                        let degreeType  = data["degreeType"] as? String ?? ""
                        let majorDetails = data["majorDetails"] as? String ?? ""
                        let educationDetailId = data["educationDetailId"] as? Int ?? 0
                        
                        let data  = EducationDetails(graduationYear: graduationYear, collegeName: collegeName, degreeType: degreeType, majorDetails: majorDetails,educationDetailId:educationDetailId)
                        educationDetail.append(data)
                    }
                }
            }
            var workDetail = [WorkDetails]()
            if let workDetailArray = feed["work_detail"] as? NSArray{
                for data in workDetailArray {
                    if let data = data as? NSDictionary {
                        let jobTitle  = data["jobTitle"] as? String ?? ""
                        let companyName = data["companyName"] as? String ?? ""
                        let industryType  = data["industryType"] as? String ?? ""
                        let jobLocation = data["jobLocation"] as? String ?? ""
                        let jobDiscription = data["jobDiscription"] as? String ?? ""
                        let workDetailId = data["workDetailId"] as? Int ?? 0
                        let currentJob = data["currentJob"] as? String ?? ""
                        let sinceYear = data["sinceYear"] as? String ?? ""
                        let data  = WorkDetails(jobTitle: jobTitle, companyName: companyName, industryType: industryType, jobLocation: jobLocation, lat: "0.0", long: "0.0", jobDiscription: jobDiscription,workDetailId:workDetailId, isCurrentJob: currentJob, sinceYear: sinceYear)
//                        print(">>>>>>>>>>>>>>>>>>>>>>>>>.Current job : ", currentJob)
                        workDetail.append(data)
                    }
                }
            }
            
            var skillsDataArray = [skillsDetailData]()
            if let skillsDetailArray = feed["user_skills_detail"] as? NSArray {
                for data in skillsDetailArray {
                    
                   let jobTitle = (data as? NSDictionary)?["name"] as? String ?? "N/A"
                  
                    
                    let data = skillsDetailData(workDetailId: 0, userId: 0, jobTitle: jobTitle, companyName: "", industryType: "")
                    skillsDataArray.append(data)
                }
            }
            
            
            profileData = ProfileDetailData(userName: userName, coverImage: coverImage, profileImage: profileImage, fullName: fullName, countryCode: countryCode, phoneNumber: phoneNumber, emailAddress: emailAddress, address: address, lat: lat, long: long, educationDetail: educationDetail, WorkDetails: workDetail, schoolName: schoolData, userType: userType, passoutYear: passOutYear, dob: date,facebookId:facebookId,linkedInId:linkedInId,likeIcon_url:likeIconUrl,unlikeIcon_url:unLikeIconUrl,shareLocation:shareLocation,student:student,graduate:graduate,connectedUsers:connectedUsers,school:school,postSeen:postSeen,newPostbyschool:newPostbyschool,newPostbyconnectedUser:newPostbyconnectedUser,schooleventhappening:schooleventhappening, newmessage: newmessage, newconnectionrequest:newconnectionrequest, userreplytomypost: userreplytomypost, birthdayonconnecteduser:birthdayonconnecteduser, newjobupdatedbyconnecteduser: newjobupdatedbyconnecteduser,skillDetail:skillsDataArray)
            
            
        }
        
    }
  
}
