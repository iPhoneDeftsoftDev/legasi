//
//  UserDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 16/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import CoreLocation

struct EducationDetails {
    var graduationYear: String!
    var collegeName: String!
    var degreeType: String!
    var majorDetails: String!
    var educationDetailId:Int!
    var jsonRepresentation : NSMutableDictionary {
        let dict = NSMutableDictionary()
        dict["graduationYear"] = graduationYear
        dict["collegeName"] = collegeName
        dict["degreeType"] = degreeType
        dict["majorDetails"] = majorDetails
        dict["educationDetailId"] = educationDetailId
        return dict
    }
}

struct WorkDetails {
    var jobTitle: String!
    var companyName: String!
    var industryType: String!
    var jobLocation: String!
    var lat: String!
    var long: String!
    var jobDiscription: String!
    var workDetailId:Int!
    var isCurrentJob: String!
    var sinceYear: String!
    var jsonRepresentation : NSMutableDictionary {
        let dict = NSMutableDictionary()
        dict["jobTitle"] = jobTitle
        dict["companyName"] = companyName
        dict["industryType"] = industryType
        dict["jobLocation"] = jobLocation
        dict["lat"] = lat
        dict["long"] = long
        dict["jobDiscription"] = jobDiscription
        dict["workDetailId"] = workDetailId
        dict["currentJob"] = isCurrentJob
        dict["sinceYear"] = sinceYear
        return dict
    }
}
struct OtherUserProfileData {
    var userId:Int!
    var name: String!
    var email: String!
    var role:Int!
    var userName: String!
    var schoolId: Int!
    var profileImage: String!
    var profileImageUrl: String!
    var coverImage: String!
    var coverImageUrl: String!
    var countryCode: String!
    var city: String!
    var phoneNumber: Int!
    var country: String!
    var state: String!
    var zipCode: String!
    var addressLine1: String!
    var addressLine2: String!
    var userType: String!
    var dob: String!
    var graduationYear: String!
    var latitude: Double!
    var longitude: Double!
    var student : Int
    var school : Int
    var graduates : Int
    var connectedUsers  : Int
    var postSeen  : Int
    var newPostBySchool : Int
    var newPostByConnectedUser : Int
    var schoolEventHapping: Int
    var newMessage : Int
    var newConnectionRequest : Int
    var userRplyToMyPost : Int
    var birthdayOnConnectedUser : Int
    var newJobUpdatedByConnectedUser : Int
    var groupParticipationCount : Int
    var totalLike : Int
    var schoolDetail : [schoolDetailData]
    var educationDetail : [educationDetailData]
    var currentWorkDetail : [workDetailData]
    var pastWorkDetail : [workDetailData]
    var skillDetail : [skillsDetailData]
    var isFollowing:Int
    var chatListId:Int?

}
struct schoolDetailData {
    var id : Int
    var schoolName:String!
    var schoolAddress :String!
    var schoolDescription :String!
    var createdAt :String!
    var schoolStatus :Int
    var updatedAt :String!
    var logo :String!
    var smallLogoIcon :String!
    var likeIcon :String!
    var studentsColor :String!
    var graduatesColours :String!
    var nameforGraduates :String!
}

struct educationDetailData {
    var educationDetailId : Int
    var userId : Int
    var graduationYear: String!
    var collegeName : String!
    var degreeType : String!
    var majorDetails : String!
    var createdAt : String!
    let updatedAt : String!
    
}

struct workDetailData {
    var workDetailId : Int
    var userId : Int
    var jobTitle: String!
    var companyName : String!
    var industryType : String!
    var jobLocation : String!
    var jobDiscription : String!
    let createdAt : String!
    let updatedAt : String!
    let isCurrentJob: String!
    var sinceYear: String!
}

struct skillsDetailData {
    var workDetailId : Int
    var userId : Int
    var jobTitle: String!
    var companyName : String!
    var industryType : String!
    
}

struct schoolListData {
    var name: String?
    var schoolId: Int?
    var nameOfGraduate:String?
}

extension UserLoginVM {
    func parseSchoolListData(responseDict: JSONDictionary) {
        self.schoolListArray.removeAll()
        if let schoolListArray = responseDict[APIKeys.kData] as? NSArray {
            for schoolList in schoolListArray {
                let name = (schoolList as? NSDictionary)? [kSchoolName] as? String
                let schoolId = (schoolList as? NSDictionary)? [kId] as? Int
                let nameOfGraduate = (schoolList as? NSDictionary)? ["nameforGraduates"] as? String
                // call struct of countryData
                
                let data = schoolListData(name: name,schoolId:schoolId,nameOfGraduate:nameOfGraduate)
                self.schoolListArray.append(data)
            }
        }
    }
    
    
    //other user profile
    func parseOtherUserProfileData(responseDict : JSONDictionary){
        if let profileDataDict = responseDict[APIKeys.kData] as? NSDictionary{
            let user_id = profileDataDict["userId"] as? Int ?? 0
            let name = profileDataDict["name"] as? String ?? "N/A"
            let chatListId =  profileDataDict["chatListId"] as? Int ?? 0
            let isFollowing = profileDataDict["isFollowing"] as? Int ?? 0
            let email = profileDataDict["email"] as? String ?? "N/A"
            let role = profileDataDict["role"] as? Int ?? 0
            let userName = profileDataDict["userName"] as? String ?? "N/A"
            let schoolID = profileDataDict["schoolId"] as? Int ?? 0
            let profileImage = profileDataDict["profileImage"] as? String ?? "N/A"
            let profileImageUrl = profileDataDict["profileImageUrl"] as? String ?? "N/A"
            let coverImage = profileDataDict["coverImage"] as? String ?? "N/A"
            let coverImageUrl = profileDataDict["coverImageUrl"] as? String ?? "N/A"
            let countryCode = profileDataDict["countryCode"] as? String ?? "N/A"
            let city = profileDataDict["city"] as? String ?? "N/A"
            let phoneNumber = profileDataDict["phoneNumber"] as? Int ?? 0
            let country = profileDataDict["country"] as? String ?? "N/A"
            let state = profileDataDict["state"] as? String ?? "N/A"
            let zipCode = profileDataDict["zipcode"] as? String ?? "N/A"
            let addressLine1 = profileDataDict["addressLine1"] as? String ?? "N/A"
            let addressLine2 = profileDataDict["addressLine2"] as? String ?? "N/A"
            let userType = profileDataDict["userType"] as? String ?? "N/A"
            let dob = profileDataDict["dob"] as? String ?? "N/A"
            let graduationYear = profileDataDict["graduationYear"] as? String ?? "N/A"
            let latitude =  profileDataDict["latitude"] as? Double ?? 0.0
            let longitude =  profileDataDict["longitude"] as? Double ?? 0.0
            let student = profileDataDict["student"] as? Int ?? 0
            let school = profileDataDict["school"] as? Int ?? 0
            let graduates = profileDataDict["graduates"] as? Int ?? 0
            let connectedUsers = profileDataDict["connectedUsers"] as? Int ?? 0
            let postSeen = profileDataDict["postSeen"] as? Int ?? 0
            let newPostBySchool = profileDataDict["newPostbyschool"] as? Int ?? 0
            let newPostByConnectedUser = profileDataDict["newPostbyconnectedUser"] as? Int ?? 0
            let schoolEventHapping = profileDataDict["schooleventhappening"] as? Int ?? 0
            let newMessage = profileDataDict["newmessage"] as? Int ?? 0
            let newConnectionRequest = profileDataDict["newconnectionrequest"] as? Int ?? 0
            let userRplyToMyPost = profileDataDict["userreplytomypost"] as? Int ?? 0
            let birthdayOnConnectedUser = profileDataDict["birthdayonconnecteduser"] as? Int ?? 0
            let newJobUpdatedByConnectedUser = profileDataDict["newjobupdatedbyconnecteduser"] as? Int ?? 0
            let groupParticipationCount = profileDataDict["groupParticipationCount"] as? Int ?? 0
            let totalLike = profileDataDict["totalLike"] as? Int ?? 0
            
            var schoolDataArray = [schoolDetailData]()
            if let schoolDetailArray = profileDataDict[APIKeys.kSchool_Detail] as? NSArray {
                for data in schoolDetailArray {
                    let id = (data as? NSDictionary)?["id"] as? Int ?? 0
                    let schoolName = (data as? NSDictionary)?["schoolName"] as? String ?? "N/A"
                    let schoolAddress = (data as? NSDictionary)?["schoolAddress"] as? String ?? "N/A"
                    let schoolDescription = (data as? NSDictionary)?["schoolDescription"] as? String ?? "N/A"
                    let createdAt = (data as? NSDictionary)?["created_at"] as? String ?? "N/A"
                    let schoolStatus = (data as? NSDictionary)?["schoolStatus"] as? Int ?? 0
                    let updatedAt = (data as? NSDictionary)?["updated_at"] as? String ?? "N/A"
                    let logo = (data as? NSDictionary)?["logo"] as? String ?? "N/A"
                    let smallLogoIcon = (data as? NSDictionary)?["smallLogoIcon"] as? String ?? "N/A"
                    let likeIcon = (data as? NSDictionary)?["likeIcon"] as? String ?? "N/A"
                    let studentsColor = (data as? NSDictionary)?["studentsColours"] as? String ?? "N/A"
                    let graduatesColours = (data as? NSDictionary)?["graduatesColours"] as? String ?? "N/A"
                    let nameforGraduates = (data as? NSDictionary)?["nameforGraduates"] as? String ?? "N/A"
                    
                    let data = schoolDetailData(id: id, schoolName: schoolName, schoolAddress: schoolAddress, schoolDescription: schoolDescription, createdAt: createdAt, schoolStatus: schoolStatus, updatedAt: updatedAt, logo: logo, smallLogoIcon: smallLogoIcon, likeIcon: likeIcon, studentsColor: studentsColor, graduatesColours: graduatesColours, nameforGraduates: nameforGraduates)
                    schoolDataArray.append(data)
                }
            }
            var currentWorkDataArray = [workDetailData]()
            var pastWorkDataArray = [workDetailData]()
            if let workDetailArray = profileDataDict[APIKeys.kWork_Detail] as? NSArray {
                for data in workDetailArray {
                    let workDetailId = (data as? NSDictionary)?["workDetailId"] as? Int ?? 0
                    let userId = (data as? NSDictionary)?["userId"] as? Int ?? 0
                    let jobTitle = (data as? NSDictionary)?["jobTitle"] as? String ?? "N/A"
                    let companyName = (data as? NSDictionary)?["companyName"] as? String ?? "N/A"
                    let industryType = (data as? NSDictionary)?["industryType"] as? String ?? "N/A"
                    let jobLocation = (data as? NSDictionary)?["jobLocation"] as? String ?? "N/A"
                    let jobDiscription = (data as? NSDictionary)?["jobDiscription"] as? String ?? "N/A"
                    let createdAt = (data as? NSDictionary)?["created_at"] as? String ?? "N/A"
                    let updatedAt = (data as? NSDictionary)?["updated_at"] as? String ?? "N/A"
                    let currentJob = (data as? NSDictionary)?["currentJob"] as? String ?? "No"
                    let sinceYear = (data as? NSDictionary)?["sinceYear"] as? String ?? ""
                    
                    let data = workDetailData(workDetailId: workDetailId, userId: userId, jobTitle: jobTitle, companyName: companyName, industryType: industryType, jobLocation:jobLocation, jobDiscription: jobDiscription, createdAt: createdAt, updatedAt: updatedAt, isCurrentJob: currentJob, sinceYear: sinceYear)
                    if currentJob == "Yes" {
                        currentWorkDataArray.append(data)
                    }else {
                        pastWorkDataArray.append(data)
                    }
                    
                }
            }
            
            var educationDataArray = [educationDetailData]()
            if let educationDetailArray = profileDataDict[APIKeys.kEducation_Detail] as? NSArray {
                for data in educationDetailArray {
                    let educationDetailId = (data as? NSDictionary)?["educationDetailId"] as? Int ?? 0
                    let userId = (data as? NSDictionary)?["userId"] as? Int ?? 0
                    let graduationYear = (data as? NSDictionary)?["graduationYear"] as? String ?? "N/A"
                    let collegeName = (data as? NSDictionary)?["collegeName"] as? String ?? "N/A"
                    let degreeType = (data as? NSDictionary)?["degreeType"] as? String ?? "N/A"
                    let majorDetails = (data as? NSDictionary)?["majorDetails"] as? String ?? "N/A"
                    let createdAt = (data as? NSDictionary)?["created_at"] as? String ?? "N/A"
                    let updatedAt = (data as? NSDictionary)?["updated_at"] as? String ?? "N/A"
                    
                    let data = educationDetailData(educationDetailId: educationDetailId, userId: userId, graduationYear: graduationYear, collegeName: collegeName, degreeType: degreeType, majorDetails: majorDetails, createdAt: createdAt, updatedAt: updatedAt)
                    educationDataArray.append(data)
                }
            }
            
            var skillsDataArray = [skillsDetailData]()
            if let skillsDetailArray = profileDataDict["user_skills_detail"] as? NSArray {
                for data in skillsDetailArray {
                    
                    let jobTitle = (data as? NSDictionary)?["name"] as? String ?? "N/A"
                    
                    
                    let data = skillsDetailData(workDetailId: 0, userId: 0, jobTitle: jobTitle, companyName: "", industryType: "")
                    skillsDataArray.append(data)
                }
            }

            self.userProfileData = OtherUserProfileData(userId: user_id, name: name, email: email, role: role, userName: userName, schoolId: schoolID, profileImage: profileImage, profileImageUrl: profileImageUrl, coverImage: coverImage, coverImageUrl: coverImageUrl, countryCode: countryCode, city: city, phoneNumber: phoneNumber, country: country, state: state, zipCode: zipCode, addressLine1: addressLine1, addressLine2: addressLine2, userType: userType, dob: dob, graduationYear: graduationYear, latitude: latitude, longitude: longitude, student: student, school: school, graduates: graduates, connectedUsers: connectedUsers, postSeen: postSeen, newPostBySchool: newPostBySchool, newPostByConnectedUser: newPostByConnectedUser, schoolEventHapping: schoolEventHapping, newMessage: newMessage, newConnectionRequest: newConnectionRequest, userRplyToMyPost: userRplyToMyPost, birthdayOnConnectedUser: birthdayOnConnectedUser, newJobUpdatedByConnectedUser: newJobUpdatedByConnectedUser, groupParticipationCount: groupParticipationCount, totalLike: totalLike, schoolDetail: schoolDataArray, educationDetail: educationDataArray, currentWorkDetail: currentWorkDataArray, pastWorkDetail: pastWorkDataArray, skillDetail: skillsDataArray,isFollowing:isFollowing,chatListId:chatListId)
        }
    }
    
    
    func parseOtherUserPostData(responseDict: JSONDictionary){
        // self.otherUserPostDataArray.removeAll()
        
        if let feedDict = responseDict[APIKeys.kData] as? NSDictionary {
            if let feedArray = feedDict[APIKeys.kData] as? NSArray {
                for feed in feedArray {
                    
                    let isPoll = (feed as? NSDictionary)?["isPolled"] as? String ?? ""
                    let user_id = (feed as? NSDictionary)?["userId"] as? Int ?? 0
                    let eventId = (feed as? NSDictionary)?["eventid"] as? Int ?? 0
                    let name = (feed as? NSDictionary)?["userName"] as? String ?? "N/A"
                    let jobName = (feed as? NSDictionary)?["jobName"] as? String ?? "N/A"
                    let rsvpStatus = (feed as? NSDictionary)?["rsvpStatus"] as? Int ?? 1
                    let companyName = (feed as? NSDictionary)?["companyName"] as? String ?? "N/A"
                    let numberofTicket = (feed as? NSDictionary)?["numberofTicket"] as? Int ?? 1
                    let skills = (feed as? NSDictionary)?["skill"] as? String ?? "N/A"
                    let jobType = (feed as? NSDictionary)?["jobType"] as? String ?? "N/A"
                    let isAllowComment = (feed as? NSDictionary)?["allowComment"] as? Int ?? 1
                    let isAllowLocation = (feed as? NSDictionary)?["shareLocation"] as? Int ?? 1
                    let link = (feed as? NSDictionary)?["link"] as? String ?? ""
                    let userType = (feed as? NSDictionary)?["userType"] as? String ?? ""
                    let userRole = (feed as? NSDictionary)?["userRole"] as? Int ?? 1
                    let status = (feed as? NSDictionary)?["jobStatus"] as? Int ?? 0
                    var coordinates: CLLocationCoordinate2D!
                    if let latitude = (feed as? NSDictionary)?["eventLatitude"] as? Double {
                        coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: (feed as! NSDictionary)["eventLongitude"] as! Double)
                    }
                    let date =  (feed as? NSDictionary)?["postDateForMobile"] as? Double ?? 0.0
                    let myDate = Date(milliseconds: date)
                    let dateString = myDate.timeAgoSinceDate(date: myDate)
                    let startDate =  (feed as? NSDictionary)?["eventStartDateMilisecond"] as? Double ?? 0.0
                    let mystartDate = Date(milliseconds: startDate)
                    let mainStartDate = mystartDate.stringFromDate(format: .longMDYDate, type: .local)
                    let startTime = mystartDate.stringFromDate(format: .time, type: .local)
                    
                    let end_Date =  (feed as? NSDictionary)?["eventEndDateMilisecond"] as? Double ?? 0.0
                    let myEndDate = Date(milliseconds: end_Date)
                    let endDate = myEndDate.stringFromDate(format: .longMDYDate, type: .local)
                    let endTime = myEndDate.stringFromDate(format: .time, type: .local)
                    let title = (feed as? NSDictionary)?["title"] as? String ?? ""
                    var description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    if description == "" {
                        description = (feed as? NSDictionary)?["description"] as? String ?? ""
                    }else{
                        description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    }
                    
                    var myTagArray = [TagListData]()
                    if let tagArray = (feed as? NSDictionary)?["newfeed_tag_list"] as? NSArray{
                        for tag in tagArray {
                            if let tagDict = (tag as? NSDictionary)?["tag_detail"] as? NSDictionary {
                                let id  = tagDict["tagId"] as? Int ?? 0
                                let title = tagDict["tagTitle"] as? String ?? ""
                                let data  = TagListData(name: title, tagId: id)
                                myTagArray.append(data)
                            }
                        }
                    }
                    let location = (feed as? NSDictionary)?["location"] as? String ?? ""
                    let mediaType = MediaType(rawValue: ((feed as? NSDictionary)?["mediaType"] as? Int ?? 3))
                    var myMediaArray = [mediaPlayer]()
                    if let mediaArray = (feed as? NSDictionary)?["newfeed_media"] as? NSArray {
                        for media in mediaArray {
                            if let mediaDict = media as? NSDictionary {
                                var url = mediaDict["mediaThumbnailUrl"] as? String
                                if url == ""{
                                    url = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: "")
                                    myMediaArray.append(data)
                                }else{
                                    url = mediaDict["mediaThumbnailUrl"] as? String ?? ""
                                    let videoUrl = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: videoUrl)
                                    myMediaArray.append(data)
                                }
                                
                            }
                        }
                    }
                    let likes = (feed as? NSDictionary)?["newfeed_like_list_count"] as? Int ?? 0  //TODO: Change Value
                    let isLikes = (feed as? NSDictionary)?["isLike"] as? Int ?? 0
                    let comments = (feed as? NSDictionary)?["newfeed_comments_list_count"] as? Int ?? 0 //TODO: Change Value
                    let type = NewsFeedType(rawValue: ((feed as? NSDictionary)?["newsfeedType"] as? Int ?? 3))
                    let profileImage = (feed as? NSDictionary)?["userImage"] as? String ?? ""
                    var myPollArray = [PollList]()
                    if let pollArray = (feed as? NSDictionary)?["newfeed_poll_list"] as? NSArray {
                        for poll in pollArray {
                            if let pollDict = poll as? NSDictionary {
                                let title = pollDict["pollOption"] as? String ?? ""
                                let id = pollDict["pollId"] as? Int ?? 0
                                let count = pollDict["totalPolls"] as? Int ?? 0
                                let data = PollList(title: title, id: id, pollCount: count)
                                myPollArray.append(data)
                            }
                        }
                    }
                    let data = NewsFeedData(isPolled: isPoll, userId: user_id, name: name, time: dateString, title: title, Description: description, newsFeedTagList: myTagArray, location: location, mediaListData: myMediaArray, likes: likes, comments: comments, type: type, profilePic: profileImage, pollList: myPollArray, mediaType: mediaType,date: mainStartDate,eventId:eventId,commentsArray:nil,status:status,isLike:isLikes,jobName:jobName,companyName:companyName,skills:skills,jobType:jobType,isAllowComment:isAllowComment,isAllowLocation:isAllowLocation,link:link,rsvpStatus:rsvpStatus,userType:userType,userRole:userRole, startTime:startTime,endDate:endDate,endTime:endTime,numberofTicket:numberofTicket,startDateTime:startDate,endDateTime:end_Date,coordinates:coordinates)
                    
                    otherUserPostDataArray.append(data)
                }
            }
        }
    }
    
}
