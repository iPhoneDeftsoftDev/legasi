//
//  NewsFeedDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 17/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import CoreLocation

enum NewsFeedType: Int {
     case normal = 1
     case job = 2
     case pollSubmit = 3
     case event = 4
     case poll = 5
}

enum MediaType: Int {
    case image = 1
    case video = 2
    case nothing = 3
}

enum FilterType {
    case post
    case tag
    case address
    case miles
    case searchType
    case graduate
    case graduateYear
    case groupIds
    
}
enum MessageType{
  case comment
  case reply
}
struct  TagListData {
    var name: String?
    var tagId: Int?
}

struct  PollList {
    var title: String!
    var id: Int!
    var pollCount: Int!
}


struct FilterData {
    var title: String
    var id: Int?
    var type: FilterType
    var coordinates: CLLocationCoordinate2D?
    var ids: [Int]?
}

struct searchFilter{
    var title: String
    var type: FilterType
    var id: Int?
}
struct NewsFeedData {
    var isPolled: String!
    var userId:Int!
    var name: String!
    var time: String!
    var title:String!
    var Description: String!
    var newsFeedTagList: [TagListData]!
    var location:String!
    var mediaListData: [mediaPlayer]!
    var likes: Int!
    var comments: Int!
    var type : NewsFeedType!
    var profilePic: String!
    var pollList : [PollList]!
    var mediaType: MediaType!
    var date: String!
    var eventId:Int!
    var commentsArray:[Comments]!
    var status:Int!
    var isLike:Int!
    var jobName:String!
    var companyName:String!
    var skills:String!
    var jobType:String!
    var isAllowComment:Int?
    var isAllowLocation:Int?
    var link:String?
    var rsvpStatus:Int?
    var userType:String!
    var userRole:Int!
    var startTime:String?
    var endDate: String?
    var endTime: String?
    var numberofTicket:Int?
    var startDateTime:Double?
    var endDateTime:Double?
    var coordinates:CLLocationCoordinate2D?
  }
struct Comments{
    var name:String!
    var time:String!
    var likes:Int?
    var comments:Int?
    var type:MessageType!
    var commentId: Int!
    var imageUrl:String?
    var isLikes:Int!
    var commentText:String!
    var userId:Int!
    var userType:String!
    var userRole:Int!
}
struct mediaPlayer{
    var imageUrl: String!
    var videoUrl:String!
}

struct geoFancingData{
    var coordinates:CLLocationCoordinate2D!
    var eventId:Int!
}

extension NewsFeedVM {
    
    func parseGeoFancingData(responseDict: JSONDictionary){
        self.geoFacingArray.removeAll()
        if let geoFacing_Array = responseDict[APIKeys.kData] as? NSArray {
            for geoFacingDict in geoFacing_Array {
                
                let eventId = (geoFacingDict as? NSDictionary)? [kSearchId] as? Int
                let coordinates = CLLocationCoordinate2D(latitude: (geoFacingDict as? NSDictionary)? ["eventLatitude"] as? Double ?? 0.0, longitude: (geoFacingDict as? NSDictionary)? ["eventLongitude"] as? Double ?? 0.0)
               
                // call struct of countryData
                
                let data = geoFancingData(coordinates:coordinates,eventId:eventId)
                self.geoFacingArray.append(data)
            }
        }
    }
    
    
    
    func parseTagListData(responseDict: JSONDictionary) {
        self.tagListArray.removeAll()
        if let tagListArray = responseDict[APIKeys.kData] as? NSArray {
            for tagList in tagListArray {
                let name = (tagList as? NSDictionary)? [kTagTitle] as? String
                let tagId = (tagList as? NSDictionary)? [kTagId] as? Int
                // call struct of countryData
                
                let data = TagListData(name: name,tagId:tagId)
                self.tagListArray.append(data)
            }
        }
    }
    func parseParticularNewsFeedData(responseDict: JSONDictionary){
        if let feed = responseDict[APIKeys.kData] as? NSDictionary {
            
            let isPoll = feed["isPolled"] as? String ?? ""
            let user_id = feed["userId"] as? Int ?? 0
            let eventId = feed["eventid"] as? Int ?? 0
            let isAllowComment = feed["allowComment"] as? Int ?? 1
            let isAllowLocation = feed["shareLocation"] as? Int ?? 1
            let rsvpStatus = feed["rsvpStatus"] as? Int ?? 1
            let numberofTicket = feed["numberofTicket"] as? Int ?? 1
            let link = feed["link"] as? String ?? ""
            let userType = feed["userType"] as? String ?? ""
            let userRole = feed["userRole"] as? Int ?? 1
            let name = feed["userName"] as? String ?? "N/A"
            let jobName = feed["jobName"] as? String ?? "N/A"
            var coordinates: CLLocationCoordinate2D!
            if let latitude = feed["eventLatitude"] as? Double {
                coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: feed["eventLongitude"] as! Double)
            }
            let companyName = feed["companyName"] as? String ?? "N/A"
            let skills = feed["skill"] as? String ?? "N/A"
            let jobType = feed["jobType"] as? String ?? "N/A"
            let status = feed["jobStatus"] as? Int ?? 0
            let date =  feed["postDateForMobile"] as? Double ?? 0.0
            let myDate = Date(milliseconds: date)
            let dateString = myDate.timeAgoSinceDate(date: myDate)
           
            let startDate =  feed["eventStartDateMilisecond"] as? Double ?? 0.0
            let mystartDate = Date(milliseconds: startDate)
            let mainStartDate = mystartDate.stringFromDate(format: .longMDYDate, type: .local)
            let startTime = mystartDate.stringFromDate(format: .time, type: .local)
            
            let end_Date =  feed["eventEndDateMilisecond"] as? Double ?? 0.0
            let myEndDate = Date(milliseconds: end_Date)
            let endDate = myEndDate.stringFromDate(format: .longMDYDate, type: .local)
            let endTime = myEndDate.stringFromDate(format: .time, type: .local)
            
            
            let title = feed["title"] as? String ?? ""
            var description = feed["body"] as? String ?? ""
            if description == "" {
                description = feed["description"] as? String ?? ""
            }else{
                description = feed["body"] as? String ?? ""
            }

            var myTagArray = [TagListData]()
            if let tagArray = feed["newfeed_tag_list"] as? NSArray{
                for tag in tagArray {
                    if let tagDict = (tag as? NSDictionary)?["tag_detail"] as? NSDictionary {
                        let id  = tagDict["tagId"] as? Int ?? 0
                        let title = tagDict["tagTitle"] as? String ?? ""
                        let data  = TagListData(name: title, tagId: id)
                        myTagArray.append(data)
                    }
                }
            }
            let location = feed["location"] as? String ?? ""
            let mediaType = MediaType(rawValue: (feed["mediaType"] as? Int ?? 3))
            var myMediaArray = [mediaPlayer]()
            if let mediaArray = feed["newfeed_media"] as? NSArray {
                for media in mediaArray {
                    if let mediaDict = media as? NSDictionary {
                        var url = mediaDict["mediaThumbnailUrl"] as? String
                        if url == ""{
                            url = mediaDict["mediaUrl"] as? String ?? ""
                            let data = mediaPlayer(imageUrl: url!, videoUrl: "")
                            myMediaArray.append(data)
                        }else{
                            url = mediaDict["mediaThumbnailUrl"] as? String ?? ""
                            let videoUrl = mediaDict["mediaUrl"] as? String ?? ""
                            let data = mediaPlayer(imageUrl: url!, videoUrl: videoUrl)
                            myMediaArray.append(data)
                        }
                        
                        
                    }
                }
            }
            let likes = feed["newfeed_like_list_count"] as? Int ?? 0
            let isLikes = feed["isLike"] as? Int ?? 0//TODO: Change Value
            let comments = feed["newfeed_comments_list_count"] as? Int ?? 0 //TODO: Change Value
            let type = NewsFeedType(rawValue: (feed["newsfeedType"] as? Int ?? 3))
            let profileImage = feed["userImage"] as? String ?? ""
            var myPollArray = [PollList]()
            if let pollArray = feed["newfeed_poll_list"] as? NSArray {
                for poll in pollArray {
                    if let pollDict = poll as? NSDictionary {
                        let title = pollDict["pollOption"] as? String ?? ""
                        let id = pollDict["pollId"] as? Int ?? 0
                        let count = pollDict["totalPolls"] as? Int ?? 0
                        let data = PollList(title: title, id: id, pollCount: count)
                        myPollArray.append(data)
                    }
                }
            }
            var commentsListArray = [Comments]()
            if let commentArray = feed["newfeed_comments_list"]as? NSArray{
                for comment in commentArray{
                    if let commentDict = comment as? NSDictionary{
                        let userId = commentDict["userId"] as? Int ?? 0
                        let commentStr = commentDict["commentText"] as? String ?? ""
                        let commentOwner = commentDict["commentOwnerDetail"]as? NSDictionary
                        let name = commentOwner?["name"] as? String ?? "N/A"
                        let commnetId = commentDict["commentId"] as? Int ?? 0
                        let ownerProfileUrl = commentOwner?["profileImageUrl"] as? String ?? ""
                        let userType = commentOwner?["userType"] as? String ?? ""
                        let userRole = commentOwner?["userRole"] as? Int ?? 1
                        let commentlikes = commentDict["totalLikeOnComment"] as? Int ?? 0
                        let isLikes = commentDict["isLike"] as? Int ?? 0
                        let commentReplies = commentDict["totalreplyOnComment"] as? Int ?? 0
                        let date = commentDict["milliseconds"] as?Double ?? 0.0
                        let myDate = Date(milliseconds: date)
                        let dateString = myDate.timeAgoSinceDate(date: myDate)
                     
                        let data  = Comments(name: name, time: dateString, likes: commentlikes, comments: commentReplies, type: .comment, commentId: commnetId, imageUrl: ownerProfileUrl,isLikes:isLikes,commentText: commentStr,userId:userId,userType:userType, userRole:userRole)
                        commentsListArray.append(data)
                        if let replyListArray = commentDict["replyOnCommentList"] as? NSArray{
                            
                            for reply in replyListArray {
                                if let replyDict = reply as? NSDictionary{
                                    let userId = replyDict["userId"]as? Int ?? 0
                                    let replyText = replyDict["replyText"]as? String ?? "N/A"
                                    let date = replyDict["milliseconds"] as?Double ?? 0.0
                                    let myDate = Date(milliseconds: date)
                                    let dateString = myDate.timeAgoSinceDate(date: myDate)
                                    let replyId = commentDict["replyId"] as? Int ?? 0
                                    let replyOwner = replyDict["replyOwnerDetail"]as? NSDictionary
                                    let name = replyOwner?["name"] as? String ?? "N/A"
                                    let ownerProfileUrl = replyOwner?["profileImageUrl"] as? String ?? ""
                                    let userType = replyOwner?["userType"] as? String ?? ""
                                    let userRole = replyOwner?["userRole"] as? Int ?? 1
                                    let data  = Comments(name: name, time: dateString, likes: nil, comments: nil, type: .reply, commentId: replyId, imageUrl: ownerProfileUrl,isLikes:nil,commentText:replyText,userId:userId,userType:userType, userRole:userRole)
                                    commentsListArray.append(data)
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            feedDetail = NewsFeedData(isPolled:isPoll,userId: user_id, name: name, time: dateString, title: title, Description: description, newsFeedTagList: myTagArray, location: location, mediaListData: myMediaArray, likes: likes, comments: comments, type: type, profilePic: profileImage, pollList: myPollArray, mediaType: mediaType,date: mainStartDate,eventId:eventId,commentsArray:commentsListArray,status:status,isLike:isLikes,jobName:jobName,companyName:companyName,skills:skills,jobType:jobType,isAllowComment:isAllowComment,isAllowLocation:isAllowLocation,link:link,rsvpStatus:rsvpStatus,userType:userType,userRole:userRole,startTime:startTime,endDate:endDate, endTime:endTime,numberofTicket:numberofTicket,startDateTime:startDate,endDateTime:end_Date, coordinates: coordinates)
            
         
        }
        
    }
    
    func parseNewsFeedListData(responseDict: JSONDictionary){
       // self.newsFeedArray.removeAll()
        
        if let feedDict = responseDict[APIKeys.kData] as? NSDictionary {
            if let feedArray = feedDict[APIKeys.kData] as? NSArray {
                for feed in feedArray {
                    
                    let isPoll = (feed as? NSDictionary)?["isPolled"] as? String ?? ""
                    let user_id = (feed as? NSDictionary)?["userId"] as? Int ?? 0
                    let isAllowComment = (feed as? NSDictionary)?["allowComment"] as? Int ?? 1
                    let isAllowLocation = (feed as? NSDictionary)?["shareLocation"] as? Int ?? 1
                    let numberofTicket = (feed as? NSDictionary)?["numberofTicket"] as? Int ?? 1
                    let rsvpStatus = (feed as? NSDictionary)?["rsvpStatus"] as? Int ?? 1
                    let link = (feed as? NSDictionary)?["link"] as? String ?? ""
                    let eventId = (feed as? NSDictionary)?["eventid"] as? Int ?? 0
                    let name = (feed as? NSDictionary)?["userName"] as? String ?? "N/A"
                    let userType = (feed as? NSDictionary)?["userType"] as? String ?? ""
                    let userRole = (feed as? NSDictionary)?["userRole"] as? Int ?? 1
                    let jobName = (feed as? NSDictionary)?["jobName"] as? String ?? "N/A"
                    let companyName = (feed as? NSDictionary)?["companyName"] as? String ?? "N/A"
                    let skills = (feed as? NSDictionary)?["skill"] as? String ?? "N/A"
                    let jobType = (feed as? NSDictionary)?["jobType"] as? String ?? "N/A"
                    var coordinates: CLLocationCoordinate2D!
                    if let latitude = (feed as? NSDictionary)?["eventLatitude"] as? Double {
                        coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: (feed as! NSDictionary)["eventLongitude"] as! Double)
                    }
                    let status = (feed as? NSDictionary)?["jobStatus"] as? Int ?? 0
                    let date =  (feed as? NSDictionary)?["postDateForMobile"] as? Double ?? 0.0
                    let myDate = Date(milliseconds: date)
                    let dateString = myDate.timeAgoSinceDate(date: myDate)
                    
                    let startDate =  (feed as? NSDictionary)?["eventStartDateMilisecond"] as? Double ?? 0.0
                    let mystartDate = Date(milliseconds: startDate)
                    let mainStartDate = mystartDate.stringFromDate(format: .longMDYDate, type: .local)
//                    let startTime = mystartDate.stringFromDate(format: .time, type: .local)
                    let startEventDate = (feed as? NSDictionary)?["dateTime"] as? String ?? ""
                    let startTime = Date.getTimeWithUTCZone(date: startEventDate)
                    
                    let end_Date =  (feed as? NSDictionary)?["eventEndDateMilisecond"] as? Double ?? 0.0
                    let myEndDate = Date(milliseconds: end_Date)
                    let endDate = myEndDate.stringFromDate(format: .longMDYDate, type: .local)
//                    let endTime = myEndDate.stringFromDate(format: .time, type: .local)
                    let endEventDate = (feed as? NSDictionary)?["eventEndDate"] as? String ?? ""
                    let endTime = Date.getTimeWithUTCZone(date: endEventDate)
                    
                    let title = (feed as? NSDictionary)?["title"] as? String ?? ""
                    var description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    if description == "" {
                        description = (feed as? NSDictionary)?["description"] as? String ?? ""
                    }else{
                        description = (feed as? NSDictionary)?["body"] as? String ?? ""
                    }
                     var myTagArray = [TagListData]()
                    if let tagArray = (feed as? NSDictionary)?["newfeed_tag_list"] as? NSArray{
                        for tag in tagArray {
                            if let tagDict = (tag as? NSDictionary)?["tag_detail"] as? NSDictionary {
                                let id  = tagDict["tagId"] as? Int ?? 0
                                let title = tagDict["tagTitle"] as? String ?? ""
                                let data  = TagListData(name: title, tagId: id)
                                myTagArray.append(data)
                            }
                        }
                    }
                    let location = (feed as? NSDictionary)?["location"] as? String ?? ""
                    let mediaType = MediaType(rawValue: ((feed as? NSDictionary)?["mediaType"] as? Int ?? 3))
                    var myMediaArray = [mediaPlayer]()
                    if let mediaArray = (feed as? NSDictionary)?["newfeed_media"] as? NSArray {
                        for media in mediaArray {
                            if let mediaDict = media as? NSDictionary {
                                var url = mediaDict["mediaThumbnailUrl"] as? String
                                if url == ""{
                                    url = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: "")
                                     myMediaArray.append(data)
                                }else{
                                    url = mediaDict["mediaThumbnailUrl"] as? String ?? ""
                                    let videoUrl = mediaDict["mediaUrl"] as? String ?? ""
                                    let data = mediaPlayer(imageUrl: url!, videoUrl: videoUrl)
                                     myMediaArray.append(data)
                                }
                               
                            }
                        }
                    }
                    let likes = (feed as? NSDictionary)?["newfeed_like_list_count"] as? Int ?? 0
                    let isLikes = (feed as? NSDictionary)?["isLike"] as? Int ?? 0//TODO: Change Value
                    let comments = (feed as? NSDictionary)?["newfeed_comments_list_count"] as? Int ?? 0 //TODO: Change Value
                    let type = NewsFeedType(rawValue: ((feed as? NSDictionary)?["newsfeedType"] as? Int ?? 3))
                    let profileImage = (feed as? NSDictionary)?["userImage"] as? String ?? ""
                    var myPollArray = [PollList]()
                    if let pollArray = (feed as? NSDictionary)?["newfeed_poll_list"] as? NSArray {
                        for poll in pollArray {
                            if let pollDict = poll as? NSDictionary {
                                let title = pollDict["pollOption"] as? String ?? ""
                                let id = pollDict["pollId"] as? Int ?? 0
                                let count = pollDict["totalPolls"] as? Int ?? 0
                                let data = PollList(title: title, id: id, pollCount: count)
                                myPollArray.append(data)
                            }
                        }
                    }
                    let data = NewsFeedData(isPolled: isPoll, userId: user_id, name: name, time: dateString, title: title, Description: description, newsFeedTagList: myTagArray, location: location, mediaListData: myMediaArray, likes: likes, comments: comments, type: type, profilePic: profileImage, pollList: myPollArray, mediaType: mediaType,date: mainStartDate,eventId:eventId, commentsArray:nil,status:status,isLike:isLikes,jobName:jobName,companyName:companyName,skills:skills,jobType:jobType,isAllowComment:isAllowComment,isAllowLocation:isAllowLocation,link:link,rsvpStatus:rsvpStatus,userType:userType,userRole:userRole, startTime:startTime,endDate:endDate,endTime:endTime,numberofTicket:numberofTicket,startDateTime:startDate,endDateTime:end_Date, coordinates: coordinates)
                    newsFeedArray.append(data)
                }
            }
        }
    }
}
