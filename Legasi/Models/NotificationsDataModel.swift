//
//  NotificationsDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

struct NotificationsData {
    var otherUserId: Int!
    var notificationType:String!
    var message: String!
    var OtherUserName:String!
    var OtherUserProfileImage: String?
    var miliseconds:String!
    var otherId:Int!
    var groupName: String?
    var isAdmin:String?
    var notificationId:Int?
}

extension NotificationsVM {
    func parseNotificationsData(responseDict: JSONDictionary){
        notificationsArray.removeAll()
        if let notificationArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in notificationArray {
                
                let friendId = (dict as? NSDictionary)?["otherUserId"] as? Int ?? 0
                let notificationId = (dict as? NSDictionary)?["notificationId"] as? Int ?? 0
                let notificationType = (dict as? NSDictionary)?["notificationType"] as? String ?? "N/A"
                let message = (dict as? NSDictionary)?["message"] as? String ?? "N/A"
                let friendName = (dict as? NSDictionary)?["OtherUserName"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["OtherUserProfileImage"] as? String ?? ""
                let date =  (dict as? NSDictionary)?["milliseconds"] as? Double ?? 0.0
                let myDate = Date(milliseconds: date)
                let dateString = myDate.timeAgoSinceDate(date: myDate)
                let otherId = (dict as? NSDictionary)?["otherId"] as? Int ?? 0
                let groupName = (dict as? NSDictionary)?["groupName"] as? String ?? ""
                let isAdmin = (dict as? NSDictionary)?["isAdmin"] as? String ?? ""
                let data = NotificationsData(otherUserId: friendId, notificationType: notificationType, message: message, OtherUserName: friendName, OtherUserProfileImage: profileImageUrl, miliseconds: dateString, otherId: otherId, groupName: groupName,isAdmin:isAdmin,notificationId:notificationId)
                notificationsArray.append(data)
            }
        }
}
}
