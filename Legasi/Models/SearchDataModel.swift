//
//  SearchDataModel.swift
//  Legasi
//
//  Created by Gaurav on 23/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

struct locationBaseUser {
    var userId:Int!
    var name: String!
    var email: String!
    var role:Int!
    var userName: String!
    var schoolId: Int!
    var profileImage: String!
    var profileImageUrl: String!
    var coverImage: String!
    var coverImageUrl: String!
    var countryCode: String!
    var city: String!
    var phoneNumber: Int!
    var country: String!
    var state: String!
    var zipCode: String!
    var addressLine1: String!
    var addressLine2: String!
    var userType: String!
    var dob: String!
    var graduationYear: String!
    var latitude: Double!
    var longitude: Double!
    var students: Int!
    var graduates: Int!
    var distance: Double!
}

extension SearchVM {
    func parseSearchLocationData(responseDict: JSONDictionary){
        locationBaseUserArray.removeAll()
        if let searchUserArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in searchUserArray {

                let user_id = (dict as? NSDictionary)?["userId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["name"] as? String ?? "N/A"
                let email = (dict as? NSDictionary)?["email"] as? String ?? "N/A"
                let role = (dict as? NSDictionary)?["role"] as? Int ?? 0
                let userName = (dict as? NSDictionary)?["userName"] as? String ?? "N/A"
                let schoolID = (dict as? NSDictionary)?["schoolId"] as? Int ?? 0
                let profileImage = (dict as? NSDictionary)?["profileImage"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["profileImageUrl"] as? String ?? "N/A"
                let coverImage = (dict as? NSDictionary)?["coverImage"] as? String ?? "N/A"
                let coverImageUrl = (dict as? NSDictionary)?["coverImageUrl"] as? String ?? "N/A"
                let countryCode = (dict as? NSDictionary)?["countryCode"] as? String ?? "N/A"
                let city = (dict as? NSDictionary)?["city"] as? String ?? "N/A"
                let phoneNumber = (dict as? NSDictionary)?["phoneNumber"] as? Int ?? 0
                let country = (dict as? NSDictionary)?["country"] as? String ?? "N/A"
                let state = (dict as? NSDictionary)?["state"] as? String ?? "N/A"
                let zipCode = (dict as? NSDictionary)?["zipcode"] as? String ?? "N/A"
                let addressLine1 = (dict as? NSDictionary)?["addressLine1"] as? String ?? "N/A"
                let addressLine2 = (dict as? NSDictionary)?["addressLine2"] as? String ?? "N/A"
                let userType = (dict as? NSDictionary)?["userType"] as? String ?? "N/A"
                let dob = (dict as? NSDictionary)?["dob"] as? String ?? "N/A"
                let graduationYear = (dict as? NSDictionary)?["graduationYear"] as? String ?? "N/A"
                let latitude =  (dict as? NSDictionary)?["latitude"] as? Double ?? 0.0
                let longitude =  (dict as? NSDictionary)?["longitude"] as? Double ?? 0.0
                let student = (dict as? NSDictionary)?["student"] as? Int ?? 0
                let graduates = (dict as? NSDictionary)?["graduates"] as? Int ?? 0
                let distance = (dict as? NSDictionary)?["distance"] as? Double ?? 0.0
                
                let data = locationBaseUser(userId: user_id, name: name, email: email, role: role, userName: userName, schoolId: schoolID, profileImage: profileImage, profileImageUrl: profileImageUrl, coverImage: coverImage, coverImageUrl: coverImageUrl, countryCode: countryCode, city: city, phoneNumber: phoneNumber, country: country, state: state, zipCode: zipCode, addressLine1: addressLine1, addressLine2: addressLine2, userType: userType, dob: dob, graduationYear: graduationYear, latitude: latitude, longitude: longitude, students: student, graduates: graduates, distance: distance)
                locationBaseUserArray.append(data)
                }
        }
    }
}
