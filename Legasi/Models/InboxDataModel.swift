//
//  InboxDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 20/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation


enum MsgType: Int {
    case nothing = 1
    case image = 2
    case video = 3
}
struct userInboxData {
    var chatListId: Int!
    var sendFrom:Int!
    var sendTo: Int!
    var msg: String!
    var msgType: MsgType!
    var miliseconds:String!
    var image:String?
    var profileImage:String!
    var name:String!
    var chatMsgListId:Int!
    var mediaListData: [mediaPlayer]!
}
struct blockListData{
    
    var name:String!
    var userId:Int!
    var profileImage:String!
    var blockStatus:Int!
}

extension InboxVM {
    func parseInboxListData(responseDict: JSONDictionary) {
        self.inboxArray.removeAll()
        if let inboxChatArray = responseDict[APIKeys.kData] as? NSArray {
            for inboxList in inboxChatArray {
                var name:String!
                var profileImage:String!
                if let userData = (inboxList as? NSDictionary)?["otherUserDetail"] as? NSDictionary {
                    name = userData["name"] as? String ?? ""
                    profileImage  = userData["profileImageUrl"] as? String ?? ""
                }
                let chatListId = (inboxList as? NSDictionary)? [APIKeys.kChatListId] as? Int ?? 0
                let sendFrom = (inboxList as? NSDictionary)? ["sendFrom"] as? Int ?? 0
                let sendTo = (inboxList as? NSDictionary)? ["sendTo"] as? Int ?? 0
                let msg = (inboxList as? NSDictionary)? ["msg"] as? String ?? ""
                let msgType = MsgType(rawValue: ((inboxList as? NSDictionary)?["msgType"] as? Int ?? 1))
                let date =  (inboxList as? NSDictionary)?["milliseconds"] as? Double ?? 0.0
                let myDate = Date(milliseconds: date)
                let dateString = myDate.timeAgoSinceDate(date: myDate)
                var myMediaArray = [mediaPlayer]()
                var url = (inboxList as? NSDictionary)? ["videoUrl"] as? String ?? ""
                if url == ""{
                    url = (inboxList as? NSDictionary)? ["imageUrl"] as? String ?? ""
                    let data = mediaPlayer(imageUrl: url, videoUrl: "")
                    myMediaArray.append(data)
                }else{
                    url = (inboxList as? NSDictionary)? ["imageUrl"] as? String ?? ""
                    let videoUrl = (inboxList as? NSDictionary)? ["videoUrl"] as? String ?? ""
                    let data = mediaPlayer(imageUrl: url, videoUrl: videoUrl)
                    myMediaArray.append(data)
                }
                let chatMsgListId = (inboxList as? NSDictionary)?["chatMsgListId"] as? Int ?? 0
                // call struct of countryData
                
                let data = userInboxData(chatListId: chatListId, sendFrom: sendFrom, sendTo: sendTo, msg: msg, msgType: msgType, miliseconds: dateString, image: url, profileImage: profileImage, name: name,chatMsgListId:chatMsgListId,mediaListData:myMediaArray)
                self.inboxArray.append(data)
            }
        }
    }
    
    
    func parseChatListData(responseDict: JSONDictionary) {
        self.chatListArray.removeAll()
        
        let data = responseDict[APIKeys.kData] as? NSDictionary
        if let ChatArray = data?["messageDetail"] as? NSArray {
            for chatList in ChatArray {
                let chatListId = (chatList as? NSDictionary)? [APIKeys.kChatListId] as? Int ?? 0
                let sendFrom = (chatList as? NSDictionary)? ["sendFrom"] as? Int ?? 0
                let sendTo = (chatList as? NSDictionary)? ["sendTo"] as? Int ?? 0
                let msg = (chatList as? NSDictionary)? ["msg"] as? String ?? ""
                let msgType = MsgType(rawValue: ((chatList as? NSDictionary)?["msgType"] as? Int ?? 1))
                let date =  (chatList as? NSDictionary)?["milliseconds"] as? Double ?? 0.0
                let myDate = Date(milliseconds: date)
                let dateString = myDate.timeAgoSinceDate(date: myDate)
                var myMediaArray = [mediaPlayer]()
                var url = (chatList as? NSDictionary)? ["videoUrl"] as? String ?? ""
                if url == ""{
                    url = (chatList as? NSDictionary)? ["imageUrl"] as? String ?? ""
                    let data = mediaPlayer(imageUrl: url, videoUrl: "")
                    myMediaArray.append(data)
                }else{
                    url = (chatList as? NSDictionary)? ["imageUrl"] as? String ?? ""
                    let videoUrl = (chatList as? NSDictionary)? ["videoUrl"] as? String ?? ""
                    let data = mediaPlayer(imageUrl: url, videoUrl: videoUrl)
                    myMediaArray.append(data)
                }
                let chatMsgListId = (chatList as? NSDictionary)? ["chatMsgListId"] as? Int ?? 0

                // call struct of countryData
                
                let data = userInboxData(chatListId: chatListId, sendFrom: sendFrom, sendTo: sendTo, msg: msg, msgType: msgType, miliseconds: dateString, image: url, profileImage: "", name: "",chatMsgListId:chatMsgListId,mediaListData:myMediaArray)
                self.chatListArray.append(data)
            }
        }
    }
    func parseFriendListData(responseDict: JSONDictionary){
        friendListArray.removeAll()
        if let membersListArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in membersListArray {
                
                let user_id = (dict as? NSDictionary)?["userId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["name"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["profileImageUrl"] as? String ?? ""
                let fullName = (dict as? NSDictionary)?["userName"] as? String ?? "N/A"
                
                let data = MemberDetail(memberId: user_id, name: name, userImageUrl: profileImageUrl, memberFullName: fullName)
                friendListArray.append(data)
            }
        }
    }
    func parseBlockListData(responseDict: JSONDictionary){
        blockListArray.removeAll()
        if let blockMemberArray = responseDict[APIKeys.kData] as? NSArray {
            for dict in blockMemberArray {
                
                let user_id = (dict as? NSDictionary)?["otherUserId"] as? Int ?? 0
                let name = (dict as? NSDictionary)?["name"] as? String ?? "N/A"
                let profileImageUrl = (dict as? NSDictionary)?["profileImageUrl"] as? String ?? ""
                let status = (dict as? NSDictionary)?["blockstatus"] as? Int ?? 0
                let data = blockListData(name: name, userId: user_id, profileImage: profileImageUrl, blockStatus: status)
                blockListArray.append(data)
            }
        }
    }
}
