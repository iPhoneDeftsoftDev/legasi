//
//  JobsDataModel.swift
//  Legasi
//
//  Created by MiniMAC22 on 04/01/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct  JobsListData {
    var name: String?
    var jobStatus: Int!
    var profileImageUrl: String!
    var title: String?
    var companyName:String!
    var skills: String!
    var jobType: String!
    var description:String!
    var location: String!
    var eventId:Int!
    var newsFeedType:NewsFeedType!
    var likes:Int!
    var comments:Int!
    var isLike:Int!
    var date: String!
    var userId:Int!
    var lat:Double!
    var long:Double!
    var userType:String!
    var userRole:Int!
}


extension AddJobPostVM {
    
    func parseJobListData(responseDict: JSONDictionary){
        // self.jobListArray.removeAll()
            let data = responseDict[APIKeys.kData] as? NSDictionary
            if let feedArray = data?[APIKeys.kData] as? NSArray {
                for feed in feedArray {
                    
                    let name = (feed as? NSDictionary)?["userName"] as? String ?? "N/A"
                    let jobName = (feed as? NSDictionary)?["jobName"] as? String ?? "N/A"
                    let companyName = (feed as? NSDictionary)?["companyName"] as? String ?? "N/A"
                    let skills = (feed as? NSDictionary)?["skill"] as? String ?? "N/A"
                    let jobType = (feed as? NSDictionary)?["jobType"] as? String ?? "N/A"
                    let status = (feed as? NSDictionary)?["jobStatus"] as? Int ?? 0
                    let userType = (feed as? NSDictionary)?["userType"] as? String ?? ""
                    let userRole = (feed as? NSDictionary)?["userRole"] as? Int ?? 1
                    let date =  (feed as? NSDictionary)?["postDateForMobile"] as? Double ?? 0.0
                    let myDate = Date(milliseconds: date)
                    let dateString = myDate.timeAgoSinceDate(date: myDate)
                    let description = (feed as? NSDictionary)?["description"] as? String ?? ""
                    let location = (feed as? NSDictionary)?["location"] as? String ?? ""
                    let likes = (feed as? NSDictionary)?["newfeed_like_list_count"] as? Int ?? 0
                    let isLikes = (feed as? NSDictionary)?["isLike"] as? Int ?? 0//TODO: Change Value
                    let comments = (feed as? NSDictionary)?["newfeed_comments_list_count"] as? Int ?? 0 //TODO: Change Value
                    let type = NewsFeedType(rawValue: ((feed as? NSDictionary)?["newsfeedType"] as? Int ?? 2))
                    let profileImage = (feed as? NSDictionary)?["userImage"] as? String ?? ""
                    let eventId = (feed as? NSDictionary)?["eventid"] as? Int ?? 0
                    let userId = (feed as? NSDictionary)?["userId"] as? Int ?? 0
                    let lat  = (feed as? NSDictionary)?["eventLatitude"] as? Double ?? 0.0
                    let long  = (feed as? NSDictionary)?["eventLongitude"] as? Double ?? 0.0
                    let data = JobsListData(name: name, jobStatus: status, profileImageUrl: profileImage, title: jobName, companyName: companyName, skills: skills, jobType: jobType, description: description, location: location, eventId: eventId, newsFeedType: type, likes: likes, comments: comments, isLike:isLikes,date: dateString, userId:userId,lat:lat,long:long,userType:userType,userRole:userRole)
                    jobListArray.append(data)
                }
            
        }
    }
    
    func parseEachJobData(responseDict: JSONDictionary){
       
        if let feed = responseDict[APIKeys.kData] as? NSDictionary {
            
            let name = feed["userName"] as? String ?? "N/A"
            let jobName = feed["jobName"] as? String ?? "N/A"
            let companyName = feed["companyName"] as? String ?? "N/A"
            let skills = feed["skill"] as? String ?? "N/A"
            let jobType = feed["jobType"] as? String ?? "N/A"
            let status = feed["jobStatus"] as? Int ?? 0
            let date = feed["postDateForMobile"] as? Double ?? 0.0
            let myDate = Date(milliseconds: date)
            let dateString = myDate.timeAgoSinceDate(date: myDate)
            let description = feed["description"] as? String ?? ""
            let location = feed["location"] as? String ?? ""
            let likes = feed["newfeed_like_list_count"] as? Int ?? 0
            let isLikes = feed["isLike"] as? Int ?? 0//TODO: Change Value
            let comments = feed["newfeed_comments_list_count"] as? Int ?? 0 //TODO: Change Value
            let type = NewsFeedType(rawValue: (feed["newsfeedType"] as? Int ?? 2))
            let profileImage = feed["userImage"] as? String ?? ""
            let eventId = feed["eventid"] as? Int ?? 0
            let userId = feed["userId"] as? Int ?? 0
            let lat  = feed["eventLatitude"] as? Double ?? 0.0
            let long  = feed["eventLongitude"] as? Double ?? 0.0
            jobDetail = JobsListData(name: name, jobStatus: status, profileImageUrl: profileImage, title: jobName, companyName: companyName, skills: skills, jobType: jobType, description: description, location: location, eventId: eventId, newsFeedType: type, likes: likes, comments: comments, isLike:isLikes,date: dateString,userId:userId,lat:lat,long:long,userType:"",userRole:0)
        }
        
    }
    

    
    
    
}
