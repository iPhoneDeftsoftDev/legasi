//
//  ConnectionsVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

public class ConnectionsVM {
   
    var listArray = NSArray()
    var infoDict: NSDictionary!
    var followerUserArray = [followerUserData]()
    var userGroupListArray = [userGroupListData]()
    var groupDetailArray = [NewsFeedData]()
    var memberListArray = [MemberDetail]()
    var groupDetailInfo = groupDetailData()
    public static let sharedInstance = ConnectionsVM()
    private init() {}
    
    func userFollowersList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.userFollowersList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseFollowerUserData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func userFollowingList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.UserFollowingList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseFollowerUserData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func userGroupList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.UserGroupList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseUserGroupData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func eachGroupNewfeedList(userId: Int,groupId:Int,page:Int, response: @escaping responseCallBack) {
        
        APIManager.EachGroupNewfeedList(userId: userId,groupId:groupId, page: page, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.groupDetailArray = [NewsFeedData]()
                }
                self.parseGroupFeedListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
   
    func AddEditGroupMethod(userId:Int,actionType:Int,schoolId:Int,groupName:String,groupDescription:String,visibleType:String,location:Int,groupLat:Double, groupLong:Double,groupId:Int,image:[String:Data], response: @escaping responseCallBack) {
        APIManager.AddEditGroupMethod(userId:userId,actionType:actionType,schoolId:schoolId,groupName:groupName,groupDescription:groupDescription,visibleType:visibleType,location:location,groupLat:groupLat, groupLong:groupLong,groupId:groupId,image:image,  successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")

        }
    }
    
    func AddMemberInGroup(userId:Int,memberId:String,groupId:Int, response: @escaping responseCallBack) {
        
        APIManager.AddMemberInGroup(userId: userId,memberId:memberId, groupId: groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func MemberList(userId:Int,schoolId:Int, response: @escaping responseCallBack) {
       APIManager.MemberList(userId: userId,schoolId:schoolId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseMemberListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func DeleteGroup(userId:Int,groupId:Int, response: @escaping responseCallBack) {
        APIManager.DeleteGroup(userId: userId,groupId:groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func JoinUserGroup(userId:Int,groupId:Int, response: @escaping responseCallBack) {
        APIManager.JoinUserGroup(userId: userId,groupId:groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func LeaveGroupMethod(userId:Int,groupId:Int, response: @escaping responseCallBack) {
        APIManager.LeaveGroupMethod(userId: userId,groupId:groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func GetEachGroupDetail(userId:Int,groupId:Int, response: @escaping responseCallBack) {
        APIManager.GetEachGroupDetail(userId: userId,groupId:groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseEachGroupData(responseDict: responseDict!)
                
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func MakeGroupAdmin(userId:Int,groupId:Int,otherUserId:Int, response: @escaping responseCallBack) {
        APIManager.MakeGroupAdmin(userId: userId,groupId:groupId,otherUserId:otherUserId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func RemoveGroupUser(userId:Int,groupId:Int,otherUserId:Int, response: @escaping responseCallBack) {
        APIManager.RemoveGroupUser(userId: userId,groupId:groupId,otherUserId:otherUserId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
 
}
