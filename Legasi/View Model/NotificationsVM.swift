//
//  NotificationsVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

public class NotificationsVM {
    
    
    var notificationsArray = [NotificationsData]()
    var infoDict: NSDictionary!
    public static let sharedInstance = NotificationsVM()
    private init() {}
    
    func NotificationList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.NotificationList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseNotificationsData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
      
    func AcceptJoinRequest(userId:Int, notificationId:Int, value:Int, response: @escaping responseCallBack) {
        
        APIManager.AcceptJoinRequest(userId: userId,notificationId:notificationId, value:value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func NotificationCount(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.NotificationCount(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.infoDict = responseDict?[APIKeys.kData] as!NSDictionary
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
   
}
