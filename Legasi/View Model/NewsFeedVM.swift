//
//  NewsFeedVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 17/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

public class NewsFeedVM {
    
    var otp:Int!
    var listArray = NSArray()
    var infoDict: NSDictionary!
    var tagListArray = [TagListData]()
    var newsFeedArray = [NewsFeedData]()
    var feedDetail = NewsFeedData()
    var geoFacingArray = [geoFancingData]()
    public static let sharedInstance = NewsFeedVM()
    private init() {}
    
    func  newsFeedDetail(userId: Int, filterApply: Int,typeOfPost:String,miles:String, lat:Double,  long:Double, ids:String,page:Int, response: @escaping responseCallBack) {
        APIManager.newsFeedDetail(userId: userId, filterApply: filterApply,typeOfPost: typeOfPost,miles: miles, lat: lat, long:long,ids:ids,page: page, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.newsFeedArray = [NewsFeedData]()
                }
                self.parseNewsFeedListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func getTagList(userId: Int, response: @escaping responseCallBack) {
        APIManager.getTagList(userId: userId, successCallback: { (responseDict) in
          
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
               self.parseTagListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
   
     func getEachNewfeed(userId: Int, newfeedId:Int, response: @escaping responseCallBack) {
        APIManager.getEachNewfeed(userId: userId,newfeedId:newfeedId, successCallback: { (responseDict) in
            
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseParticularNewsFeedData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func voteOnPoll(userId: Int, eventId:Int,pollId:Int, response: @escaping responseCallBack) {
        APIManager.voteOnPoll(userId: userId,eventId:eventId,pollId:pollId, successCallback: { (responseDict) in
            
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseParticularNewsFeedData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func CommentNewFeed(userId: Int, newfeedId:Int,comment:String, response: @escaping responseCallBack) {
        APIManager.CommentNewFeed(userId: userId,newfeedId:newfeedId,comment:comment, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func LikeNewfeed(userId:Int, eventId:Int, newfeedType:Int, value:Int, response: @escaping responseCallBack) {
        APIManager.LikeNewfeed(userId: userId, eventId: eventId, newfeedType: newfeedType, value: value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func LikeOnComment(userId:Int,commentId:Int, eventId:Int, value:Int, response: @escaping responseCallBack) {
        APIManager.LikeOnComment(userId: userId, commentId:commentId, eventId: eventId, value: value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func  AddReplyService(userId: Int, commentId: Int, eventId: Int, text: String, response: @escaping responseCallBack) {
        APIManager.AddReplyService(userId: userId, commentId:commentId, eventId: eventId, text: text, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
   
    
    func SubmitRSVPRequest(userId: Int, eventId: Int, value: Int, response: @escaping responseCallBack) {
        APIManager.SubmitRSVPRequest(userId: userId, eventId: eventId, value: value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func GetOwnNewsFeed(userId: Int,page:Int, response: @escaping responseCallBack) {
        APIManager.GetOwnNewsFeed(userId: userId, page: page, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.newsFeedArray = [NewsFeedData]()
                }
                self.parseNewsFeedListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }

    func GeoFancingMethod(userId: Int,lat:Double, long:Double, response: @escaping responseCallBack) {
        APIManager.GeoFancingMethod(userId: userId, lat: lat, long: long, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseGeoFancingData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
}
