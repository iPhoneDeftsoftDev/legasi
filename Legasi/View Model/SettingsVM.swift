//
//  SettingsVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 21/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
public class SettingsVM {
    
    var profileData = ProfileDetailData()
    public static let sharedInstance = SettingsVM()
    private init() {}
    
    func NotificationSettings(userId: Int, value:Int, notificationType:Int, response: @escaping responseCallBack) {
        
        APIManager.NotificationSettings(userId: userId,value:value,notificationType:notificationType, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func PrivacySetting(userId: Int, shareMylocation: Int, student: Int, graduates:Int, school: Int, connectedUsers: Int, postSeen: Int, response: @escaping responseCallBack) {
        
        APIManager.PrivacySetting(userId: userId, shareMylocation: shareMylocation, student: student, graduates:graduates, school: school, connectedUsers: connectedUsers, postSeen: postSeen, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func HelpQueryMethod(userId:Int, typeOfProblem: String,subject: String, description: String, media: [String:Data], response: @escaping responseCallBack) {
        APIManager.HelpQueryMethod(userId: userId, typeOfProblem: typeOfProblem, subject: subject, description: description, media: media,  successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
           
        }
    }
    func GetOwnProfile(userId: Int, response: @escaping responseCallBack) {
        APIManager.GetOwnProfile(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseUserProfileData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func UpdateAddressDetail(userId: Int,addressLine1:String,latitude:Double,longitude:Double, response: @escaping responseCallBack) {
        APIManager.UpdateAddressDetail(userId: userId,addressLine1:addressLine1,latitude:latitude,longitude:longitude, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func UpdateSchoolDetail(userId:Int ,schoolId:Int, userType:String ,dob:String,graduationYear:String, response: @escaping responseCallBack) {
        APIManager.UpdateSchoolDetail(userId: userId, schoolId:schoolId, userType:userType, dob:dob,graduationYear:graduationYear, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
    func UpdateEducationDetail(userId:Int ,educationDetail:String, response: @escaping responseCallBack) {
        APIManager.UpdateEducationDetail(userId: userId, educationDetail:educationDetail, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func UpdateWorkDetail(userId:Int ,workDetail:String, response: @escaping responseCallBack) {
        APIManager.UpdateWorkDetail(userId: userId, workDetail:workDetail, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func LinkAccount(userId:Int ,accountTypeFB:String,accountTypeLink:String, response: @escaping responseCallBack) {
        APIManager.LinkAccount(userId: userId, accountTypeFB:accountTypeFB,accountTypeLink
            :accountTypeLink, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func UpdateProfileMethod(userId:Int, fullName:String, userName:String, countryCode:String, phoneNumber:String, email:String,image:[String:Data], response: @escaping responseCallBack) {
        APIManager.UpdateProfileMethod(userId:userId,fullName:fullName,userName:userName,countryCode:countryCode,phoneNumber:phoneNumber,email:email,image:image,  successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
            
        }
    }
   
    func  SkillsUpdate(userId:Int ,skills:String, response: @escaping responseCallBack) {
        
        APIManager.SkillsUpdate(userId: userId, skills:skills, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func  changePassword(userId:Int ,oldPassword:String, newPassword: String, response: @escaping responseCallBack) {
        
        APIManager.ChangePassword(userId: userId, oldPassword: oldPassword, newPassword: newPassword, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }

}
