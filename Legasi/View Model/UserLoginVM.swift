//
//  UserLoginVM.swift
//  Stash
//
//  Created by 123 on 26/10/16.
//  Copyright © 2016 developer. All rights reserved.
//
import Foundation


public class UserLoginVM {
    
    var otp:Int!
    var listArray = NSArray()
    var infoDict: NSDictionary!
    var isVehicle:String!
    var schoolListArray = [schoolListData]()
    //other user profile
    
    var userProfileData: OtherUserProfileData!
    var otherUserPostDataArray = [NewsFeedData]()
    public static let sharedInstance = UserLoginVM()
    private init() {}
    
    func loginByEmail(email: String, password: String,lat:Double,long:Double,device_type:String, device_token: String, response: @escaping responseCallBack) {
        APIManager.loginBYEmail(email: email, password: password, lat: lat,long:long,device_type:device_type,device_token: device_token, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.infoDict = responseDict![APIKeys.kData]as!NSDictionary
                DataManager.user_id = self.infoDict[kUser_id]as?Int
                DataManager.user_image = self.infoDict["profileImageUrl"]as? String
                DataManager.school_logo = (self.infoDict["school_detail"]as? NSDictionary)?["smallLogoIconUrl"]as? String
                DataManager.schoolId = (self.infoDict["school_detail"]as? NSDictionary)?[kId]as? Int
                DataManager.likeIcon_url = (self.infoDict["school_detail"]as? NSDictionary)?["likeIconUrl"]as? String
                DataManager.unlikeIcon_url = (self.infoDict["school_detail"]as? NSDictionary)?["unlikeIconUrl"]as? String
                DataManager.user_name = self.infoDict["name"]as? String
                DataManager.role = self.infoDict["role"]as? Int
                DataManager.graduationYear = self.infoDict["graduationYear"]as? String
                DataManager.userType = self.infoDict["userType"]as? String
                DataManager.totalLikes = self.infoDict["totalLikes"]as? Int ?? 0
                let name = (self.infoDict["school_detail"]as? NSDictionary)?["nameforGraduates"]as? String
                DataManager.nameForGraduate = name!.isEmpty ? "Graduate": name
                let schoolName = (self.infoDict["school_detail"]as? NSDictionary)?["schoolName"]as? String
                DataManager.schoolName = schoolName
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func signUp(fullName: String, username: String, profileImage: [String:Data], countryCode: String, phoneNumber: String, email: String, password: String, addressLine1: String, latitude: Double, longitude: Double, schoolId: String, userType: String, dob: String, linkedInId: String, facebookId: String, deviceType: String, deviceToken: String, graduationYear: String,educationDetails:String, workDetails:String,skills:String, response: @escaping responseCallBack) {
        APIManager.signUp(fullName: fullName, username: username, profileImage: profileImage, countryCode: countryCode, phoneNumber: phoneNumber, email: email, password: password, addressLine1: addressLine1, latitude: latitude, longitude: longitude, schoolId: schoolId, userType: userType, dob: dob, linkedInId: linkedInId, facebookId: facebookId, deviceType: deviceType, deviceToken: deviceToken, graduationYear: graduationYear,educationDetails:educationDetails,workDetails:workDetails,skills:skills, successCallback: { (responseDict) in
            
            debugPrint(responseDict)
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason)
        }
    }
    
    //Get Year Array
    func getYearList() -> [Int] {
        
        let date = Date()
        let calendar = Calendar.current
        var year = calendar.component(.year, from: date)
        let from = year - 100
        year = year + 6
        var yearArray = [Int]()
        
        for value in from..<year {
            yearArray.append(value)
        }
        return yearArray
    }
    
    func forgotPassword(email: String, response: @escaping responseCallBack) {
        APIManager.forgotPassword(email: email, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func checkUserAccountExist(email: String,userName:String, response: @escaping responseCallBack) {
        APIManager.CheckUserAccountExist(email: email,userName:userName, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func schoolList(response: @escaping responseCallBack) {
        APIManager.schoolList(successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseSchoolListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
            response(1, responseDict![APIKeys.kMessage] as? String, nil)
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func UpdateLatLong(userId: Int,lat:Double, long:Double, response: @escaping responseCallBack) {
        APIManager.UpdateLatLong(userId: userId, lat: lat, long: long, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    //Other user profile
    func getOtherUserProfile(otherUserId: Int, response: @escaping responseCallBack){
        APIManager.otherUserProfile(userId: DataManager.user_id!, otherUserId: otherUserId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseOtherUserProfileData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func otherUserPost(otherUserId:Int,page:Int, response: @escaping responseCallBack) {
        APIManager.otherUserPostData(userId: DataManager.user_id!, otherUserId: otherUserId, page:page, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.otherUserPostDataArray = [NewsFeedData]()
                }
                self.parseOtherUserPostData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
    func followUnfollowUser(userId:Int,friendId: Int,value: Int, response: @escaping responseCallBack) {
        APIManager.followUnfollowUser(userId: userId, friendId: friendId, value: value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func Logout(userId:Int, response: @escaping responseCallBack) {
        APIManager.Logout(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
            
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
}
