//
//  InboxVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 20/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

public class InboxVM {
    
    var chatListId:Int!
    var inboxArray = [userInboxData]()
    var chatListArray = [userInboxData]()
    var friendListArray = [MemberDetail]()
    var blockListArray = [blockListData]()
    public static let sharedInstance = InboxVM()
    private init() {}
    
    func ChatInboxList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.ChatInboxList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseInboxListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func EachChatDetail(userId: Int,chatListId:Int, response: @escaping responseCallBack) {
        
        APIManager.EachChatDetail(userId: userId, chatListId: chatListId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseChatListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func GetBlockList(userId: Int, response: @escaping responseCallBack) {
        
        APIManager.GetBlockList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseBlockListData(responseDict:responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func BlockUnblockUser(userId:Int, otherUserId:Int, value:Int, response: @escaping responseCallBack) {
        
        APIManager.BlockUnblockUser(userId: userId, otherUserId: otherUserId, value: value, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func SendMsgMethod(userId:Int, chatListId:Int, text: String,friendId:Int, msgType: String,mediaName: String?,vedioArray:[Data]?, thumbNailName: String?, mediaArray: [Data]?, mimeType: MimeType, response: @escaping responseCallBack) {
        
        APIManager.SendMsgMethod(userId: userId, chatListId: chatListId, text: text,friendId:friendId,vedioArray: vedioArray, thumbNailName: thumbNailName,msgType: msgType, mediaArray: mediaArray, mediaName: mediaName, mimeType: mimeType, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                let dict = responseDict![APIKeys.kData] as? NSDictionary
                self.chatListId = dict?[APIKeys.kChatListId] as? Int ?? 0
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func UserFriendList(userId:Int, response: @escaping responseCallBack) {
        
        APIManager.UserFriendList(userId: userId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseFriendListData(responseDict:responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            } else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
}

