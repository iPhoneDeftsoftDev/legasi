//
//  SearchVM.swift
//  Legasi
//
//  Created by Gaurav on 23/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
public class SearchVM {
    
    public static let sharedInstance = SearchVM()
    private init() {}
    
    var locationBaseUserArray = [locationBaseUser]()

    
    func searchByLocation(student: String, graduate: String, lat:Double,  long:Double, response: @escaping responseCallBack) {
        
        APIManager.locationBaseSearch(userId: DataManager.user_id!, student: student, graduate: graduate, lat: lat, long: long,successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseSearchLocationData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
    func checkIn(lat:Double,  long:Double,location: String, response: @escaping responseCallBack) {
        APIManager.checkIN(userId: DataManager.user_id!, lat: lat, long: long, location: location, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
}
