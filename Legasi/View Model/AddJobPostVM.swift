//
//  AddJobPostVM.swift
//  Legasi
//
//  Created by 123 on 22/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
public class AddJobPostVM {
    
    var jobListArray = [JobsListData]()
    var infoDict: NSDictionary!
    var jobDetail = JobsListData()
    
    public static let sharedInstance = AddJobPostVM()
    private init() {}
    
    func addNewJobPost(userId: Int, newsFeedType: Int, jobName: String, companyName:String, location: String, description: String, skills: String, jobType: Int, lat:Double,  long:Double, title: String, body:String, privacy: String, allowComment: String, shareLocation: String, tags: String,  mediaType: String, pollOption: String,shareLink:String,mediaName: String?,vedioArray:[Data]?, thumbNailName: String?, mediaArray: [Data]?, mimeType: MimeType,groupId:Int, response: @escaping responseCallBack) {
        
        APIManager.addJobPost(userId: userId, newsFeedType: newsFeedType, jobName: jobName, companyName:companyName, location: location, description: description, skills: skills, jobType: jobType, lat:lat,  long:long, title: title, body:body, privacy: privacy, allowComment: allowComment, shareLocation: shareLocation, tags: tags,  vedioArray:vedioArray,thumbNailName: thumbNailName, mediaType: mediaType, mediaArray: mediaArray, mediaName: mediaName, mimeType: mimeType, pollOption: pollOption,shareLink:shareLink,groupId:groupId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
     func AddNewTag(userId: Int, tagTitle:String, response: @escaping responseCallBack) {
        
        APIManager.AddNewTag(userId: userId, tagTitle: tagTitle, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
               self.infoDict = responseDict![APIKeys.kData]as?NSDictionary
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func GetMyJobPost(userId: Int,page:Int,  response: @escaping responseCallBack) {
        
        APIManager.GetMyJobPost(userId: userId,page:page,  successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.jobListArray = [JobsListData]()
                }
                self.parseJobListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    func GetAllJobPost(userId: Int, searchType: Int,page:Int,  response: @escaping responseCallBack) {
        
        APIManager.GetAllJobPost(userId: userId,searchType:searchType, page: page,  successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                if page == 1 {
                    self.jobListArray = [JobsListData]()
                }
                self.parseJobListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
    func EditOwnJob(userId:Int , eventId:Int, jobName:String, companyName:String, location:String, description:String, skillsNeeded:String, lat:Double, long:Double, jobType:Int, jobStatus:Int, response: @escaping responseCallBack) {
        
        APIManager.EditOwnJob(userId:userId , eventId:eventId, jobName:jobName, companyName:companyName, location:location, description:description, skillsNeeded:skillsNeeded, lat:lat, long:long, jobType:jobType, jobStatus:jobStatus, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }

    func DeleteNewfeed(userId:Int, newfeedId:Int, response: @escaping responseCallBack) {
        APIManager.DeleteNewfeed(userId:userId , newfeedId:newfeedId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    func GetEachJobDetail(userId:Int, eventId:Int, response: @escaping responseCallBack) {
        APIManager.GetEachJobDetail(userId:userId, eventId:eventId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseEachJobData(responseDict:responseDict!)

                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }

    
}
