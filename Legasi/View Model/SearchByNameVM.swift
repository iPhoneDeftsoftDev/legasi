//
//  SearchByNameVM.swift
//  Legasi
//
//  Created by MiniMAC22 on 30/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
public class SearchByNameVM {
    var listArray = NSArray()
    var infoDict: NSDictionary!
    var searchUserArray = [SearchUserData]()
    public static let sharedInstance = SearchByNameVM()
    private init() {}
    
    func searchByName(userId: Int, searchText: String, requestDict:JSONDictionary, response: @escaping responseCallBack) {
        
        APIManager.searchByName(userId: userId, searchText: searchText, requestDict:requestDict, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.parseSearchUserListData(responseDict: responseDict!)
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
    func groupListMethod(userId: Int, schoolId: Int, response: @escaping responseCallBack) {
        
        APIManager.groupListMethod(userId: userId,schoolId: schoolId, successCallback: { (responseDict) in
            if  responseDict?[APIKeys.kResult]as?String == "Success"{
                self.listArray = (responseDict![APIKeys.kData]as?NSArray)!
                response(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                response(0, responseDict![APIKeys.kMessage] as? String, nil)
            }
        }) { (errorReason, error) in
            response(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
            debugPrint(errorReason ?? "")
        }
    }
    
    
}
