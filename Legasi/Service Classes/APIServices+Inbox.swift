//
//  APIServices+Inbox.swift
//  Legasi
//
//  Created by MiniMAC22 on 20/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum InboxAPIServices: APIService {
    
    //SearchByNameAPI
    case ChatInboxList(userId: Int)
    case EachChatDetail(userId:Int, chatListId:Int)
    case GetBlockList(userId:Int)
    case BlockUnblockUser(userId:Int, otherUserId:Int, value:Int)
    case SendMsgMethod(userId:Int, chatListId:Int, text:String,friendId:Int,msgType:String)
    case UserFriendList(userId:Int)
    
    var path: String {
        var path = ""
        switch self {
        case .ChatInboxList:
            path = BASE_API_URL.appending("chatInbox")
        case .EachChatDetail:
            path = BASE_API_URL.appending("eachChatDetail")
        case .GetBlockList:
            path = BASE_API_URL.appending("getBlockList")
        case .BlockUnblockUser:
            path = BASE_API_URL.appending("blockUnblockUser")
        case .SendMsgMethod:
            path = BASE_API_URL.appending("sendMsg")
        case .UserFriendList:
            path = BASE_API_URL.appending("userFriendList")
        
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .ChatInboxList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        case let .EachChatDetail(userId,chatListId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kChatListId] = chatListId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
    
        case let .GetBlockList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .BlockUnblockUser(userId, otherUserId, value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kOtherUserId] = otherUserId
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
      
        case let .SendMsgMethod(userId, chatListId, text,friendId,msgType):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kChatListId] = chatListId
            parametersDict[APIKeys.kText] = text
            parametersDict[APIKeys.kFriendId] = friendId
            parametersDict[APIKeys.kMsgType] = msgType
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)

        case let .UserFriendList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        }
        return resource
    }
}
extension APIManager {
    
    //SearchByNameAPI
    class func ChatInboxList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.ChatInboxList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func EachChatDetail(userId: Int,chatListId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.EachChatDetail(userId: userId,chatListId:chatListId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func GetBlockList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.GetBlockList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func BlockUnblockUser(userId:Int, otherUserId:Int, value:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.BlockUnblockUser(userId:userId, otherUserId:otherUserId, value:value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func SendMsgMethod(userId:Int, chatListId:Int, text: String,friendId:Int,vedioArray:[Data]?, thumbNailName: String?,  msgType: String, mediaArray: [Data]?, mediaName: String?, mimeType: MimeType = .image,  successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.SendMsgMethod(userId:userId, chatListId:chatListId, text:text,friendId:friendId,msgType:msgType).uploadMultipleArray(imageArray: mediaArray, imageName: mediaName, thumbNailArray:vedioArray, thumbNailName: thumbNailName,mediaType: mimeType, success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
        
    }
    
    class func UserFriendList(userId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        InboxAPIServices.UserFriendList(userId:userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
   
    
}



