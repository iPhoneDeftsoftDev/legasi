//
//  APIServices+AddJobPost.swift
//  Legasi
//
//  Created by 123 on 22/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
enum AddJobPostAPIServices: APIService {
  
    //ADD JOB POST
    case AddJobPost(userId: Int, newsFeedType: Int, jobName: String, companyName:String, location: String, description: String, skills: String, jobType: Int, lat:Double,  long:Double, title: String, body:String, privacy: String, allowComment: String, shareLocation: String, tags: String,  mediaType: String, pollOption: String, shareLink:String,groupId:Int)
    case AddNewTag(userId:Int, tagTitle:String)
    
    case GetMyJobPost(userId:Int,page:Int)
    case GetAllJobPost(userId:Int,searchType:Int,page:Int)
    case EditOwnJob(userId:Int , eventId:Int, jobName:String, companyName:String, location:String, description:String, skillsNeeded:String, lat:Double, long:Double, jobType:Int,jobStatus: Int)
    case DeleteNewfeed(userId:Int , newfeedId:Int)
    case GetEachJobDetail(userId:Int , eventId:Int)
    
    var path: String {
        var path = ""
        switch self {
        case .AddJobPost:
            path = BASE_API_URL.appending("addNewsfeed")
        
        case .AddNewTag:
            path = BASE_API_URL.appending("addNewTag")
            
        case .GetMyJobPost:
            path = BASE_API_URL.appending("getMyJobPost")
       
        case .GetAllJobPost:
            path = BASE_API_URL.appending("getAllJobPost")
        
        case .EditOwnJob:
            path = BASE_API_URL.appending("editOwnjob")
            
        case .DeleteNewfeed:
            path = BASE_API_URL.appending("deleteNewfeed")
        
        case .GetEachJobDetail:
            path = BASE_API_URL.appending("getEachJobDetail")
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {

        case let .AddJobPost(userId, newsFeedType, jobName, companyName, location, description, skills, jobType, lat,  long, title, body, privacy, allowComment, shareLocation, tags,  mediaType, pollOption,shareLink,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kNewsFeedType] = newsFeedType
            parametersDict[APIKeys.kJobName] = jobName
            parametersDict[APIKeys.kCompanyName] = companyName
            parametersDict[APIKeys.kLocation] = location
            parametersDict[APIKeys.kDescription] = description
            parametersDict[APIKeys.kSkill] = skills
            parametersDict[APIKeys.kJobType] = jobType
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            parametersDict[APIKeys.kTitle] = title
            parametersDict[APIKeys.kBody] = body
            parametersDict[APIKeys.kPrivacy] = privacy
            parametersDict[APIKeys.kAllowComment] = allowComment
            parametersDict[APIKeys.kShareLocation] = shareLocation
            parametersDict[APIKeys.KTags] = tags
            parametersDict[APIKeys.kMediaType] = mediaType
            parametersDict[APIKeys.kShareLink] = shareLink
            parametersDict[APIKeys.kPollOption] = pollOption
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
      
         case let .AddNewTag(userId, tagTitle):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kTagTitle] = tagTitle
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .GetMyJobPost(userId,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .GetAllJobPost(userId,searchType,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSearchType] = searchType
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .EditOwnJob(userId , eventId, jobName, companyName, location, description, skillsNeeded, lat, long, jobType, jobStatus):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.kJobName] = jobName
            parametersDict[APIKeys.kCompanyName] = companyName
            parametersDict[APIKeys.kLocation] = location
            parametersDict[APIKeys.kDescription] = description
            parametersDict[APIKeys.kSkillsNeeded] = skillsNeeded
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            parametersDict[APIKeys.kJobType] = jobType
            parametersDict[APIKeys.kJobStatus] = jobStatus
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
         
        case let .DeleteNewfeed(userId,newfeedId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kNewfeedId] = newfeedId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .GetEachJobDetail(userId,eventId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEventId] = eventId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        
        }
        return resource
    }
}
extension APIManager {
    
    //ADD JOB POST
    class func addJobPost(userId: Int, newsFeedType: Int, jobName: String, companyName:String, location: String, description: String, skills: String, jobType: Int, lat:Double,  long:Double, title: String, body:String, privacy: String, allowComment: String, shareLocation: String, tags: String,vedioArray:[Data]?, thumbNailName: String?,  mediaType: String, mediaArray: [Data]?, mediaName: String?, mimeType: MimeType = .image, pollOption:String,shareLink:String,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        AddJobPostAPIServices.AddJobPost(userId: userId, newsFeedType: newsFeedType, jobName: jobName, companyName:companyName, location: location, description: description, skills: skills, jobType: jobType, lat:lat,  long:long, title: title, body:body, privacy: privacy, allowComment: allowComment, shareLocation: shareLocation, tags: tags,  mediaType: mediaType, pollOption: pollOption, shareLink: shareLink, groupId: groupId).uploadMultipleArray(imageArray: mediaArray, imageName: mediaName, thumbNailArray:vedioArray, thumbNailName: thumbNailName,mediaType: mimeType, success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
        
    }
    class func AddNewTag(userId: Int, tagTitle:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.AddNewTag(userId: userId, tagTitle:tagTitle).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func GetMyJobPost(userId: Int,page:Int,  successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.GetMyJobPost(userId: userId,page:page).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func GetAllJobPost(userId: Int, searchType: Int,page:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.GetAllJobPost(userId: userId, searchType: searchType,page:page).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func EditOwnJob(userId:Int , eventId:Int, jobName:String, companyName:String, location:String, description:String, skillsNeeded:String, lat:Double, long:Double, jobType:Int, jobStatus:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.EditOwnJob(userId:userId , eventId:eventId, jobName:jobName, companyName:companyName, location:location, description:description, skillsNeeded:skillsNeeded, lat:lat, long:long, jobType:jobType, jobStatus:jobStatus).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func DeleteNewfeed(userId:Int, newfeedId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.DeleteNewfeed(userId:userId , newfeedId:newfeedId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func GetEachJobDetail(userId:Int, eventId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        AddJobPostAPIServices.GetEachJobDetail(userId:userId , eventId:eventId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

   
}
