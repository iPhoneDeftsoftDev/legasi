//
//  APIServices+Search.swift
//  Legasi
//
//  Created by Gaurav on 23/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
enum SearchAPIServices: APIService {
    
    //ADD JOB POST
    case locationBaseSearch(userId: Int, student: String, graduate: String, lat:Double,  long:Double)
    case checkIn(userId: Int,lat: Double, long : Double,location : String)

    
    
    
    var path: String {
        var path = ""
        switch self {
        case .locationBaseSearch:
            path = BASE_API_URL.appending("locationBasedSearch")
            
        case .checkIn:
            path = BASE_API_URL.appending("userCheckIn")
            
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .locationBaseSearch(userId,student,graduate,lat,long):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSearchForStudent] = student
            parametersDict[APIKeys.kSearchForGraduate] = graduate
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .checkIn(userId,lat,long,location):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            parametersDict[APIKeys.kLocation] = location
            
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        }
        return resource
    }
}

extension APIManager {
    
    class func locationBaseSearch(userId: Int, student: String, graduate: String, lat:Double,  long:Double, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SearchAPIServices.locationBaseSearch(userId: userId, student: student, graduate: graduate, lat: lat, long: long).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func checkIN(userId: Int, lat:Double,  long:Double,location: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SearchAPIServices.checkIn(userId: userId, lat: lat, long: long,location: location).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
}
