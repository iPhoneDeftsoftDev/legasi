//
//  APIServices+Notifications.swift
//  Legasi
//
//  Created by MiniMAC22 on 13/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum NotificationsAPIServices: APIService {
    
    //SearchByNameAPI
    case NotificationList(userId: Int)
    case AcceptJoinRequest(userId:Int, notificationId:Int, value:Int)
    case NotificationCount(userId: Int)
    
    var path: String {
        var path = ""
        switch self {
        case .NotificationList:
            path = BASE_API_URL.appending("notificationList")
        case .AcceptJoinRequest:
            path = BASE_API_URL.appending("acceptJoinRequest")

        case .NotificationCount:
            path = BASE_API_URL.appending("count_list")
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .NotificationList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .AcceptJoinRequest(userId, notificationId, value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kNotificationId] = notificationId
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .NotificationCount(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        }
        return resource
    }
}
extension APIManager {
    
    //SearchByNameAPI
    class func NotificationList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        NotificationsAPIServices.NotificationList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func NotificationCount(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        NotificationsAPIServices.NotificationCount(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func AcceptJoinRequest(userId:Int, notificationId:Int, value:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        NotificationsAPIServices.AcceptJoinRequest(userId: userId,notificationId:notificationId,value:value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
}



