//
//  APIServices+NewsFeed.swift
//  Legasi
//
//  Created by MiniMAC22 on 17/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

enum NewsFeedAPIServices: APIService {
    //NewsFeed Api
    case NewsFeedDetail(userId: Int, filterApply: Int,typeOfPost:String,miles:String, lat:Double,  long:Double, ids:String, page: Int)
    case GetTagList(userId:Int)
    case GetEachNewfeed(userId:Int, newfeedId:Int)
    case VoteOnPoll(userId:Int, eventId:Int, pollId:Int)
    case CommentNewFeed(userId:Int, newfeedId:Int, comment:String)
    case LikeNewfeed(userId:Int,eventId:Int,newfeedType:Int,value:Int)
    case LikeOnComment(userId:Int, commentId:Int, eventId:Int, value:Int)
    case AddReplyService(userId:Int, commentId:Int, eventId: Int, text:String)
    case SubmitRSVPRequest(userId:Int, eventId:Int, value:Int)
    case GetOwnNewsFeed(userId:Int,page:Int)
    case GeoFancingMethod(userId:Int, lat:Double, long:Double)
    
   
    
    var path: String {
        var path = ""
        switch self {
        //NewsFeed Api
        case .NewsFeedDetail:
            path = BASE_API_URL.appending("getNewfeed")
      
        case .GetTagList:
            path = BASE_API_URL.appending("getTagList")
        
        case .GetEachNewfeed:
            path = BASE_API_URL.appending("geteachNewfeed")
     
        case .VoteOnPoll:
            path = BASE_API_URL.appending("voteOnPoll")
        
        case .CommentNewFeed:
            path = BASE_API_URL.appending("commentNewfeed")
            
        case .LikeNewfeed:
            path = BASE_API_URL.appending("likeNewfeed")
      
        case .LikeOnComment:
            path = BASE_API_URL.appending("addLikeOnComment")
        
        case .AddReplyService:
            path = BASE_API_URL.appending("addReply")
        
        case .SubmitRSVPRequest:
            path = BASE_API_URL.appending("submitRSVPRequest")
        
        case .GetOwnNewsFeed:
            path = BASE_API_URL.appending("getOwnNewfeed")
            
        case .GeoFancingMethod:
            path = BASE_API_URL.appending("geofencing")
        
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
        //NewsFeed Api
        case let .NewsFeedDetail(userId,filterApply,typeOfPost,miles,lat,long,ids,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kFilterApply] = filterApply
            parametersDict[APIKeys.kTypeOfPost] = typeOfPost
            parametersDict[APIKeys.kMiles] = miles
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            parametersDict[APIKeys.kTag] = ids
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .GetTagList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .GetEachNewfeed(userId,newfeedId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kNewfeedId] = newfeedId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
            
        case let .VoteOnPoll(userId,eventId,pollId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.kPollId] = pollId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)

        case let .CommentNewFeed(userId, newfeedId, comment):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kNewfeedId] = newfeedId
            parametersDict[APIKeys.kComment] = comment
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .LikeNewfeed(userId,eventId,newfeedType,value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.kNewsFeedType] = newfeedType
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)

        case let .LikeOnComment(userId,commentId, eventId,value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kCommentId] = commentId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .AddReplyService(userId, commentId, eventId, text):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kCommentId] = commentId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.KText] = text
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
         
        case let .SubmitRSVPRequest(userId,eventId, value):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEventId] = eventId
            parametersDict[APIKeys.kValue] = value
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
     
        case let .GetOwnNewsFeed(userId,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
      
        case let .GeoFancingMethod(userId, lat, long):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kLat] = "\(lat)"
            parametersDict[APIKeys.kLong] = "\(long)"
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        }
        return resource
  }
}
extension APIManager {
        //NewsFeed Api
        
    class func newsFeedDetail(userId: Int, filterApply: Int,typeOfPost:String,miles:String, lat:Double,  long:Double, ids:String,page: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
            NewsFeedAPIServices.NewsFeedDetail(userId: userId, filterApply: filterApply, typeOfPost: typeOfPost, miles: miles, lat: lat,long: long, ids: ids, page: page).requestNew(success: { (response) in
                if let responseDict = response as? JSONDictionary
                {
                    successCallback(responseDict)
                }
                else
                {
                    successCallback([:])
                }
            }, failure: failureCallback)
        }
    class func getTagList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.GetTagList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
   
    class func getEachNewfeed(userId: Int, newfeedId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.GetEachNewfeed(userId: userId,newfeedId:newfeedId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
 
    
    
    class func voteOnPoll(userId: Int, eventId:Int,pollId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.VoteOnPoll(userId: userId,eventId:eventId,pollId:pollId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func CommentNewFeed(userId: Int, newfeedId:Int,comment:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.CommentNewFeed(userId: userId,newfeedId:newfeedId,comment:comment).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func LikeNewfeed(userId:Int, eventId:Int, newfeedType:Int, value:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.LikeNewfeed(userId: userId, eventId: eventId, newfeedType: newfeedType,value: value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func LikeOnComment(userId:Int,commentId:Int, eventId:Int, value:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.LikeOnComment(userId: userId,commentId:commentId, eventId: eventId,value: value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func AddReplyService(userId: Int, commentId: Int, eventId: Int, text: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.AddReplyService(userId: userId,commentId:commentId, eventId: eventId,text: text).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func SubmitRSVPRequest(userId: Int, eventId: Int, value: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.SubmitRSVPRequest(userId: userId, eventId: eventId,value: value).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func GetOwnNewsFeed(userId: Int,page:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.GetOwnNewsFeed(userId: userId,page:page).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func GeoFancingMethod(userId: Int,lat:Double, long:Double, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        NewsFeedAPIServices.GeoFancingMethod(userId: userId, lat: lat, long: long).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

   
}

