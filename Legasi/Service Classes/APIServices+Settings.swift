//
//  APIServices+Settings.swift
//  Legasi
//
//  Created by MiniMAC22 on 21/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum SettingsAPIServices: APIService {
    
    //SearchByNameAPI
    case NotificationSettings(userId: Int,value:Int,notificationType:Int)
    case PrivacySetting(userId:Int, shareMylocation:Int, student:Int,graduates:Int,school:Int, connectedUsers: Int, postSeen:Int)
    case HelpQueryMethod(userId:Int , typeOfProblem:String, subject:String, description:String)
    case GetOwnProfile(userId:Int)
    case UpdateAddressDetail(userId:Int, addressLine1:String, latitude:Double, longitude:Double)
    case UpdateSchoolDetail(userId:Int ,schoolId:Int, userType:String ,dob:String,graduationYear:String)
    case UpdateEducationDetail(userId:Int, educationDetail:String)
    case UpdateWorkDetail(userId:Int, workDetail:String)
    case LinkAccount(userId:Int, accountTypeFB:String,accountTypeLink:String)
    case UpdateProfileMethod(userId:Int, fullName:String, userName:String, countryCode:String, phoneNumber:String, email:String)
    case SkillsUpdate(userId:Int, skills:String)
    case ChangePassword(userId: Int, oldPassword: String, newPassword: String)
    
    
    var path: String {
        var path = ""
        switch self {
        case .NotificationSettings:
            path = BASE_API_URL.appending("notificationSettings")
        case .PrivacySetting:
            path = BASE_API_URL.appending("privacySetting")
        case .HelpQueryMethod:
            path = BASE_API_URL.appending("helpQuery")
        case .GetOwnProfile:
            path = BASE_API_URL.appending("getOwnProfile")
        case .UpdateAddressDetail:
            path = BASE_API_URL.appending("updateAddressDetail")
        case .UpdateSchoolDetail:
            path = BASE_API_URL.appending("updateSchoolDetail")
        case .UpdateEducationDetail:
            path = BASE_API_URL.appending("updateEducationDetail")
        case .UpdateWorkDetail:
            path = BASE_API_URL.appending("updateWorkDetail")
        case .LinkAccount:
            path = BASE_API_URL.appending("linkAcount")
        case .UpdateProfileMethod:
            path = BASE_API_URL.appending("updateProfile")
        case .SkillsUpdate:
            path = BASE_API_URL.appending("updateSkillsDetail")
        case .ChangePassword:
            path = BASE_API_URL.appending("changePassword")
        
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .NotificationSettings(userId,value,notificationType):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kValue] = value
            parametersDict[APIKeys.kNotificationType] = notificationType
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .PrivacySetting(userId, shareMylocation,student,graduates,school,connectedUsers, postSeen):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kShareMyLocation] = shareMylocation
            parametersDict[APIKeys.kStudent] = student
            parametersDict[APIKeys.kGraduates] = graduates
            parametersDict[APIKeys.kSchool] = school
            parametersDict[APIKeys.kConnectedUsers] = connectedUsers
            parametersDict[APIKeys.kPostSeen] = postSeen
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .HelpQueryMethod(userId , typeOfProblem, subject, description):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kTypeOfProblem] = typeOfProblem
            parametersDict[APIKeys.kSubject] = subject
            parametersDict[APIKeys.kDescription] = description
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)

        case let .GetOwnProfile(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .UpdateAddressDetail(userId, addressLine1, latitude, longitude):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kAddressLine1] = addressLine1
            parametersDict[APIKeys.KLatitude] = "\(latitude)"
            parametersDict[APIKeys.KLongitude] = "\(longitude)"
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .UpdateSchoolDetail(userId ,schoolId, userType, dob,graduationYear):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSchoolId] = schoolId
            parametersDict[APIKeys.kUserType] = userType
            parametersDict[APIKeys.kDob] = dob
            parametersDict[APIKeys.kGraduationYear] = graduationYear
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .UpdateEducationDetail(userId ,educationDetail):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kEducationDetail] = educationDetail
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
     
        case let .UpdateWorkDetail(userId ,workDetail):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kWorkDetail] = workDetail
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .LinkAccount(userId ,accountTypeFB, accountTypeLink):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kAccountTypeFB] = accountTypeFB
            parametersDict[APIKeys.kAccountTypeLink] = accountTypeLink
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .UpdateProfileMethod(userId, fullName, userName, countryCode, phoneNumber, email):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kFullname] = fullName
            parametersDict[APIKeys.kUsername] = userName
            parametersDict[APIKeys.kCountryCode] = countryCode
            parametersDict[APIKeys.kPhoneNumber] = phoneNumber
            parametersDict[APIKeys.kEmail] = email
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .SkillsUpdate(userId, skills):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSkill] = skills
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .ChangePassword(userId, oldPassword, newPassword):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kOldPassword] = oldPassword
            parametersDict[APIKeys.kNewPassword] = newPassword
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        }
        
        return resource
    }
}
extension APIManager {
    
    //SettingsAPI
    class func NotificationSettings(userId: Int,value:Int, notificationType:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.NotificationSettings(userId: userId, value:value, notificationType: notificationType).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
     class func PrivacySetting(userId: Int, shareMylocation: Int, student: Int, graduates:Int, school: Int, connectedUsers: Int, postSeen: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.PrivacySetting(userId:userId, shareMylocation:shareMylocation,student:student,graduates:graduates,school:school,connectedUsers:connectedUsers,postSeen:postSeen).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func HelpQueryMethod(userId :Int, typeOfProblem:String, subject:String, description:String,media:[String:Data], successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback){
        SettingsAPIServices.HelpQueryMethod(userId: userId, typeOfProblem: typeOfProblem, subject: subject, description: description).uploadMultiple(imageDict: media, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func GetOwnProfile(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.GetOwnProfile(userId:userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func UpdateAddressDetail(userId: Int,addressLine1:String,latitude:Double,longitude:Double, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.UpdateAddressDetail(userId:userId,addressLine1:addressLine1,latitude:latitude,longitude:longitude).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func UpdateSchoolDetail(userId:Int ,schoolId:Int, userType:String ,dob:String,graduationYear:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.UpdateSchoolDetail(userId:userId,schoolId:schoolId,userType:userType,dob:dob,graduationYear:graduationYear).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func UpdateEducationDetail(userId:Int ,educationDetail:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.UpdateEducationDetail(userId:userId,educationDetail:educationDetail).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func UpdateWorkDetail(userId:Int ,workDetail:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        SettingsAPIServices.UpdateWorkDetail(userId:userId,workDetail:workDetail).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func LinkAccount(userId:Int ,accountTypeFB:String,accountTypeLink:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        SettingsAPIServices.LinkAccount(userId:userId,accountTypeFB:accountTypeFB,accountTypeLink:accountTypeLink).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func UpdateProfileMethod(userId:Int, fullName:String, userName:String, countryCode:String, phoneNumber:String, email:String,image:[String:Data], successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.UpdateProfileMethod(userId:userId,fullName:fullName,userName:userName,countryCode:countryCode,phoneNumber:phoneNumber,email:email).uploadMultiple(imageDict: image, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func SkillsUpdate(userId:Int ,skills:String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.SkillsUpdate(userId:userId,skills:skills).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func ChangePassword(userId:Int ,oldPassword: String, newPassword: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SettingsAPIServices.ChangePassword(userId: userId, oldPassword: oldPassword, newPassword: newPassword).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
}



