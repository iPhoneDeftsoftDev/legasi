

//
//  APIServices+SearchByName.swift
//  Legasi
//
//  Created by MiniMAC22 on 30/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum SearchByNameAPIServices: APIService {
    
    //SearchByNameAPI
    case searchByName(userId: Int, searchText: String, requestDict:JSONDictionary)
    
    case groupListMethod(userId: Int,schoolId:Int)
    
    var path: String {
        var path = ""
        switch self {
        case .searchByName:
            path = BASE_API_URL.appending("searchByName")
            
        case .groupListMethod:
            path = BASE_API_URL.appending("getGroupListForSearch")
            
            
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .searchByName(userId, searchText,requestDict):
            var parametersDict = requestDict
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSearchText] = searchText
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
       
        case let .groupListMethod(userId,schoolId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSchoolId] = schoolId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        }
        return resource
    }
}
extension APIManager {
    
    //SearchByNameAPI
    class func searchByName(userId: Int, searchText: String,requestDict:JSONDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SearchByNameAPIServices.searchByName(userId: userId, searchText: searchText,requestDict:requestDict).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //SearchByNameAPI
    class func groupListMethod(userId: Int,schoolId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        SearchByNameAPIServices.groupListMethod(userId: userId,schoolId:schoolId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
}
