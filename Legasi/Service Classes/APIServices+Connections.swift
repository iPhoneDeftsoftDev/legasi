//
//  APIServices+Connections.swift
//  Legasi
//
//  Created by MiniMAC22 on 08/12/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

enum ConnectionsAPIServices: APIService {
    
    //SearchByNameAPI
    case UserFollowersList(userId: Int)
    case UserFollowingList(userId: Int)
    case UserGroupList(userId: Int)
    case EachGroupNewfeedList(userId: Int, groupId: Int,page:Int)
    case AddEditGroupMethod(userId: Int, actionType: Int, schoolId: Int, groupName: String,groupDescription: String, visibleType: String, location: Int, groupLat: Double, groupLong:Double, groupId: Int)
    case AddMemberInGroup(userId:Int, memberId:String,groupId:Int)
    case MemberList(userId:Int, schoolId:Int)
    case DeleteGroup(userId:Int, groupId:Int)
    case JoinUserGroup(userId:Int, groupId:Int)
    case LeaveGroupMethod(userId:Int, groupId:Int)
    case GetEachGroupDetail(userId:Int, groupId:Int)
    case MakeGroupAdmin(userId:Int, groupId:Int,otherUserId:Int)
    case RemoveGroupUser(userId:Int, groupId:Int, otherUserId:Int)
    
    
    var path: String {
        var path = ""
        switch self {
        case .UserFollowersList:
            path = BASE_API_URL.appending("userFollowersList")
        case .UserFollowingList:
            path = BASE_API_URL.appending("userFollowingList")
        case .UserGroupList:
            path = BASE_API_URL.appending("userGroupList")
        case .EachGroupNewfeedList:
            path = BASE_API_URL.appending("eachGroupNewfeedList")
        case .AddEditGroupMethod:
            path = BASE_API_URL.appending("addEditGroup")
        case .AddMemberInGroup:
            path = BASE_API_URL.appending("addMemberInGroup")
        case .MemberList:
            path = BASE_API_URL.appending("memberList")
        case .DeleteGroup:
            path = BASE_API_URL.appending("deleteGroup")
        case .JoinUserGroup:
            path = BASE_API_URL.appending("joinUserGroup")
        case .LeaveGroupMethod:
            path = BASE_API_URL.appending("leaveGroup")
        case .GetEachGroupDetail:
            path = BASE_API_URL.appending("getEachGroupDetail")
        case .MakeGroupAdmin:
            path = BASE_API_URL.appending("createGroupAdmin")
        case .RemoveGroupUser:
            path = BASE_API_URL.appending("removeGroupUser")
                
        }
        return path
    }
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .UserFollowersList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .UserFollowingList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
      
        case let .UserGroupList(userId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .EachGroupNewfeedList(userId, groupId,page):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            parametersDict[APIKeys.kPage] = page
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .AddEditGroupMethod(userId,actionType,schoolId,groupName,groupDescription,visibleType,location,groupLat, groupLong,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kActionType] = actionType
            parametersDict[APIKeys.kSchoolId] = schoolId
            parametersDict[APIKeys.kGroupName] = groupName
            parametersDict[APIKeys.kGroupDescription] = groupDescription
            parametersDict[APIKeys.kVisibleType] = visibleType
            parametersDict[APIKeys.kLocation] = location
            parametersDict[APIKeys.kGroupLat] = "\(groupLat)"
            parametersDict[APIKeys.kGroupLong] = "\(groupLong)"
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
      
        case let .AddMemberInGroup(userId, memberId,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kMemberId] = memberId
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .MemberList(userId,schoolId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kSchoolId] = schoolId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .DeleteGroup(userId,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
            
        case let .JoinUserGroup(userId,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .LeaveGroupMethod(userId,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        case let .GetEachGroupDetail(userId,groupId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .MakeGroupAdmin(userId,groupId,otherUserId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            parametersDict[APIKeys.kOtherUserId] = otherUserId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
        
        case let .RemoveGroupUser(userId,groupId,otherUserId):
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId] = userId
            parametersDict[APIKeys.kGroupId] = groupId
            parametersDict[APIKeys.kOtherUserId] = otherUserId
            resource = Resource(method: .post, parameters: parametersDict, headers: nil)
       
        }
        return resource
    }
}
extension APIManager {
    
    //SearchByNameAPI
    class func userFollowersList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        ConnectionsAPIServices.UserFollowersList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func UserFollowingList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        ConnectionsAPIServices.UserFollowingList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func UserGroupList(userId: Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        ConnectionsAPIServices.UserGroupList(userId: userId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func EachGroupNewfeedList(userId: Int,groupId:Int,page:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        ConnectionsAPIServices.EachGroupNewfeedList(userId: userId,groupId:groupId,page:page).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }




    class func AddEditGroupMethod(userId:Int,actionType:Int,schoolId:Int,groupName:String,groupDescription:String,visibleType:String,location:Int,groupLat:Double, groupLong:Double,groupId:Int,image:[String:Data], successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
    
        ConnectionsAPIServices.AddEditGroupMethod(userId:userId,actionType:actionType,schoolId:schoolId,groupName:groupName,groupDescription:groupDescription,visibleType:visibleType,location:location,groupLat:groupLat, groupLong:groupLong,groupId:groupId).uploadMultiple(imageDict: image, success: { (response) in
                if let responseDict = response as? JSONDictionary {
                    successCallback(responseDict)
                }
                else {
                    successCallback([:])
                }
        }, failure: failureCallback)
    }

    class func AddMemberInGroup(userId:Int,memberId:String,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.AddMemberInGroup(userId:userId,memberId:memberId,groupId:groupId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func MemberList(userId:Int,schoolId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.MemberList(userId:userId,schoolId:schoolId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func DeleteGroup(userId:Int,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.DeleteGroup(userId:userId,groupId:groupId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func JoinUserGroup(userId:Int,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.JoinUserGroup(userId:userId,groupId:groupId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func LeaveGroupMethod(userId:Int,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.LeaveGroupMethod(userId:userId,groupId:groupId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func GetEachGroupDetail(userId:Int,groupId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.GetEachGroupDetail(userId:userId,groupId:groupId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func MakeGroupAdmin(userId:Int,groupId:Int, otherUserId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.MakeGroupAdmin(userId:userId,groupId:groupId,otherUserId:otherUserId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func RemoveGroupUser(userId:Int,groupId:Int, otherUserId:Int, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        ConnectionsAPIServices.RemoveGroupUser(userId:userId,groupId:groupId,otherUserId:otherUserId).requestNew(success: { (response) in
            if let responseDict = response as? JSONDictionary
            {
                successCallback(responseDict)
            }
            else
            {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
  
}



