//
//  DataManager.swift
//  Legasi
//
//  Created by Apple on 07/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class DataManager {
    
    static var isUserLoggedIn:Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: kIsUserLoggedIn)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kIsUserLoggedIn)
        }
    }
    //kLIAccessToken
    static var linkedInAccessToken:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: kLIAccessToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kLIAccessToken)
        }
    }
    static var user_image:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: kUser_image)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUser_image)
        }
    }
    static var user_name:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kUsername)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kUsername)
        }
    }
    static var school_logo:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: kSchoolLogo)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kSchoolLogo)
        }
    }
    
    static var likeIcon_url:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: kLikeIconUrl)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kLikeIconUrl)
        }
    }
    static var unlikeIcon_url:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: kUnlikeIconUrl)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUnlikeIconUrl)
        }
    }
    static var deviceToken:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: APIKeys.kDeviceId)
            UserDefaults.standard.synchronize()
            
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kDeviceId)
        }
    }
    static var user_id:Int?{
        set {
            UserDefaults.standard.set(newValue, forKey: kUser_id)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: kUser_id)
        }
    }
    
    static var role:Int?{
        set {
            UserDefaults.standard.set(newValue, forKey: kRole)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: kRole)
        }
    }
    static var schoolId:Int?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kSchoolId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: APIKeys.kSchoolId)
        }
    }

    static var facebook_id:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kFacebookId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kFacebookId)
        }
    }
    
    static var linkedin_id:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kLinkedInId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kLinkedInId)
        }
    }
    
    static var userType:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kUserType)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kUserType)
        }
    }
    static var graduationYear:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kGraduationYear)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kGraduationYear)
        }
    }
    static var totalLikes:Int?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kTotalLikes)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: APIKeys.kTotalLikes)
        }
    }
    static var nameForGraduate:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kNameForGraduate)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kNameForGraduate)
        }
    }
    
    static var schoolName:String?{
        set {
            UserDefaults.standard.set(newValue, forKey: APIKeys.kSchoolName)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: APIKeys.kSchoolName)
        }
    }

}
